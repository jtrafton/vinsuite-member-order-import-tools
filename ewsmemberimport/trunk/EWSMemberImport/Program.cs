﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using EWSDataTaskSupport;
using EWSDataTaskSupport.EWSData;
using System.Threading;
using MemberValidationRules;

namespace EWSMemberImport
{
  /// <summary>
  /// Holds data about a member import.
  /// </summary>
  internal class MemberImportPackage
  {
    public bool     Debug           { get; set; }
    public DataTask Task            { get; set; }
    public int      MembersPerRound { get; set; }
    public String   AdminWSUsername { get; set; }
    public String   AdminWSPassword { get; set; }

    public MemberImportPackage(DataTask task, String adminWSUsername, String adminWSPassword)
    {
      this.Task            = task;
      this.AdminWSUsername = adminWSUsername;
      this.AdminWSPassword = adminWSPassword;
      this.Debug           = false;
      this.MembersPerRound = 75;
    }
  }


  
  public class Program
  {
    /// <summary>
    /// Starts a member import in a background thread
    /// </summary>
    /// <param name="debug">true performs validation only</param>
    /// <param name="importFile">the file containing member data</param>
    /// <param name="membersPerRound">how many members to import in each iteration</param>
    /// <param name="adminEmail">who to notify when the task is complete</param>
    /// <param name="adminWSUsername">web service credential</param>
    /// <param name="adminWSPassword">web service credential</param>
    /// <param name="wineryID">which winery to import members into</param>
    /// <param name="dataTaskID">optional data task tracking identifier</param>
    public static void RunMemberImportAsync(DataTask task, String adminWSUsername, String adminWSPassword)
    {
      MemberImportPackage pkg = new MemberImportPackage(task, adminWSUsername, adminWSPassword);
      ThreadPool.QueueUserWorkItem(new WaitCallback(Program.RunMemberImportAsync), pkg);
    }

    /// <summary>
    /// Entry point for a background thread
    /// </summary>
    protected static void RunMemberImportAsync(object memberImportPackage)
    {
      MemberImportPackage pkg = memberImportPackage as MemberImportPackage;
      if(pkg == null)
        return;

      RunMemberImport(
        pkg.Debug,
        pkg.Task.File,
        pkg.MembersPerRound,
        pkg.Task.NotifyEmail,
        pkg.AdminWSUsername,
        pkg.AdminWSPassword,
        pkg.Task.WineryID,
        pkg.Task.DataTaskTrackingID);
    }


    /// <summary>
    /// Starts a member import
    /// </summary>
    /// <param name="debug">true performs validation only</param>
    /// <param name="importFile">the file containing member data</param>
    /// <param name="membersPerRound">how many members to import in each iteration</param>
    /// <param name="adminEmail">who to notify when the task is complete</param>
    /// <param name="adminWSUsername">web service credential</param>
    /// <param name="adminWSPassword">web service credential</param>
    /// <param name="wineryID">which winery to import members into</param>
    /// <param name="dataTaskID">optional data task tracking identifier</param>
    public static int RunMemberImport(bool debug, String importFile, int membersPerRound, 
      String adminEmail, String adminWSUsername, String adminWSPassword, Guid wineryID, int? dataTaskID)
    {
      // Allow self-signed certs.
      System.Net.ServicePointManager.ServerCertificateValidationCallback += (se, cert, chain, sslerror) => true;

      GetDataCredentialsResponse dataCredentials = null;

      using (DataServiceHttps ds = new DataServiceHttps())
      {
        ds.Timeout = 30000;

        DataRequest credentialsRequest = new DataRequest()
        {
          Username = adminWSUsername,
          Password = adminWSPassword,
          WineryID = wineryID
        };

        dataCredentials = ds.GetDataCredentials(credentialsRequest);

        if (String.IsNullOrEmpty(dataCredentials.Username) || String.IsNullOrEmpty(dataCredentials.Password))
        {
          //failed to get credentials. abort
        }

        MemberGetter                     mg                     = new MemberGetter(importFile, "Members", dataCredentials.Username, dataCredentials.Password, debug);
        Dictionary<String,Member>        members                = mg.GetMembers();
        List<ErrorReportMember>                memGetterErrors        = new List<ErrorReportMember>();

        #region Run external validation rules
        //ValidationDriver valDriver = new ValidationDriver(members.Values);
        //valDriver.RunValidation();
        #endregion

        #region return Errors
        memGetterErrors.AddRange(mg.GetErrors());
        if (memGetterErrors.Count > 0)
        {
          //return errors
          foreach (ErrorReportMember error in memGetterErrors)
          {
            Console.WriteLine("{0} {1}", error.ErrorMessage, error.Resolution);
          }
        }
        #endregion

        if (debug)
        {
          Console.WriteLine("Not sending because Debug mode is enabled.");
        }

        //import members
        return ImportMembers(debug, importFile, membersPerRound, ds, members, adminEmail, adminWSUsername, adminWSPassword, wineryID, dataTaskID);
      }
    }



    /// <summary>
    /// Import Members into EWS database
    /// </summary>
    /// <param name="debug"></param>
    /// <param name="importFile"></param>
    /// <param name="membersPerRound"></param>
    /// <param name="ds"></param>
    /// <param name="members"></param>
    /// <param name="adminEmail"></param>
    /// <param name="adminWSUsername"></param>
    /// <param name="adminWSPassword"></param>
    /// <param name="wineryID"></param>
    /// <param name="dataTaskID"></param>
    /// <returns></returns>
    private static int ImportMembers(bool debug, String importFile, int membersPerRound, DataServiceHttps ds, Dictionary<String, Member> members,
      String adminEmail, String adminWSUsername, String adminWSPassword, Guid wineryID, int? dataTaskID)
    {
      String importDirectory                           = Path.GetDirectoryName(importFile);
      String importTimestamp                           = DateTime.Now.ToString().Replace(@"/", "").Replace(":", "");
      ImportMemberRequest importMemberRequest          = new ImportMemberRequest();
      List<ErrorReportMember> memGetterErrors                = new List<ErrorReportMember>();
      List<MemberResponseLog> memResponseLogs          = new List<MemberResponseLog>();
      List<CreditCardResponseLog> ccLogs               = new List<CreditCardResponseLog>();
      List<ClubMembershipResponseLog> clubLogs         = new List<ClubMembershipResponseLog>();
      List<ShippingAddressResponseLog> shipAddressLogs = new List<ShippingAddressResponseLog>();
      List<MemberNoteResponseLog> noteLogs             = new List<MemberNoteResponseLog>();
      CSVHelper csv                                    = new EWSDataTaskSupport.CSVHelper();
      int membersToImport                              = 0;
      ImportMemberResponse importMemberResponse        = null;

      #region Setup requests
      importMemberRequest.Username = adminWSUsername;
      importMemberRequest.Password = adminWSPassword;
      importMemberRequest.WineryID = wineryID;
      importMemberRequest.EmailTo  = adminEmail;

      UpdateDataTaskRequest updTaskRequest = new UpdateDataTaskRequest()
      {
        Username   = adminWSUsername,
        Password   = adminWSPassword,
        UpdateType = ProgressUpdateType.INCREMENT_PROGRESS,
        DataTask   = new DataTask_Update()
        {
          DataTaskTrackingID = dataTaskID ?? 0
        }
      };

      GetDataTasksRequest getTaskRequest = new GetDataTasksRequest()
      {
        Username           = adminWSUsername,
        Password           = adminWSPassword,
        DataTaskTrackingID = dataTaskID
      };
      #endregion

      int round = 1;
      bool stopRequested = false;
      importMemberRequest.Members = members.Values.Take(membersPerRound).ToArray();

      //Random r = new Random();

      while (importMemberRequest.Members.Length > 0 && !stopRequested)
      {
        int firstMem = ((round - 1) * membersPerRound) + 1;
        int lastMem = firstMem + membersPerRound - 1;
        Console.WriteLine("Importing members {0} through {1}", firstMem, lastMem);

        #region Import some members
        if (!debug)
          importMemberResponse = DataTaskHelper.TryNTimes(ds.ImportMembers, importMemberRequest, 90, TimeSpan.FromSeconds(10));

        if (importMemberResponse != null)
        {
          if (importMemberResponse.IsSuccessful)
          {
            #region Write responses to log
            for (int i = 0; i < importMemberResponse.Responses.Length; ++i)
            {
              Member currentMember = importMemberRequest.Members[i];
              ImportMemberResponses currentResponse = importMemberResponse.Responses[i];
              AddUpdateMemberResponse currentMemberResponse = currentResponse.memberResponse;

              memResponseLogs.Add(new MemberResponseLog(currentMember, currentMemberResponse));

              foreach (AddUpdateCreditCardResponse ccResponse in currentResponse.ccResponses)
                ccLogs.Add(new CreditCardResponseLog(currentMember, currentMemberResponse, ccResponse));

              foreach (AddUpdateClubMembershipResponse clubResponse in currentResponse.clubResponses)
                clubLogs.Add(new ClubMembershipResponseLog(currentMember, currentMemberResponse, clubResponse));

              foreach (AddUpdateShippingAddressResponse shipResponse in currentResponse.shipResponses)
                shipAddressLogs.Add(new ShippingAddressResponseLog(currentMember, currentMemberResponse, shipResponse));

              foreach (AddMemberNoteResponse noteResponse in currentResponse.notesResponses)
                noteLogs.Add(new MemberNoteResponseLog(currentMember, currentMemberResponse, noteResponse));
            }
            #endregion

            #region Update data task tracking progress
            if (dataTaskID.HasValue)
            {
              updTaskRequest.DataTask.JobProgress = importMemberRequest.Members.Length;
              UpdateDataTaskResponse updTaskResponse = DataTaskHelper.TryNTimes(ds.UpdateDataTask, updTaskRequest, 100, TimeSpan.FromSeconds(10));

              // Store if the task was requested to stop
              stopRequested = updTaskResponse.Task.StopRequested ?? false;
            }
            #endregion
          }
        }
        else
        {
          if (!debug)
          {
            String exStr = "EXCEPTION: Members " + firstMem + " - " + lastMem + ". Web services did not respond";
            Console.WriteLine(exStr);
            using (TextWriter file = new StreamWriter(importDirectory + @"\Exceptions.txt", true))
            {
              file.WriteLine(exStr);
            }
          }
        }
        #endregion

        // Increment the round and get the next set of members to import
        ++round;
        importMemberRequest.Members = members.Values.Skip(round * membersPerRound).Take(membersPerRound).ToArray();
      }

      #region Write logs to disk
      Console.WriteLine("Writing log");
      if (!debug)
      {
        csv.WriteCSVFile(importDirectory + @"\memResponse " + importTimestamp + ".csv", memResponseLogs);
        csv.WriteCSVFile(importDirectory + @"\shipResponse " + importTimestamp + ".csv", shipAddressLogs);
        csv.WriteCSVFile(importDirectory + @"\clubResponse " + importTimestamp + ".csv", clubLogs);
        csv.WriteCSVFile(importDirectory + @"\ccResponse " + importTimestamp + ".csv", ccLogs);
        csv.WriteCSVFile(importDirectory + @"\noteResponse " + importTimestamp + ".csv", noteLogs);
      }
      #endregion

      return membersToImport;
    }
  }
}
﻿using System;
using EWSDataTaskSupport.EWSData;

namespace EWSMemberImport
{
  abstract class ImportResponseLog
  {
    public    String   Key       { get; set; }
    public    Guid?    MemberID  { get; set; }
    public    String   Error     { get; set; }
    public    String   FirstName { get; set; }
    public    String   LastName  { get; set; }
    public    String   Email     { get; set; }
    protected Guid?    objectID  { get; set; }

    protected ImportResponseLog(Member mem, AddUpdateMemberResponse memResponse, Guid? objectID)
    {
      this.Key       = mem.AltMemberID;
      this.MemberID  = memResponse.MemberID;
      this.FirstName = mem.FirstName;
      this.LastName  = mem.LastName;
      this.Email     = mem.Email;
      this.objectID  = objectID;
    }
  }


  class MemberResponseLog : ImportResponseLog
  {
    public MemberResponseLog(Member mem, AddUpdateMemberResponse memResponse)
      : base(mem, memResponse, null)
    {
      Error = memResponse.ErrorMessage;
    }
  }


  class CreditCardResponseLog : ImportResponseLog
  {
    public CreditCardResponseLog(Member mem, AddUpdateMemberResponse memResponse, AddUpdateCreditCardResponse ccResponse)
      : base(mem, memResponse, ccResponse.CreditCardID)
    {
      Error = ccResponse.ErrorMessage;
    }

    public Guid? CreditCardID
    {
      get { return this.objectID; }
      set { this.objectID = value; }
    }
  }


  class ClubMembershipResponseLog : ImportResponseLog
  {
    public ClubMembershipResponseLog(Member mem, AddUpdateMemberResponse memResponse, AddUpdateClubMembershipResponse clubResponse)
      : base(mem, memResponse, clubResponse.ClubMemberID)
    {
      Error = clubResponse.ErrorMessage;
    }

    public Guid? ClubmemberID
    {
      get { return this.objectID; }
      set { this.objectID = value; }
    }
  }


  class ShippingAddressResponseLog : ImportResponseLog
  {
    public ShippingAddressResponseLog(Member mem, AddUpdateMemberResponse memResponse, AddUpdateShippingAddressResponse shipResponse)
      : base(mem, memResponse, shipResponse.ShippingAddressID)
    {
      Error = shipResponse.ErrorMessage;
    }

    public Guid? ShipmemberID
    {
      get { return this.objectID; }
      set { this.objectID = value; }
    }
  }


  class MemberNoteResponseLog : ImportResponseLog
  {
    public MemberNoteResponseLog(Member mem, AddUpdateMemberResponse memResponse, AddMemberNoteResponse noteResponse)
      : base(mem, memResponse, noteResponse.MemberNoteID)
    {
      Error = noteResponse.ErrorMessage;
    }

    public Guid? MemberNoteID
    {
      get { return this.objectID; }
      set { this.objectID = value; }
    }
  }
}
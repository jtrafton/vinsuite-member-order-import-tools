﻿using System;
using System.Collections.Generic;
using System.Linq;
using EWSDataTaskSupport;
using EWSDataTaskSupport.EWSData;
using LinqToExcel;
using System.Reflection;

namespace EWSMemberImport
{
  class MemberGetter : IErrorReporter
  {
    private String importFile;
    private Dictionary<String, Member> members;
    private String wsUsername;
    private String wsPassword;
    private bool debug;
    private List<ErrorReportMember> errors;
    private String worksheetName;
    private int maxNoteLength;
    private IEnumerable<ImportMember> csvMembers;

    /// <summary>
    /// Init.
    /// </summary>
    /// <param name="sheetName"></param>
    /// <param name="strictMapping"></param>
    /// <param name="username"></param>
    /// <param name="password"></param>
    /// <param name="debug"></param>
    public MemberGetter(String importFile, String worksheetName, String username, String password, bool debug)
    {
      this.importFile    = importFile;
      this.worksheetName = worksheetName;
      this.wsUsername    = username;
      this.wsPassword    = password;
      this.debug         = debug;
      this.errors        = new List<ErrorReportMember>();
      this.maxNoteLength = 4000;
    }


    /// <summary>
    /// Returns any errors during member deserialization
    /// </summary>
    /// <returns></returns>
    public List<ErrorReportMember> GetErrors()
    {
      return this.errors;
    }

    /// <summary>
    /// Get a collection of members.
    /// </summary>
    /// <returns></returns>
    public Dictionary<String, Member> GetMembers()
    {
      var spreadSheet = new ExcelQueryFactory(this.importFile);
      
      #region Spreadsheet format validation
      if (!spreadSheet.GetWorksheetNames().Contains(worksheetName))
      {
        errors.Add(new ErrorReportMember("The spreadsheet must contain a sheet called \"" + worksheetName + "\" which contains the data you wish to import.", 
                   ErrorReportMember.FixTheFile,
                   "Member Getter",
                   ErrorReportMember.ERRORTYPE.CRITICAL));
        return null;
      }
      
      IEnumerable<String> props = typeof(ImportMember).GetProperties().Select(prop => prop.Name);
      IEnumerable<String> columns = spreadSheet.GetColumnNames(worksheetName);

      List<String> unMapped =
        (from col in columns
         where !props.Contains(col)
         select col).ToList();

      if (unMapped.Count > 0)
        errors.Add(new ErrorReportMember("The following columns in the spreadsheet did not match the expected properties: " + String.Join(", ", unMapped),
                   ErrorReportMember.FixTheFile,
                   "Member Getter",
                   ErrorReportMember.ERRORTYPE.CRITICAL));

      List<String> notInSheet =
        (from prop in props
         where !columns.Contains(prop)
         select prop).ToList();

      if (notInSheet.Count > 0)
        errors.Add(new ErrorReportMember("The following properties did not have matching columns in the spreadsheet: " + String.Join(", ", notInSheet),
                   ErrorReportMember.FixTheFile,
                   "Member Getter",
                   ErrorReportMember.ERRORTYPE.CRITICAL));

      if (errors.Count > 0)
        return null;

      #endregion

      MemberTypeGetter memberTypeGetter;

      #region Init
      using (MemberServiceHttps memberService = new MemberServiceHttps())
          memberTypeGetter = new MemberTypeGetter(memberService, wsUsername, wsPassword, debug);
          
      #region Check getters for errors
      errors.AddRange(memberTypeGetter.GetErrors());
      if (errors.Count > 0)
        return null;
      #endregion

      #region Dictionaries to hold member X other data mappings
      //MembersXMemberTypes
      Dictionary<String, Dictionary<Guid, MemberType>> memTypes =
        new Dictionary<String, Dictionary<Guid, MemberType>>();
      //ShipMembers
      Dictionary<String, SortedSet<ShippingAddress>> shippingAddresses =
        new Dictionary<String, SortedSet<ShippingAddress>>();
      //ClubMembers
      Dictionary<String, SortedSet<ClubMembership>> clubMemberships =
        new Dictionary<String, SortedSet<ClubMembership>>();
      //MemberNotes
      Dictionary<String, Stack<MemberNote>> memberNotes =
        new Dictionary<String, Stack<MemberNote>>();
      ////ClubMemberNotes
      //  Dictionary<String, Stack<ClubMemberNote>> clubMemberNotes =
      //  new Dictionary<String, Stack<ClubMemberNote>>();
      //CreditCards
      Dictionary<String, SortedSet<CreditCardRecord>> creditCards =
        new Dictionary<String, SortedSet<CreditCardRecord>>();
      #endregion

      #region Identify the standard winery member types
      MemberType clubType         = memberTypeGetter.ClubMemberType;
      MemberType subscriberType   = memberTypeGetter.SubscriberType;
      MemberType unsubscriberType = memberTypeGetter.UnsubscriberType;
      MemberType purchaserType    = memberTypeGetter.PurchaserType;
      #endregion

      // Strict mapping is useful to make sure all the columns in the 
      // spreadsheet map to properties in the ImportMember class.
      spreadSheet.StrictMapping = true;
      
      // Already populated.
      if (this.members != null)
        return this.members;

      this.members = new Dictionary<String, Member>();

      //vars for number of shipments calculations
      DateTime today = DateTime.Today;
      #endregion

      #region Mappings and Transforms
      spreadSheet.AddMapping<ImportMember>(m => m.AltMemberID, "AltMemberID");
      spreadSheet.AddTransformation<ImportMember>(m => m.Password, cellValue => cellValue ?? DataTaskHelper.RandomString(8));
      spreadSheet.AddTransformation<ImportMember>(m => m.CreditCardType,
        cellValue =>
        {
          cellValue = cellValue.Trim();
          if (cellValue.ToLower() == "mastercard" || cellValue.ToLower() == "master card" || cellValue.ToLower() == "mc")
            return "MC";
          else if (cellValue.ToLower() == "american express" || cellValue.ToLower() == "amex" || cellValue.ToLower() == "amx")
            return "AmEx";
          else if (cellValue.ToLower() == "visa")
            return "VISA";
          else if (cellValue.ToLower() == "discover card" || cellValue.ToLower() == "discover")
            return "Discover";
          else if (cellValue == "")
            return null;
          return cellValue;
        });

      spreadSheet.AddTransformation<ImportMember>(m => m.BillingDateOfBirth  , DataTaskHelper.DefaultDateTransform);
      spreadSheet.AddTransformation<ImportMember>(m => m.CustomerCreationDate, DataTaskHelper.DefaultDateTransform);
      spreadSheet.AddTransformation<ImportMember>(m => m.CustomerLastUpdated , DataTaskHelper.DefaultDateTransform);
      //spreadSheet.AddTransformation<ImportMember>(m => m.ShippingBirthDate   , DataTaskHelper.DefaultDateTransform);
      spreadSheet.AddTransformation<ImportMember>(m => m.ClubBirthDate       , DataTaskHelper.DefaultDateTransform);
      spreadSheet.AddTransformation<ImportMember>(m => m.ClubStartDate       , DataTaskHelper.DefaultDateTransform);
      //spreadSheet.AddTransformation<ImportMember>(m => m.ClubEndDate         , DataTaskHelper.DefaultDateTransform);
      spreadSheet.AddTransformation<ImportMember>(m => m.ClubHoldStartDate   , DataTaskHelper.DefaultDateTransform);
      spreadSheet.AddTransformation<ImportMember>(m => m.ClubHoldEndDate     , DataTaskHelper.DefaultDateTransform);
      spreadSheet.AddTransformation<ImportMember>(m => m.ClubCancelDate      , DataTaskHelper.DefaultDateTransform);
      spreadSheet.AddTransformation<ImportMember>(m => m.IsPurchaser         , DataTaskHelper.DefaultBooleanTransform);
      spreadSheet.AddTransformation<ImportMember>(m => m.IsSubscriber        , DataTaskHelper.DefaultBooleanTransform);
      spreadSheet.AddTransformation<ImportMember>(m => m.ClubIsActive        , DataTaskHelper.DefaultBooleanTransform);
      spreadSheet.AddTransformation<ImportMember>(m => m.ClubOnHold          , DataTaskHelper.DefaultBooleanTransform);
      //spreadSheet.AddTransformation<ImportMember>(m => m.IsPrimaryShipAddress, DataTaskHelper.DefaultBooleanTransform);
      spreadSheet.AddTransformation<ImportMember>(m => m.IsGiftMembership    , DataTaskHelper.DefaultBooleanTransform);
      spreadSheet.AddTransformation<ImportMember>(m => m.IsUnsubscriber      , DataTaskHelper.DefaultBooleanTransform);
      //spreadSheet.AddTransformation<ImportMember>(m => m.MemberNumber        , DataTaskHelper.DefaultIntTransform);


      #endregion

      #region Query the spreadsheet
      this.csvMembers = (from m in spreadSheet.Worksheet<ImportMember>(worksheetName)
                        select m).ToList();
      #endregion


      #region De-dupe
      // Create actual Members from the ImportMember.  Members are
      // de-duped by their memberID.


      foreach (ImportMember member in this.csvMembers)
      {
        #region Create member
        Member curMem       = new Member();
        curMem.Salutation   = member.Salutation;
        curMem.BirthDate    = member.BillingDateOfBirth;
        curMem.Address1     = member.Address;
        curMem.Address2     = member.Address2;              
        curMem.City         = member.City;
        curMem.Company      = member.Company;
        curMem.Email        = member.Email;
        curMem.FirstName    = member.FirstName;
        curMem.LastName     = member.LastName;
        curMem.AltMemberID  = member.AltMemberID;
        curMem.Password     = member.Password;
        curMem.Phone1       = member.Phone;
        curMem.Phone2       = member.Phone2;
        curMem.SourceCode   = member.SourceCode;
        curMem.State        = member.State;
        //curMem.Province     = member.Province;
        curMem.Username     = member.UserName;
        curMem.ZipCode      = member.ZipCode;
        curMem.DateAdded    = member.CustomerCreationDate;
        curMem.DateModified = member.CustomerLastUpdated;
        //curMem.MemberNumber = member.MemberNumber;

        //Console.WriteLine("Number: " + curMem.AltMemberID + " Zip: " + curMem.ZipCode);
        // Wallet feature enabled per Jon 7/11/13        
        /*
        curMem.CreditCardType            = member.CreditCardType;
        curMem.CreditCardExpirationMonth = member.CreditCardExpMo;
        curMem.CreditCardExpirationYear  = member.CreditCardExpYr;
        curMem.CreditCardNumber          = member.CreditCardNumber;
        curMem.NameOnCreditCard          = member.NameOnCard;
        */

        if (String.IsNullOrEmpty(member.AltMemberID))
        {
          errors.Add(new ErrorReportMember(curMem, "AltMemberID is missing", ErrorReportMember.FixTheFile, "Member Getter", ErrorReportMember.ERRORTYPE.CRITICAL));
          continue;
        }

        if (!this.members.ContainsKey(member.AltMemberID))
        {
          this.members.Add(member.AltMemberID, curMem);
        }
        else
        {
          // Merge members with the same AltMemberID per Jon 6/25/13
          if (this.members[member.AltMemberID].Email == curMem.Email)
            this.members[member.AltMemberID] = curMem;
          else
          {
            errors.Add(new ErrorReportMember(curMem, "AltMemberID assigned to multiple different emails", 
                                       ErrorReportMember.FixTheFile, "Member Getter", ErrorReportMember.ERRORTYPE.CRITICAL));
            continue;
          }

          /*
          if (this.members[member.AltMemberID].LastName  != member.LastName ||
              this.members[member.AltMemberID].FirstName != member.FirstName ||
              this.members[member.AltMemberID].Email     != member.Email)
          {
            errors.Add(new ErrorReport(member.AltMemberID, "is not unique.", ErrorReport.FixTheFile));
            continue;
          }
          */
        }
        #endregion

        #region Create member types
        MemberType curType = new MemberType();

        // Add MemberType to a member if it was passed in
        if (!String.IsNullOrEmpty(member.MemberType))
        {
          curType.MemberTypeDescription = member.MemberType;
          // Assign MemberTypeID
          curType.MemberTypeID = memberTypeGetter.GetMemberType(member.MemberType.ToUpper().Trim()).MemberTypeID;
        }

        // If this member already exists in the member types dictionary.
        if (memTypes.ContainsKey(member.AltMemberID))
        {
          // A member type may not have been provided in the CSV.
          if (curType.MemberTypeID.HasValue)
          {
            // If the current member type is not already associated with the member.
            if (!memTypes[member.AltMemberID].ContainsKey(curType.MemberTypeID.Value))
              memTypes[member.AltMemberID].Add(curType.MemberTypeID.Value, curType);
          }
        }
        // Else this member doesn't exist in the dictionary yet.
        else
        {
          Dictionary<Guid, MemberType> types = new Dictionary<Guid, MemberType>();
          memTypes.Add(member.AltMemberID, types);

          // A member type was not provided in the CSV.
          if (curType.MemberTypeID.HasValue)
            types.Add(curType.MemberTypeID.Value, curType);
        }


        // If this member is a purchaser they should be assigned to the 
        // Purchasers member type if they are not already.
        if (member.IsPurchaser)
        {
          if (!memTypes[member.AltMemberID].ContainsKey(purchaserType.MemberTypeID.Value))
          {
            memTypes[member.AltMemberID].Add(purchaserType.MemberTypeID.Value, purchaserType);
            //Console.WriteLine("Member " + member.AltMemberID + " added to Purchasers.");
          }
        }

        // If this member is a purchaser they should be assigned to the
        // Subscriber member type if they are not already
        if (member.IsSubscriber)
        {
          if (!memTypes[member.AltMemberID].ContainsKey(subscriberType.MemberTypeID.Value))
          {
            memTypes[member.AltMemberID].Add(subscriberType.MemberTypeID.Value, subscriberType);
            //Console.WriteLine("Member " + member.AltMemberID + " added to Subscribers.");
          }
        }

        // If this member is an opt out add them to the Unsubscriber member type.
        if (member.IsUnsubscriber)
        {
          if (!memTypes[member.AltMemberID].ContainsKey(unsubscriberType.MemberTypeID.Value))
          {
            memTypes[member.AltMemberID].Add(unsubscriberType.MemberTypeID.Value, unsubscriberType);
            //Console.WriteLine("Member " + member.AltMemberID + " added to Unsubscribers.");
          }
        }
        #endregion

        #region Create shipping address
        // Add non-duplicate ship address
        if (!String.IsNullOrEmpty(member.ClubAddress))
        {

            ShippingAddress curSAddr   = new ShippingAddress();
            curSAddr.Address1          = member.ClubAddress;
            curSAddr.Address2          = member.ClubAddress2;
            curSAddr.BirthDate         = member.ClubBirthDate;
            curSAddr.City              = member.ClubCity;
            curSAddr.Company           = member.ClubCompany;
            curSAddr.Email             = member.ClubEmailAddress;
            curSAddr.FirstName         = member.ClubFirstName;
            curSAddr.IsPrimary         = true;
            curSAddr.LastName          = member.ClubLastName;
            curSAddr.Phone1            = member.ClubPhone;
            curSAddr.Phone2            = member.ClubPhone2;
            curSAddr.Salutation        = member.ClubSalutation;
            curSAddr.State             = member.ClubState;
            //curSAddr.Province        = member.ShipProvince;
            curSAddr.ZipCode           = member.ClubZipCode;
            curSAddr.AddressName       = member.ClubAddress;

            // If this member already exists in the ShippingAddress dictionary.
            if (shippingAddresses.ContainsKey(member.AltMemberID))
            {
              // If the current shipping address is not already associated with the member.
              Boolean isDuplicate = (from sa in shippingAddresses[member.AltMemberID]
                                     where String.Equals(sa.Address1, curSAddr.Address1, StringComparison.OrdinalIgnoreCase) &&
                                           String.Equals(sa.LastName, curSAddr.LastName, StringComparison.OrdinalIgnoreCase) &&
                                           String.Equals(sa.FirstName, curSAddr.FirstName, StringComparison.OrdinalIgnoreCase)
                                     select sa).Count() != 0;

              if (!isDuplicate)
                shippingAddresses[member.AltMemberID].Add(curSAddr);

            }
            // Else this member doesn't exist in the dictionary yet.
            else
            {
              SortedSet<ShippingAddress> addresses = new SortedSet<ShippingAddress>(new ReflectiveComparer<ShippingAddress>());
              addresses.Add(curSAddr);
              shippingAddresses.Add(member.AltMemberID, addresses);
            }
          
        }
        #endregion

        #region Create club memberships
        if (!String.IsNullOrEmpty(member.ClubLevel))
        {
          ClubMembership curCMem = new ClubMembership();
          curCMem.ClubLevel                  = member.ClubLevel;
          curCMem.GiftMessage                = member.GiftMessage;
          curCMem.HoldStartDate              = member.ClubHoldStartDate;
          curCMem.IsActive                   = member.ClubIsActive;
          curCMem.IsOnHold                   = member.ClubOnHold;
          curCMem.OnHoldDate                 = member.ClubHoldEndDate;
          curCMem.ShippingAddress            = member.ClubAddress;
          curCMem.ShippingAddress2           = member.ClubAddress2;
          curCMem.ShippingBirthDate          = member.ClubBirthDate;
          curCMem.ShippingCity               = member.ClubCity;
          curCMem.ShippingCompany            = member.ClubCompany;
          curCMem.ShippingEmail              = member.ClubEmailAddress;
          curCMem.ShippingFirstName          = member.ClubFirstName;
          curCMem.ShippingLastName           = member.ClubLastName;
          curCMem.ShippingPhone1             = member.ClubPhone;
          curCMem.ShippingPhone2             = member.ClubPhone2;
          curCMem.ShippingSalutation         = member.ClubSalutation;
          curCMem.ShippingState              = member.ClubState;
          curCMem.WineShippingType           = member.ClubShippingPreference;
          curCMem.ShippingZipCode            = member.ClubZipCode;
          //Removed per Jon
          //curCMem.ShippingProvince           = member.ClubProvince;
          //Hardcoded per Jon 3/29/13
          curCMem.CurrentNumberOfShipments   = 0;
          curCMem.TotalNumberOfShipments     = 999;
          //Dates brought back per Jon 6/13/13
          curCMem.StartDate                  = member.ClubStartDate;
          curCMem.DateAdded                  = member.ClubStartDate;
          curCMem.EndDate                    = member.ClubCancelDate;
          curCMem.CancelDate                 = member.ClubCancelDate;
          //curCMem.CancelReason               = member.CancelReason;
          
          // If this member already exists in the ClubMemberships dictionary.
          if (clubMemberships.ContainsKey(member.AltMemberID))
          {
            // If the current club membership is not already associated with the member.
            if (!clubMemberships[member.AltMemberID].Contains(curCMem))
              clubMemberships[member.AltMemberID].Add(curCMem);
          }
          // Else this member doesn't exist in the dictionary yet.
          else
          {
            SortedSet<ClubMembership> memberships = new SortedSet<ClubMembership>(new ReflectiveComparer<ClubMembership>());
            memberships.Add(curCMem);
            clubMemberships.Add(member.AltMemberID, memberships);
          }

          // This member is a club, so the member should be assigned to the
          // Club Members member type if they are not already AND membership is active (per Jon 6/18)
          if (member.ClubIsActive)
          {
            if (!memTypes[member.AltMemberID].ContainsKey(clubType.MemberTypeID.Value))
              memTypes[member.AltMemberID].Add(clubType.MemberTypeID.Value, clubType);
          } 
        }
        #endregion

        #region Create credit card
        if (!String.IsNullOrEmpty(member.CreditCardType) || !String.IsNullOrEmpty(member.CreditCardNumber) || !String.IsNullOrEmpty(member.NameOnCard))
        {
          CreditCardRecord curCard          = new CreditCardRecord();
          curCard.CreditCardExpirationMonth = member.CreditCardExpMo;
          curCard.CreditCardExpirationYear  = member.CreditCardExpYr;
          curCard.CreditCardNumber          = member.CreditCardNumber;
          curCard.NameOnCreditCard          = member.NameOnCard;
          curCard.CreditCardType            = member.CreditCardType;
          
          // if current member exists in creditcards dictionary
          if (creditCards.ContainsKey(member.AltMemberID))
          {
            // If the current credictcard is not already associated with the member.
            if (!creditCards[member.AltMemberID].Contains(curCard))
            {
              creditCards[member.AltMemberID].Add(curCard);
            }
          }
          // add new member to creditcard dictionary
          else
          {
            SortedSet<CreditCardRecord> memberCCs =
              new SortedSet<CreditCardRecord>(new ReflectiveComparer<CreditCardRecord>());
            memberCCs.Add(curCard);
            creditCards.Add(member.AltMemberID, memberCCs);
          }
        }
        #endregion

        #region Create member notes
        if (!String.IsNullOrWhiteSpace(member.CustomerNotes))
        {
          //de-dupe notes
          if (!memberNotes.ContainsKey(member.AltMemberID))
          {
            Stack<MemberNote> notes = new Stack<MemberNote>();
            memberNotes.Add(member.AltMemberID, notes);
                
            // Break large notes into smaller substrings by last index of '.' (dot) that is smaller than maxNoteLength
            int position = 0;
            int lastIndex;
            while (position < member.CustomerNotes.Length)
            {
              String note;
              // Calculate substring indeces
              if (position + maxNoteLength > member.CustomerNotes.Length)
                lastIndex = member.CustomerNotes.Length;
              else
                lastIndex = member.CustomerNotes.LastIndexOf('.', position + maxNoteLength, maxNoteLength) + 1;

              if (lastIndex < 1)
                lastIndex = position + maxNoteLength;

              note                    = member.CustomerNotes.Substring(position, lastIndex - position).Trim();
              position                = lastIndex;
              MemberNote memberNote   = new MemberNote();
              memberNote.IsSystemNote = true;
              memberNote.DateAdded    = DateTime.Now;
              memberNote.Notes        = note;
              //add sub note to member notes
              notes.Push(memberNote);
            }
          }
        }
        #endregion

        //#region Create clubmember notes
        //if (!String.IsNullOrWhiteSpace(member.ClubMemberNotes))
        //{
        //    //de-dupe notes
        //    if (!ClubMemberNote.ContainsKey(member.AltMemberID))
        //    {
        //        Stack<ClubMemberNote> notes = new Stack<ClubMemberNote>();
        //        memberNotes.Add(member.AltMemberID, notes);

        //        // Break large notes into smaller substrings by last index of '.' (dot) that is smaller than maxNoteLength
        //        int position = 0;
        //        int lastIndex;
        //        while (position < member.CustomerNotes.Length)
        //        {
        //            String note;
        //            // Calculate substring indeces
        //            if (position + maxNoteLength > member.CustomerNotes.Length)
        //                lastIndex = member.CustomerNotes.Length;
        //            else
        //                lastIndex = member.CustomerNotes.LastIndexOf('.', position + maxNoteLength, maxNoteLength) + 1;

        //            if (lastIndex < 1)
        //                lastIndex = position + maxNoteLength;

        //            note = member.CustomerNotes.Substring(position, lastIndex - position).Trim();
        //            position = lastIndex;
        //            ClubMemberNote clubMemberNote = new ClubMemberNote();
        //            clubMemberNote.IsSystemNote = true;
        //            clubMemberNote.DateAdded = DateTime.Now;
        //            clubMemberNote.Notes = note;
        //            //add sub note to member notes
        //            notes.Push(ClubMemberNote);
        //        }
        //    }
        //}
        //#endregion

      }
      #endregion


      #region Make them "know" each other
      foreach (KeyValuePair<String, Member> member in this.members)
      {
        #region Add types to member
        if (memTypes.ContainsKey(member.Key))
        {
          member.Value.MemberTypes = memTypes[member.Key].Values.ToArray();
        }
        #endregion

        #region Add shipping addresses to member
        if (shippingAddresses.ContainsKey(member.Key))
        {
          member.Value.ShippingAddresses = shippingAddresses[member.Key].ToArray();
        }
        #endregion

        #region Add club memberships to member
        if (clubMemberships.ContainsKey(member.Key))
        {
          member.Value.ClubMemberships = clubMemberships[member.Key].ToArray();
        }
        #endregion

        #region Add credit cards to member
        if (creditCards.ContainsKey(member.Key))
        {
          member.Value.CreditCards = creditCards[member.Key].ToArray();
        }
        #endregion

        #region Add member notes
        if (memberNotes.ContainsKey(member.Key))
        {
          member.Value.MemberNotes = memberNotes[member.Key].ToArray();
        }
        #endregion

        //#region Add clubmember notes
        //  //if (club
      }
      #endregion

      return this.members;
    }

    /**
     * Dump a member using reflection.
     */
    public void pukeMember(Object member)
    {
      foreach (var prop in member.GetType().GetProperties())
      {
        var val = prop.GetValue(member, null);

        /*if (val is IEnumerable<>)
        {
          pukeMember(val);
        }
        else*/
        {
          Console.WriteLine(prop.Name + " => " + prop.GetValue(member, null));
        }
      }
    }


    /// <summary>
    /// Get spreadsheet members
    /// </summary>
    /// <param name="IDs">Array of AltMemberIDs</param>
    /// <returns>List of spreadsheet members that match passed in array of AltMemberIDs</returns>
    public IEnumerable<ImportMember> GetSpreadsheetMembers(IEnumerable<String> IDs)
    {
      List<ImportMember> members = new List<ImportMember>();

      if (this.csvMembers != null)
      {
        members = this.csvMembers
          .Where(csv => IDs.Contains(csv.AltMemberID))
          .OrderBy(csv => csv.AltMemberID)
          .ToList();
      }

      return members;
    }


    /// <summary>
    /// Get spreadsheet members and errors
    /// </summary>
    /// <param name="errors"></param>
    /// <returns></returns>
    public IEnumerable<ImportMember> GetSpreadsheetMembersAndErrors(IEnumerable<ErrorReportMember> errors)
    {
      List<ImportMember> members = new List<ImportMember>();

      if (this.csvMembers != null)
      {
        // group and flatten error messages by recordid
        var err = (from e in errors
                   group e by e.RecordID into eGrp
                   select new { ID = eGrp.Key, Error = String.Join(" ", eGrp.Select(e => e.ErrorMessage)) }).ToList();

        // get all members that have errors
        members = this.csvMembers.Where(csv => err.Select(e => e.ID).Contains(csv.AltMemberID)).ToList();

        // add errors to members
        foreach (var e in err)
          members.Where(m => m.AltMemberID == e.ID).Select(em => { em.Errors = e.Error; return em; }).OrderBy(csv => csv.AltMemberID).ToList();
      }

      return members;
    }
  }
}

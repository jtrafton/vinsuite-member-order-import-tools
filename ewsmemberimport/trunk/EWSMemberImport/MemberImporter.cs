﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EWSDataTaskSupport.EWSData;
using EWSDataTaskSupport;
using MemberValidationRules;
using System.IO;
using System.Threading;
using System.Xml.Serialization;

namespace EWSMemberImport
{
  public class MemberImporter
  {
    private bool debug;
    private bool isValid;
    private bool IsAllowDupeClubs;
    private int recordsPerRound;
    private int? dataTaskID;
    private String importFile;
    private String adminEmail;
    private String adminWSUsername;
    private String adminWSPassword;
    private Guid wineryID;
    private Dictionary<String, Member> members;
    private List<ErrorReportMember> errors;
    private GetDataCredentialsResponse dataCredentials;
    private DataServiceHttps ds;
    private CSVHelper csv;
    private String importDirectory;
    private String importTimestamp;
    private MemberGetter memberGetter;
    private ExcelWriter excelWriter;
    private String worksheetName;
    private String pathToLogs;

    /// <summary>
    /// Construct Member Importer Object
    /// </summary>
    /// <param name="debug">true performs validation only</param>
    /// <param name="importFile">the file containing member data</param>
    /// <param name="membersPerRound">how many members to import in each iteration</param>
    /// <param name="adminEmail">who to notify when the task is complete</param>
    /// <param name="adminWSUsername">web service credential</param>
    /// <param name="adminWSPassword">web service credential</param>
    /// <param name="wineryID">which winery to import members into</param>
    /// <param name="dataTaskID">optional data task tracking identifier</param>
    public MemberImporter(bool debug, String importFile, int membersPerRound,
      String adminEmail, String adminWSUsername, String adminWSPassword, Guid wineryID, int? dataTaskID, bool IsAllowDupeClubs)
    {
      // Allow self-signed certs.
      System.Net.ServicePointManager.ServerCertificateValidationCallback += (se, cert, chain, sslerror) => true;
      
      this.debug                = debug;
      this.importFile           = importFile;
      this.recordsPerRound      = membersPerRound;
      this.adminEmail           = adminEmail;
      this.adminWSUsername      = adminWSUsername;
      this.adminWSPassword      = adminWSPassword;
      this.wineryID             = wineryID;
      this.dataTaskID           = dataTaskID;
      this.errors               = new List<ErrorReportMember>();
      this.ds                   = new DataServiceHttps();
      this.ds.Timeout           = 30000;
      this.isValid              = false;
      this.IsAllowDupeClubs     = IsAllowDupeClubs;
      this.worksheetName        = "Members";

      // Get data Credentials
      DataRequest credentialsRequest = new DataRequest()
      {
        Username = adminWSUsername,
        Password = adminWSPassword,
        WineryID = wineryID
      };

      dataCredentials = ds.GetDataCredentials(credentialsRequest);
      if (String.IsNullOrEmpty(dataCredentials.Username) || String.IsNullOrEmpty(dataCredentials.Password))
        errors.Add(new ErrorReportMember("Failed to get data credentials",
                   "Make sure admin credentials are valid",
                   "Data Credentials Getter",
                   ErrorReportMember.ERRORTYPE.CRITICAL));

      csv = new EWSDataTaskSupport.CSVHelper();
      importDirectory = Path.GetDirectoryName(importFile);
      importTimestamp = DateTime.Now.ToString("yyyyMMdd-HHmmss");
      this.pathToLogs = importDirectory + @"\" + importTimestamp + "_MemberImportLog" + ".xls";
      this.memberGetter = new MemberGetter(importFile, this.worksheetName, dataCredentials.Username, dataCredentials.Password, debug);
      excelWriter = new ExcelWriter(this.pathToLogs);
    }

    /// <summary>
    /// Destructor
    /// </summary>
    ~MemberImporter()
    {
      ds.Dispose();
    }

    /// <summary>
    /// Load and Validate Spreadsheet Data
    /// </summary>
    /// <returns>true if valid, false if invalid</returns>
    public bool LoadAndValidateSpreadsheet()
    {
      ValidationDriver valDriver;
      members = memberGetter.GetMembers();

      errors.AddRange(memberGetter.GetErrors());
      
      #region Run external validation rules
      if (members != null)
      {
        valDriver = new ValidationDriver(members.Values, dataCredentials.Username, dataCredentials.Password);
        Console.WriteLine("loaded validators: {0}", String.Join(", ", valDriver.GetCurrentValidators()));
        valDriver.RunValidation();
        errors.AddRange(valDriver.GetValidationErrors());

        isValid = errors.Count == 0;

        if (!isValid)
        {
          // Remove records from the import that have critical errors
          members = members
            .Where(m => !errors.Where(e => e.ErrorType() == ErrorReportMember.ERRORTYPE.CRITICAL).Select(ce => ce.RecordID).Contains(m.Key))
            .ToDictionary(x => x.Key, x => x.Value);
        }
      }
      else
        errors.Add(new ErrorReportMember("No members were found in the spreadsheet.", 
                   ErrorReportMember.FixTheFile, "Spreadsheet loader and validator",
                   ErrorReportMember.ERRORTYPE.CRITICAL));
      #endregion

      return isValid;
    }

    /// <summary>
    /// Get Errors
    /// </summary>
    /// <returns></returns>
    public List<ErrorReportMember> GetErrors()
    {
      return errors;
    }

    /// <summary>
    /// Get number of members ready to be inserted
    /// </summary>
    /// <returns></returns>
    public int GetMemberCount()
    {
      return members != null ? members.Count : 0;
    }

    /// <summary>
    /// Get number of active club memberships ready to be inserted
    /// </summary>
    /// <returns></returns>
    public int GetActiveClubMembershipCount()
    {
      return this.members != null ? 
        members.Where(x => x.Value.ClubMemberships != null)
        .SelectMany(m => m.Value.ClubMemberships)
        .Count(cm => cm.IsActive.Value) : 0; 
    }

    public String GetLogLocation()
    {
      return this.pathToLogs;
    }

    /// <summary>
    /// Import Members into EWS Database
    /// </summary>
    /// <returns>True if import process has started, False if it didn't</returns>
    public bool ImportMembers()
    {
      ImportMemberRequest importMemberRequest = new ImportMemberRequest();
      ImportRunnerMembers impMembers;
      UpdateDataTaskRequest updTaskRequest;
      GetDataTasksRequest getTaskRequest;

      if (this.members != null)
      {
        #region Setup requests
        importMemberRequest.Username              = adminWSUsername;
        importMemberRequest.Password              = adminWSPassword;
        importMemberRequest.WineryID              = wineryID;
        importMemberRequest.EmailTo               = adminEmail;
        importMemberRequest.IsAllowDupeClubLevels = this.IsAllowDupeClubs;

        updTaskRequest = new UpdateDataTaskRequest()
        {
          Username = adminWSUsername,
          Password = adminWSPassword,
          UpdateType = ProgressUpdateType.INCREMENT_PROGRESS,
          DataTask = new DataTask_Update()
          {
            DataTaskTrackingID = dataTaskID ?? 0
          }
        };

        getTaskRequest = new GetDataTasksRequest()
        {
          Username = adminWSUsername,
          Password = adminWSPassword,
          DataTaskTrackingID = dataTaskID
        };
        #endregion

        Console.WriteLine("Members:");
        impMembers = new ImportRunnerMembers(importMemberRequest, ds, excelWriter, updTaskRequest, debug, recordsPerRound);
        errors.AddRange(impMembers.Import(this.members.Select(m => m.Value).ToArray()));
      }

      #region Write logs to disk
      Console.WriteLine("Writing log");
      var criticalErrors = errors.Where(e => e.ErrorType() == ErrorReportMember.ERRORTYPE.CRITICAL);
      var warnings = errors.Where(e => e.ErrorType() == ErrorReportMember.ERRORTYPE.WARNING);
      excelWriter.WriteToXLS(criticalErrors.OrderBy(e => e.RecordID).ToArray(), "Errors");
      excelWriter.WriteToXLS(warnings.OrderBy(e => e.RecordID).ToArray(), "Warnings");
      excelWriter.WriteToXLS(memberGetter.GetSpreadsheetMembersAndErrors(errors.Except(warnings).ToArray()), "Failed Members");
      #endregion
      
      return true;
    }

    /// <summary>
    /// Run Import on the background
    /// </summary>
    public void ImportMembersAsync()
    {
      ThreadPool.QueueUserWorkItem(new WaitCallback(ImportMembersAsync), null);
    }

    /// <summary>
    /// Entry point for a background thread
    /// </summary>
    protected void ImportMembersAsync(object memberImportPackage)
    {
      ImportMembers();
    }
  }
}

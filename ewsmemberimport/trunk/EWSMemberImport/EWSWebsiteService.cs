﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EWSDataTaskSupport.EWSWebsite;
using EWSDataTaskSupport;

namespace EWSMemberImport
{
  class EWSWebsiteService
  {
    private static TestConnectionResponse connResponse;
    private static ConnectionManager conMan;

    private static void Init()
    {
      if (conMan == null)
        conMan = new ConnectionManager();
      TestConnectionRequest connRequest = new TestConnectionRequest();
      connRequest.Username = conMan.username;
      connRequest.Password = conMan.password;
      using (WebsiteService ws = new WebsiteService())
      {
        //ws.Url = ws.Url.Replace("http:", "https:");
        connResponse = ws.TestConnection(connRequest);
      }
    }

    /// <summary>
    /// Get app.config connectionManager
    /// </summary>
    /// <returns></returns>
    public static ConnectionManager getConnectionManager()
    {
      if (conMan == null)
        conMan = new ConnectionManager();
      return conMan;
    }

    /// <summary>
    /// Get misc connection info associated with ews credentials
    /// </summary>
    /// <param name="username"></param>
    /// <param name="password"></param>
    /// <returns></returns>
    public static TestConnectionResponse GetConnectionInfo()
    {
      if (connResponse == null)
      {
        Init();
      }
      return connResponse;
    }
  }
}

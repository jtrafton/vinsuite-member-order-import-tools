﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EWSDataTaskSupport;
using EWSDataTaskSupport.EWSData;

namespace EWSMemberImport
{
  public class ImportRunnerMembers : EWSCallerBase<ImportMemberRequest, ImportMemberResponse, ErrorReportMember, Member>
  {
    private List<MemberResponseLog> memResponseLogs;
    private List<CreditCardResponseLog> ccLogs;
    private List<ClubMembershipResponseLog> clubLogs;
    private List<ShippingAddressResponseLog> shipAddressLogs;
    private List<MemberNoteResponseLog> noteLogs;

    public ImportRunnerMembers(ImportMemberRequest request, DataServiceHttps ds, ExcelWriter excelWriter, UpdateDataTaskRequest updTaskRequest, bool debug, int recordsPerRound)
      : base(ds.ImportMembers, request, ds, excelWriter, updTaskRequest, debug, recordsPerRound)
    { }

    protected override void SetRequestProperty(IEnumerable<Member> subList)
    {
      wsRequest.Members = subList.ToArray();
    }

    protected override List<ErrorReportMember> ProcessResponseProperty()
    {
      List<ErrorReportMember> errors = new List<ErrorReportMember>();
      memResponseLogs                = new List<MemberResponseLog>();
      ccLogs                         = new List<CreditCardResponseLog>();
      clubLogs                       = new List<ClubMembershipResponseLog>();
      shipAddressLogs                = new List<ShippingAddressResponseLog>();
      noteLogs                       = new List<MemberNoteResponseLog>();
      int i                          = 0;

      foreach (ImportMemberResponses currentResponse in wsResponse.Responses)
      {
        memResponseLogs.Add(new MemberResponseLog(wsRequest.Members[i], currentResponse.memberResponse));
        if (!currentResponse.memberResponse.IsSuccessful)
          errors.Add(new ErrorReportMember(wsRequest.Members[i], currentResponse.memberResponse.ErrorMessage, ErrorReportMember.FixTheFile, "Import - Members", ErrorReportMember.ERRORTYPE.CRITICAL));

        foreach (AddUpdateCreditCardResponse response in currentResponse.ccResponses)
        {
          ccLogs.Add(new CreditCardResponseLog(wsRequest.Members[i], currentResponse.memberResponse, response));
          if (!response.IsSuccessful)
            errors.Add(new ErrorReportMember(wsRequest.Members[i], response.ErrorMessage, ErrorReportMember.FixTheFile, "Import - CreditCards", ErrorReportMember.ERRORTYPE.CRITICAL));
        }

        foreach (AddUpdateClubMembershipResponse response in currentResponse.clubResponses)
        {
          clubLogs.Add(new ClubMembershipResponseLog(wsRequest.Members[i], currentResponse.memberResponse, response));
          if (!response.IsSuccessful)
            errors.Add(new ErrorReportMember(wsRequest.Members[i], response.ErrorMessage, ErrorReportMember.FixTheFile, "Import - ClubMembers", ErrorReportMember.ERRORTYPE.CRITICAL));
        }

        foreach (AddUpdateShippingAddressResponse response in currentResponse.shipResponses)
        {
          shipAddressLogs.Add(new ShippingAddressResponseLog(wsRequest.Members[i], currentResponse.memberResponse, response));
          if (!response.IsSuccessful)
            errors.Add(new ErrorReportMember(wsRequest.Members[i], response.ErrorMessage, ErrorReportMember.FixTheFile, "Import - ShipMembers", ErrorReportMember.ERRORTYPE.CRITICAL));
        }

        foreach (AddMemberNoteResponse response in currentResponse.notesResponses)
        {
          noteLogs.Add(new MemberNoteResponseLog(wsRequest.Members[i], currentResponse.memberResponse, response));
          if (!response.IsSuccessful)
            errors.Add(new ErrorReportMember(wsRequest.Members[i], response.ErrorMessage, ErrorReportMember.FixTheFile, "Import - MemberNotes", ErrorReportMember.ERRORTYPE.CRITICAL));
        }
        ++i;
      }
      // Log all member import responses
      excelWriter.WriteToXLS(memResponseLogs, "Member Responses");
      excelWriter.WriteToXLS(ccLogs,          "Credit Card Responses");
      excelWriter.WriteToXLS(clubLogs,        "Club Responses");
      excelWriter.WriteToXLS(shipAddressLogs, "Ship Responses");
      excelWriter.WriteToXLS(noteLogs,        "Note Responses");

      return errors;
    }

    protected override ErrorReportMember AddExceptionError(Member item, string exceptionErr)
    {
      ErrorReportMember error;
      error = new ErrorReportMember(item, exceptionErr, ErrorReportMember.ContactDeveloper, "Import", ErrorReportMember.ERRORTYPE.EXCEPTION);
      return error;
    }
  }
}

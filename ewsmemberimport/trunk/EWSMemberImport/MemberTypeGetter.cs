﻿using System;
using System.Collections.Generic;
using EWSDataTaskSupport;
using EWSDataTaskSupport.EWSMember;

namespace EWSMemberImport
{
  public class MemberTypeGetter : IErrorReporter
  {
    private Dictionary<String, EWSDataTaskSupport.EWSData.MemberType> memberTypes;
    private String wsUsername;
    private String wsPassword;
    private bool debug;
    private MemberService serv;
    private List<ErrorReportMember> errors;

    /**
     * Pull all the member types for the winery and add them to a
     * sorted collection.
     */
    public MemberTypeGetter(MemberService ms, String username, String password, bool debug)
    {
      this.debug = debug;
      this.wsUsername = username;
      this.wsPassword = password;
      this.serv = ms;
      this.errors = new List<ErrorReportMember>();

      EWSDataTaskSupport.EWSMember.GetWineryMemberTypeRequest req = new EWSDataTaskSupport.EWSMember.GetWineryMemberTypeRequest();

      // Set up the credentials.
      req.Username = wsUsername;
      req.Password = wsPassword;

      // Get the member types.
      EWSDataTaskSupport.EWSMember.GetWineryMemberTypeResponse resp = DataTaskHelper.TryNTimes(serv.GetWineryMemberType, req, 1, TimeSpan.FromSeconds(1));
      if (resp == null)
      {
        errors.Add(new ErrorReportMember("Could not retrieve member types from the web services", 
                   ErrorReportMember.ContactDeveloper, "MemberTypeGetter",
                   ErrorReportMember.ERRORTYPE.CRITICAL));
        return;
      }

      this.memberTypes = new Dictionary<String, EWSDataTaskSupport.EWSData.MemberType>();

      if (resp.IsSuccessful)
      {
        EWSDataTaskSupport.EWSData.MemberType memberType;

        foreach (EWSDataTaskSupport.EWSMember.MemberType mt in resp.MemberTypes)
        {
          memberType = new EWSDataTaskSupport.EWSData.MemberType();
          memberType.MemberTypeDescription = mt.MemberTypeDescription;
          memberType.MemberTypeID = mt.MemberTypeID;
          memberType.WineryID = mt.WineryID;

          // Create the lookup key for the dictionary
          String memberTypeKey = String.IsNullOrEmpty(mt.Keyword) ? mt.MemberTypeDescription : mt.Keyword;
          memberTypeKey = memberTypeKey.ToUpper().Trim();

          if (!this.memberTypes.ContainsKey(memberTypeKey))
            this.memberTypes.Add(memberTypeKey, memberType);
          else
            Console.WriteLine("Duplicate member type: {0}", mt.MemberTypeDescription);
        }
      }
      else
        errors.Add(new ErrorReportMember("Unsuccessful web service response when retrieving member types: \n" + resp.ErrorMessage,
          ErrorReportMember.ContactDeveloper, 
          "MemberTypeGetter",
          ErrorReportMember.ERRORTYPE.CRITICAL));

      if (this.ClubMemberType == null)
        errors.Add(new ErrorReportMember("Member type \"ClubMember\" was not found", 
                   ErrorReportMember.FixTheAdmin,
                   "MemberTypeGetter",
                   ErrorReportMember.ERRORTYPE.CRITICAL));
      if (this.SubscriberType == null)
        errors.Add(new ErrorReportMember("Member type \"Subscriber\" was not found",
                   ErrorReportMember.FixTheAdmin,
                   "MemberTypeGetter",
                   ErrorReportMember.ERRORTYPE.CRITICAL));
      if (this.UnsubscriberType == null)
        errors.Add(new ErrorReportMember("Member type \"Unsubscriber\" was not found",
                   ErrorReportMember.FixTheAdmin,
                   "MemberTypeGetter",
                   ErrorReportMember.ERRORTYPE.CRITICAL));
      if (this.PurchaserType == null)
        errors.Add(new ErrorReportMember("Member type \"Purchaser\" was not found",
                   ErrorReportMember.FixTheAdmin,
                   "MemberTypeGetter",
                   ErrorReportMember.ERRORTYPE.CRITICAL));
    }

    
    /// <summary>
    /// Returns the list of errors encountered 
    /// while collecting member types
    /// </summary>
    /// <returns></returns>
    public List<ErrorReportMember> GetErrors()
    {
      return this.errors;
    }


    /// <summary>
    /// Return the standard club member type
    /// </summary>
    public EWSDataTaskSupport.EWSData.MemberType ClubMemberType
    {
      get { return this.GetStandardType("CLUBMEMBER"); }
    }


    /// <summary>
    /// Return the standard subscriber type
    /// </summary>
    public EWSDataTaskSupport.EWSData.MemberType SubscriberType
    {
      get { return this.GetStandardType("SUBSCRIBER"); }
    }

    /// <summary>
    /// Return the standard unsubscriber type
    /// </summary>
    public EWSDataTaskSupport.EWSData.MemberType UnsubscriberType
    {
      get { return this.GetStandardType("UNSUBSCRIBER"); }
    }


    /// <summary>
    /// Return the standard unsubscriber type
    /// </summary>
    public EWSDataTaskSupport.EWSData.MemberType PurchaserType
    {
      get { return this.GetStandardType("PURCHASER"); }
    }


    /// <summary>
    /// Wraps retrieval of a standard member types in logic
    /// that returns null if not found
    /// </summary>
    /// <param name="nameOrKeyword"></param>
    /// <returns></returns>
    private EWSDataTaskSupport.EWSData.MemberType GetStandardType(String nameOrKeyword)
    {
      if (this.HasMemberType(nameOrKeyword))
        return this.GetMemberType(nameOrKeyword);
      else
        return null;
    }


    /// <summary>
    /// Check if this object already found a member type by name/keyword
    /// </summary>
    /// <param name="nameOrKeyword"></param>
    /// <returns></returns>
    public bool HasMemberType(String nameOrKeyword)
    {
      nameOrKeyword = nameOrKeyword.ToUpper();
      return this.memberTypes.ContainsKey(nameOrKeyword);
    }


    /// <summary>
    /// Return the member type matching the name.
    /// If no match is found, a new member type is created!
    /// </summary>
    /// <param name="nameOrKeyword"></param>
    /// <returns></returns>
    public EWSDataTaskSupport.EWSData.MemberType GetMemberType(String nameOrKeyword)
    {
      // Keep a copy of the name as it was passed in (in case we need to add it)
      String originalName = nameOrKeyword;
      // Make the name/keyword uppercase so we can look it up in our dictionary
      nameOrKeyword = nameOrKeyword.ToUpper();

      if (this.HasMemberType(nameOrKeyword))
      {
        return this.memberTypes[nameOrKeyword];
      }
      else
      {
        EWSDataTaskSupport.EWSData.MemberType newType = new EWSDataTaskSupport.EWSData.MemberType()
        {
          MemberTypeID = this.AddMemberType(originalName),
          MemberTypeDescription = originalName
        };

        this.memberTypes.Add(nameOrKeyword, newType);
        return newType;
      }
    }

    /**
     * Add member type to the winery and return MemberTypeID
     */
    private Guid AddMemberType(string memberType)
    {
      if (debug)
      {
        // Return a fake guid if debug mode is enabled
        return Guid.NewGuid();
      }
      else
      {
        // Add the member type to the winery
        EWSDataTaskSupport.EWSMember.AddMemberTypeRequest amtRequest = new EWSDataTaskSupport.EWSMember.AddMemberTypeRequest();
        EWSDataTaskSupport.EWSMember.AddMemberTypeResponse amtResponse;
        amtRequest.Username = wsUsername;
        amtRequest.Password = wsPassword;
        amtRequest.MemberType = new EWSDataTaskSupport.EWSMember.MemberType { MemberTypeDescription = memberType };

        amtResponse = DataTaskHelper.TryNTimes(serv.AddMemberType, amtRequest, 10, TimeSpan.FromSeconds(10));

        if (amtResponse != null)
        {
          if (amtResponse.IsSuccessful)
            return amtResponse.MemberTypeID.Value;
          else
            throw new Exception("Error adding member type: " + amtResponse.ErrorMessage);
        }
        else
        {
          throw new Exception("Error communicating with the web services while trying to add new member type \"" + memberType + "\"");
        }
      }
    }
  }
}
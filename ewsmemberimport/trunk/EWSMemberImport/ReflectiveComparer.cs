﻿using System;
using System.Collections.Generic;
using System.Reflection;

namespace EWSMemberImport
{
  class ReflectiveComparer<T> : IComparer<T>
  {
    private PropertyInfo[] props;

    /// <summary>
    /// Store all the properties of the type.
    /// </summary>
    public ReflectiveComparer()
    {
      props = typeof(T).GetProperties();
    }

    /// <summary>
    /// Compare two Ts. This may be slow since it uses reflection.
    /// </summary>
    /// <param name="obj1"></param>
    /// <param name="obj2"></param>
    /// <returns></returns>
    public int Compare(T obj1, T obj2)
    {
      foreach (PropertyInfo prop in props)
      {
        object val1 = prop.GetValue(obj1, null);
        object val2 = prop.GetValue(obj2, null);

        if (val1 != null && val2 != null)
        {
          int comp = String.Compare(val1.ToString(), val2.ToString());

          if (comp != 0) return comp;
        }
      }

      return 0;
    }
  }
}

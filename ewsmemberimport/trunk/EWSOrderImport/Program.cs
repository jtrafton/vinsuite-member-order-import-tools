﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using EWSDataTaskSupport;
using EWSDataTaskSupport.EWSMember;
using EWSDataTaskSupport.EWSOrder;
using EWSDataTaskSupport.EWSProduct;
using LinqToExcel;
using System.Net;
using System.Reflection;

namespace EWSOrderImport
{
  class Program
  {
    /// <summary>
    /// Product and order import process.
    /// Don't forget to set up app.config!
    /// </summary>
    /// <param name="args"></param>
    static void Main(string[] args)
    {
      bool debugEnabled = ConfigurationManager.AppSettings["Debug"] == "1";
      bool importProducts = ConfigurationManager.AppSettings["ImportProducts"] == "1";
      bool importOrders = ConfigurationManager.AppSettings["ImportOrders"] == "1";

      // Allow self-signed ssl certificates
      ServicePointManager.ServerCertificateValidationCallback += (se, cert, chain, sslerror) => true;

      if (debugEnabled)
        Console.WriteLine("DEBUG mode is enabled, nothing will be sent");

      if (importProducts)
        ImportProducts();

      if (importOrders)
        ImportOrders();

      Console.WriteLine("Process complete!");
      Console.ReadLine();
    }


    /// <summary>
    /// Import products using data from app.config
    /// </summary>
    static void ImportProducts()
    {
      ImportProducts(
        ConfigurationManager.AppSettings["WSUsername"], 
        ConfigurationManager.AppSettings["WSPassword"],
        ConfigurationManager.AppSettings["ImportFile"],
        ConfigurationManager.AppSettings["Debug"] == "1",
        ConfigurationManager.AppSettings["WineryID"]);
    }


    /// <summary>
    /// Import orders using data from app.config
    /// </summary>
    static void ImportOrders()
    {
      ImportOrders(
        ConfigurationManager.AppSettings["WSUsername"], 
        ConfigurationManager.AppSettings["WSPassword"],
        ConfigurationManager.AppSettings["ImportFile"],
        ConfigurationManager.AppSettings["OrderNumberPrefix"],
        GetMemberLookupPreferences(),
        ConfigurationManager.AppSettings["Debug"] == "1");
    }


    /// <summary>
    /// Import products using custom parameters
    /// </summary>
    /// <param name="wsUsername"></param>
    /// <param name="wsPassword"></param>
    /// <param name="spreadSheet"></param>
    static void ImportProducts(String wsUsername, String wsPassword, String spreadSheet, bool debug, String WineryIDstr)
    {
      Console.WriteLine("Importing products...");
      string importDirectory = Path.GetDirectoryName(spreadSheet);
      string importTimestamp = DateTime.Now.ToString("yyyyMMdd-HHmmss");
      string pathToLogs = importDirectory + @"\" + importTimestamp + "_ProductWineImportLog" + ".xls";
      ExcelWriter excelWriter = new ExcelWriter(pathToLogs);
      ExcelQueryFactory sheet = new ExcelQueryFactory(spreadSheet);
      ImportRunnerProducts impProducts;
      ImportRunnerWines impWines;
      EWSDataTaskSupport.EWSData.ImportProductWineRequest request;
      List<EWSDataTaskSupport.EWSData.Product> products = new List<EWSDataTaskSupport.EWSData.Product>();
      List<EWSDataTaskSupport.EWSData.Wine> wines = new List<EWSDataTaskSupport.EWSData.Wine>();
      sheet.StrictMapping = true;
      Guid WineryID;


      #region ImportProduct mappings
      sheet.AddMapping<ImportProduct>(p => p.ProductSKU,   "SKU*");
      sheet.AddMapping<ImportProduct>(p => p.ProductName,  "Name*");
      sheet.AddMapping<ImportProduct>(p => p.ShortName,    "Short Name");
      sheet.AddMapping<ImportProduct>(p => p.IsWine,       "Wine? (yes (1) /no (0))*",    DataTaskHelper.DefaultBooleanTransform);
      sheet.AddMapping<ImportProduct>(p => p.IsTaxable,    "Taxable? (yes (1) /no (0))*", DataTaskHelper.DefaultBooleanTransform);
      sheet.AddMapping<ImportProduct>(p => p.Teaser,       "Teaser");
      sheet.AddMapping<ImportProduct>(p => p.Description,  "Description");
      sheet.AddMapping<ImportProduct>(p => p.BottleCount,  "BottleCount", x => String.IsNullOrWhiteSpace(x) ? 1 : int.Parse(x));
      sheet.AddMapping<ImportProduct>(p => p.IsFirstParty, "ProductType", x => String.IsNullOrWhiteSpace(x) || x.StartsWith("first", StringComparison.OrdinalIgnoreCase));
      sheet.AddMapping<ImportProduct>(p => p.ReportGroup,  "Product Report Group");
      sheet.AddMapping<ImportProduct>(p => p.Price,        "Product Price");
      #endregion

      var allProducts =
        from p in sheet.Worksheet<ImportProduct>("Products")
        select p;

      List<AddUpdateThirdPartyProductRequest> tpProductRequests = new List<AddUpdateThirdPartyProductRequest>();
      List<AddUpdateThirdPartyWineRequest> tpWineRequests = new List<AddUpdateThirdPartyWineRequest>();
      List<AddUpdateProductRequest> fpProductRequests = new List<AddUpdateProductRequest>();
      List<AddUpdateWineRequest> fpWineRequests = new List<AddUpdateWineRequest>();

      foreach (ImportProduct impProduct in allProducts)
      {
        if (impProduct.IsWine)
        {
          if (true || impProduct.IsFirstParty)
          {
            #region First party wine
            Wine wine = new Wine();
            wine.BottlesInACase = impProduct.BottleCount;
            wine.Description = impProduct.Description;
            wine.IsActive = false;
            wine.IsTaxable = impProduct.IsTaxable;
            wine.Teaser = impProduct.Teaser;
            wine.WinePricePerBottle = impProduct.Price;
            //wine.WinePricePerCase = impProduct.Price;
            wine.ProductName = impProduct.ProductName;
            wine.ProductSKU = impProduct.ProductSKU;
            wine.ReportGroup = impProduct.ReportGroup;
            //wines.Add(wine);

            AddUpdateWineRequest auwRequest = new AddUpdateWineRequest();
            auwRequest.Username = wsUsername;
            auwRequest.Password = wsPassword;
            auwRequest.Wine = wine;
            fpWineRequests.Add(auwRequest);

            #endregion
          }
          else
          {
            #region Third party wine
            // Third party credentials required
            // Not yet implemented
            #endregion
          }
        }
        else
        {
          if (true || impProduct.IsFirstParty)
          {
            #region First party product
            Product product = new Product();
            product.Description = impProduct.Description;
            product.IsActive = false;
            product.IsTaxable = impProduct.IsTaxable;
            product.Price1 = impProduct.Price;
            product.ProductName = impProduct.ProductName;
            product.ProductSKU = impProduct.ProductSKU;
            product.ShortName = impProduct.ShortName;
            product.Teaser = impProduct.Teaser;
            product.ReportGroup = impProduct.ReportGroup;
            //products.Add(product);

            AddUpdateProductRequest aupRequest = new AddUpdateProductRequest();
            aupRequest.Username = wsUsername;
            aupRequest.Password = wsPassword;
            aupRequest.Product = product;
            fpProductRequests.Add(aupRequest);

            #endregion
          }
          else
          {
            #region Third party product
            // Third party credentials required
            // Not yet implemented
            #endregion
          }
        }
      }

      Console.WriteLine(allProducts.Count() + " Products and Wines found");

      if (!debug)
      {
        #region Insert the products
        int numProcessed = 0;

        Dictionary<String, Guid> insertedProducts = new Dictionary<String, Guid>();
        Dictionary<String, String> failedProducts = new Dictionary<String, String>();

        using (ProductService ps = new ProductServiceHttps())
        {
          ps.Timeout = 900000;

          foreach (AddUpdateProductRequest req in fpProductRequests)
          {
            if (!debug)
            {
              AddUpdateProductWineResponse response = ps.AddUpdateProduct(req);
              if (response.IsSuccessful)
                insertedProducts.Add(req.Product.ProductSKU, response.ProductID);
              else
                failedProducts.Add(req.Product.ProductSKU, response.ErrorMessage);
            }
            ++numProcessed;
            Console.WriteLine(numProcessed);
          }

          foreach (AddUpdateWineRequest req in fpWineRequests)
          {
            if (!debug)
            {
              AddUpdateProductWineResponse response = ps.AddUpdateWine(req);
              if (response.IsSuccessful)
                insertedProducts.Add(req.Wine.ProductSKU, response.ProductID);
              else
                failedProducts.Add(req.Wine.ProductSKU, response.ErrorMessage);
            }
            ++numProcessed;
            Console.WriteLine(numProcessed);
          }
        }
        #endregion

        #region Write results to CSV
        CSVHelper helper = new CSVHelper();
        String path = Environment.CurrentDirectory + Path.DirectorySeparatorChar;
        helper.WriteCSVFile(path + "Successful Products.csv", insertedProducts.Select(rec => new { SKU = rec.Key, ProductID = rec.Value }));
        helper.WriteCSVFile(path + "Failed Products.csv", failedProducts.Select(rec => new { SKU = rec.Key, ProductID = rec.Value }));
        #endregion


        //using (DataServiceHttps ds = new DataServiceHttps())
        //{
        //  if (!Guid.TryParse(WineryIDstr, out WineryID))
        //  {
        //    return;
        //  }

        //  request = new EWSDataTaskSupport.EWSData.ImportProductWineRequest();
        //  request.Username = wsUsername;
        //  request.Password = wsPassword;
        //  request.WineryID = WineryID;
        //  impProducts = new ImportRunnerProducts(request, ds, excelWriter, null, debug, 75);
        //  impProducts.Import(products);

        //  request = new EWSDataTaskSupport.EWSData.ImportProductWineRequest();
        //  request.Username = wsUsername;
        //  request.Password = wsPassword;
        //  request.WineryID = WineryID;
        //  impWines = new ImportRunnerWines(request, ds, excelWriter, null, debug, 75);
        //  impWines.Import(wines);
        //}
      }
    }


    /// <summary>
    /// Import orders using custom parameters
    /// </summary>
    /// <param name="wsUsername"></param>
    /// <param name="wsPassword"></param>
    /// <param name="orderSpreadSheet"></param>
    /// <param name="orderItemSpreadsheet"></param>
    static int ImportOrders(String wsUsername, String wsPassword, String spreadsheet, String orderNumberPrefix, List<String> memberLookupPreferences, bool debug)
    {
      ExcelQueryFactory sheet = new ExcelQueryFactory(spreadsheet);
      sheet.StrictMapping = true;

      #region Order Mappings and Transforms
      sheet.AddMapping<ImportOrder>(o => o.OrderNumber,                 "OrderNumber");
      sheet.AddMapping<ImportOrder>(o => o.MemberID,                    "AltMemberID");
      sheet.AddMapping<ImportOrder>(o => o.BillingBirthdate,            "BillBirthDate", DataTaskHelper.DefaultDateTransform);
      sheet.AddMapping<ImportOrder>(o => o.BillingFirstName,            "BillFirstName");
      sheet.AddMapping<ImportOrder>(o => o.BillingLastName,             "BillLastName");
      sheet.AddMapping<ImportOrder>(o => o.BillingCompany,              "BillCompany");
      sheet.AddMapping<ImportOrder>(o => o.BillingAddress,              "BillAddress");
      sheet.AddMapping<ImportOrder>(o => o.BillingAddress2,             "BillAddress2");
      sheet.AddMapping<ImportOrder>(o => o.BillingCity,                 "BillCity");
      sheet.AddMapping<ImportOrder>(o => o.BillingState,                "BillState");
      sheet.AddMapping<ImportOrder>(o => o.BillingZipcode,              "BillZipCode");
      sheet.AddMapping<ImportOrder>(o => o.BillingPhone,                "BillPhone");
      sheet.AddMapping<ImportOrder>(o => o.BillingEmail,                "BillEmail");
      sheet.AddMapping<ImportOrder>(o => o.ShippingBirthdate,           "ShipBirthDate", DataTaskHelper.DefaultDateTransform);
      sheet.AddMapping<ImportOrder>(o => o.ShippingFirstName,           "ShipFirstName");
      sheet.AddMapping<ImportOrder>(o => o.ShippingLastName,            "ShipLastName");
      sheet.AddMapping<ImportOrder>(o => o.ShippingCompany,             "ShipCompany");
      sheet.AddMapping<ImportOrder>(o => o.ShippingAddress,             "ShipAddress");
      sheet.AddMapping<ImportOrder>(o => o.ShippingAddress2,            "ShipAddress2");
      sheet.AddMapping<ImportOrder>(o => o.ShippingCity,                "ShipCity");
      sheet.AddMapping<ImportOrder>(o => o.ShippingState,               "ShipState");
      sheet.AddMapping<ImportOrder>(o => o.ShippingZipcode,             "ShipZipCode");
      sheet.AddMapping<ImportOrder>(o => o.ShippingPhone,               "ShipPhone");
      sheet.AddMapping<ImportOrder>(o => o.ShippingEmail,               "ShipEmail");
      sheet.AddMapping<ImportOrder>(o => o.GiftMessage,                 "GiftMessage");
      sheet.AddMapping<ImportOrder>(o => o.OrderNote,                   "OrderNotes");
      sheet.AddMapping<ImportOrder>(o => o.Taxes,                       "Taxes", DataTaskHelper.DefaultDecimalTransform);
      sheet.AddMapping<ImportOrder>(o => o.Handling,                    "Handling", DataTaskHelper.DefaultDecimalTransform);
      sheet.AddMapping<ImportOrder>(o => o.Shipping,                    "Shipping", DataTaskHelper.DefaultDecimalTransform);
      sheet.AddMapping<ImportOrder>(o => o.SubTotal,                    "OrderSubTotal", DataTaskHelper.DefaultDecimalTransform);
      sheet.AddMapping<ImportOrder>(o => o.Total,                       "OrderTotal", DataTaskHelper.DefaultDecimalTransform);
      //sheet.AddMapping<ImportOrder>(o => o.CreditCardType,              "CreditCardType");
      //sheet.AddMapping<ImportOrder>(o => o.CreditCardExpirationMonth,   "CreditCardExpirationMonth");
      //sheet.AddMapping<ImportOrder>(o => o.CreditCardExpirationYear,    "CreditCardExpirationYear");
      //sheet.AddMapping<ImportOrder>(o => o.NameonCreditCard,            "NameonCreditCard");
      sheet.AddMapping<ImportOrder>(o => o.OrderDate,                   "OrderDate", DataTaskHelper.DefaultDateTransform);
      //sheet.AddMapping<ImportOrder>(o => o.OrderType,                   "OrderType");
      sheet.AddMapping<ImportOrder>(o => o.OrderStatus,                 "OrderStatus");
      sheet.AddMapping<ImportOrder>(o => o.IsClubOrder,                 "IsClubOrder", DataTaskHelper.DefaultBooleanTransform);
      sheet.AddMapping<ImportOrder>(o => o.IsAdminOrder,                "IsAdminOrder", DataTaskHelper.DefaultBooleanTransform); 
      sheet.AddMapping<ImportOrder>(o => o.IsPosOrder,                  "IsPOSOrder", DataTaskHelper.DefaultBooleanTransform);
      sheet.AddMapping<ImportOrder>(o => o.IsReservationOrder,          "IsReservationOrder", DataTaskHelper.DefaultBooleanTransform);
      sheet.AddMapping<ImportOrder>(o => o.IsMobilePOSOrder,            "IsMobilePOSOrder", DataTaskHelper.DefaultBooleanTransform);
      sheet.AddMapping<ImportOrder>(o => o.IsTelesalesOrder,            "IsTelesalesOrder", DataTaskHelper.DefaultBooleanTransform);

      //TODO: WillCall orders require willcalllocationID. The value is ignored by the API 
      sheet.AddMapping<ImportOrder>(o => o.IsWillCall,                  "IsWillCallOrder", DataTaskHelper.DefaultBooleanTransform);
       //TODO: Can't set pickup flag without first setting willcalllocationID
      sheet.AddMapping<ImportOrder>(o => o.IsPickedUp,                  "IsPickedUp", DataTaskHelper.DefaultBooleanTransform);

      #endregion

      #region Create the Import Orders
      Console.WriteLine("Loading orders...");
      // Put all the orders in a list
      var allOrders =
        (from o in sheet.Worksheet<ImportOrder>("Orders")
         select o).ToList();
      #endregion

      #region Order Item Mappings
      sheet.AddMapping<ImportOrderItem>(oi => oi.OrderNumber, "OrderNumber");
      sheet.AddMapping<ImportOrderItem>(oi => oi.Quantity, "Quantity");
      sheet.AddMapping<ImportOrderItem>(oi => oi.SKU, "SKU");
      sheet.AddMapping<ImportOrderItem>(oi => oi.ProductName, "ProductName");
      sheet.AddMapping<ImportOrderItem>(oi => oi.CustomizedPrice, "SoldPrice");
      sheet.AddMapping<ImportOrderItem>(oi => oi.Price, "RetailPrice");
      sheet.AddMapping<ImportOrderItem>(oi => oi.isWine, "isWine?", DataTaskHelper.DefaultBooleanTransform);
      #endregion

      #region Create the Import Order Items lookup
      Console.WriteLine("Loading order items...");
      // Put all the order items in a lookup
      var allOrderItems =
        (from oi in sheet.Worksheet<ImportOrderItem>("OrderItems")
         select oi).ToLookup(oi => oi.OrderNumberPrefix + oi.OrderNumber, oi => oi);
      #endregion

      int totalOrders = allOrders.Count();
      Console.WriteLine(totalOrders + " Orders found");
      // TODO: Refactor to use EWSCallBase class!
      if (!debug)
      {
        #region Insert the Orders
        using (MemberService memberService = new MemberServiceHttps())
        {
          using (OrderService orderService = new OrderServiceHttps())
          {
            #region Set up AddOrder objects
            orderService.Timeout = 900000;

            AddOrderRequest addOrderRequest = new AddOrderRequest();
            addOrderRequest.Username = wsUsername;
            addOrderRequest.Password = wsPassword;

            AddOrderResponse addOrderResponse;
            #endregion

            Dictionary<String, Guid?> cachedMemberIDs = new Dictionary<String, Guid?>();
            Dictionary<String, Guid> insertedOrders = new Dictionary<String, Guid>();
            Dictionary<String, String> failedOrders = new Dictionary<String, String>();

            int numProcessed = 0;
            foreach (ImportOrder impOrder in allOrders)
            {
              #region Create a web service order
              Order wsOrder = new Order();
              String lookupKey = impOrder.OrderNumberPrefix + impOrder.OrderNumber;

              #region Get the MemberID for this order
              if (!String.IsNullOrWhiteSpace(impOrder.MemberID))
              {
                if (cachedMemberIDs.ContainsKey(impOrder.MemberID))
                {
                  // It was cached, yay!
                  wsOrder.MemberID = cachedMemberIDs[impOrder.MemberID];
                }
                else
                {
                  // Lookup the MemberID for the order
                  Member member = LookupMember(memberService, wsUsername, wsPassword, impOrder, memberLookupPreferences);

                  if (member != null)
                  {
                    // Cache this member and use it for this order
                    cachedMemberIDs.Add(impOrder.MemberID, member.MemberID);
                    wsOrder.MemberID = member.MemberID;
                  }
                }
              }
              #endregion

              wsOrder.OrderNumber               = impOrder.OrderNumber.ToString();
              wsOrder.OrderNumberPrefix         = orderNumberPrefix + impOrder.OrderNumberPrefix;
              wsOrder.BillingBirthDate          = impOrder.BillingBirthdate;
              wsOrder.BillingFirstName          = impOrder.BillingFirstName;
              wsOrder.BillingLastName           = impOrder.BillingLastName;
              wsOrder.BillingCompany            = impOrder.BillingCompany;
              wsOrder.BillingAddress1           = impOrder.BillingAddress;
              wsOrder.BillingAddress2           = impOrder.BillingAddress2;
              wsOrder.BillingCity               = impOrder.BillingCity;
              wsOrder.BillingState              = impOrder.BillingState;
              wsOrder.BillingZipCode            = impOrder.BillingZipcode;
              wsOrder.BillingPhone              = impOrder.BillingPhone;
              wsOrder.BillingEmail              = String.IsNullOrWhiteSpace(impOrder.BillingEmail) ? 
                                                    impOrder.MemberID + "@noemail.com" : 
                                                    impOrder.BillingEmail;
              wsOrder.ShippingBirthDate         = impOrder.ShippingBirthdate;
              wsOrder.ShippingFirstName         = impOrder.ShippingFirstName;
              wsOrder.ShippingLastName          = impOrder.ShippingLastName;
              wsOrder.ShippingAddress1          = impOrder.ShippingAddress;
              wsOrder.ShippingAddress2          = impOrder.ShippingAddress2;
              wsOrder.ShippingCompany           = impOrder.ShippingCompany;
              wsOrder.ShippingCity              = impOrder.ShippingCity;
              wsOrder.ShippingState             = impOrder.ShippingState;
              wsOrder.ShippingZipCode           = impOrder.ShippingZipcode;
              wsOrder.ShippingPhone             = impOrder.ShippingPhone;
              wsOrder.ShippingEmail             = impOrder.ShippingEmail;
              wsOrder.GiftMessage               = impOrder.GiftMessage;
              wsOrder.OrderNotes                = impOrder.OrderNote;
              wsOrder.Taxes                     = impOrder.Taxes;
              wsOrder.TaxableHandling           = impOrder.Handling;
              wsOrder.Shipping                  = impOrder.Shipping;
              //wsOrder.CreditCardType            = impOrder.CreditCardType;
              //wsOrder.CreditCardExpirationMonth = impOrder.CreditCardExpirationMonth;
              //wsOrder.CreditCardExpirationYear  = impOrder.CreditCardExpirationYear;
              //wsOrder.NameOnCreditCard          = impOrder.NameonCreditCard;
              wsOrder.DateCompleted             = impOrder.OrderDate;
              //wsOrder.OrderType                 = impOrder.OrderType;
              wsOrder.SubTotal                  = impOrder.SubTotal;
              //wsOrder.CouponDiscount            = impOrder.OrderDiscount;
              wsOrder.Total                     = impOrder.Total;
              wsOrder.IsWillCall                = impOrder.IsWillCall;
              wsOrder.IsPickedUp                = impOrder.IsPickedUp;
              wsOrder.IsClubOrder               = impOrder.IsClubOrder;
              wsOrder.IsPOSOrder                = impOrder.IsPosOrder;
              wsOrder.IsAdminOrder              = impOrder.IsAdminOrder;
              wsOrder.IsReservation             = impOrder.IsReservationOrder;
              wsOrder.IsMobilePOSOrder          = impOrder.IsMobilePOSOrder;
              wsOrder.IsTeleSalesOrder          = impOrder.IsTelesalesOrder;
              #endregion

              #region Link the order items to the order
              wsOrder.OrderItem = allOrderItems[lookupKey].Select(impItem =>
                new OrderItem()
                {
                  Quantity        = impItem.Quantity,
                  SKU             = impItem.SKU,
                  Name            = impItem.ProductName,
                  Price           = impItem.Price,
                  CustomizedPrice = impItem.CustomizedPrice,
                  BottleCount     = 1,
                  isWine          = impItem.isWine,
                }).ToArray();
              #endregion

              addOrderRequest.Order = wsOrder;

              addOrderResponse = DataTaskHelper.TryNTimes(orderService.AddOrder, addOrderRequest, 10, TimeSpan.FromSeconds(10));

              if (addOrderResponse != null)
              {
                if (addOrderResponse.IsSuccessful)
                {
                  insertedOrders.Add(lookupKey, addOrderResponse.OrderID);
                }
                else
                {
                  if (!failedOrders.ContainsKey(lookupKey))
                    failedOrders.Add(lookupKey, addOrderResponse.ErrorMessage);
                }
              }
              else
              {
                if (!failedOrders.ContainsKey(lookupKey))
                  failedOrders.Add(lookupKey, "Could not connect to web service!");
              }

              ++numProcessed;
              Console.WriteLine(numProcessed);
            }

            #region Write results to CSV
            CSVHelper helper = new CSVHelper();
            String path = Environment.CurrentDirectory + Path.DirectorySeparatorChar;
            helper.WriteCSVFile(path + "Successful Orders.csv", insertedOrders.Select(rec => new { OrderNumber = rec.Key, CartID = rec.Value }));
            helper.WriteCSVFile(path + "Failed Orders.csv", failedOrders.Select(rec => new { OrderNumber = rec.Key, CartID = rec.Value }));
            #endregion
          }
        }
        #endregion
      }

      return totalOrders;
    }


    /// <summary>
    /// Lookup a member using an ImportOrder that contains member data
    /// </summary>
    /// <param name="ms"></param>
    /// <param name="wsUsername"></param>
    /// <param name="wsPassword"></param>
    /// <param name="impOrder"></param>
    /// <returns></returns>
    public static Member LookupMember(MemberService ms, String wsUsername, String wsPassword, ImportOrder impOrder, List<String> memberLookupPreferences)
    {
      foreach (String criteria in memberLookupPreferences)
      {
        GetMemberRequest gmRequest = new GetMemberRequest()
        {
          Username = wsUsername,
          Password = wsPassword
        };

        switch (criteria.ToUpper())
        {
          case "SEARCHBYALTMEMBERID":
            gmRequest.AltMemberID = impOrder.MemberID;
            break;

          case "SEARCHBYMEMBERNUMBER":
            gmRequest.MemberNumber = int.Parse(impOrder.MemberID);
            break;

          case "SEARCHBYEMAIL":
            gmRequest.Email = impOrder.BillingEmail;
            break;

          case "SEARCHBYNAME":
            gmRequest.FirstName = impOrder.BillingFirstName;
            gmRequest.LastName = impOrder.BillingLastName;
            break;

          default:
            throw new Exception("\"" + criteria + "\" is not a valid member lookup option");
        }

        GetMemberResponse gmResponse = DataTaskHelper.TryNTimes(ms.GetMember, gmRequest, 10, TimeSpan.FromSeconds(10));

        if (gmResponse != null)
        {
          if (gmResponse.IsSuccessful && gmResponse.Members.Length == 1)
            return gmResponse.Members[0];
        }
      }

      return null;
    }


    /// <summary>
    /// Read in the member lookup preferences from app.config
    /// </summary>
    /// <returns></returns>
    private static List<String> GetMemberLookupPreferences()
    {
      // Get the "search by" settings from app.config
      var searchSettings =
        from key in ConfigurationManager.AppSettings.AllKeys
        where key.StartsWith("SearchBy")
        select new
        {
          Criteria = key,
          Order = int.Parse(ConfigurationManager.AppSettings[key])
        };

      // Sort the settings by order
      var memberLookupPreferences =
        from s in searchSettings
        where s.Order > 0
        orderby s.Order, s.Criteria
        select s.Criteria;

      // Return the ordered settings as a list
      return memberLookupPreferences.ToList();
    }
  }
}

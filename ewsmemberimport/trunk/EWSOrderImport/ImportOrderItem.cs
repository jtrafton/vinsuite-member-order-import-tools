﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EWSOrderImport
{
  class ImportOrderItem
  {
    public int      OrderNumber         { get; set; }
    public int      Quantity            { get; set; }
    public String   SKU                 { get; set; }
    public String   ProductName         { get; set; }  
    public Decimal? Price               { get; set; } //retail price
    public Decimal? CustomizedPrice     { get; set; } //paid price
    public String OrderNumberPrefix     { get; set; }
    public bool isWine                  { get; set; }
    public ImportOrderItem()
    { }
  }
}

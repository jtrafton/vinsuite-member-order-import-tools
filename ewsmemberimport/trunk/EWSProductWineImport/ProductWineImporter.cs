﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using EWSDataTaskSupport;
using EWSDataTaskSupport.EWSData;
using System.Threading;

namespace EWSProductWineImport
{
  public class ProductWineImporter
  {
    private bool debug;
    private int recordsPerRound;
    private int? dataTaskID;
    private String importFile;
    private String adminEmail;
    private String adminWSUsername;
    private String adminWSPassword;
    private String importDirectory;
    private String importTimestamp;
    private String pathToLogs;
    private String productSheetName;
    private String wineSheetName;
    private DataServiceHttps ds;
    private GetDataCredentialsResponse dataCredentials;
    private ExcelWriter excelWriter;
    private ProductWineGetter pwGetter;
    private List<ErrorReportProduct> errors;
    private List<Product> products;
    private List<Wine> wines;
    private Guid WineryID;

    /// <summary>
    /// Construct Member Importer Object
    /// </summary>
    /// <param name="debug">true performs validation only</param>
    /// <param name="importFile">the file containing product/wine data</param>
    /// <param name="RecordsPerRound">how many records to import in each iteration</param>
    /// <param name="adminEmail">who to notify when the task is complete</param>
    /// <param name="adminWSUsername">web service credential</param>
    /// <param name="adminWSPassword">web service credential</param>
    /// <param name="wineryID">which winery to import into</param>
    /// <param name="dataTaskID">optional data task tracking identifier</param>
    public ProductWineImporter(bool debug, String importFile, int RecordsPerRound,
      String adminEmail, String adminWSUsername, String adminWSPassword, Guid wineryID, int? dataTaskID)
    {
      // Allow self-signed certs.
      System.Net.ServicePointManager.ServerCertificateValidationCallback += (se, cert, chain, sslerror) => true;
      
      this.debug                = debug;
      this.importFile           = importFile;
      this.recordsPerRound      = RecordsPerRound;
      this.adminEmail           = adminEmail;
      this.adminWSUsername      = adminWSUsername;
      this.adminWSPassword      = adminWSPassword;
      this.dataTaskID           = dataTaskID;
      this.errors               = new List<ErrorReportProduct>();
      this.ds                   = new DataServiceHttps();
      this.ds.Timeout           = 30000;
      this.productSheetName     = "ProductsExtractTemplate";
      this.wineSheetName        = "WinesExtractTemplate";
      this.WineryID             = wineryID;

      // Get data Credentials
      DataRequest credentialsRequest = new DataRequest()
      {
        Username = adminWSUsername,
        Password = adminWSPassword,
        WineryID = this.WineryID
      };

      dataCredentials = ds.GetDataCredentials(credentialsRequest);
      if (String.IsNullOrEmpty(dataCredentials.Username) || String.IsNullOrEmpty(dataCredentials.Password))
        errors.Add(new ErrorReportProduct("Failed to get data credentials", "Make sure admin credentials are valid", "Data Credentials Getter", ErrorReportProduct.ERRORTYPE.CRITICAL));

      importDirectory = Path.GetDirectoryName(importFile);
      importTimestamp = DateTime.Now.ToString("yyyyMMdd-HHmmss");
      this.pathToLogs = importDirectory + @"\" + importTimestamp + "_ProductWineImportLog" + ".xls";
      excelWriter = new ExcelWriter(this.pathToLogs);

      pwGetter = new ProductWineGetter(importFile, this.productSheetName, this.wineSheetName, dataCredentials.Username, dataCredentials.Password, debug);
    }


    /// <summary>
    /// Destructor
    /// </summary>
    ~ProductWineImporter()
    {
      ds.Dispose();
    }


    /// <summary>
    /// Load and validate product import spreadsheet
    /// </summary>
    public void LoadAndValidateProducts()
    {
      products = pwGetter.GetProducts();
      // Eliminate duplicate errors from the getter.
      // These duplicates exist because all errors are stored in a single list for simplicity
      var hash = new HashSet<ErrorReportProduct>(this.errors);
      hash.UnionWith(pwGetter.GetErrors());
      this.errors = hash.ToList();

      if (products == null)
        errors.Add(new ErrorReportProduct(
          "No products were found in the spreadsheet.", 
          ErrorReportProduct.FixTheFile, 
          "Spreadsheet loader and validator",
          ErrorReportProduct.ERRORTYPE.CRITICAL));
    }


    /// <summary>
    /// Load and validate wine import spreadsheet
    /// </summary>
    public void LoadAndValidateWines()
    {
      wines = pwGetter.GetWines();
      // Eliminate duplicate errors from the getter.
      // These duplicates exist because all errors are stored in a single list for simplicity
      var hash = new HashSet<ErrorReportProduct>(this.errors);
      hash.UnionWith(pwGetter.GetErrors());
      this.errors = hash.ToList();

      if (wines == null)
        errors.Add(new ErrorReportProduct(
          "No wines were found in the spreadsheet.",
          ErrorReportProduct.FixTheFile, 
          "Spreadsheet loader and validator",
          ErrorReportProduct.ERRORTYPE.CRITICAL));
    }


    /// <summary>
    /// Get import errors.
    /// </summary>
    /// <returns>List of errors based on search criteria</returns>
    public List<ErrorReportProduct> GetErrors()
    {
      var errs = this.errors;
      return errs;
    }


    /// <summary>
    /// Get product count
    /// </summary>
    /// <returns></returns>
    public int GetProductCount()
    {
      return this.products == null ? -1 : products.Count;
    }


    /// <summary>
    /// Get wine count
    /// </summary>
    /// <param name="thirdParty">Count criteria. NULL: all wines, TRUE: 3rd party only, FALSE: 1st party only</param>
    /// <returns></returns>
    public int GetWineCount(bool? thirdParty)
    {
      if (this.wines == null)
        return -1;

      if (thirdParty.HasValue)
        return this.wines.Where(w => w.IsThirdParty == thirdParty).Count();
      else
        return this.wines.Count;

    }

    /// <summary>
    /// Import products/wines into EWS Database
    /// </summary>
    /// <returns>True if import process has started, False if it didn't</returns>
    public bool RunImport()
    {
      UpdateDataTaskRequest updTaskRequest;
      GetDataTasksRequest getTaskRequest;
      ImportProductWineRequest importPWRequest;
      ImportRunnerProducts impProducts;
      ImportRunnerWines impWines;
      ErrorReportProduct.PRODUCTTYPE type;

      #region Setup requests
      updTaskRequest = new UpdateDataTaskRequest()
      {
        Username = adminWSUsername,
        Password = adminWSPassword,
        UpdateType = ProgressUpdateType.INCREMENT_PROGRESS,
        DataTask = new DataTask_Update()
        {
          DataTaskTrackingID = dataTaskID ?? 0
        }
      };

      getTaskRequest = new GetDataTasksRequest()
      {
        Username = adminWSUsername,
        Password = adminWSPassword,
        DataTaskTrackingID = dataTaskID
      };
      #endregion

      #region Run Import against various collections
      if (this.products != null)
      {
        Console.WriteLine("Products:");
        importPWRequest = new ImportProductWineRequest();
        InitRequest(importPWRequest);
        impProducts = new ImportRunnerProducts(importPWRequest, ds, excelWriter, updTaskRequest, debug, recordsPerRound);
        errors.AddRange(impProducts.Import(this.products));
      }

      if (this.wines != null)
      {
        //tpWines = this.wines.Where(w => w.ThirdPartySupplierID.HasValue).ToList();
        //fpWines = this.wines.Except(tpWines).Cast<Wine>().ToList();

        //// Import Third Party Wines
        //Console.WriteLine("Third Party Wines:");
        //importPWRequest = new ImportProductWineRequest();
        //InitRequest(importPWRequest);
        //impTPWines = new ImportRunnerTPWines(importPWRequest, ds, excelWriter, updTaskRequest, debug, recordsPerRound);
        //errors.AddRange(impTPWines.Import(tpWines));

        // Import First Party Wines
        Console.WriteLine("Wines:");
        importPWRequest = new ImportProductWineRequest();
        InitRequest(importPWRequest);
        impWines = new ImportRunnerWines(importPWRequest, ds, excelWriter, updTaskRequest, debug, recordsPerRound);
        errors.AddRange(impWines.Import(wines));
      }
      #endregion

      #region Write logs to disk
      Console.WriteLine("Writing log");
      // categorize errors
      type = ErrorReportProduct.PRODUCTTYPE.GENERAL;
      var generalErrors  = errors.Where(e => e.ProductType.HasFlag(type)).ToArray();
      var criticalErrors = errors.Where(e => e.ErrorType() == ErrorReportMember.ERRORTYPE.CRITICAL).ToArray();
      var exceptions     = errors.Where(e => e.ErrorType() == ErrorReportMember.ERRORTYPE.EXCEPTION).ToArray();
      var warnings       = errors.Where(e => e.ErrorType() == ErrorReportMember.ERRORTYPE.WARNING).ToArray();

      // write general errors and exceptions
      excelWriter.WriteToXLS(generalErrors.ToArray(), "General Errors");

      // write products log
      if (this.products != null)
      {
        type = ErrorReportProduct.PRODUCTTYPE.PRODUCT;
        excelWriter.WriteToXLS(criticalErrors.Where(ce => ce.ProductType.HasFlag(type))
          .OrderBy(e => e.RecordID).ToArray(), "Product Errors");
        excelWriter.WriteToXLS(warnings.Where(ce => ce.ProductType.HasFlag(type))
          .OrderBy(e => e.RecordID).ToArray(), "Product Warnings");
        excelWriter.WriteToXLS(pwGetter.GetSpreadsheetProducts(criticalErrors.Where(ce => ce.ProductType.HasFlag(type))
          .OrderBy(e => e.RecordID).ToArray()), "Products");
        excelWriter.WriteToXLS(pwGetter.GetSpreadsheetProducts(exceptions.Where(ce => ce.ProductType.HasFlag(type))
          .OrderBy(e => e.RecordID).ToArray()), "Product Exceptions");
      }

      // write wines log
      if (this.wines != null)
      {
        type = ErrorReportProduct.PRODUCTTYPE.WINE;
        excelWriter.WriteToXLS(criticalErrors.Where(ce => ce.ProductType.HasFlag(type))
          .OrderBy(e => e.RecordID).ToArray(), "Wine Errors");
        excelWriter.WriteToXLS(warnings.Where(ce => ce.ProductType.HasFlag(type))
          .OrderBy(e => e.RecordID).ToArray(), "Wine Warnings");
        excelWriter.WriteToXLS(pwGetter.GetSpreadsheetWines(criticalErrors.Where(ce => ce.ProductType.HasFlag(type))
          .OrderBy(e => e.RecordID).ToArray()), "Wines");
        excelWriter.WriteToXLS(pwGetter.GetSpreadsheetWines(exceptions.Where(ce => ce.ProductType.HasFlag(type))
          .OrderBy(e => e.RecordID).ToArray()), "Wine Exceptions");
      }
      #endregion

      return true;
    }


    /// <summary>
    /// Get filename and path to import log
    /// </summary>
    /// <returns></returns>
    public String GetLogPath()
    {
      return this.pathToLogs;
    }


    /// <summary>
    /// Run Import on the background
    /// </summary>
    public void RunImportAsync()
    {
      ThreadPool.QueueUserWorkItem(new WaitCallback(RunImportAsync), null);
    }


    /// <summary>
    /// Entry point for a background thread
    /// </summary>
    protected void RunImportAsync(object ImportPackage)
    {
      RunImport();
    }


    /// <summary>
    /// Init request credentials etc
    /// </summary>
    /// <param name="request"></param>
    private void InitRequest(DataRequest request)
    {
      request.Username = adminWSUsername;
      request.Password = adminWSPassword;
      request.WineryID = this.WineryID;
      request.EmailTo = adminEmail;
    }
  }
}

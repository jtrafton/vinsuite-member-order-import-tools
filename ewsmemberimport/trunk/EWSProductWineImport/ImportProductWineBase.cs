﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EWSProductWineImport
{
  /// <summary>
  /// Properties shared by both Products First Party and Third Party Wines
  /// </summary>
  abstract class ImportProductWineBase
  {
    // This is set by the web services. Not needed?
    public String WineryName           { get; set; }

    public String MarketingURL         { get; set; }
    public String MemberTeaser         { get; set; }
    public String Teaser               { get; set; }
    public String NonMemberTeaser      { get; set; }
    public String MetaTagDescription   { get; set; }
    public String MetaTagKeywords      { get; set; }
    public String MetaTagTitle         { get; set; }
    public String Picture              { get; set; }
    public String PhotoPath            { get; set; }

    public int? InventoryCount         { get; set; }

    public Boolean? IsActive           { get; set; }
    public Boolean? IsFavorite         { get; set; }
    public Boolean? IsForExport        { get; set; }
    public Boolean? IsShownOnAddOrder  { get; set; }
    public Boolean? IsTaxable          { get; set; }
    public Boolean? OverrideCompliance { get; set; }

    internal String Errors { get; set; }
  }
}

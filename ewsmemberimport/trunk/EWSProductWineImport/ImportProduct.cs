﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EWSProductWineImport
{
  /// <summary>
  /// Product specific properties.
  /// </summary>
  class ImportProduct : ImportProductWineBase
  {
    public Guid? ProductKeyID     { get; set; }
    public String ProductSKU      { get; set; }
    public String ProductName     { get; set; }
    public String Description     { get; set; }
    public DateTime? DateAdded    { get; set; }
    public DateTime? DateModified { get; set; }
    public Decimal? Price1 { get; set; }
    public Decimal? Price2 { get; set; }
  }
}

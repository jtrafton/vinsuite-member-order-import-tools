﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EWSProductWineImport
{
  class ImportWine : ImportProductWineBase
  {
    #region 3rd Party Specific properties
    public Decimal? CostPerBottle            { get; set; }
    public Decimal? SuggestedPricePerBottle  { get; set; }
    public Decimal? CostPerCase              { get; set; }
    public Decimal? SuggestedPricePerCase    { get; set; }
    #endregion

    #region Shared by both 1st and 3rd party    
    public Guid? ProductKeyID                { get; set; }

    //public Boolean? IsThirdParty             { get; set; }
    public Boolean? IsActiveBySupplier       { get; set; }
    public Boolean? IsWineStartPage          { get; set; }
    public Boolean? IsSellAsBottle           { get; set; }
    public Boolean? IsSellAsCase             { get; set; }

    public DateTime? BottlingDate            { get; set; }

    public int? MaxBottlesPerOrder           { get; set; }
    public int? MinBottlesPerOrder           { get; set; }
    public int? BottlesInACase               { get; set; }
    public int? Vintage                      { get; set; }

    public Decimal? Weight                   { get; set; }
    public Decimal? WineSalePricePerBottle   { get; set; }
    public Decimal? WineSalePricePerCase     { get; set; }
    public Decimal? WinePricePerBottle       { get; set; }
    public Decimal? WinePricePerCase         { get; set; }

    public String WineSKU                    { get; set; }
    public String WineName                   { get; set; }
    public String WineDescription            { get; set; }
    public String Ageability                 { get; set; }
    public String Aging                      { get; set; }
    public String AlcoholPercentage          { get; set; }
    public String Awards                     { get; set; }
    public String Fermentation               { get; set; }
    public String FoodPairing                { get; set; }
    public String HarvestDate                { get; set; }
    public String OtherNotes                 { get; set; }
    public String PH                         { get; set; }
    public String ProductionNotes            { get; set; }
    public String Production                 { get; set; }
    public String ShortName                  { get; set; }
    public String Sugar                      { get; set; }
    public String Tannin                     { get; set; }
    public String TastingNotes               { get; set; }
    public String VineyardDesignation        { get; set; }
    public String VineyardNotes              { get; set; }
    public String WineAppellation            { get; set; }
    public String WineBottleSize             { get; set; }
    public String WineBrand                  { get; set; }
    public String WineRegion                 { get; set; }
    public String WineMakerNotes             { get; set; }
    public String WineType                   { get; set; }
    public String WineVarietal               { get; set; }

    //public Guid? ThirdPartySupplierID { get; set; }
    public String ThirdPartySupplier { get; set; }

    #endregion
  }
}

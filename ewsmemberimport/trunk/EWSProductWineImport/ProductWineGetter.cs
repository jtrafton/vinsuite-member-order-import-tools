﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EWSDataTaskSupport;
using LinqToExcel;
using EWSDataTaskSupport.EWSData;
using System.Reflection;

namespace EWSProductWineImport
{
  class ProductWineGetter
  {
    private String importFile;
    private String wsUsername;
    private String wsPassword;
    private bool debug;
    private List<ErrorReportProduct> errors;
    private String productSheetName;
    private String wineSheetName;
    private IEnumerable<ImportProduct> xlsProducts;
    private IEnumerable<ImportWine> xlsWines;
    private List<Product> products;
    private List<Wine> wines;

    /// <summary>
    /// Initialize product wine getter
    /// </summary>
    /// <param name="importFile">Path to import spreadsheet</param>
    /// <param name="productSheetName">Name of worksheet containing products</param>
    /// <param name="wineSheetName">Name of worksheet containing 1st/3rd party wines</param>
    /// <param name="username">Datatask username</param>
    /// <param name="password">Datatask password</param>
    /// <param name="debug">Debug switch</param>
    /// <param name="importType">Indicate what type of products to import</param>
    public ProductWineGetter(String importFile, String productSheetName, String wineSheetName, String username, String password, bool debug)
    {
      this.importFile       = importFile;
      this.productSheetName = productSheetName;
      this.wineSheetName    = wineSheetName;
      this.wsUsername       = username;
      this.wsPassword       = password;
      this.debug            = debug;
      this.errors           = new List<ErrorReportProduct>();
      this.products         = new List<Product>();
      this.wines            = new List<Wine>();
    }


    /// <summary>
    /// Get list of products from the spreadsheet
    /// </summary>
    /// <returns></returns>
    public List<Product> GetProducts()
    {
      ExcelQueryFactory spreadsheet;
      Product product;
      #region Init
      spreadsheet = GetSpreadsheet<ImportProduct>(this.productSheetName);
      if (spreadsheet == null)
        return null;
      #endregion

      #region Mappings and Transforms
      spreadsheet.AddTransformation<ImportProduct>(p => p.ProductKeyID,       DataTaskHelper.DefaultGuidTransform);
      // boolean transforms
      spreadsheet.AddTransformation<ImportProduct>(p => p.IsActive,           DataTaskHelper.DefaultBooleanTransform);
      spreadsheet.AddTransformation<ImportProduct>(p => p.IsFavorite,         DataTaskHelper.DefaultBooleanTransform);
      spreadsheet.AddTransformation<ImportProduct>(p => p.IsForExport,        DataTaskHelper.DefaultBooleanTransform);
      spreadsheet.AddTransformation<ImportProduct>(p => p.IsShownOnAddOrder,  DataTaskHelper.DefaultBooleanTransform);
      spreadsheet.AddTransformation<ImportProduct>(p => p.IsTaxable,          DataTaskHelper.DefaultBooleanTransform);
      spreadsheet.AddTransformation<ImportProduct>(p => p.OverrideCompliance, DataTaskHelper.DefaultBooleanTransform);
      // numeric transforms
      spreadsheet.AddTransformation<ImportProduct>(p => p.InventoryCount,     DataTaskHelper.DefaultIntTransform);
      spreadsheet.AddTransformation<ImportProduct>(p => p.Price1,             DataTaskHelper.DefaultDecimalTransform);
      spreadsheet.AddTransformation<ImportProduct>(p => p.Price2,             DataTaskHelper.DefaultDecimalTransform);
      /*
      spreadsheet.AddTransformation<ImportProduct>(p => p.SalePrice1,         DataTaskHelper.DefaultDecimalTransform);
      spreadsheet.AddTransformation<ImportProduct>(p => p.SalePrice2,         DataTaskHelper.DefaultDecimalTransform);
      */       

      // datetime transforms
      spreadsheet.AddTransformation<ImportProduct>(p => p.DateAdded,          DataTaskHelper.DefaultDateTransform);
      spreadsheet.AddTransformation<ImportProduct>(p => p.DateModified,       DataTaskHelper.DefaultDateTransform);
      #endregion

      this.xlsProducts = spreadsheet.Worksheet<ImportProduct>(this.productSheetName).ToList();

      foreach (ImportProduct importProduct in this.xlsProducts)
      {
        product = new Product();
        product.ProductID          = importProduct.ProductKeyID;
        product.ProductSKU         = importProduct.ProductSKU;
        product.ProductName        = importProduct.ProductName;
        product.Description        = importProduct.Description;
        product.MarketingURL       = importProduct.MarketingURL;
        product.MemberTeaser       = importProduct.MemberTeaser;
        product.Teaser             = importProduct.Teaser;
        product.NonMemberTeaser    = importProduct.NonMemberTeaser;
        product.MetaTagDescription = importProduct.MetaTagDescription;
        product.MetaTagKeywords    = importProduct.MetaTagKeywords;
        product.MetaTagTitle       = importProduct.MetaTagTitle;
        product.Picture            = importProduct.Picture;
        product.PhotoPath          = importProduct.PhotoPath;
        product.NumInStock         = importProduct.InventoryCount;
        product.IsActive           = importProduct.IsActive;
        product.IsFavorite         = importProduct.IsFavorite;
        product.IsForExport        = importProduct.IsForExport;
        product.IsShownOnAddOrder  = importProduct.IsShownOnAddOrder;
        product.IsTaxable          = importProduct.IsTaxable;
        product.DateAdded          = importProduct.DateAdded;
        product.DateModified       = importProduct.DateModified;
        product.Price1             = importProduct.Price1;
        product.Price2             = importProduct.Price2;

        if (!product.ProductID.HasValue)
        {
          // Both ProductKeyID and SKU must be present in the spreadsheet
          if (String.IsNullOrWhiteSpace(product.ProductSKU))
          {
            errors.Add(new ErrorReportProduct(
              product,
              "Product is missing ProductKeyID and ProductSKU",
              ErrorReportBase.FixTheFile,
              "Product Getter",
              ErrorReportBase.ERRORTYPE.CRITICAL,
              ErrorReportProduct.PRODUCTTYPE.PRODUCT));
            continue;
          }

          // New products (without ProductKeyID in the spreadsheet) must have unique SKUs
          if (products.Exists(p => String.Equals(p.ProductSKU, importProduct.ProductSKU, StringComparison.InvariantCultureIgnoreCase)))
          {
            errors.Add(new ErrorReportProduct(
              product,
              "Product with the same SKU already exists in the spreadsheet. Skipping",
              ErrorReportBase.FixTheFile,
              "ProductGetter",
              ErrorReportBase.ERRORTYPE.CRITICAL,
              ErrorReportProduct.PRODUCTTYPE.PRODUCT));
            continue;
          }
        }

        DataTaskHelper.CorrectBadChars(product, false);
        products.Add(product);
      }
      return this.products;
    }


    /// <summary>
    /// Get list of wines from the spreadsheet
    /// </summary>
    /// <returns></returns>
    public List<Wine> GetWines()
    {
      ExcelQueryFactory spreadsheet;
      #region Init
      spreadsheet = GetSpreadsheet<ImportWine>(this.wineSheetName);
      if (spreadsheet == null)
        return null;
      #endregion

      #region Mappings and Transforms
      spreadsheet.AddTransformation<ImportWine>(w => w.ProductKeyID,           DataTaskHelper.DefaultGuidTransform);
      //spreadsheet.AddTransformation<ImportWine>(w => w.ThirdPartySupplierID,   DataTaskHelper.DefaultGuidTransform);
      //spreadsheet.AddTransformation<ImportWine>(w => w.ThirdPartyWineID,       DataTaskHelper.DefaultGuidTransform);

      // boolean transforms                          
      spreadsheet.AddTransformation<ImportWine>(w => w.IsActive,               DataTaskHelper.DefaultBooleanTransform);
      spreadsheet.AddTransformation<ImportWine>(w => w.IsFavorite,             DataTaskHelper.DefaultBooleanTransform);
      spreadsheet.AddTransformation<ImportWine>(w => w.IsForExport,            DataTaskHelper.DefaultBooleanTransform);
      spreadsheet.AddTransformation<ImportWine>(w => w.IsShownOnAddOrder,      DataTaskHelper.DefaultBooleanTransform);
      spreadsheet.AddTransformation<ImportWine>(w => w.IsTaxable,              DataTaskHelper.DefaultBooleanTransform);
      spreadsheet.AddTransformation<ImportWine>(w => w.IsActiveBySupplier,     DataTaskHelper.DefaultBooleanTransform);
      spreadsheet.AddTransformation<ImportWine>(w => w.IsSellAsBottle,         DataTaskHelper.DefaultBooleanTransform);
      spreadsheet.AddTransformation<ImportWine>(w => w.IsSellAsCase,           DataTaskHelper.DefaultBooleanTransform);
      spreadsheet.AddTransformation<ImportWine>(w => w.IsWineStartPage,        DataTaskHelper.DefaultBooleanTransform);
      spreadsheet.AddTransformation<ImportWine>(w => w.OverrideCompliance,     DataTaskHelper.DefaultBooleanTransform);

      // numeric transforms
      spreadsheet.AddTransformation<ImportWine>(w => w.InventoryCount,         DataTaskHelper.DefaultIntTransform);
      spreadsheet.AddTransformation<ImportWine>(w => w.Vintage,                DataTaskHelper.DefaultIntTransform);
      spreadsheet.AddTransformation<ImportWine>(w => w.MaxBottlesPerOrder,     DataTaskHelper.DefaultIntTransform);
      spreadsheet.AddTransformation<ImportWine>(w => w.MinBottlesPerOrder,     DataTaskHelper.DefaultIntTransform);
      //spreadsheet.AddTransformation<ImportWine>(w => w.SalePrice1,             DataTaskHelper.DefaultDecimalTransform);
      //spreadsheet.AddTransformation<ImportWine>(w => w.SalePrice2,             DataTaskHelper.DefaultDecimalTransform);
      //spreadsheet.AddTransformation<ImportWine>(w => w.Price1,                 DataTaskHelper.DefaultDecimalTransform);
      //spreadsheet.AddTransformation<ImportWine>(w => w.PricePerBottle,         DataTaskHelper.DefaultDecimalTransform);
      //spreadsheet.AddTransformation<ImportWine>(w => w.PricePerCase,           DataTaskHelper.DefaultDecimalTransform);
      //spreadsheet.AddTransformation<ImportWine>(w => w.Price2,                 DataTaskHelper.DefaultDecimalTransform);
      spreadsheet.AddTransformation<ImportWine>(w => w.WinePricePerBottle,     DataTaskHelper.DefaultDecimalTransform);
      spreadsheet.AddTransformation<ImportWine>(w => w.WineSalePricePerBottle, DataTaskHelper.DefaultDecimalTransform);
      spreadsheet.AddTransformation<ImportWine>(w => w.WinePricePerCase,       DataTaskHelper.DefaultDecimalTransform);
      spreadsheet.AddTransformation<ImportWine>(w => w.WineSalePricePerCase,   DataTaskHelper.DefaultDecimalTransform);
      spreadsheet.AddTransformation<ImportWine>(w => w.Weight,                 DataTaskHelper.DefaultDecimalTransform);
      spreadsheet.AddTransformation<ImportWine>(w => w.CostPerBottle,          DataTaskHelper.DefaultDecimalTransform);
      spreadsheet.AddTransformation<ImportWine>(w => w.CostPerCase,            DataTaskHelper.DefaultDecimalTransform);

      // datetime transforms
      spreadsheet.AddTransformation<ImportWine>(w => w.BottlingDate,           DataTaskHelper.DefaultDateTransform);
      spreadsheet.AddTransformation<ImportWine>(w => w.HarvestDate,            DataTaskHelper.DefaultDateTransform);
      #endregion

      this.xlsWines = spreadsheet.Worksheet<ImportWine>(this.wineSheetName).ToList();

      foreach (ImportWine importWine in this.xlsWines)
      {
        Wine wine                   = new Wine();
        wine.CostPerBottle          = importWine.CostPerBottle;
        wine.CostPerCase            = importWine.CostPerCase;
        wine.ProductID              = importWine.ProductKeyID;
        wine.ProductName            = importWine.WineName;
        wine.ProductSKU             = importWine.WineSKU;
        wine.Ageability             = importWine.Ageability;
        wine.Aging                  = importWine.Aging;
        wine.AlcoholPercentage      = importWine.AlcoholPercentage;
        wine.Awards                 = importWine.Awards;
        wine.BottlesInACase         = importWine.BottlesInACase;
        wine.BottlingDate           = importWine.BottlingDate.ToString();
        wine.Fermentation           = importWine.Fermentation;
        wine.FoodPairing            = importWine.FoodPairing;
        wine.HarvestDate            = importWine.HarvestDate;
        wine.NumInStock             = importWine.InventoryCount;
        wine.MaxBottlesPerOrder     = importWine.MaxBottlesPerOrder;
        wine.MinBottlesPerOrder     = importWine.MinBottlesPerOrder;
        wine.OtherNotes             = importWine.OtherNotes;
        wine.PH                     = importWine.PH;        
        wine.ProductionNotes        = importWine.ProductionNotes;
        wine.Production             = importWine.Production;
        wine.ShortName              = importWine.ShortName;
        wine.Sugar                  = importWine.Sugar;
        wine.Tannin                 = importWine.Tannin;
        wine.TastingNotes           = importWine.TastingNotes;
        wine.VineyardDesignation    = importWine.VineyardDesignation;
        wine.VineyardNotes          = importWine.VineyardNotes;
        wine.Vintage                = importWine.Vintage;
        wine.Weight                 = importWine.Weight;
        wine.WineAppellation        = importWine.WineAppellation;
        wine.WineBottleSize         = importWine.WineBottleSize;
        wine.WineBrand              = importWine.WineBrand;
        wine.WineDescription        = importWine.WineDescription;
        wine.WineRegion             = importWine.WineRegion;
        wine.WineMakerNotes         = importWine.WineMakerNotes;
        wine.WineSalePricePerBottle = importWine.WineSalePricePerBottle;
        wine.WineSalePricePerCase   = importWine.WineSalePricePerCase;
        wine.WineType               = importWine.WineType;
        wine.WineVarietal           = importWine.WineVarietal;
        wine.WinePricePerBottle     = importWine.WinePricePerBottle;
        wine.WinePricePerCase       = importWine.WinePricePerCase;
        wine.MarketingURL           = importWine.MarketingURL;
        wine.MemberTeaser           = importWine.MemberTeaser;
        wine.Teaser                 = importWine.Teaser;
        wine.NonMemberTeaser        = importWine.NonMemberTeaser;
        wine.MetaTagDescription     = importWine.MetaTagDescription;
        wine.MetaTagKeywords        = importWine.MetaTagKeywords;
        wine.MetaTagTitle           = importWine.MetaTagTitle;
        wine.Picture                = importWine.Picture;
        wine.PhotoPath              = importWine.PhotoPath;
        wine.IsActive               = importWine.IsActive;
        wine.IsFavorite             = importWine.IsFavorite;
        wine.IsForExport            = importWine.IsForExport;
        wine.IsSellAsBottle         = importWine.IsSellAsBottle;
        wine.IsSellAsCase           = importWine.IsSellAsCase;
        wine.IsShownOnAddOrder      = importWine.IsShownOnAddOrder;
        wine.IsTaxable              = importWine.IsTaxable;
        wine.IsThirdParty           = !String.IsNullOrWhiteSpace(importWine.ThirdPartySupplier) ? true : false;
        wine.IsWineStartPage        = importWine.IsWineStartPage;
        wine.ThirdPartySupplier     = importWine.ThirdPartySupplier;
        //wine.ThirdPartySupplierID   = importWine.ThirdPartySupplierID;

        //if (importWine.IsThirdParty.HasValue && importWine.IsThirdParty.Value && !importWine.ThirdPartySupplierID.HasValue)
        //{
        //  errors.Add(new ErrorReportProduct(
        //    wine,
        //    "Third Party Wine is missing a valid ThirdPartySupplierID",
        //    ErrorReportBase.FixTheFile,
        //    "Wine Getter",
        //    ErrorReportBase.ERRORTYPE.CRITICAL,
        //    ErrorReportProduct.PRODUCTTYPE.WINE));
        //  continue;
        //}

        if (!wine.ProductID.HasValue)
        {
          // Both ProductKeyID and SKU must be present in the spreadsheet
          if (String.IsNullOrWhiteSpace(wine.ProductSKU))
          {
            errors.Add(new ErrorReportProduct(
              wine,
              "Wine is missing ProductKeyID and WineSKU",
              ErrorReportBase.FixTheFile,
              "Wine Getter",
              ErrorReportBase.ERRORTYPE.CRITICAL,
              ErrorReportProduct.PRODUCTTYPE.WINE));
            continue;
          }

          // New wines (without ProductKeyID in the spreadsheet) must have unique SKUs
          if (wines.Exists(p => String.Equals(p.ProductSKU, importWine.WineSKU, StringComparison.InvariantCultureIgnoreCase)))
          {
            errors.Add(new ErrorReportProduct(
              wine,
              "Wine with the same SKU already exists in the spreadsheet. Skipping",
              ErrorReportBase.FixTheFile,
              "ProductGetter",
              ErrorReportBase.ERRORTYPE.CRITICAL,
              ErrorReportProduct.PRODUCTTYPE.WINE));
            continue;
          }
        }

        DataTaskHelper.CorrectBadChars(this.wines, false);
        wines.Add(wine);    
      }
      return this.wines;
    }


    /// <summary>
    /// Get spreadsheet loader errors
    /// </summary>
    /// <returns></returns>
    public List<ErrorReportProduct> GetErrors()
    {
      var errs = this.errors;
      return errs;
    }


    /// <summary>
    /// Load spreadsheet and validate columns
    /// </summary>
    /// <typeparam name="T">Type of import object</typeparam>
    /// <param name="sheetName">Name of the sheet to look for import data</param>
    /// <returns>spreadsheet object or null if encountered errors</returns>
    private ExcelQueryFactory GetSpreadsheet<T>(String sheetName)
    {
      IEnumerable<String> props;
      IEnumerable<String> columns;
      Boolean isValid = true;
      ExcelQueryFactory spreadsheet = new ExcelQueryFactory(this.importFile);

      #region Spreadsheet format validation
      if (!spreadsheet.GetWorksheetNames().Contains(sheetName))
      {
        errors.Add(new ErrorReportProduct("The spreadsheet must contain a sheet called \"" + sheetName + "\" which contains the data you wish to import.", 
          ErrorReportProduct.FixTheFile, "Spreadsheet Getter", ErrorReportProduct.ERRORTYPE.CRITICAL));
        return null;
      }

      props = typeof(T).GetProperties().Select(prop => prop.Name);
      columns = spreadsheet.GetColumnNames(sheetName);

      var unMapped =
        (from col in columns
         where !props.Contains(col)
         select col);

      var notInSheet =
        (from prop in props
         where !columns.Contains(prop)
         select prop);

      if (unMapped.Count() > 0)
      {
        errors.Add(new ErrorReportProduct("The following columns in the spreadsheet '" + sheetName + "' did not match '" + typeof(T).Name + "' properties: "
          + String.Join(", ", unMapped), ErrorReportBase.FixTheFile, "Spreadsheet Getter", ErrorReportProduct.ERRORTYPE.CRITICAL));
        isValid = false;
      }

      if (notInSheet.Count() > 0)
      {
        errors.Add(new ErrorReportProduct("The following properties did not have matching columns in the spreadsheet: " + String.Join(", ", notInSheet),
          ErrorReportBase.FixTheFile, "Spreadsheet Getter", ErrorReportProduct.ERRORTYPE.CRITICAL));
        isValid = false;
      }

      // Strict mapping is useful to make sure all the columns in the 
      // spreadsheet map to properties in the ImportMember class.
      spreadsheet.StrictMapping = true;

      return isValid ? spreadsheet : null;

      #endregion
    }


    /// <summary>
    /// Get spreadsheet products
    /// </summary>
    /// <param name="IDs">Array of ProductKeyIDs</param>
    /// <returns>List of spreadsheet products that match passed in array of ProductKeyIDs</returns>
    public List<ImportProduct> GetSpreadsheetProducts(String[] IDs)
    {
      return this.xlsProducts
        .Where(xls => IDs.Contains(xls.ProductKeyID.ToString()))
        .OrderBy(xls => xls.ProductKeyID)
        .ToList();
    }

    /// <summary>
    /// Get Speadsheet Products
    /// </summary>
    /// <param name="errors">List of errors</param>
    /// <returns></returns>
    public List<ImportProduct> GetSpreadsheetProducts(IEnumerable<ErrorReportProduct> errors)
    {
      List<ImportProduct> records = new List<ImportProduct>();

      // group and flatten error messages
      var err = (from e in errors
                 where e.ProductType.HasFlag(ErrorReportProduct.PRODUCTTYPE.PRODUCT)
                 group e by new { e.RecordID, e.SKU } into eGrp
                 select new { RecordID = eGrp.Key.RecordID, SKU = eGrp.Key.SKU, Error = String.Join(" ", eGrp.Select(e => e.ErrorMessage)) }).ToList();

      // add errors based on different matching criterias
      foreach (var e in err)
      {
        var matches = 
          this.xlsProducts.Where(r => String.Equals(r.ProductSKU, e.SKU, StringComparison.InvariantCultureIgnoreCase))
          .Select(er => { er.Errors = e.Error; return er; });

        if (String.IsNullOrWhiteSpace(e.RecordID))
          matches = matches.Where(r => !r.ProductKeyID.HasValue);
        else
          matches = matches.Where(r => r.ProductKeyID.HasValue && r.ProductKeyID.ToString() == e.RecordID);

        records.AddRange(matches.ToList());
      }

      return records;
    }


    /// <summary>
    /// Get spreadsheet wines
    /// </summary>
    /// <param name="IDs">Array of ProductKeyIDs</param>
    /// <returns>List of spreadsheet wines that match passed in array of ProductKeyIDs</returns>
    public List<ImportWine> GetSpreadsheetWines(String[] IDs)
    {
      return this.xlsWines
        .Where(xls => IDs.Contains(xls.ProductKeyID.ToString()))
        .OrderBy(xls => xls.ProductKeyID)
        .ToList();
    }


    /// <summary>
    /// Get Speadsheet Wines
    /// </summary>
    /// <param name="errors">List of errors</param>
    /// <returns></returns>
    public List<ImportWine> GetSpreadsheetWines(IEnumerable<ErrorReportProduct> errors)
    {
      List<ImportWine> records = new List<ImportWine>();

      // group and flatten error messages
      var err = (from e in errors
                 where e.ProductType.HasFlag(ErrorReportProduct.PRODUCTTYPE.WINE)
                 group e by new { e.RecordID, e.SKU } into eGrp
                 select new { RecordID = eGrp.Key.RecordID, SKU = eGrp.Key.SKU, Error = String.Join(" ", eGrp.Select(e => e.ErrorMessage)) }).ToArray();

      // add errors based on different matching criterias
      foreach (var e in err)
      {
        var matches =
          (this.xlsWines.Where(r => String.Equals(r.WineSKU, e.SKU, StringComparison.InvariantCultureIgnoreCase))
          .Select(er => { er.Errors = e.Error; return er; })).ToList();

        if (String.IsNullOrWhiteSpace(e.RecordID))
          matches = matches.Where(r => !r.ProductKeyID.HasValue).ToList();
        else
          matches = matches.Where(r => r.ProductKeyID.HasValue && r.ProductKeyID.ToString() == e.RecordID).ToList();

        records.AddRange(matches);
      }

      return records;
    }
  }
}

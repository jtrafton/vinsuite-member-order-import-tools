﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EWSDataTaskSupport;

namespace EWSProductWineImport
{
  class Program
  {
    static void Main(string[] args)
    {
      Boolean isDebug = true;
      ProductWineImporter pwImporter = new ProductWineImporter(
        isDebug,
        @"C:\dotnet\pw_import_test.xlsx",
        75,
        "alexc@benningfieldgroup.com",
        "achernyak",
        "99alex",
        new Guid("f103684d-a136-c588-4996-ff08f5456b52"),
        null);

      //pwImporter.LoadAndValidateProducts();
      pwImporter.LoadAndValidateWines();
      pwImporter.LoadAndValidateProducts();
      pwImporter.RunImport();

      Console.WriteLine("Done");
      Console.ReadLine();
    }
  }
}

﻿using EWSDataTaskSupport;
using EWSDataTaskSupport.EWSMember;
using EWSDataTaskSupport.EWSOrder;
using System.Configuration;
using LinqToExcel;
using System.Linq;
using System;
using System.Collections.Generic;

namespace EWSCreditCardImport
{
  class Program
  {
    static void log(string inStr1, string inStr2, Func<string, string, string> convert)
    //static void log(string inStr)
    {
      Console.WriteLine("Original string1: " + inStr1 + " Original 2: " + inStr2);

      String newStr = convert(inStr1, inStr2);

      Console.WriteLine("New string: " + newStr);
    }

    static void Main(string[] args)
    {
      /*log("First string", "Second string.", (myStr1, myStr2) => myStr1.ToLower() + myStr2.ToUpper());
      return;*/

      // Allow self-signed certs.
      System.Net.ServicePointManager.ServerCertificateValidationCallback += (se, cert, chain, sslerror) =>
      {
        return true;
      };

      using (MemberServiceHttps memberService = new MemberServiceHttps())
      {
        using (OrderServiceHttps orderService = new OrderServiceHttps())
        {
          GetMemberResponse resp;
          AddUpdateCreditCardResponse ccResp;
          var spreadSheet = new ExcelQueryFactory();
          spreadSheet.FileName = ConfigurationManager.AppSettings["ImportFile"].ToString();
          CreditCardTypeGetter ccTypeGetter = new CreditCardTypeGetter(orderService, ConfigurationManager.AppSettings["WSUsername"].ToString(), ConfigurationManager.AppSettings["WSPassword"].ToString());

          int curNum = 0;

          spreadSheet.StrictMapping = true;
          //spreadSheet.AddMapping<ImportCreditCard>(c => c.NameOnCard, "Customer Number");

          spreadSheet.AddTransformation<ImportCreditCard>(c => c.Type,
            cellValue =>
            {
              if (cellValue.ToLower() == "mastercard" || cellValue.ToLower() == "master card" || cellValue.ToLower() == "mc")
                return "mc";
              else if (cellValue.ToLower() == "american express" || cellValue.ToLower() == "amex" || cellValue.ToLower() == "americanexpress")
                return "amex";
              else if (cellValue.ToLower() == "visa")
                return "visa";
              else if (cellValue.ToLower() == "discover card" || cellValue.ToLower() == "discover")
                return "discover";
              else if (cellValue.Trim() == "")
                return null;
              return cellValue;
            });

          spreadSheet.AddTransformation<ImportCreditCard>(c => c.IsPrimary, cellValue => Convert.ToInt32(cellValue) == 1);

          var csvCards = from c in spreadSheet.Worksheet<ImportCreditCard>()
                         select c;
          // TODO: Refactor to use EWSCallBase class!
          foreach (ImportCreditCard card in csvCards)
          {
            try
            {
              GetMemberRequest gmReq = new GetMemberRequest();
              AddUpdateCreditCardRequest ccReq = new AddUpdateCreditCardRequest();

              Console.WriteLine("Updating number " + ++curNum);

              gmReq.Username = ccReq.Username = ConfigurationManager.AppSettings["WSUsername"].ToString();
              gmReq.Password = ccReq.Password = ConfigurationManager.AppSettings["WSPassword"].ToString();
              gmReq.AltMemberID = card.AltMemberID;

              resp = memberService.GetMember(gmReq);

              //Console.WriteLine("Number of members: " + resp.Members.Length);

              if (!resp.IsSuccessful || resp.Members.Length != 1)
              {
                String exStr = "Error finding member with alt memberID: " + card.AltMemberID;
                Console.WriteLine(exStr);
                throw new Exception(exStr);
              }

              ccReq.CreditCard = new CreditCardRecord();

              ccReq.CreditCard.MemberID = resp.Members[0].MemberID;
              ccReq.CreditCard.NameOnCreditCard = card.NameOnCard;
              ccReq.CreditCard.CreditCardNumber = card.CardNumber;
              ccReq.CreditCard.CreditCardExpirationMonth = card.ExpMo;
              ccReq.CreditCard.CreditCardExpirationYear = card.ExpYr;

              ccReq.CreditCard.CreditCardType = ccTypeGetter.GetCreditCardType(card.Type).CreditCardType;
              ccReq.CreditCard.CreditCardTypeShort = ccTypeGetter.GetCreditCardType(card.Type).CreditCardTypeShort;
              ccReq.CreditCard.CreditCardTypeID = ccTypeGetter.GetCreditCardType(card.Type).CreditCardTypeID;
              ccResp = memberService.AddUpdateCreditCard(ccReq);

              if (!ccResp.IsSuccessful)
              {
                String exStr = "Error updating credit card.  Alt Member ID: " + card.AltMemberID +
                               "  Error message: " + ccResp.ErrorMessage;
                Console.WriteLine(exStr);
                throw new Exception(exStr);
              }
            }
            catch (Exception ex)
            {
              using (System.IO.StreamWriter file = new System.IO.StreamWriter(@"C:\Exceptions.txt", true))
                file.WriteLine(ex.Message);
            }
          }

          /*AddUpdateCreditCardRequest request = new AddUpdateCreditCardRequest();

          request.Username = ConfigurationManager.AppSettings["WSUsername"].ToString();
          request.Username = ConfigurationManager.AppSettings["WSPassword"].ToString();

          request.CreditCard = new CreditCardRecord();*/



          //request.CreditCard.CreditCardType
        }
      }
    }
  }
}
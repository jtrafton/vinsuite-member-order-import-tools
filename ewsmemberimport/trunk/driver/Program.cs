﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EWSMemberImport;
using EWSDataTaskSupport;

namespace driver
{
  /// <summary>
  /// This project is here for testing the various class libraries in the solution.
  /// </summary>
  class Program
  {
    static void Main(string[] args)
    {
      // TODO: test actual import process

      bool isDebug = true;
      bool IsAllowDupeClubLevels = true;
      bool isValid;
      MemberImporter memberImporter = new MemberImporter(
        isDebug,
        @"C:\dotnet\AnticaTerra - Import Missing Members.xlsx",//@"C:\dotnet\test_sheet_bad_chars.xlsx",
        75,
        "alexc@benningfieldgroup.com",
        "achernyak",
        "99alex",
        new Guid("f103684d-a136-c588-4996-ff08f5456b52"),
        null,
        IsAllowDupeClubLevels);

      isValid = memberImporter.LoadAndValidateSpreadsheet();

      foreach (ErrorReportMember error in memberImporter.GetErrors())
        Console.WriteLine("{0} {1} {2} {3}", error.RecordID, error.ErrorMessage, error.Resolution, error.Source);

      Console.WriteLine("Members to import: {0}", memberImporter.GetMemberCount());
      Console.WriteLine("Active Club Members to import: {0}", memberImporter.GetActiveClubMembershipCount());

      bool success = memberImporter.ImportMembers();
      Console.WriteLine(success);

      
      Console.WriteLine("Import is Done.");
      Console.ReadLine();
    }
  }
}

﻿var intervalID;

$(document).ready(function()
{
  $("#btnCreateTask").click(function() { window.location = "CreateDataTask.aspx"; });

  startUpdates();
});


/**************************************
 * Pause the task update calls
 **************************************/
function pauseUpdates()
{
  clearInterval(intervalID);
}


/**************************************
 * Start the task update calls
 **************************************/
function startUpdates()
{
  updateTasks();
  // Update the task progress every 20 seconds
  intervalID = setInterval(updateTasks, 20000);
}


/**************************************
 * Retrieve updates tasks
 **************************************/
var updateTasks = function()
{
  $.ajax(
  {
    type: "GET",
    url: "ShowDataTasks.aspx",
    contentType: "application/json; charset=utf-8",
    success: function(response)
    {
      $("#taskContainer").html(response);
    }
  });
}


/**************************************
 * Start a data task
 **************************************/
function startTask(taskID)
{
  $.ajax(
  {
    type: "POST",
    url: "DataTaskSupportServices.asmx/StartDataTask",
    contentType: "application/json; charset=utf-8",
    data: JSON.stringify(
    {
      "dataTaskTrackingID": taskID
    }),
    dataType: "json",
    success: function()
    {
      $("#btnStart" + taskID).attr("disabled", "disabled");
    },
    error: function(response)
    {
      alert("Error: " + response.d);
    }
  });
}


/**************************************
 * Stop a data task
 **************************************/
function stopTask(taskID)
{
  $.ajax(
  {
    type: "POST",
    url: "DataTaskSupportServices.asmx/StopDataTask",
    contentType: "application/json; charset=utf-8",
    data: JSON.stringify(
    {
      "dataTaskTrackingID": taskID
    }),
    dataType: "json",
    success: function()
    {
      $("#btnStop" + taskID).attr("disabled", "disabled");
    },
    error: function(response)
    {
      alert("Error: " + response.d);
    }
  });
}
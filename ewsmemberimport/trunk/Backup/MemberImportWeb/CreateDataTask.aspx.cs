﻿using System;
using System.IO;
using System.Web;
using System.Web.UI.WebControls;
using EWSDataTaskSupport;
using EWSDataTaskSupport.EWSData;
using System.Collections.Generic;
using System.Drawing;
using System.Web.UI;
using System.Text;

namespace DataTaskWeb
{
  public partial class MemberImport : System.Web.UI.Page
  {
    private Dictionary<String, String> importTypes;

    protected void Page_Load(object sender, EventArgs e)
    {
      ProcessImportSpreadsheet(true);
    }

    protected void ProcessImportSpreadsheet(Boolean isValidateOnly)
    {
      using (DataServiceHttps ds = new DataServiceHttps())
      {
        CreateImportTypes();
        PopulateWineries(ds);
        PopulateImportTypes();
        bool validateTask = true;

        if (Session["newTask"] == null)
        {
          Session["newTask"] = new DataTask();
          validateTask = false;
        }

        DataTask newTask = Session["newTask"] as DataTask;
        newTask.TaskName = txtImportName.Text;
        newTask.TaskType = importTypes[selImportType.SelectedValue];

        newTask.WineryID = Guid.Parse(selWinery.SelectedValue);

        if (Request.Files.Count == 1 && Request.Files[0].ContentLength > 0)
        {
          #region Save the posted file
          HttpPostedFile file = Request.Files[0];
          String path = MapPath("DATA") + @"\" + newTask.WineryID + @"\member import\";

          if (!Directory.Exists(path))
            Directory.CreateDirectory(path);

          String filename = String.Format("{0:yyyyMMdd-HHmmss}", DateTime.Now) + "_" + file.FileName;

          file.SaveAs(path + filename);
          newTask.File = path + filename;
          #endregion
        }

        if (validateTask)
        {
          try
          {
            // Validate the file
            ImportSupportServices supportSvc = new ImportSupportServices();
            newTask.JobSize = supportSvc.StartDataTask(newTask, true, false);
            StringBuilder builder = new StringBuilder();
            builder.Append("Validation is complete. Please check logs for more details. ");
            builder.AppendLine();
            builder.Append(supportSvc.MembersToImport.ToString())
              .Append(" members and ")
              .Append(supportSvc.ActiveMembershipsToInsert)
              .Append(" active club memberships are a ready to be imported.");
            builder.AppendLine();
            builder.Append("Only valid member records will be inserted. Invalid or failed members will be saved separately for further investigation.");
            txtMessage.Text = builder.ToString();
            txtMessage.Visible = true;
            if (supportSvc.MembersToImport > 0)
            {
              btnImport.Visible = true;
              chkAllowDupe.Visible = true;
            }
            else
            {
              btnImport.Visible = false;
              chkAllowDupe.Visible = false;
            }

            if (isValidateOnly)
            {
              List<ErrorReportMember> errors = supportSvc.GetErrors();
              if (errors.Count > 0)
              {                
                int rowNumber            = 1;
                TableRow row             = new TableRow();
                TableCell cellID         = new TableCell();
                TableCell cellEmail      = new TableCell();                
                TableCell cellLName      = new TableCell();
                TableCell cellFName      = new TableCell();
                TableCell cellError      = new TableCell();
                TableCell cellResolution = new TableCell();
                TableCell cellSource     = new TableCell();
                TableCell cellType       = new TableCell();
                cellID.Text              = "AltMemberID";
                cellEmail.Text           = "Email";
                cellLName.Text           = "Last Name";
                cellFName.Text           = "First Name";
                cellError.Text           = "Error Message";
                cellResolution.Text      = "Error Resolution";
                cellSource.Text          = "Error Source";
                cellType.Text            = "Error Type";
                row.BackColor            = Color.Gold;
                row.Font.Bold            = true;
                row.TableSection         = TableRowSection.TableHeader;
                row.Cells.Add(cellID);
                row.Cells.Add(cellError);
                row.Cells.Add(cellResolution);
                row.Cells.Add(cellSource);
                row.Cells.Add(cellType);
                tblValidationErrors.Rows.Add(row);
                foreach (ErrorReportMember error in errors)
                {
                  row                    = new TableRow();
                  cellID                 = new TableCell();
                  cellError              = new TableCell();
                  cellResolution         = new TableCell();
                  cellType               = new TableCell();
                  cellSource             = new TableCell();
                  cellID.Text            = error.RecordID;
                  cellID.HorizontalAlign = HorizontalAlign.Right;
                  cellError.Text         = error.ErrorMessage;
                  cellResolution.Text    = error.Resolution;
                  cellSource.Text        = error.Source;
                  cellType.Text          = error.ErrorType().ToString();
                  row.BackColor          = rowNumber % 2 == 0 ? Color.LightYellow : Color.White;
                  row.TableSection       = TableRowSection.TableBody;
                  row.Cells.Add(cellID);
                  row.Cells.Add(cellError);
                  row.Cells.Add(cellResolution);
                  row.Cells.Add(cellSource);
                  row.Cells.Add(cellType);
                  tblValidationErrors.Rows.Add(row);
                  ++rowNumber;
                }
              }
            }
            else           
            {
              #region Save the data task
              CreateDataTaskRequest createTaskRequest = new CreateDataTaskRequest()
              {
                Username = Session["wsUsername"] as String,
                Password = Session["wsPassword"] as String,
                DataTask = newTask,
                WineryID = newTask.WineryID
              };

              CreateDataTaskResponse createTaskResponse = ds.CreateDataTask(createTaskRequest);
              if (createTaskResponse.IsSuccessful)
              {
                // remove the task from the session, we're done with it
                Session["newTask"] = null;
                Session["IsAllowDupeClubLevels"] = chkAllowDupe.Checked;
                Response.Redirect("DataTaskDashboard.aspx");
              }
              else
              {
                lblMessage.Text = "Error creating task: " + createTaskResponse.ErrorMessage;
              }
              #endregion
            }
            
          }
          catch (Exception ex)
          {
            lblMessage.Text = "Exception: ";
            TableCell exceptionCell = new TableCell();
            TableRow exceptionRow = new TableRow();
            exceptionRow.Cells.Add(exceptionCell);
            tblValidationErrors.Rows.Add(exceptionRow);
            exceptionCell.Text = ex.Message;
          }
        }
      }
    }

    protected void CreateImportTask(object sender, System.EventArgs e)
    {
      ProcessImportSpreadsheet(false);
    }

    /// <summary>
    /// Make a dictionary of the available import types
    /// </summary>
    protected void CreateImportTypes()
    {
      importTypes = new Dictionary<String, String>();
      importTypes.Add("1", "Member import");
    }


    /// <summary>
    /// Get the list of wineries
    /// </summary>
    /// <param name="ds"></param>
    protected void PopulateWineries(DataService ds)
    {
      String username = Session["wsUsername"] as String;
      String password = Session["wsPassword"] as String;

      if (String.IsNullOrEmpty(username) || String.IsNullOrEmpty(password))
        return;

      // Populate the dropdown with the wineries
      SimpleWineryHolder[] wineries = ds.GetWineryInfo(username, password);

      foreach (SimpleWineryHolder win in wineries)
        selWinery.Items.Add(new ListItem(win.WineryName, win.WineryID.ToString()));
    }


    /// <summary>
    /// Create the list of available import types
    /// </summary>
    protected void PopulateImportTypes()
    {
      if (importTypes == null)
        CreateImportTypes();

      selImportType.Items.Clear();

      foreach (KeyValuePair<String, String> kvp in importTypes)
        selImportType.Items.Add(new ListItem(kvp.Value, kvp.Key));

      selImportType.SelectedIndex = 0;
    }
  }
}
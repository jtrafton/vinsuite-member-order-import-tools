﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EWSDataTaskSupport.EWSData;
using System.Web.UI.HtmlControls;
using EWSDataTaskSupport;

namespace DataTaskWeb
{
  public partial class ShowDataTasks : System.Web.UI.Page
  {
    protected void Page_Load(object sender, EventArgs e)
    {
      using (DataServiceHttps ds = new DataServiceHttps())
      {
        GetDataTasksRequest getTasksRequest = new GetDataTasksRequest()
        {
          Username = Session["wsUsername"] as String,
          Password = Session["wsPassword"] as String
        };

        GetDataTasksResponse getTasksResponse = ds.GetDataTasks(getTasksRequest);

        if (getTasksResponse.IsSuccessful)
        {
          foreach (DataTask task in getTasksResponse.Tasks)
            tasks.Controls.Add(RenderDataTask(task));
        }
      }
    }

    protected Control RenderDataTask(DataTask task)
    {
      Panel panel = new Panel();
      HtmlGenericControl lastUpdateSpan = new HtmlGenericControl("span");

      panel.Controls.Add(new Literal() { Text = "<hr />" });

      HtmlGenericControl span = new HtmlGenericControl("span");
      span.InnerHtml = "(" + task.DataTaskTrackingID + ") - " + task.TaskName + @" (<span>" + task.PercentComplete + "</span>% complete)<br />";
      panel.Controls.Add(span);

      span = new HtmlGenericControl("span");
      span.InnerHtml = task.TaskType + " for " + task.WineryName + "<br/>";
      panel.Controls.Add(span);

      span = new HtmlGenericControl("span");
      span.InnerHtml = "Started: " + String.Format("{0:MM/dd/yyyy h:mm tt}", task.DateStarted + "<br/>");
      panel.Controls.Add(span);

      lastUpdateSpan = new HtmlGenericControl("span");
      lastUpdateSpan.InnerHtml = "Last Update: " + String.Format("{0:MM/dd/yyyy h:mm tt}", task.DateModified + "<br/>");
      lastUpdateSpan.ID = "lastUpdate";
      panel.Controls.Add(lastUpdateSpan);

      span = new HtmlGenericControl("span");
      span.InnerHtml = @"Status: <span>" + task.Status + "</span><br/>";
      panel.Controls.Add(span);

      Button btn = new Button();
      switch (task.Status.ToUpper())
      {
        case "NEW":
          btn.Text = "Start";
          btn.ID = "btnStart" + task.DataTaskTrackingID;
          btn.OnClientClick = String.Format("startTask({0}); return false;", task.DataTaskTrackingID);
          panel.Controls.Add(btn);
          break;

        case "RUNNING":
          btn.Text = "Stop";
          btn.ID = "btnStop" + task.DataTaskTrackingID;
          btn.OnClientClick = String.Format("stopTask({0}); return false;", task.DataTaskTrackingID);
          panel.Controls.Add(btn);
          break;
      }

      return panel;
    }
  }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EWSDataTaskSupport.DB;
using EWSDataTaskSupport;
using System.IO;
using System.Data.SqlClient;
using System.Data.Common;
using System.Data;

namespace SQLToCSV
{
  /// <summary>
  /// Write a LINQ or SQL Query.
  /// Serialize it to a CSV.
  /// Profit.
  /// </summary>
  class Program
  {
    static void Main(string[] args)
    {
      String outputFile = Environment.CurrentDirectory + Path.DirectorySeparatorChar + "My CSV File.csv";

      using (EWSDatabaseObjectsDataContext db = new EWSDatabaseObjectsDataContext())
      {
        db.Connection.ConnectionString = AppConfigProxy.MainConnectionString;
        
        //ExportLinqQuery(db, outputFile);
        ExportSQLQuery(db, outputFile);
      }

      Console.WriteLine("Done: " + outputFile);
      Console.ReadLine();
    }


    /// <summary>
    /// Serialize Linq query to CSV
    /// </summary>
    /// <param name="db"></param>
    /// <param name="outputFile"></param>
    static void ExportLinqQuery(EWSDatabaseObjectsDataContext db, String outputFile)
    {
      Guid wineryID = new Guid("9EC9426D-1B78-7BFC-A2A7-E65EED0727D2");
      
      CSVHelper csvHelper = new CSVHelper();
      csvHelper.AddTransform<bool?>(o => ((bool?)o) == true ? "1" : "0");

      var membersExample =
        from m in db.Members
        where m.WineryID == wineryID
        where m.LastName == "hubka"
        select m;

      csvHelper.WriteCSVFile(outputFile, membersExample);
    }


    /// <summary>
    /// Serilialize a SQL query to CSV
    /// </summary>
    /// <param name="db"></param>
    /// <param name="outputFile"></param>
    static void ExportSQLQuery(EWSDatabaseObjectsDataContext db, String outputFile)
    {
      CSVHelper csvHelper = new CSVHelper();
      Guid wineryID = new Guid("9EC9426D-1B78-7BFC-A2A7-E65EED0727D2");

      using (SqlCommand command = (SqlCommand)db.Connection.CreateCommand())
      {
        command.CommandText = @"
          SELECT  TOP 10 *
          FROM    Members
          WHERE   WineryID = @WineryID";

        command.Parameters.Add("@WineryID", System.Data.SqlDbType.UniqueIdentifier).Value = wineryID;

        using (SqlDataAdapter da = new SqlDataAdapter(command))
        {
          using (DataTable table = new DataTable())
          {
            da.Fill(table);
            csvHelper.WriteCSVFile(outputFile, table);
          }
        }
      }
    }
  }
}

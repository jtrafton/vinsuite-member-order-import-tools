﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DataTaskDashboard.aspx.cs" Inherits="DataTaskWeb.ChooseImportType" MasterPageFile="~/DataTaskWeb.Master" ClientIDMode="Static" %>

<asp:Content ContentPlaceHolderID="head" runat="server">
  <script type="text/javascript" src="js/dataTaskDashboard.js"></script>
</asp:Content>

<asp:Content ContentPlaceHolderID="content" runat="server">
  <button id="btnCreateTask">Create Task</button>
  <button id="btnLogout">Log Out</button>
  <div id="taskContainer"></div>
</asp:Content>
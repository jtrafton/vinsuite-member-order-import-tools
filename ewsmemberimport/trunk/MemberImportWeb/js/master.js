﻿/********************************************
 * Ends the current session, redirects to 
 * login page
 ********************************************/
function logout()
{
  $.ajax(
  {
    type: "POST",
    url: "DataTaskLoginServices.asmx/Logout",
    data : "{}",
    contentType: "application/json; charset=utf-8",
    success: function()
    {
      window.location = "Login.aspx";
    },
    dataType: "json"
  });
}


$(document).ready(function()
{
  $("#btnLogout").click(logout);

  $("#btnBackToDash").click(function()
  {
    window.location = "DataTaskDashboard.aspx";
  });

  $('#tblValidationErrors').dataTable({
    "iDisplayLength": 100
  });
});
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Script.Services;
using EWSDataTaskSupport.EWSData;
using EWSDataTaskSupport;
using System.Threading;
using EWSMemberImport;
using System.Net;

namespace DataTaskWeb
{
  [WebService(Namespace = "http://tempuri.org/")]
  [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
  [System.ComponentModel.ToolboxItem(false)]
  [ScriptService]
  public class ImportSupportServices : WebService
  {
    private List<ErrorReportMember> errors;
    private int _MembersToImport;
    private int _ActiveMembershipsToImport;

    public ImportSupportServices()
      : base()
    {
      ServicePointManager.ServerCertificateValidationCallback += (se, cert, chain, sslerror) => true;
      errors = new List<ErrorReportMember>();
      _MembersToImport = -1;
      _ActiveMembershipsToImport = -1;
    }


    [WebMethod(EnableSession = true)]
    public GetDataTasksResponse GetDataTasks()
    {
      using (DataServiceHttps ds = new DataServiceHttps())
      {
        return GetDataTasks(ds, null);
      }
    }


    [WebMethod(EnableSession = true)]
    public bool StopDataTask(int dataTaskTrackingID)
    {
      using (DataServiceHttps ds = new DataServiceHttps())
      {
        GetDataTasksResponse getTaskResponse = GetDataTasks(ds, dataTaskTrackingID);

        if (getTaskResponse.IsSuccessful && getTaskResponse.Tasks.Length == 1)
        {
          DataTask task = getTaskResponse.Tasks[0];
          task.StopRequested = true;

          UpdateDataTaskRequest updTaskRequest = new UpdateDataTaskRequest()
          {
            Username = Session["wsUsername"] as String,
            Password = Session["wsPassword"] as String,
            DataTask = task
          };

          return ds.UpdateDataTask(updTaskRequest).IsSuccessful;
        }
        else
          return false;
      }
    }


    private GetDataTasksResponse GetDataTasks(DataService ds, int? dataTaskTrackingID)
    {
      GetDataTasksRequest request = new GetDataTasksRequest()
      {
        Username = Session["wsUsername"] as String,
        Password = Session["wsPassword"] as String,
        DataTaskTrackingID = dataTaskTrackingID
      };

      return ds.GetDataTasks(request);
    }



    [WebMethod(EnableSession = true)]
    public String StartDataTask(int dataTaskTrackingID)
    {
      using (DataServiceHttps ds = new DataServiceHttps())
      {
        GetDataTasksResponse response = GetDataTasks(ds, dataTaskTrackingID);

        if (response.IsSuccessful && response.Tasks.Length == 1)
        {
          DataTask task = response.Tasks[0];
          Boolean? IsAllowDupeClubLevels = Session["IsAllowDupeClubLevels"] as Boolean?;
          StartDataTask(task, false, IsAllowDupeClubLevels);

          return "";
        }
        else
          return "Error retrieving data task: " + response.ErrorMessage;
      }
    }

    /// <summary>
    /// Get Service errors
    /// </summary>
    /// <returns></returns>
    public List<ErrorReportMember> GetErrors()
    {
      return errors;
    }

    public int ActiveMembershipsToInsert
    {
      get
      {
        return _ActiveMembershipsToImport;
      }
    }

    public int MembersToImport
    {
      get
      {
        return _MembersToImport;
      }
    }

    /// <summary>
    /// Validates and optionally starts a data task. 
    /// This method will throw exceptions to report validation errors.
    /// </summary>
    /// <param name="task"></param>
    /// <param name="validateOnly">
    /// If true, this call will block until validation is complete. 
    /// If false, this call will spawn a background thread and perfrm the data task</param>
    /// <returns>The size of the job</returns>
    public int StartDataTask(DataTask task, bool validateOnly, bool? IsAllowDupeClubLevels)
    {
      MemberImporter importer;
      bool isValid;
      String adminWSUsername = Session["wsUsername"] as String;
      String adminWSPassword = Session["wsPassword"] as String;

      if (String.IsNullOrEmpty(adminWSUsername) || String.IsNullOrEmpty(adminWSPassword))
        errors.Add(new ErrorReportMember("Master admin web service credentials are required", "", "", ErrorReportMember.ERRORTYPE.CRITICAL));

      if (String.IsNullOrEmpty(task.TaskName))
        errors.Add(new ErrorReportMember("A task name is required", "", "", ErrorReportMember.ERRORTYPE.CRITICAL));

      switch (task.TaskType.ToUpper())
      {
        case "MEMBER IMPORT":
          if (String.IsNullOrEmpty(task.File))
            errors.Add(new ErrorReportMember("Member import requires an import file", "", "", ErrorReportMember.ERRORTYPE.CRITICAL));

          importer = new MemberImporter(
            validateOnly,
            task.File,
            75,
            task.NotifyEmail,
            adminWSUsername,
            adminWSPassword,
            task.WineryID,
            task.DataTaskTrackingID,
            IsAllowDupeClubLevels ?? false);

          isValid                    = importer.LoadAndValidateSpreadsheet();
          _MembersToImport           = importer.GetMemberCount();
          _ActiveMembershipsToImport = importer.GetActiveClubMembershipCount();
          errors.AddRange(importer.GetErrors());

          if (validateOnly)
          {
            return _MembersToImport;
          }
          else
          {
            // Start the job in a background thread
            importer.ImportMembersAsync();
          }
          break;          

        default:
          errors.Add(new ErrorReportMember("Unknown task type", "", "", ErrorReportMember.ERRORTYPE.CRITICAL));
          break;
      }

      using (DataServiceHttps ds = new DataServiceHttps())
      {
        task.DateStarted = DateTime.Now;
        UpdateDataTaskRequest req = new UpdateDataTaskRequest()
        {
          Username = adminWSUsername,
          Password = adminWSPassword,
          DataTask = task
        };

        ds.UpdateDataTask(req);

        return task.JobSize ?? -1;
      }
    }
  }
}

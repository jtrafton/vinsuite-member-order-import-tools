﻿using System;
using System.Linq;
using System.Windows.Forms;
using System.IO;
using EWSDataTaskSupport.EWSWebsite;
using EWSDataTaskSupport.DB;
using EWSDataTaskSupport;
using System.Diagnostics;
using System.Transactions;

namespace DirectDataExtraction
{
  public partial class DataExtractionForm : Form
  {
    private EWSDatabaseObjectsDataContext database;

    public DataExtractionForm()
    {
      InitializeComponent();
      database = new EWSDatabaseObjectsDataContext();
      database.CommandTimeout = int.MaxValue;

      // Close the database connection when the form is closed
      this.FormClosing += (obj, args) => { database.Dispose(); };

      // When the environment is changed, change the connection string and repopulate the wineries dropdown
      cmbEnvironment.SelectedIndexChanged += (obj, args) => 
      {
        ComboBoxItem<String, String> selectedEnvironment = (ComboBoxItem<String, String>)cmbEnvironment.SelectedItem;
        database.Connection.ConnectionString = selectedEnvironment.Value;
        PopulateWineriesDropdown(); 
      };

      // Add the supported environments
      cmbEnvironment.Items.Add(new ComboBoxItem<String, String>("Main (Reporting)", AppConfigProxy.MainConnectionString_Reporting));
      cmbEnvironment.Items.Add(new ComboBoxItem<String, String>("KJ (Reporting)", AppConfigProxy.KJConnectionString_Reporting));
      cmbEnvironment.Items.Add(new ComboBoxItem<String, String>("Diageo (Reporting)", AppConfigProxy.DiageoConnectionString_Reporting));
      cmbEnvironment.Items.Add(new ComboBoxItem<String, String>("Treasury (Reporting)", AppConfigProxy.TreasuryConnectionString_Reporting));
      cmbEnvironment.Items.Add(new ComboBoxItem<String, String>("Fosters AU (Reporting)", AppConfigProxy.FostersAUConnectionString_Reporting));
      cmbEnvironment.SelectedIndex = 0;

      // Check all and check none buttons
      btnCheckAll.Click += (obj, args) => { CheckAllPreferences(true); };
      btnCheckNone.Click += (obj, args) => { CheckAllPreferences(false); };

      // When active is checked/unchecked, repopulate the wineries dropdown
      cbWineryActive.CheckedChanged += (obj, args) => { PopulateWineriesDropdown(); };

      // Run the export
      btnRunExport.Click += (obj, args) =>
        {
          ComboBoxItem<String, Guid> selectedWinery = (ComboBoxItem<String, Guid>)cmbWinery.SelectedItem;
          String wineryName = selectedWinery.Key;
          Guid wineryID = selectedWinery.Value;

          if (cbCreditCards.Checked)
          {
            if (String.IsNullOrEmpty(tbWSUsername.Text) || String.IsNullOrEmpty(tbWSPassword.Text))
            {
              MessageBox.Show("Web service credentials are required to extract credit cards.");
              tbWSUsername.Focus();
              return;
            }
            else
            {
              TestConnectionResponse tcResponse = Program.TestWSCredentials(tbWSUsername.Text, tbWSPassword.Text);
              if (!tcResponse.IsSuccessful || tcResponse.PrimaryAuthInfo.WineryID != wineryID)
              {
                MessageBox.Show("Web service credentials are invalid or do not match the selected winery");
                return;
              }
            }
          }

          #region Create the output directory
          String outputPath = Environment.CurrentDirectory + Path.DirectorySeparatorChar + wineryName + Path.DirectorySeparatorChar;
          if (!Directory.Exists(outputPath))
            Directory.CreateDirectory(outputPath);
          #endregion
          
          ActivateAllControls(this, false);

          // Create a transaction scope, READ UNCOMMITTED
          using (TransactionScope transaction = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions() { IsolationLevel = IsolationLevel.ReadUncommitted }))
          {
            #region Member Data
            if (cbMembers.Checked)
              Program.ExtractMembers(wineryID, database, outputPath + "members.csv");

            if (cbClubMembers.Checked)
              Program.ExtractClubMembers(wineryID, database, outputPath + "club members.csv");

            if (cbShipMembers.Checked)
              Program.ExtractShipMembers(wineryID, database, outputPath + "ship members.csv");

            if (cbCreditCards.Checked)
              Program.ExtractCreditCards(wineryID, tbWSUsername.Text, tbWSPassword.Text, database, outputPath + "credit cards.csv");

            if (cbMemberTypes.Checked)
              Program.ExtractMembersXMemberTypes(wineryID, database, outputPath + "member types.csv");

            if (cbMemberNotes.Checked)
              Program.ExtractMemberNotes(wineryID, database, outputPath + "member notes.csv");

            if (cbVirtualMembers.Checked)
              Program.ExtractVirtualMemberData(wineryID, database, outputPath + "virtual members.csv");
            #endregion

            #region Order Data
            if (cbCarts.Checked)
              Program.ExtractOrderData(wineryID, database, outputPath + "orders.csv");

            if (cbCartItems.Checked)
              Program.ExtractOrderItemData(wineryID, database, outputPath + "orderitems.csv");

            if (cbOrderNotes.Checked)
              Program.ExtractOrderNotes(wineryID, database, outputPath + "order notes.csv");
            #endregion

            #region Product Data
            if (cbFPWines.Checked)
              Program.ExtractFirstPartyWineData(wineryID, database, outputPath + "first party wines.csv");

            if (cbFPProducts.Checked)
              Program.ExtractFirstPartyProductData(wineryID, database, outputPath + "first party products.csv");

            if (cbTPWines.Checked)
              Program.ExtractThirdPartyWineData(wineryID, database, outputPath + "third party wines.csv");

            if (cbTPProducts.Checked)
              Program.ExtractThirdPartyProductData(wineryID, database, outputPath + "third party products.csv");
            #endregion
          }
         

          MessageBox.Show("Extract for " + wineryName + " complete!");
          ActivateAllControls(this, true);

          // Open an explorer window showing the exported files
          Process.Start(outputPath);
        };

      cmbEnvironment.Focus();
    }


    /// <summary>
    /// Check or uncheck all the checkboxes in the export preferences groupbox
    /// </summary>
    /// <param name="enabled"></param>
    private void CheckAllPreferences(bool enabled)
    {
      foreach (CheckBox cb in grpExportPreferences.Controls.OfType<CheckBox>())
        cb.Checked = enabled;
    }


    /// <summary>
    /// Retrieve wineries from the current environment
    /// </summary>
    private void PopulateWineriesDropdown()
    {
      var wineries =
        from w in database.Wineries
        where w.isActive == cbWineryActive.Checked
        orderby w.WineryName
        select new ComboBoxItem<String, Guid>(w.WineryName, w.WineryID);

      cmbWinery.Items.Clear();
      cmbWinery.Items.AddRange(wineries.ToArray());
      if (cmbWinery.Items.Count > 0)
      {
        btnRunExport.Enabled = true;
        cmbWinery.SelectedIndex = 0;
      }
      else
        btnRunExport.Enabled = false;
    }


    /// <summary>
    /// Recursively enable or disable the base control and all child controls
    /// </summary>
    /// <param name="baseControl"></param>
    /// <param name="enabled"></param>
    private void ActivateAllControls(Control baseControl, bool enabled)
    {
      foreach (Control c in baseControl.Controls)
        ActivateAllControls(c, enabled);

      baseControl.Enabled = enabled;
    }
  }
}
﻿using System;
using System.Collections;
using System.IO;
using System.Linq;
using System.Collections.Generic;
using EWSDataTaskSupport.EWSMember;
using System.Windows.Forms;
using EWSDataTaskSupport.EWSWebsite;
using EWSDataTaskSupport.DB;
using EWSDataTaskSupport;
using System.Net;

namespace DirectDataExtraction
{
  class Program
  {
    static CSVHelper csvHelper;

    [STAThread]
    static void Main(string[] args)
    {
      #region Custom CSV transforms
      csvHelper = new CSVHelper();
      csvHelper.AddTransform<bool>(o => ((bool)o) ? "1" : "0");
      #endregion

      // Allow self-signed certs.
      ServicePointManager.ServerCertificateValidationCallback += (se, cert, chain, sslerror) => true;

      DataExtractionForm form = new DataExtractionForm();
      Application.Run(form);
    }


    /// <summary>
    /// Test some web service credentials
    /// </summary>
    /// <param name="username"></param>
    /// <param name="password"></param>
    /// <param name="wineryID"></param>
    /// <param name="wsURL"></param>
    /// <returns></returns>
    public static TestConnectionResponse TestWSCredentials(String username, String password)
    {
      using (WebsiteService websiteService = new WebsiteServiceHttps())
      {
        TestConnectionRequest tcRequest = new TestConnectionRequest();
        tcRequest.Username = username;
        tcRequest.Password = password;

        return websiteService.TestConnection(tcRequest);
      }
    }


    /// <summary>
    /// Extract Virtual Member data for a winery
    /// </summary>
    /// <param name="wineryID"></param>
    /// <param name="db"></param>
    /// <param name="outputFile"></param>
    public static void ExtractVirtualMemberData(Guid wineryID, EWSDatabaseObjectsDataContext db, String outputFile)
    {
      PrintWithTimestamp("Extracting Virtual Member Data...");

      var vmData =
        from vm in db.VirtualMembers
        join vmxm in db.VirtualMembersXMembers on vm.VirtualMemberID equals vmxm.VirtualMemberID
        join vms in db.VirtualMemberStatuses on vmxm.VirtualMemberStatusID equals vms.VirtualMemberStatusID
        join tpsxw in db.ThirdPartySuppliersxWineries on vm.ThirdPartySupplierID equals tpsxw.ThirdPartySupplierID
        join w in db.Wineries on tpsxw.WineryID equals w.WineryID
        join m in db.Members on vmxm.MemberID equals m.MemberID
        where w.WineryID == wineryID
        where m.WineryID == wineryID
        orderby m.MemberID, vm.VirtualMemberID
        select new
        {
          VirtualMemberID = vm.VirtualMemberID,
          WineryName = w.WineryName,
          MemberID = m.MemberID,
          DateAdded = vmxm.DateAdded,
          DateModified = vmxm.DateModified,
          Status = vms.Status,
          PrimaryAffiliationFlag = (vm.PrimaryMemberID == m.MemberID)
        };

      csvHelper.WriteCSVFile(outputFile, vmData);
    }


    /// <summary>
    /// Extract third party wine data from a winery
    /// </summary>
    /// <param name="wineryID"></param>
    /// <param name="db"></param>
    /// <param name="outputFile"></param>
    public static void ExtractThirdPartyWineData(Guid wineryID, EWSDatabaseObjectsDataContext db, String outputFile)
    {
      PrintWithTimestamp("Extracting Third Party Wine Data...");

      var tpWineData =
        from tpw in db.ThirdPartyWines
        join tpsxw in db.ThirdPartySuppliersxWineries on tpw.ThirdPartySupplierID equals tpsxw.ThirdPartySupplierID
        from tpwin in db.ThirdPartyWineries.Where(tpwin => tpwin.ThirdPartyWineryID == tpw.ThirdPartyWineryID).DefaultIfEmpty()
        from tpwb in db.ThirdPartyWineBrands.Where(tpwb => tpwb.ThirdPartyWineBrandID == tpw.ThirdPartyWineBrandID).DefaultIfEmpty()
        from tpwbs in db.ThirdPartyWineBottleSizes.Where(tpwbs => tpwbs.ThirdPartyWineBottleSizeID == tpw.ThirdPartyWineBottleSizeID).DefaultIfEmpty()
        from tpwv in db.ThirdPartyWineVarietals.Where(tpwv => tpwv.ThirdPartyWineVarietalID == tpw.ThirdPartyWineVarietalID).DefaultIfEmpty()
        from tpwr in db.ThirdPartyWineRegions.Where(tpwr => tpwr.ThirdPartyWineRegionID == tpw.ThirdPartyWineRegionID).DefaultIfEmpty()
        from tpwa in db.ThirdPartyWineAppellations.Where(tpwa => tpwa.ThirdPartyWineAppellationID == tpw.ThirdPartyWineAppellationID).DefaultIfEmpty()
        from tpwt in db.ThirdPartyWineTypes.Where(tpwt => tpwt.ThirdPartyWineTypeID == tpw.ThirdPartyWineTypeID).DefaultIfEmpty()
        from tpsup in db.ThirdPartySuppliers.Where(tpsup => tpsup.ThirdPartySupplierID == tpw.ThirdPartySupplierID).DefaultIfEmpty()
        from tpst in db.ThirdPartyStores.Where(tpst => tpst.ThirdPartyStoreID == tpw.ThirdPartyStoreID).DefaultIfEmpty()
        where tpsxw.WineryID == wineryID
        select new
        {
          ThirdPartyWineID = tpw.ThirdPartyWineID,
          ThirdPartySupplier = tpsup.ThirdPartySupplier1,
          ThirdPartyStore = tpst.ThirdPartyStore1,
          WineName = tpw.WineName,
          WineSKU = tpw.WineSKU,
          ThirdPartyWinery = tpwin.WineryName,
          ThirdPartyWineBrand = tpwb.WineBrand,
          VineyardDesignation = tpw.VineyardDesignation,
          Teaser = tpw.Teaser,
          Picture = tpw.Picture,
          ThirdPartyWineBottleSizeID = tpw.ThirdPartyWineBottleSizeID,
          Weight = tpw.Weight,
          Vintage = tpw.Vintage,
          ThirdPartyWineType = tpwt.WineType,
          ThirdPartyWineVarietal = tpwv.WineVarietal,
          ThirdPartyWineAppellation = tpwa.WineAppellation,
          isSellAsBottle = tpw.isSellAsBottle,
          isSellAsCase = tpw.isSellAsCase,
          CostPerBottle = tpw.CostPerBottle,
          SuggestedPricePerBottle = tpw.SuggestedPricePerBottle,
          CostPerCase = tpw.CostPerCase,
          SuggestedPricePerCase = tpw.SuggestedPricePerCase,
          BottlesInACase = tpw.BottlesInACase,
          MinBottlesPerPurchase = tpw.MinBottlesPerPurchase,
          MaxBottlesPerPurchase = tpw.MaxBottlesPerPurchase,
          MaxCasesPerPurchase = tpw.MaxCasesPerPurchase,
          isActive = tpw.isActive,
          Description = tpw.Description,
          TastingNotes = tpw.TastingNotes,
          Ratings = tpw.Ratings,
          Awards = tpw.Awards,
          VineyardNotes = tpw.VineyardNotes,
          ProductionNotes = tpw.ProductionNotes,
          WineMakerNotes = tpw.WineMakerNotes,
          FoodPairing = tpw.FoodPairing,
          Production = tpw.Production,
          OtherNotes = tpw.OtherNotes,
          HarvestDate = tpw.HarvestDate,
          Sugar = tpw.Sugar,
          Acid = tpw.Acid,
          PH = tpw.PH,
          Aging = tpw.Aging,
          Fermentation = tpw.Fermentation,
          BottlingDate = tpw.BottlingDate,
          ResidualSugar = tpw.ResidualSugar,
          Tannin = tpw.Tannin,
          AlcoholPercentage = tpw.AlcoholPercentage,
          MetaTagTitle = tpw.MetaTagTitle,
          MetaTagKeywords = tpw.MetaTagKeywords,
          MetaTagDescription = tpw.MetaTagDescription,
          WSRating = tpw.WSRating,
          RPRating = tpw.RPRating,
          WERating = tpw.WERating,
          RGSRating = tpw.RGSRating,
          AverageRating = tpw.AverageRating,
          DateAdded = tpw.DateAdded,
          DateModified = tpw.DateModified,
          ModifiedBy = tpw.ModifiedBy,
          ThirdPartyWineRegion = tpwr.WineRegion,
          WASRating = tpw.WASRating,
          STRating = tpw.STRating,
          TWNRating = tpw.TWNRating,
          OverrideCompliance = tpw.OverrideCompliance,
          BarrelAgingName = tpw.BarrelAgingName,
          Ageability = tpw.Ageability,
          WineNotesPDF = tpw.WineNotesPDF,
          NumberOfBottlesTowardMultiple = tpw.NumberOfBottlesTowardMultiple,
          isTaxable = tpw.isTaxable,
          MarketplaceSKU = tpw.MarketplaceSKU,
          MarketingFee = tpw.MarketingFee,
          ShortName = tpw.ShortName,
          PlainName = tpw.PlainName,
        };

      csvHelper.WriteCSVFile(outputFile, tpWineData);
    }


    /// <summary>
    /// Extract third party products from a winery
    /// </summary>
    /// <param name="wineryID"></param>
    /// <param name="db"></param>
    /// <param name="outputFile"></param>
    public static void ExtractThirdPartyProductData(Guid wineryID, EWSDatabaseObjectsDataContext db, String outputFile)
    {
      PrintWithTimestamp("Extracting Third Party Product Data...");

      var tpProductData =
        from tpp in db.ThirdPartyProducts
        join tpst in db.ThirdPartyStores on tpp.ThirdPartyStoreID equals tpst.ThirdPartyStoreID
        join tpsxw in db.ThirdPartySuppliersxWineries on tpst.ThirdPartySupplierID equals tpsxw.ThirdPartySupplierID
        join tpsup in db.ThirdPartySuppliers on tpst.ThirdPartySupplierID equals tpsup.ThirdPartySupplierID
        from tpwb in db.ThirdPartyWineBrands.Where(tpwb => tpwb.ThirdPartyWineBrandID == tpp.ThirdPartyBrandID).DefaultIfEmpty()
        where tpsxw.WineryID == wineryID
        select new
        {
          ThirdPartyProductID = tpp.ThirdPartyProductID,
          ThirdPartySupplier = tpsup.ThirdPartySupplier1,
          ThirdPartyStore = tpst.ThirdPartyStore1,
          Picture = tpp.Picture,
          ProductName = tpp.ProductName,
          ProductSKU = tpp.ProductSKU,
          MetaTagTitle = tpp.MetaTagTitle,
          MetaTagKeywords = tpp.MetaTagKeywords,
          MetaTagDescription = tpp.MetaTagDescription,
          Teaser = tpp.Teaser,
          Description = tpp.Description,
          IsActive = tpp.isActive,
          DateAdded = tpp.DateAdded,
          DateModified = tpp.DateModified,
          ThirdPartyBrand = tpwb.WineBrand,
          IsWebServiceEnabled = tpp.isWebserviceEnabled,
          COGS = tpp.cogs,
          MarketPlaceSKU = tpp.MarketplaceSKU,
          MarketingFee = tpp.MarketingFee,
          ShortName = tpp.ShortName,
          PlainName = tpp.PlainName
        };

      csvHelper.WriteCSVFile(outputFile, tpProductData);
    }


    /// <summary>
    /// Extract members with their member types.
    /// </summary>
    /// <param name="wineryID"></param>
    /// <param name="wsUsername"></param>
    /// <param name="wsPassword"></param>
    /// <param name="db"></param>
    /// <param name="outputFile"></param>
    public static void ExtractMembersXMemberTypes(Guid wineryID, EWSDatabaseObjectsDataContext db, String outputFile)
    {
      PrintWithTimestamp("Extracting Member Types...");

      var membersWithTypes =
        from m in db.Members
        join mxmt in db.MembersXMemberTypes on m.MemberID equals mxmt.MemberID
        join mt in db.MemberTypes on mxmt.MemberTypeID equals mt.MemberTypeID
        where m.WineryID == wineryID
        select new
        {
          MemberID = m.MemberID,
          MemberTypeID = mt.MemberTypeID,
          MemberType = mt.MemberTypeID,
          DateAdded = mxmt.DateAdded
        };

      csvHelper.WriteCSVFile(outputFile, membersWithTypes);
    }


    /// <summary>
    /// Extract credit card data for members in a winery.  Uses EWS GetCreditCard web service.
    /// </summary>
    /// <param name="wineryID"></param>
    /// <param name="db"></param>
    /// <param name="outputFile"></param>
    public static void ExtractCreditCards(Guid wineryID, String wsUsername, String wsPassword, EWSDatabaseObjectsDataContext db, String outputFile)
    {
      PrintWithTimestamp("Extracting Credit Card Data...");

      var wineryMembers =
        from m in db.Members
        where m.WineryID == wineryID
        select m.MemberID;

      List<CreditCardRecord> creditCards = new List<CreditCardRecord>();
      List<Guid> failedMembers = new List<Guid>();
      int jobSize = wineryMembers.Count();
      int reportIteration = jobSize / 20;
      GetCreditCardRequest getCCRequest = new GetCreditCardRequest();
      GetCreditCardResponse getCCResponse;
      getCCRequest.Username = wsUsername;
      getCCRequest.Password = wsPassword;

      using (MemberService memberService = new MemberServiceHttps())
      {
        TimeSpan retryWait = TimeSpan.FromSeconds(10);
        int maxTries = 6;
        int iteration = 0;

        foreach (Guid memberID in wineryMembers)
        {
          getCCRequest.MemberID = memberID;
          getCCResponse = DataTaskHelper.TryNTimes(memberService.GetCreditCard, getCCRequest, maxTries, retryWait);

          if (getCCResponse != null)
          {
            if (getCCResponse.IsSuccessful)
            {
              foreach (CreditCardRecord cc in getCCResponse.CreditCards)
                creditCards.Add(cc);
            }
            else
              failedMembers.Add(memberID);
          }
          else
            failedMembers.Add(memberID);

          ++iteration;

          if (iteration % reportIteration == 0)
            PrintWithTimestamp(((100 * iteration) / jobSize) + "%");
        }
      }

      csvHelper.WriteCSVFile(outputFile, creditCards);
      
      if(failedMembers.Count > 0)
        csvHelper.WriteCSVFile(Path.GetDirectoryName(outputFile) + " failed CCs.csv", failedMembers.Select(guid => new { MemberID = guid.ToString() }));
    }


    /// <summary>
    /// Extract product data from a winery
    /// </summary>
    /// <param name="wineryID"></param>
    /// <param name="db"></param>
    /// <param name="outputFile"></param>
    public static void ExtractFirstPartyProductData(Guid wineryID, EWSDatabaseObjectsDataContext db, String outputFile)
    {
      PrintWithTimestamp("Extracting First Party Product Data...");

      var productData =
        from p in db.Products
        join w in db.Wineries on p.WineryID equals w.WineryID
        join i in db.Inventories on p.ProductID equals i.ProductID
        where p.WineryID == wineryID
        select new
        {
          WineryName = w.WineryName,
          ProductID = p.ProductID,
          ProductName = p.ProductName,
          ProductSKU = p.ProductSKU,
          DateAdded = p.DateAdded,
          DateModified = p.DateModified,
          Description = p.Description,
          InventoryCount = i.Count,
          IsActive = p.isActive,
          IsActivebySupplier = p.isActiveBySupplier,
          IsFavorite = p.isFavorite,
          IsForExport = p.isForExport,
          IsShownOnAddOrder = p.isShownOnAddOrder,
          IsTaxable = p.isTaxable,
          IsWineStartPage = p.isWineStartPage,
          MarketingURL = p.MarketingURL,
          MemberTeaser = p.MemberTeaser,
          MetaTagDescription = p.MetaTagDescription,
          MetaTagKeywords = p.MetaTagKeywords,
          MetaTagTitle = p.MetaTagTitle,
          NonMemberTeaser = p.NonMemberTeaser,
          OverrideCompliance = p.OverrideCompliance,
          PhotoPath = p.PhotoPath,
          Picture = p.Picture,
          Price1 = p.Price1,
          Price2 = p.Price2,
          Priceunit1 = p.PriceUnit1,
          PriceUnit2 = p.PriceUnit2,
          SalePrice1 = p.SalePrice1,
          SalePrice2 = p.SalePrice2,
          Teaser = p.Teaser,
          WinePricePerBottle = p.WinePricePerBottle,
          WinePricePerCase = p.WinePricePerCase,
          WineSalePricePerBottle = p.WineSalePricePerBottle,
          WineSalePricePerCase = p.WineSalePricePerCase
        };

      csvHelper.WriteCSVFile(outputFile, productData);
    }


    /// <summary>
    /// Extract order notes for orders from a winery
    /// </summary>
    /// <param name="wineryID"></param>
    /// <param name="db"></param>
    /// <param name="outputFile"></param>
    public static void ExtractOrderNotes(Guid wineryID, EWSDatabaseObjectsDataContext db, String outputFile)
    {
      PrintWithTimestamp("Extracting Order Notes...");

      var orderNotesData =
        from c in db.Carts
        join nt in db.OrderNotes on c.CartID equals nt.CartID
        join w in db.Wineries on c.WineryID equals w.WineryID
        join m in db.Members on c.MemberID equals m.MemberID
        join cs in db.CartStatuses on c.CartStatusID equals cs.CartStatusID
        from ac in db.AdminContacts.Where(ac => ac.AdminContactID == nt.ModifiedBy).DefaultIfEmpty()
        where c.WineryID == wineryID
        where cs.Keyword == "Completed"
        orderby c.DateAdded, c.CartID
        select new
        {
          WineryName = w.WineryName,
          OrderID = c.CartID,
          OrderNotesID = nt.OrderNotesID,
          DateAdded = nt.DateAdded,
          ModifierName = (ac.FirstName + " " + ac.LastName).Trim(),
          Notes = nt.Notes,
          NoteType = "Order Note"
        };

      csvHelper.WriteCSVFile(outputFile, orderNotesData);
    }


    /// <summary>
    /// Extract order item data from orders in a winery
    /// </summary>
    /// <param name="wineryID"></param>
    /// <param name="db"></param>
    /// <param name="outputFile"></param>
    public static void ExtractFirstPartyWineData(Guid wineryID, EWSDatabaseObjectsDataContext db, String outputFile)
    {
      PrintWithTimestamp("Extracting First Party Wine Data...");

      var orderItemData =
        from p in db.Products
        join w in db.Wines on p.ProductKeyID equals w.WineID
        join win in db.Wineries on p.WineryID equals win.WineryID
        from wa in db.WineAppellations.Where(wa => wa.WineAppellationID == w.WineAppellationID).DefaultIfEmpty()
        from wbs in db.WineBottleSizes.Where(wbs => wbs.WineBottleSizeID == w.WineBottleSizeID).DefaultIfEmpty()
        from wb in db.WineBrands.Where(wb => wb.WineBrandID == p.BrandID).DefaultIfEmpty()
        from wr in db.WineRegions.Where(wr => wr.WineRegionID == w.WineRegionID).DefaultIfEmpty()
        from wt in db.WineTypes.Where(wt => wt.WineTypeID == w.WineTypeID).DefaultIfEmpty()
        from wv in db.WineVarietals.Where(wv => wv.WineVarietalID == w.WineVarietalID).DefaultIfEmpty()
        from i in db.Inventories.Where(i => i.ProductID == p.ProductID).DefaultIfEmpty()
        where p.WineryID == wineryID
        select new
        {
          WineryName = win.WineryName,
          ProductID = p.ProductID,
          WineID = w.WineID,
          WineName = w.WineName,
          WineSKU = w.WineSKU,
          ProductName = p.ProductName,
          ProductSKU = p.ProductSKU,
          Ageability = w.Ageability,
          Aging = w.Aging,
          AlcoholPercentage = w.AlcoholPercentage,
          Awards = w.Awards,
          BottlesInACase = w.BottlesInACase,
          BottlingDate = w.BottlingDate,
          Fermentation = w.Fermentation,
          FoodPairing = w.FoodPairing,
          HarvestDate = w.HarvestDate,
          InventoryCount = i.Count,
          IsActive = p.isActive,
          IsActiveBySupplier = p.isActiveBySupplier,
          IsFavorite = p.isFavorite,
          IsForExport = p.isForExport,
          IsShownOnAddOrder = p.isShownOnAddOrder,
          IsTaxable = p.isTaxable,
          IsWineStartPage = w.isWineStartPage,
          MarketingURL = p.MarketingURL,
          MaxBottlesPerOrder = w.MaxBottlesPerOrder,
          MemberTeaser = p.MemberTeaser,
          MetaTagDescription = w.MetaTagDescription,
          MetaTagKeywords = w.MetaTagKeywords,
          MetaTagTitle = w.MetaTagTitle,
          MinBottlesPerOrder = w.MinBottlesPerOrder,
          NonMemberTeaser = p.NonMemberTeaser,
          OtherNotes = w.OtherNotes,
          OverrideCompliance = p.OverrideCompliance,
          PH = w.PH,
          Photopath = p.PhotoPath,
          Picture = p.Picture,
          Price1 = p.Price1,
          Price2 = p.Price2,
          PricePerBottle = w.PricePerBottle,
          PricePerCase = w.PricePerCase,
          PriceUnit1 = p.PriceUnit1,
          PriceUnit2 = p.PriceUnit2,
          ProductionNotes = w.ProductionNotes,
          SalePrice1 = p.SalePrice1,
          SalePrice2 = p.SalePrice2,
          Production = w.Production,
          ShortName = p.ShortName,
          Sugar = w.Sugar,
          Tannin = w.Tannin,
          TastingNotes = w.TastingNotes,
          Teaser = p.Teaser,
          VineyardDesignation = w.VineyardDesignation,
          Vintage = w.Vintage,
          VinyardNotes = w.VineyardNotes,
          Weight = w.Weight,
          WineAppellation = wa.WineAppellation1,
          WineBottleSize = wbs.WineBottleSize1,
          WineBottleSizeID = w.WineBottleSizeID,
          WineBrand = wb.WineBrand1,
          WineDescription = w.Description,
          WineRegion = wr.WineRegion1,
          WinemakerNotes = w.WineMakerNotes,
          WineSalePricePerBottle = w.SalePricePerBottle,
          WineSlaePricePerCase = w.SalePricePerCase,
          WineType = wt.WineType1,
          WineVarietal = wv.WineVarietal1,
          WinePricePerBottle = p.WinePricePerBottle,
          WinePricePerCase = p.WinePricePerCase
        };

      csvHelper.WriteCSVFile(outputFile, orderItemData);
    }

    
    /// <summary>
    /// Extract order item data from orders in a winery
    /// </summary>
    /// <param name="wineryID"></param>
    /// <param name="db"></param>
    /// <param name="outputFile"></param>
    public static void ExtractOrderItemData(Guid wineryID, EWSDatabaseObjectsDataContext db, String outputFile)
    {
      PrintWithTimestamp("Extracting Order Item Data...");

      var orderItemData =
        from c in db.Carts
        join ci in db.CartItems on c.CartID equals ci.CartID
        join w in db.Wineries on c.WineryID equals w.WineryID
        join m in db.Members on c.MemberID equals m.MemberID
        join cs in db.CartStatuses on c.CartStatusID equals cs.CartStatusID
        from ws in db.WineShippers.Where(ws => ws.WineShipperID == ci.ShipperID).DefaultIfEmpty()
        from css in db.CartShippings.Where(css => css.CartShippingID == ci.CartShippingID).DefaultIfEmpty()
        from wa in db.WineAppellations.Where(wa => wa.WineAppellationID == ci.WineAppellationID).DefaultIfEmpty()
        from wbs in db.WineBottleSizes.Where(wbs => wbs.WineBottleSizeID == ci.WineBottleSizeID).DefaultIfEmpty()
        from wb in db.WineBrands.Where(wb => wb.WineBrandID == ci.WineBrandID).DefaultIfEmpty()
        from wt in db.WineTypes.Where(wt => wt.WineTypeID == ci.WineTypeID).DefaultIfEmpty()
        from wv in db.WineVarietals.Where(wv => wv.WineVarietalID == ci.WineVarietalID).DefaultIfEmpty()
        where c.WineryID == wineryID
        where cs.Keyword == "Completed"
        orderby c.DateAdded, c.CartID
        select new
        {
          WineryName = w.WineryName,
          MemberID = c.MemberID,
          OrderID = c.CartID,
          OrderItemID = ci.CartID,
          BottleCount = ci.BottleCount,
          CostofGoodsSold = ci.COGS,
          DateAdded = ci.DateAdded,
          DateModified = ci.DateModified,
          Description = ci.Description,
          Discount = ci.Discount,
          KitSKU = ci.KitID,
          Name = ci.ProductName,
          OriginalPrice = ci.OriginalPrice,
          Price = ci.Price,
          PriceUnits = ci.PriceUnits,
          ProductID = ci.ProductID,
          Quantity = ci.Quantity,
          Shipper = ws.WineShipper1,
          ShippingType = css.ShippingType,
          ShippingTypeCode = css.ShippingCode,
          SKU = ci.ProductSKU,
          TaxAmount = ci.TaxAmount,
          TaxRate = ci.TaxRate,
          Weight = ci.Weight,
          WineAppellation = wa.WineAppellation1,
          WineBottleSize = wbs.WineBottleSize1,
          WineBrand = wb.WineBrand1,
          WineBrandKey = wb.WineBrandKey,
          WineType = wt.WineType1,
          WineVarietal = wv.WineVarietal1
        };

      csvHelper.WriteCSVFile(outputFile, orderItemData);
    }


    /// <summary>
    /// Extract order data from a winery
    /// </summary>
    /// <param name="wineryID"></param>
    /// <param name="db"></param>
    /// <param name="outputFile"></param>
    public static void ExtractOrderData(Guid wineryID, EWSDatabaseObjectsDataContext db, String outputFile)
    {
      PrintWithTimestamp("Extracting Order Data...");

      var orderData =
        from c in db.Carts
        join w in db.Wineries on c.WineryID equals w.WineryID
        join m in db.Members on c.MemberID equals m.MemberID
        join cs in db.CartStatuses on c.CartStatusID equals cs.CartStatusID
        from cpt in db.CartPaymentTypes.Where(cpt => c.CartPaymentTypeID == cpt.CartPaymentTypeID).DefaultIfEmpty()
        from bco in db.Countries.Where(bco => bco.CountryID == c.BillCountryID).DefaultIfEmpty()
        from sco in db.Countries.Where(sco => sco.CountryID == c.ShipCountryID).DefaultIfEmpty()
        from cct in db.CreditCardTypes.Where(cct => cct.CreditCardTypeID == c.CreditCardTypeID).DefaultIfEmpty()
        from ct in db.CartTrackings.Where(ct => ct.CartID == c.CartID).DefaultIfEmpty() // WARNING: THIS CAN CAUSE A CARTESIAN PRODUCT!!! 1 cart <-> M cart tracking entries
        where c.WineryID == wineryID
        where cs.Keyword == "Completed"
        orderby c.DateAdded, c.CartID
        select new
        {
          WineryName = w.WineryName,
          MemberID = m.MemberID,
          OrderID = c.CartID,
          OrderDate = c.DateAdded,
          OrderNotes = c.OrderNotes,
          OrderNumber = c.OrderNumber,
          OrderNumberPrefix = c.OrderNumberPrefix,
          OrderType = 
            c.isPOSOrder ? "POS" : 
            c.isReservation ? "Reservation" : 
            (c.RefundOrderNumber != null && c.RefundOrderNumber > 0) ? "Refund" : 
            (c.isClubOrder == true) ? "Club" :
            (c.isMobileOrder == true) ? "Mobile" :
            (c.isAdminOrder == true) ? ((c.isOrderGroup == true) ? "Admin-Group" : "Admin") :
            (c.AllocationID != null && c.AllocationID != Guid.Empty) ? "Allocation" :
            (c.isOrderGroup == true) ? "Store-Group" : 
            (c.isMobileOrder == true) ? "Mobile POS" :
            (c.isTeleSalesOrder == true) ? "TeleSales" :
            (c.isFacebookOrder == true) ? "Facebook" :
            "Store",
          ActualShipDate = c.ActualShipDate,
          AffiliateCommission = c.AffiliateCommission,
          AmountPaidByCreditCard = c.AmountPaidByCreditCard,
          AmountPaidByGiftCertificate = c.AmountPaidByGiftCertificate,
          AmountPaidByPointsProgram = c.AmountPaidByPointsProgram,
          BillingAddress1 = c.BillAddress,
          BillingAddress2 = c.BillAddress2,
          BillingBirthDate = c.BillBirthDate,
          BillingCity = c.BillCity,
          BillingCompany = c.BillCompany,
          BillingCountry = bco.CountryName,
          BillingCountryCode = bco.CountryCode,
          BillingEmail = c.BillEmail,
          BillingFirstName = c.BillFirstName,
          BillingLastName = c.BillLastName,
          BillingPhone = c.BillPhone,
          BillingProvince = c.BillProvince,
          BillingState = c.BillState,
          BillingZipCode = c.BillZipCode,
          BottleDeposit = c.BottleDeposit,
          CartPaymentType = cpt.CartPaymentType1,
          CashierName = c.CashierName,
          CouponDiscount = c.CouponDiscount,
          CreditCardExpirationMonth = c.CardExpiryMo,
          CreditCardExpirationYear = c.CardExpiryYr,
          CreditCardNumber = c.CardNumber,
          CreditCardType = cct.CreditCardType1,
          CustomizedShippingPrice = c.CustomizedShippingPrice,
          DateAdded = c.DateAdded,
          DateCompleted = c.DateCompleted,
          DateModified = c.DateModified,
          Donation = c.Donation,
          GiftCertificateNumber = c.GiftCertificateNo,
          GiftMessage = c.GiftMessage,
          IsGiftMessage = c.isGiftMessage,
          IsPickedUp = c.isPickedUp,
          IsTaxFree = c.isTaxFree,
          IsWillCall = c.WillCallLocationID == null ? false : true,
          Name = c.OrderName,
          NameonCreditCard = c.NameOnCard,
          PointsProgramAccountNumber = c.PointsProgramAccountNo,
          RefundOrderNumber = c.RefundOrderNumber,
          RegisterID = c.RegisterID,
          RequestShipDate = c.ShipDate,
          SalesOrderKey = 
            (c.OrderNumber == null) ? c.CartID.ToString() :
            (c.isPOSOrder ? "" :
            w.ShipCompliantOrderPrefix ?? "") + (c.OrderNumberPrefix ?? "") + c.OrderNumber + (c.RefundOrderNumber == null ? "" : ("-R" + c.RefundOrderNumber)),
          ShipmentStatus = c.ShipCompliantShipmentStatus,
          Shipping = c.Shipping,
          Shippingaddress1 = c.ShipAddress,
          ShippingAddress2 = c.ShipAddress2,
          ShippingAddressID = c.ShipMemberID,
          ShippingBirthDate = c.ShipBirthDate,
          ShippingCity = c.ShipCity,
          ShippingCompany = c.ShipCompany,
          ShippingCountry = sco.CountryName,
          ShippingCountryCode = sco.CountryCode,
          ShippingEmail = c.ShipEmail,
          ShippingFirstName = c.ShipFirstName,
          ShippingLastName = c.ShipLastName,
          ShippingNotes = c.ShippingNotes,
          ShippingPhone = c.ShipPhone,
          ShippingProvince = c.ShipProvince,
          ShippingState = c.ShipState,
          ShippingZipCode = c.ShipZipCode,
          SourceCode = c.SourceCode,
          Status = cs.CartStatus,
          Subtotal = c.OrderSubTotal,
          TaxableHandling = c.TaxableHandling,
          Taxes = c.Taxes,
          Total = c.OrderTotal,
          Carrier = ct.Carrier,
          TrackingNumber = ct.TrackingNumber,
          ShipDate = c.ActualShipDate,
          TransactionID = c.TransactionID
        };

      csvHelper.WriteCSVFile(outputFile, orderData);
    }


    /// <summary>
    /// Extract member notes from a winery
    /// </summary>
    /// <param name="wineryID"></param>
    /// <param name="db"></param>
    /// <param name="outputFile"></param>
    public static void ExtractMemberNotes(Guid wineryID, EWSDatabaseObjectsDataContext db, String outputFile)
    {
      PrintWithTimestamp("Extracting Member Notes...");

      var memberNoteData =
        from nt in db.MemberNotes
        join m in db.Members on nt.MemberID equals m.MemberID
        join w in db.Wineries on m.WineryID equals w.WineryID
        from ac in db.AdminContacts.Where(ac => ac.AdminContactID == nt.ModifiedBy).DefaultIfEmpty()
        where m.WineryID == wineryID
        select new
        {
          WineryName = w.WineryName,
          MemberID = m.MemberID,
          MemberNumber = m.MemberNumber,
          MemberNotesID = nt.MemberNotesID,
          DateAdded = nt.DateAdded,
          ModifierName = (ac.FirstName + " " + ac.LastName).Trim(),
          Notes = nt.Notes,
          NoteType = "Member Note"
        };

      csvHelper.WriteCSVFile(outputFile, memberNoteData);
    }


    /// <summary>
    /// Extract club memberships from a winery
    /// </summary>
    /// <param name="wineryID"></param>
    /// <param name="db"></param>
    /// <param name="outputFile"></param>
    public static void ExtractClubMembers(Guid wineryID, EWSDatabaseObjectsDataContext db, String outputFile)
    {
      PrintWithTimestamp("Extracting Club Memberships...");

      var clubMemberData =
        from cm in db.ClubMembers
        join m in db.Members on cm.MemberID equals m.MemberID
        join w in db.Wineries on m.WineryID equals w.WineryID
        join cl in db.ClubLevels on cm.ClubLevelID equals cl.ClubLevelID
        from clxcf in db.ClubLevelsXClubFrequencies.Where(clxcf => clxcf.ClubLevelID == cl.ClubLevelID).DefaultIfEmpty()
        from cf in db.ClubFrequencies.Where(cf => cf.ClubFrequencyID == clxcf.ClubLevelXClubFrequencyID).DefaultIfEmpty()
        from wst in db.WineShippingTypes.Where(wst => wst.WineShippingTypeID == cm.WineShippingTypeID).DefaultIfEmpty()
        from co in db.Countries.Where(co => co.CountryID == cm.ShipCountryID).DefaultIfEmpty()
        where m.WineryID == wineryID
        select new
        {
          WineryName = w.WineryName,
          MemberID = cm.MemberID,
          ClubName = cl.Title,
          ClubLevelID = cl.ClubLevelID,
          Affiliate = cm.Affiliate,
          AffiliateCommission = cm.AffiliateCommission,
          AltClubMembershipID = cm.AltClubMemberID,
          AltShippingAddressID = cm.AltShipMemberID,
          CancelDate = cm.CancelDate,
          ClubFrequency = cf.ClubFrequency1,
          CurrentNumberOfShipments = cm.CurrentNumberOfShipments,
          DateAdded = cm.DateAdded,
          DateModified = cm.DateModified,
          EndDate = cm.EndDate,
          GiftMessage = cm.GiftMessage,
          IsGiftMessage = cm.isGiftMessage,
          IsActive = cm.isActive,
          IsOnHold = cm.isOnHold,
          LastBatchNumber = cm.LastBatchNumber,
          LastBillDate = cm.LastBillDate,
          LastPaymentStatus = cm.LastPaymentStatus,
          NextBillDate = cm.NextBillDate,
          NumberOfBottles = cm.NumberOfBottles,
          OnHoldDate = cm.OnHoldDate,
          Rate = cm.Rate,
          ShippingAddress = cm.ShipAddress,
          ShippingAddress2 = cm.ShipAddress2,
          ShippingAddressID = cm.ShipMemberID,
          ShippingBirhtDate = cm.ShipBirthDate,
          ShippingCity = cm.ShipCity,
          ShippingCode = wst.ShippingCode,
          ShippingCompany = cm.ShipCompany,
          ShippingCountry = co.CountryName,
          ShippingCountryCode = co.CountryCode,
          ShippingEmail = cm.ShipEmail,
          ShippingFirstName = cm.ShipFirstName,
          ShippingLastName = cm.ShipLastName,
          ShippingPhone1 = cm.ShipPhone,
          ShippingPhone2 = cm.ShipPhone2,
          ShippingProvince = cm.ShipProvince,
          ShippingSalutation = cm.ShipSalutation,
          ShippingState = cm.ShipState,
          ShippingZipCode = cm.ShipZipCode,
          SourceCode = cm.SourceCode,
          TotalNumberofShipments = cm.TotalNumberOfShipments
        };

      csvHelper.WriteCSVFile(outputFile, clubMemberData);
    }


    /// <summary>
    /// Extract shipping addresses from a winery
    /// </summary>
    /// <param name="wineryID"></param>
    /// <param name="db"></param>
    /// <param name="outputFile"></param>
    public static void ExtractShipMembers(Guid wineryID, EWSDatabaseObjectsDataContext db, String outputFile)
    {
      PrintWithTimestamp("Extracting Shipping Addresses...");

      var shipMemberData =
        from sm in db.ShipMembers
        join m in db.Members on sm.MemberID equals m.MemberID
        join w in db.Wineries on m.WineryID equals w.WineryID
        from co in db.Countries.Where(co => co.CountryID == sm.CountryID).DefaultIfEmpty()
        where m.WineryID == wineryID
        select new
        {
          WineryName = w.WineryName,
          MemberID = m.MemberID,
          ShippingAddressID = sm.ShipMemberID,
          IsPrimary = sm.isPrimary,
          AddressName = sm.AddressName,
          Email = sm.Email,
          Salutation = sm.Salutation,
          FirstName = sm.FirstName,
          LastName = sm.LastName,
          Company = sm.Company,
          Address1 = sm.Address,
          Address2 = sm.Address2,
          City = sm.City,
          State = sm.State,
          Province = sm.Province,
          Country = co.CountryName,
          ZipCode = sm.ZipCode,
          Phone1 = sm.Phone,
          Phone2 = sm.Phone2,
          BirthDate = sm.BirthDate,
          DateAdded = sm.DateAdded,
          DateModified = sm.DateModified
        };

      csvHelper.WriteCSVFile(outputFile, shipMemberData);
    }


    /// <summary>
    /// Extract members from a winery
    /// </summary>
    /// <param name="wineryID"></param>
    /// <param name="db"></param>
    /// <param name="outputFile"></param>
    public static void ExtractMembers(Guid wineryID, EWSDatabaseObjectsDataContext db, String outputFile)
    {
      PrintWithTimestamp("Extracting Member Data...");

      // Get Member Data
      var memberData =
        from m in db.Members
        join w in db.Wineries on m.WineryID equals w.WineryID
        from cct in db.CreditCardTypes.Where(cct => cct.CreditCardTypeID == m.CreditCardTypeID).DefaultIfEmpty()
        from co in db.Countries.Where(co => co.CountryID == m.CountryID).DefaultIfEmpty()
        where m.WineryID == wineryID
        select new
        {
          WineryName = w.WineryName,
          MemberID = m.MemberID,
          AltAccountNumber = m.AltAccountNumber,
          MemberNumber = m.MemberNumber,
          Email = m.Email,
          Salutation = m.Salutation,
          FirstName = m.FirstName,
          LastName = m.LastName,
          Company = m.Company,
          Address = m.Address,
          Address2 = m.Address2,
          City = m.City,
          State = m.State,
          Province = m.Province,
          Country = co.CountryName,
          Phone = m.Phone,
          Phone2 = m.Phone2,
          BirthDate = m.BirthDate,
          DateAdded = m.DateAdded,
          DateModified = m.DateModified,
          CreditCardType = cct.CreditCardType1,
          NameonCreditCard = m.NameOnCard,
          CreditCardNumber = m.CreditCardNumber,
          CreditCardExpirationMonth = m.CreditCardExpMo,
          CreditCardExpirationYear = m.CreditCardExpYr,
          Username = m.Username,
          Password = m.Password,
          SourceCode = m.SourceCode,
          ZipCode = m.ZipCode
        };

      csvHelper.WriteCSVFile(outputFile, memberData);
    }


    static void PrintWithTimestamp(String message)
    {
      Console.WriteLine(DateTime.Now + ": " + message);
    }
  }
}

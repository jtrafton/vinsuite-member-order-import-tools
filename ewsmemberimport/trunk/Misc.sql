DECLARE @wineryID UNIQUEIDENTIFIER
SET @WineryID =
(
  SELECT WineryID
  FROM Wineries
  WHERE wineryName LIKE '%roche%' 
)

DECLARE @ACID UNIQUEIDENTIFIER
SET @ACID = 
(
  SELECT AdminContactID
  FROM   AdminContacts ac
  WHERE  ac.username = 'benb'
    AND  ac.password = 'ewinery11'
)

--SELECT TOP 200 *
--FROM   Members
--WHERE  wineryID   = @WineryID
--  AND  modifiedBy = @ACID

--SELECT m.AltMemberID, sa.*
--FROM   Members m
--LEFT OUTER JOIN ShipMembers sa ON m.memberID = sa.memberID
--WHERE  WineryID = @WineryID
--  AND m.AltMemberID IS NOT NULL

--BEGIN TRANSACTION
--DELETE FROM Members  WHERE memberID = '658F57CA-5225-482F-8A75-9E5020B3F098'
--ROLLBACK TRANSACTION

-------
 -- Update my AC Winery.
-------
--SELECT *
--FROM   AdminContacts ac
--WHERE  ac.AdminContactID = @ACID
  
--BEGIN TRANSACTION
--UPDATE AdminContacts SET wineryID = @wineryID
--WHERE AdminContactID = @ACID
--ROLLBACK TRANSACTION
----COMMIT TRANSACTION

--BEGIN TRANSACTION
--UPDATE memberTypes SET MemberType = 'Purchasers' 
--WHERE WineryID = @WineryID 
--  AND MemberType = 'Purchaser'
--ROLLBACK TRANSACTION


-------
 -- Create member types.
-------
--SELECT *
--FROM   MemberTypes
--WHERE  WineryID = @WineryID

--BEGIN TRANSACTION 
----INSERT INTO MemberTypes (MemberTypeID, WineryID, MemberType, isDeletable, Keyword)
----VALUES (NEWID(), @WineryID, 'Club Member', 0, 'ClubMember')
--ROLLBACK TRANSACTION

-------
 -- Create club levels.
-------
--BEGIN TRANSACTION
--DECLARE @ClubLevel VARCHAR(255)
--SET     @ClubLevel = 'Red Wine Only'

--INSERT INTO ClubLevels (ClubLevelID, WineryID, Title, isActive, DisplayName)
--VALUES (NEWID(), @WineryID, @ClubLevel, 1, @ClubLevel)

--SELECT *
--FROM   ClubLevels
--WHERE  WineryID = @WineryID
--ROLLBACK TRANSACTION


--SELECT *
--FROM   AdminContacts ac
--WHERE  ac.wineryID = 'D811AF59-9BEA-2186-3C30-9D7F5FA44A95'

--SELECT *
--FROM membertypes
--WHERE wineryID = 'D811AF59-9BEA-2186-3C30-9D7F5FA44A95'


--------
 -- Rollback an import.
--------

--BEGIN TRANSACTION
--DELETE FROM membersXmemberTypes 
--WHERE  memberID IN
--(
--  SELECT m.memberID
--  FROM   members m
--  WHERE  m.wineryID   = @WineryID
--    AND  m.modifiedBy = @ACID
--)

--DELETE FROM ClubMembers 
--WHERE  memberID IN
--(
--  SELECT m.memberID
--  FROM   members m
--  WHERE  m.wineryID   = @WineryID
--    AND  m.modifiedBy = @ACID
--)

--DELETE FROM ShipMembers 
--WHERE  memberID IN
--(
--  SELECT m.memberID
--  FROM   members m
--  WHERE  m.wineryID   = @WineryID
--    AND  m.modifiedBy = @ACID
--)

--DELETE FROM members 
--FROM   members m
--WHERE  m.wineryID   = @WineryID
--  AND  m.modifiedBy = @ACID

------ROLLBACK TRANSACTION
--COMMIT TRANSACTION


--BEGIN TRANSACTION
--------
 -- Backup before an import.
--------

--SELECT mxmt.*
--INTO   membersXmemberTypes_BV_BACKUP_20130201
--FROM   membersXmemberTypes mxmt
--INNER JOIN Members m ON mxmt.memberID = m.memberID
--WHERE  m.wineryID = @WineryID

--SELECT cm.*
--INTO   ClubMembers_BV_BACKUP_20130201
--FROM   ClubMembers cm
--INNER JOIN Members m ON cm.memberID = m.memberID
--WHERE  m.wineryID = @WineryID

--SELECT sm.*
--INTO   ShipMembers_BV_BACKUP_20130201
--FROM   ShipMembers sm
--INNER JOIN Members m ON sm.memberID = m.memberID
--WHERE  m.wineryID = @WineryID

--SELECT m.*
--INTO   Members_BV_BACKUP_20130201
--FROM   Members m
--WHERE  m.wineryID = @WineryID
--END TRANSACTION
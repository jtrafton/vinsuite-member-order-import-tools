﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EWSDataTaskSupport.EWSData;

namespace EWSDataTaskSupport
{
  public class ImportRunnerWines : ImportRunnerBase<Wine>
  {
    private Wine[] wines;
    private int record;

    public ImportRunnerWines(ImportProductWineRequest request, DataServiceHttps ds, ExcelWriter excelWriter, UpdateDataTaskRequest updTaskRequest, bool debug, int recordsPerRound)
      : base(request, ds, excelWriter, updTaskRequest, debug, recordsPerRound, ErrorReportProduct.PRODUCTTYPE.WINE)
    { }

    protected override void SetRequestProperty(IEnumerable<Wine> subList)
    {
      wines = subList.ToArray();
      wsRequest.Wines = wines;
    }

    protected override List<ErrorReportProduct> ProcessResponseProperty()
    {
      List<ErrorReportProduct> errors = new List<ErrorReportProduct>();
      record = 0;
      excelWriter.WriteToXLS(wsResponse.ProductWineResponses, "Wine Responses");

      foreach (AddUpdateProductWineResponse resp in wsResponse.ProductWineResponses)
      {
        if (!resp.IsSuccessful)
        {
          errors.Add(new ErrorReportProduct(
            wines[record],
            resp.ErrorMessage,
            ErrorReportBase.FixTheFile,
            "Import - AddUpdateWines",
            ErrorReportBase.ERRORTYPE.CRITICAL,
            productType));
        }
        ++record;
      }

      return errors;
    }
  }
}

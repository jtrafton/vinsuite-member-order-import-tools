﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EWSDataTaskSupport
{
  /// <summary>
  /// Contains an abstract definition of an error object
  /// </summary>
  public abstract class ErrorReportBase
  {
    public static readonly String ContactDeveloper = "Contact developer for assistance";
    public static readonly String FixTheFile = "Please make corrections to the data in your import file";
    public static readonly String FixTheAdmin = "Please make corrections to the data in the admin panel";

    public enum ERRORTYPE
    {
      WARNING,
      CRITICAL,
      EXCEPTION
    }

    protected String recordID;
    protected String errorMessage;
    protected String resolution;
    protected String source;
    protected ERRORTYPE errorType;

    protected ErrorReportBase()
    { }

    protected ErrorReportBase(String errorMessage, String resolution, String source, ERRORTYPE errorType)
    {
      this.errorMessage = errorMessage;
      this.resolution   = resolution;
      this.source       = source;
      this.errorType    = errorType;
    }

    public String RecordID
    {
      get { return this.recordID; }
    }    

    public String ErrorMessage
    {
      get { return this.errorMessage; }
    }

    public String Resolution
    {
      get { return this.resolution; }
    }

    public String Source
    {
      get { return this.source; }
    }

    public ERRORTYPE ErrorType()
    {
      return this.errorType;
    }
  }
}
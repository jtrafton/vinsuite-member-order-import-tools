﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EWSDataTaskSupport.EWSOrder;

namespace EWSDataTaskSupport
{
  public class ShippingTypeGetter : IErrorReporter
  {
    private Dictionary<String, WineryShippingOptions> shipOptions;
    private List<ErrorReportMember> errors;


    public ShippingTypeGetter(OrderService os, String wsUsername, String wsPassword)
    {
      shipOptions = new Dictionary<String, WineryShippingOptions>();
      this.errors = new List<ErrorReportMember>();

      GetShippingOptionsRequest gsReq = new GetShippingOptionsRequest();
      gsReq.Username = wsUsername;
      gsReq.Password = wsPassword;

      GetShippingOptionsResponse gsResp = DataTaskHelper.TryNTimes(os.GetShippingOptions, gsReq, 1, TimeSpan.FromSeconds(1));

      if (gsResp == null)
      {
        errors.Add(new ErrorReportMember("Could not retrieve shipping types from the web services", 
          ErrorReportMember.ContactDeveloper, 
          "ShippingTypeGetter",
          ErrorReportMember.ERRORTYPE.CRITICAL));
        return;
      }

      if (gsResp.IsSuccessful)
      {
        foreach (WineryShippingOptions wso in gsResp.WineriesShippingOptions)
        {
          if (wso.ShippingOptionType == "WINE")
            shipOptions.Add(wso.ShippingType.Trim().ToUpper(), wso);
        }
      }
      else
      {
        errors.Add(new ErrorReportMember("Unsuccessful web service response when retrieving shipping types: \n" + gsResp.ErrorMessage, 
          ErrorReportMember.ContactDeveloper, 
          "ShippingTypeGetter",
          ErrorReportMember.ERRORTYPE.CRITICAL));
      }
    }


    /// <summary>
    /// Returns any errors encountered when getting 
    /// shipping types from the web services
    /// </summary>
    /// <returns></returns>
    public List<ErrorReportMember> GetErrors()
    {
      return this.errors;
    }


    /// <summary>
    /// Check if a shipping type exists by name
    /// </summary>
    /// <param name="shippingType"></param>
    /// <returns></returns>
    public bool HasShippingType(String shippingType)
    {
      if (String.IsNullOrWhiteSpace(shippingType))
        return false;
      else
      {
        shippingType = shippingType.Trim().ToUpper();
        return this.shipOptions.ContainsKey(shippingType);
      }
    }


    /// <summary>
    /// Get a winery shipping type by name
    /// </summary>
    /// <param name="shippingType"></param>
    /// <returns></returns>
    public WineryShippingOptions GetShippingType(String shippingType)
    {
      WineryShippingOptions type = null;
      
      this.shipOptions.TryGetValue(shippingType.Trim().ToUpper(), out type);

      return type;
    }
  }
}

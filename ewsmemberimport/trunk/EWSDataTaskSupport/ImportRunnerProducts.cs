﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EWSDataTaskSupport.EWSData;

namespace EWSDataTaskSupport
{
  public class ImportRunnerProducts : ImportRunnerBase<Product>
  {
    public ImportRunnerProducts(ImportProductWineRequest wsRequest, DataServiceHttps ds, ExcelWriter excelWriter, UpdateDataTaskRequest updTaskRequest, bool debug, int recordsPerRound)
      : base(wsRequest, ds, excelWriter, updTaskRequest, debug, recordsPerRound, ErrorReportProduct.PRODUCTTYPE.PRODUCT)
    { }

    protected override void SetRequestProperty(IEnumerable<Product> subList)
    {
      wsRequest.Products = subList.ToArray();
    }

    protected override List<ErrorReportProduct> ProcessResponseProperty()
    {
      List<ErrorReportProduct> errors = new List<ErrorReportProduct>();
      int record = 0;
      excelWriter.WriteToXLS(wsResponse.ProductWineResponses, "Product Responses");

      foreach (AddUpdateProductWineResponse resp in wsResponse.ProductWineResponses)
      {
        if (!resp.IsSuccessful)
        {
          errors.Add(new ErrorReportProduct(
            wsRequest.Products[record],
            resp.ErrorMessage,
            ErrorReportBase.FixTheFile,
            "Import - AddUpdateProduct",
            ErrorReportBase.ERRORTYPE.CRITICAL,
            productType));
        }
        ++record;
      }

      return errors;
    }
  }
}

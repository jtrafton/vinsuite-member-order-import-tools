﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EWSDataTaskSupport.EWSData;

namespace EWSDataTaskSupport
{
  /// <summary>
  /// Abstract definition of product/wine/thirdparty wine importer
  /// Used both in EWSProductWineImport and EWSOrderImport
  /// </summary>
  /// <typeparam name="T"></typeparam>
  public abstract class ImportRunnerBase<T> : EWSCallerBase<ImportProductWineRequest, ImportProductWineResponse, ErrorReportProduct, T>
    where T: ProductWineBase
  {
    protected readonly ErrorReportProduct.PRODUCTTYPE productType;

    protected ImportRunnerBase(
      ImportProductWineRequest request, DataServiceHttps ds, ExcelWriter excelWriter, 
      UpdateDataTaskRequest updTaskRequest, bool debug, int recordsPerRound, ErrorReportProduct.PRODUCTTYPE productType)
      : base(ds.ImportProductsWines, request, ds, excelWriter, updTaskRequest, debug, recordsPerRound)
    {
      this.productType      = productType;
    }

    protected override ErrorReportProduct AddExceptionError(T item, string exceptionErr)
    {
      ErrorReportProduct error;
      error = new ErrorReportProduct(item, exceptionErr, ErrorReportProduct.ContactDeveloper, "Import", ErrorReportProduct.ERRORTYPE.EXCEPTION, productType);
      return error;
    }
  }
}

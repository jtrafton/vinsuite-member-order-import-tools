﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EWSDataTaskSupport.EWSData;

namespace EWSDataTaskSupport
{
  /// <summary>
  /// Product import specific error object
  /// </summary>
  public class ErrorReportProduct : ErrorReportBase
  {
    public enum PRODUCTTYPE
    {
      GENERAL= 1,
      PRODUCT = 1 << 1,
      WINE = 1 << 2
    }

    private String sku;
    private PRODUCTTYPE productType;

    public ErrorReportProduct()
    { }

    public ErrorReportProduct(String errorMessage, String resolution, String source, ERRORTYPE errorType)
      : base(errorMessage, resolution, source, errorType)
    {
      productType = PRODUCTTYPE.GENERAL;
    }

    public ErrorReportProduct(ProductWineBase pw, String errorMessage, String resolution, String source, ERRORTYPE errorType)
      : this(errorMessage, resolution, source, errorType)
    {
      this.recordID = pw.ProductID.ToString();
      this.sku = pw.ProductSKU;
    }

    public ErrorReportProduct(ProductWineBase pw, String errorMessage, String resolution, String source, ERRORTYPE errorType, PRODUCTTYPE productType)
      : this(pw, errorMessage, resolution, source, errorType)
    {
      this.productType = productType;
    }
    
    public String SKU
    {
      get { return sku; }
    }

    public PRODUCTTYPE ProductType
    {
      get { return productType; }
    }
  }
}

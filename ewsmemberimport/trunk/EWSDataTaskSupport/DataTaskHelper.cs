﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Web.Services.Protocols;
using System.Net;
using System.Text.RegularExpressions;
using System.Reflection;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;

namespace EWSDataTaskSupport
{
  public class DataTaskHelper
  {
    private static Random random = new Random();
    private static Dictionary<Type, PropertyInfo[]> targetTypeProperties = new Dictionary<Type, PropertyInfo[]>();

    #region Common helpful transforms
    private static Func<String, Object> defaultBool        = null;
    private static Func<String, Object> defaultDate        = null;
    private static Func<String, Object> defaultGuid        = null;
    private static Func<String, Object> defaultInt         = null;
    private static Func<String, Object> defaultDecimal     = null;
    #endregion

    //Range of invalid XML chars (from XML specs). NOTE: this range does not include 'gray area' chars that are valid but generally restricted.
    private static Regex badChars = new Regex(@"[^\x09\x0A\x0D\x20-\uD7FF\uE000-\uFFFD\u10000-u10FFFF]", RegexOptions.IgnoreCase | RegexOptions.Compiled);

    /// <summary>
    /// Generate a string of random characters of a specified length.
    /// Useful for generating passwords or password salt.
    /// </summary>
    /// <param name="length"></param>
    /// <returns></returns>
    public static String RandomString(int length)
    {
      length = Math.Abs(length);
      StringBuilder sb = new StringBuilder(length, length);

      for (int i = 0; i < length; ++i)
        sb.Append((char)random.Next(33, 127));

      return sb.ToString();
    }


    /// <summary>
    /// Transforms 1/0, yes/no, true/false to a boolean. Also sets true if string starts with 'Third'
    /// </summary>
    public static Func<String, Object> DefaultBooleanTransform
    {
      get 
      {
        if (defaultBool == null)
        {
          defaultBool = (cellValue) =>
          {
            if (string.IsNullOrWhiteSpace(cellValue))
              return false;

            if (cellValue == "1" ||
              String.Equals("true", cellValue, StringComparison.OrdinalIgnoreCase) ||
              String.Equals("yes", cellValue, StringComparison.OrdinalIgnoreCase) ||
              cellValue.StartsWith("Third", StringComparison.InvariantCultureIgnoreCase)) 
            {
              return true;
            }

            if (cellValue == "0" ||
              String.Equals("false", cellValue, StringComparison.OrdinalIgnoreCase) ||
              String.Equals("no", cellValue, StringComparison.OrdinalIgnoreCase))
            {
              return false;
            }
            return null;
            //throw new Exception("DataTaskHelper: Invalid boolean format: \"" + cellValue + "\"");
          };
        }

        return defaultBool; 
      }
    }


    /// <summary>
    /// Transforms a date string into a date or null if it doesn't parse
    /// </summary>
    public static Func<String, Object> DefaultDateTransform
    {
      get
      {
        if (defaultDate == null)
        {
          defaultDate = (cellValue) =>
          {
            DateTime t;

            if (String.IsNullOrWhiteSpace(cellValue))
              return null;

            if (DateTime.TryParse(cellValue, out t))
              return t;
            else
            {
              return null;
              //throw new Exception("Invalid date format: \"" + cellValue + "\"");
            }
          };
        }

        return defaultDate;
      }
    }


    /// <summary>
    /// Transforms a guid string into a guid or null if it doesn't parse
    /// </summary>
    public static Func<String, Object> DefaultGuidTransform
    {
      get
      {
        defaultGuid = (cellValue) =>
        {
          Guid g;
          if (String.IsNullOrWhiteSpace(cellValue))
            return null;

          if (Guid.TryParse(cellValue, out g))
            return g;
          else
            return null;
        };
        return defaultGuid;
      }
    }

    /// <summary>
    /// Transforms an int string into an int or null if it doesn't parse
    /// </summary>
    public static Func<String, Object> DefaultIntTransform
    {
      get
      {
        defaultInt = (cellValue) =>
        {
          int i;
          if (String.IsNullOrWhiteSpace(cellValue))
            return null;

          if (Int32.TryParse(cellValue, out i))
            return i;
          else
            return null;
        };
        return defaultInt;
      }
    }

    /// <summary>
    /// Transforms a decimal string into a decimal or null if it doesn't parse
    /// </summary>
    public static Func<String, Object> DefaultDecimalTransform
    {
      get
      {
        defaultDecimal = (cellValue) =>
        {
          Decimal d;
          if (String.IsNullOrWhiteSpace(cellValue))
            return null;

          if (Decimal.TryParse(cellValue, out d))
            return d;
          else
            return null;
        };
        return defaultDecimal;
      }
    }

    /// <summary>
    /// Call a method wrapped in a try/catch block
    /// </summary>
    /// <typeparam name="RQ">Request type</typeparam>
    /// <typeparam name="RS">Response type</typeparam>
    /// <param name="method">Web service method</param>
    /// <param name="input">Request object</param>
    /// <param name="maxTries">Number of retries before failure</param>
    /// <param name="retryWait">How long to wait between retries</param>
    /// <returns>A valid instance of RS if the call succeeded, otherwise null</returns>
    public static RS TryNTimes<RQ, RS>(Func<RQ, RS> method, RQ input, int maxTries, TimeSpan retryWait)
      where RS : class
    {
      int failCount = 0;
      int originalTimeout = 30000;
      RS returnValue = null;

      WebClientProtocol wsClient = method.Target as WebClientProtocol;
      if (wsClient != null)
      {
        // We're calling a web method of some kind
        // Set the timeout to 5 minutes so this method won't wait for eternity
        originalTimeout = wsClient.Timeout;
        wsClient.Timeout = 300000;
      }

      do
      {
        try
        {
          returnValue = method.Invoke(input);
          break;
        }
        catch (WebException wex)
        {
          HttpWebResponse response = wex.Response as HttpWebResponse;
          if (response != null)
          {
            switch (response.StatusDescription)
            {
              case "Bad Request":
                // skip batch with bad request
                return returnValue;
              default:
                break;
            }
          }

          ++failCount;
          Thread.Sleep(retryWait);
        }
        catch (Exception ex)
        {
          ++failCount;
          Thread.Sleep(retryWait);
        }
      } while (failCount < maxTries);

      if (wsClient != null)
      {
        // Restore the original client timeout value
        wsClient.Timeout = originalTimeout;
      }

      return returnValue;
    }


    /// <summary>
    /// Remove invalid characters from strings in the object.
    /// Uses recursion to look into arrays and complex objects.  
    /// </summary>
    /// <param name="target">Object to correct</param>
    /// <param name="isNullifyEmptyStrings">Set to true if you want to nullify empty strings</param>
    public static void CorrectBadChars(Object target, bool isNullifyEmptyStrings)
    {
      Object propertyValue;
      Type targetType;
      PropertyInfo[] properties;

      if (target == null)
        return;

      targetType = target.GetType();

      if (targetTypeProperties.ContainsKey(targetType))
        properties = targetTypeProperties[targetType];
      else
      {
        properties = targetType.GetProperties().Where(p => !p.PropertyType.IsValueType).ToArray();
        targetTypeProperties.Add(targetType, properties);
      }

      foreach (PropertyInfo p in properties)
      {
        if (p.PropertyType.Name == "String")
        {
          propertyValue = p.GetValue(target, null);
          if (propertyValue != null)
          {
            p.SetValue(target, badChars.Replace(propertyValue.ToString(), ""), null);
            if (isNullifyEmptyStrings && String.IsNullOrWhiteSpace(propertyValue.ToString()))
              p.SetValue(target, null, null);
          }
          continue;
        }

        switch (p.PropertyType.BaseType.Name.ToString())
        {
          case "Array":
            Array arrayObject = (Array)p.GetValue(target, null);
            // Recursivelly correct an array data
            if (arrayObject != null)
            {
              foreach (var item in arrayObject)
              {
                CorrectBadChars(item, isNullifyEmptyStrings);
              }
            }
            break;

          case "Object":
            propertyValue = p.GetValue(target, null);
            if (propertyValue != null)
              CorrectBadChars(propertyValue, isNullifyEmptyStrings);
            break;
        }
      }
    }


    /// <summary>
    /// Per round sublist generator
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="mainList">Main Collection</param>
    /// <param name="itemsPerRound"></param>
    /// <returns></returns>
    public static IEnumerable<List<T>> getSubListsPerRound<T>(IEnumerable<T> mainList, int itemsPerRound)
    {
      if (mainList == null)
        yield return null;

      int round = 0;
      int lastRecord = 0;
      int mainListCount = mainList.Count();
      while (lastRecord < mainListCount)
      {
        yield return mainList.Skip(lastRecord).Take(itemsPerRound).ToList();
        ++round;
        lastRecord = round * itemsPerRound;
      }
    }
  }
}

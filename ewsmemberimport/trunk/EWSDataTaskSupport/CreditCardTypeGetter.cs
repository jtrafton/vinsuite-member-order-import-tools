﻿using System;
using System.Collections.Generic;
using EWSDataTaskSupport.EWSOrder;
using System.Collections;

namespace EWSDataTaskSupport
{
  public class CreditCardTypeGetter : IErrorReporter
  {
    private Dictionary<String, CreditCardTypeInfo> cardTypes;
    private List<ErrorReportMember> errors;
    

    public CreditCardTypeGetter(OrderService serv, String username, String password)
    {
      this.cardTypes = new Dictionary<String, CreditCardTypeInfo>();
      this.errors = new List<ErrorReportMember>();

      GetCreditCardTypeRequest req = new GetCreditCardTypeRequest();
      GetCreditCardTypeResponse resp;

      // Set up the credentials.
      req.Username = username;
      req.Password = password;

      // Get the member types.
      resp = DataTaskHelper.TryNTimes(serv.GetCreditCardType, req, 1, TimeSpan.FromSeconds(1));

      if (resp == null)
      {
        errors.Add(new ErrorReportMember("Could not retrieve credit card types from the web services", 
          ErrorReportMember.ContactDeveloper,
          "CreditCardGetter",
          ErrorReportMember.ERRORTYPE.CRITICAL));
        return;
      }

      if (resp.IsSuccessful)
      {
        this.cardTypes = new Dictionary<String, CreditCardTypeInfo>();

        foreach (CreditCardTypeInfo ct in resp.CreditCardTypes)
          this.cardTypes.Add(ct.CreditCardTypeShort.Trim().ToUpper(), ct);
      }
      else
        errors.Add(new ErrorReportMember("Unsuccessful web service response when retrieving credit card types",
          ErrorReportMember.ContactDeveloper,
          "CreditCardGetter",
          ErrorReportMember.ERRORTYPE.CRITICAL));
    }


    /// <summary>
    /// Get a list of the acceptable credit card types
    /// </summary>
    public List<String> ValidTypes
    {
      get
      {
        List<String> keys = new List<String>();

        foreach (String key in this.cardTypes.Keys)
          keys.Add(key);

        return keys;
      }
    }


    /// <summary>
    /// Returns any errors getting the valid
    /// credit card type from the web services
    /// </summary>
    /// <returns></returns>
    public List<ErrorReportMember> GetErrors()
    {
      return this.errors;
    }


    /// <summary>
    /// Check if a credit card type is already known
    /// </summary>
    /// <param name="type"></param>
    /// <returns></returns>
    public bool HasCreditCardType(String type)
    {
      if (String.IsNullOrEmpty(type))
        return false;
      else
      {
        type = type.Trim().ToUpper();
        return this.cardTypes.ContainsKey(type);
      }
    }


    /// <summary>
    /// Return a credit card type based on name
    /// </summary>
    /// <param name="type"></param>
    /// <returns></returns>
    public CreditCardTypeInfo GetCreditCardType(String type)
    {
      CreditCardTypeInfo cc = null;

      this.cardTypes.TryGetValue(type.Trim().ToUpper(), out cc);

      return cc;
    }
  }
}

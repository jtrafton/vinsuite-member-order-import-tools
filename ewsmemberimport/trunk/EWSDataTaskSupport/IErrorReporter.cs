﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EWSDataTaskSupport
{
  public interface IErrorReporter
  {
    List<ErrorReportMember> GetErrors();
  }
}

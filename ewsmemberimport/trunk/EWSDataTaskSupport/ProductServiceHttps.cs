﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EWSDataTaskSupport.EWSProduct;

namespace EWSDataTaskSupport
{
  /// <summary>
  /// This class exposes a normal ProductService object 
  /// but changes the URL to use HTTPS
  /// </summary>
  public class ProductServiceHttps : ProductService
  {
    public ProductServiceHttps()
      : base()
    {
      if (!(this.Url.Contains("localhost") || this.Url.Contains("127.0.0.1")))
        this.Url = this.Url.Replace("http:", "https:");
    }
  }
}

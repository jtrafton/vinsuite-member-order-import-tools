﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EWSDataTaskSupport
{
  public class WineryData
  {
    private ClubGetter cg;
    private CreditCardTypeGetter cctg;
    private ShippingTypeGetter stg;

    public WineryData(MemberServiceHttps ms, OrderServiceHttps os, string wsUsername, string wsPassword)
    {
      cg   = new ClubGetter(ms, wsUsername, wsPassword);
      cctg = new CreditCardTypeGetter(os, wsUsername, wsPassword);
      stg  = new ShippingTypeGetter(os, wsUsername, wsPassword);

      if (cg == null)
        throw new Exception("Failed to initialize club getter");
      if (cctg == null)
        throw new Exception("Failed to initialize credit card type getter");
      if (stg == null)
        throw new Exception("Failed to initialize shipping type getter");
    }

    #region Public Getters
    public ClubGetter clubGetter 
    { 
      get 
      { 
        return cg; 
      } 
    }

    public CreditCardTypeGetter ccGetter 
    { 
      get 
      { 
        return cctg; 
      } 
    }

    public ShippingTypeGetter shipTypeGetter 
    {
      get 
      { 
        return stg; 
      }
    }
    #endregion
  }
}

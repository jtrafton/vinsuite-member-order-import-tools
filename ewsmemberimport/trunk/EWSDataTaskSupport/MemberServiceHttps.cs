﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EWSDataTaskSupport.EWSMember;

namespace EWSDataTaskSupport
{
  /// <summary>
  /// This class exposes a normal MemberService object 
  /// but changes the URL to use HTTPS
  /// </summary>
  public class MemberServiceHttps : MemberService
  {
    public MemberServiceHttps()
      : base()
    {
      if (!(this.Url.Contains("localhost") || this.Url.Contains("127.0.0.1")))
        this.Url = this.Url.Replace("http:", "https:");
    }
  }
}

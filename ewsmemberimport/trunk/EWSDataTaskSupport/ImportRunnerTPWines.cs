﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EWSDataTaskSupport.EWSData;

namespace EWSDataTaskSupport
{
  public class ImportRunnerTPWines : ImportRunnerBase<ThirdPartyWine>
  {
    private ThirdPartyWine[] tpw;
    private int record;

    public ImportRunnerTPWines(ImportProductWineRequest request, DataServiceHttps ds, ExcelWriter excelWriter, UpdateDataTaskRequest updTaskRequest, bool debug, int recordsPerRound)
      : base(request, ds, excelWriter, updTaskRequest, debug, recordsPerRound, ErrorReportProduct.PRODUCTTYPE.WINE)
    { }

    protected override void SetRequestProperty(IEnumerable<ThirdPartyWine> subList)
    {
      tpw = subList.ToArray();
      wsRequest.ThirdPartyWines = tpw;
    }

    protected override List<ErrorReportProduct> ProcessResponseProperty()
    {
      List<ErrorReportProduct> errors = new List<ErrorReportProduct>();
      record = 0;
      excelWriter.WriteToXLS(wsResponse.ThirdPartyWineResponses, "Third Party Wine Responses");

      foreach (AddUpdateThirdPartyWineResponse resp in wsResponse.ThirdPartyWineResponses)
      {
        if (!resp.IsSuccessful)
        {
          errors.Add(new ErrorReportProduct(
            tpw[record],
            resp.ErrorMessage,
            ErrorReportBase.FixTheFile,
            "Import - AddUpdateThirdPartyWines",
            ErrorReportBase.ERRORTYPE.CRITICAL,
            productType));
        }
        ++record;
      }

      return errors;
    }
  }
}

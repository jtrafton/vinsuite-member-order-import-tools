﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.IO;
using System.Linq;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;

namespace EWSDataTaskSupport
{
  public class ExcelWriter
  {
    private HSSFWorkbook workbook;
    private String filePath;
    private object oldValue;
    private ICellStyle headerCellStyle, normalCellStyle, highlightCellStyle;

    /// <summary>
    /// Creates excel file with a given spreadsheet name at a specified path
    /// </summary>
    /// <param name="fileToSave"></param>
    /// <param name="sheetName"></param>
    public ExcelWriter(String filePath)
    {
      this.filePath = filePath;
      workbook = new HSSFWorkbook();
      oldValue = null;

      #region Create styles
      headerCellStyle = workbook.CreateCellStyle();      
      headerCellStyle.FillBackgroundColor = NPOI.HSSF.Util.HSSFColor.GOLD.index;
      headerCellStyle.FillForegroundColor = NPOI.HSSF.Util.HSSFColor.GOLD.index;
      headerCellStyle.FillPattern = FillPatternType.SOLID_FOREGROUND;

      normalCellStyle = workbook.CreateCellStyle();
      normalCellStyle.FillBackgroundColor = NPOI.HSSF.Util.HSSFColor.COLOR_NORMAL;
      normalCellStyle.FillForegroundColor = NPOI.HSSF.Util.HSSFColor.COLOR_NORMAL;
      normalCellStyle.FillPattern = FillPatternType.SOLID_FOREGROUND;

      highlightCellStyle = workbook.CreateCellStyle();
      highlightCellStyle.FillBackgroundColor = NPOI.HSSF.Util.HSSFColor.GREY_25_PERCENT.index;
      highlightCellStyle.FillForegroundColor = NPOI.HSSF.Util.HSSFColor.GREY_25_PERCENT.index;
      highlightCellStyle.FillPattern = FillPatternType.SOLID_FOREGROUND;
      #endregion
    }

    /// <summary>
    /// Writes enumerable object to a spreadsheet
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="list"></param>
    /// <returns>Success status</returns>
    public bool WriteToXLS<T>(IEnumerable<T> list, String sheetName)
    {
      ISheet sheet = workbook.GetSheet(sheetName);
      IEnumerable<PropertyInfo> typeProperties = typeof(T).GetProperties(BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public);
      IRow currentRow;
      object value;
      int cellNum = 0;
      int rowNum;      
      bool isHighlight = false;
      bool isFirstCell = true;

      if (sheet == null)
      {
        sheet = workbook.CreateSheet(sheetName);
        // Generate random tab color
        sheet.TabColorIndex = IndexedColors.AQUA.Index;
        // Generate Headers
        rowNum = 0;
        currentRow = sheet.CreateRow(rowNum);
        foreach (PropertyInfo property in typeProperties)
        {
          ICell currentCell = currentRow.CreateCell(cellNum);
          currentCell.SetCellValue(property.Name);
          currentCell.CellStyle = headerCellStyle;
          ++cellNum;
        }
      }
      else
      {
        rowNum = sheet.LastRowNum;
      }

      if (list == null)
        return false;

      // Write list to a sheet
      foreach (T row in list)
      {
        
        ++rowNum;
        currentRow = sheet.CreateRow(rowNum);
        cellNum = 0;
        foreach (PropertyInfo property in typeProperties)
        {
          ICell currentCell = currentRow.CreateCell(cellNum);
          value = property.GetValue(row, null);
          if (value != null)
            currentCell.SetCellValue(value.ToString());
          ++cellNum;

          // if first cell of the row (AltMemberID) is different from the previous, change highlighting
          if (isFirstCell)
          {
            if (!Object.Equals(oldValue, value))
              isHighlight = !isHighlight;
            oldValue = value;
            isFirstCell = false;
          }
          currentCell.CellStyle = isHighlight ? highlightCellStyle : normalCellStyle;
        }
        isFirstCell = true;
      }

      //Write the stream data of workbook to the specified directory
      try
      {
        using (FileStream file = new FileStream(this.filePath, FileMode.OpenOrCreate))
        {
          workbook.Write(file);
        }
      }
      catch (Exception ex)
      {
        return false;
      }

      return true;
    }

    private void GroupBy(String sheetName, String column)
    {
      #region Validate parameters
      ISheet sheet = workbook.GetSheet(sheetName);
      // Exit call if sheet is not present
      if (sheet == null)
        return;
      else
      {
        // If sheet exists, validate column name
        if (!sheet.GetRow(0).Cells.Exists(c => c.StringCellValue.Equals(column)))
          return;
      }
      #endregion

      #region Sort by column
      
      #endregion

      #region Merge cells with the same value
      #endregion

      #region Highlight every second row
      #endregion
    }
  }
}

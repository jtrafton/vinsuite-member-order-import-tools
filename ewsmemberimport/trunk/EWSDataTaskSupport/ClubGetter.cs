﻿using System;
using System.Collections.Generic;
using EWSDataTaskSupport.EWSMember;

namespace EWSDataTaskSupport
{
  public class ClubGetter : IErrorReporter
  {
    private Dictionary<String, WineClub> clubs;
    private String wsUsername;
    private String wsPassword;
    private List<ErrorReportMember> errors;

    /**
     * Pull a list of clubs and add them to a dictionary.
     */
    public ClubGetter(MemberService serv, String username, String password)
    {
      this.wsUsername = username;
      this.wsPassword = password;
      this.errors = new List<ErrorReportMember>();

      GetClubRequest req = new GetClubRequest();
      GetClubResponse resp;

      // Set up the credentials.
      req.Username = wsUsername;
      req.Password = wsPassword;

      // Get all clubs, not just active ones.
      req.IncludeInactiveClubs = true;
        
      // Get the clubs.
      resp = DataTaskHelper.TryNTimes(serv.GetClub, req, 1, TimeSpan.FromSeconds(1));

      if (resp == null)
      {
        errors.Add(new ErrorReportMember("Could not retrieve club levels from the web services", 
          ErrorReportMember.ContactDeveloper,
          "ClubGetter",
          ErrorReportMember.ERRORTYPE.CRITICAL));
        return;
      }

      if (resp.IsSuccessful)
      {
        this.clubs = new Dictionary<String, WineClub>();

        foreach (WineClub club in resp.WineClubs)
        {
          if (!this.clubs.ContainsKey(club.Title.Trim().ToUpper()))
            this.clubs.Add(club.Title.Trim().ToUpper(), club);
          else
            Console.WriteLine("Duplicate club level: {0}", club.Title);
        }
      }
      else
        errors.Add(new ErrorReportMember("Unsuccessful web service response when retrieving club levels: \n" + resp.ErrorMessage, 
          ErrorReportMember.ContactDeveloper,
          "ClubGetter",
          ErrorReportMember.ERRORTYPE.CRITICAL));
    }


    /// <summary>
    /// Return the errors found when getting the
    /// existing club levels from the winery
    /// </summary>
    /// <returns></returns>
    public List<ErrorReportMember> GetErrors()
    {
      return this.errors;
    }


    /// <summary>
    /// Check if a club exists by club title
    /// </summary>
    /// <param name="clubTitle"></param>
    /// <returns></returns>
    public bool HasClubLevel(String clubTitle)
    {
      if (String.IsNullOrWhiteSpace(clubTitle))
        return false;
      else
      {
        clubTitle = clubTitle.Trim().ToUpper();
        return this.clubs.ContainsKey(clubTitle);
      }
    }


    /// <summary>
    /// Get a club level based on title
    /// </summary>
    /// <param name="clubTitle"></param>
    /// <returns></returns>
    public WineClub GetClubLevel(String clubTitle)
    {
      WineClub club = null;

      this.clubs.TryGetValue(clubTitle.Trim().ToUpper(), out club);

      return club;
    }
  }
}

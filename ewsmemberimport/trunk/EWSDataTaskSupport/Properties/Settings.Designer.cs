﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace EWSDataTaskSupport.Properties {
    
    
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.VisualStudio.Editors.SettingsDesigner.SettingsSingleFileGenerator", "11.0.0.0")]
    internal sealed partial class Settings : global::System.Configuration.ApplicationSettingsBase {
        
        private static Settings defaultInstance = ((Settings)(global::System.Configuration.ApplicationSettingsBase.Synchronized(new Settings())));
        
        public static Settings Default {
            get {
                return defaultInstance;
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.SpecialSettingAttribute(global::System.Configuration.SpecialSetting.ConnectionString)]
        [global::System.Configuration.DefaultSettingValueAttribute("Data Source=maindb;Initial Catalog=ewinerysolutionswineries;Persist Security Info" +
            "=True;User ID=webservices;Password=@Ddmiss4ble")]
        public string ewinerysolutionswineriesConnectionString {
            get {
                return ((string)(this["ewinerysolutionswineriesConnectionString"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.SpecialSettingAttribute(global::System.Configuration.SpecialSetting.ConnectionString)]
        [global::System.Configuration.DefaultSettingValueAttribute("Data Source=TreasuryDB;Initial Catalog=ewinerysolutionswineries;Persist Security " +
            "Info=True;User ID=webservices;Password=@Ddmiss4ble")]
        public string ewinerysolutionswineriesConnectionStringTreasury {
            get {
                return ((string)(this["ewinerysolutionswineriesConnectionStringTreasury"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.SpecialSettingAttribute(global::System.Configuration.SpecialSetting.ConnectionString)]
        [global::System.Configuration.DefaultSettingValueAttribute("Data Source=DiageoDB;Initial Catalog=ewinerysolutionswineries;Persist Security In" +
            "fo=True;User ID=webservices;Password=@Ddmiss4ble")]
        public string ewinerysolutionswineriesConnectionStringDiageo {
            get {
                return ((string)(this["ewinerysolutionswineriesConnectionStringDiageo"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.SpecialSettingAttribute(global::System.Configuration.SpecialSetting.ConnectionString)]
        [global::System.Configuration.DefaultSettingValueAttribute("Data Source=FostersAUDB;Initial Catalog=ewinerysolutionswineries;Persist Security" +
            " Info=True;User ID=webservices;Password=@Ddmiss4ble")]
        public string ewinerysolutionswineriesConnectionStringFostersAU {
            get {
                return ((string)(this["ewinerysolutionswineriesConnectionStringFostersAU"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.SpecialSettingAttribute(global::System.Configuration.SpecialSetting.ConnectionString)]
        [global::System.Configuration.DefaultSettingValueAttribute("Data Source=KendallJacksonDB;Initial Catalog=ewinerysolutionswineries;Persist Sec" +
            "urity Info=True;User ID=webservices;Password=@Ddmiss4ble")]
        public string ewinerysolutionswineriesConnectionStringKJ {
            get {
                return ((string)(this["ewinerysolutionswineriesConnectionStringKJ"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.SpecialSettingAttribute(global::System.Configuration.SpecialSetting.ConnectionString)]
        [global::System.Configuration.DefaultSettingValueAttribute("Data Source=TestDB;Initial Catalog=ewinerysolutionswineries;Persist Security Info" +
            "=True;User ID=webservices;Password=@Ddmiss4ble")]
        public string ewinerysolutionswineriesConnectionStringTest {
            get {
                return ((string)(this["ewinerysolutionswineriesConnectionStringTest"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("https://ws.ewinerysolutions.com/1.12/")]
        public string WS_ProductionURL {
            get {
                return ((string)(this["WS_ProductionURL"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("https://ws-qa.ewinerysolutions.com/1.12/")]
        public string WS_TestURL {
            get {
                return ((string)(this["WS_TestURL"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("https://ws-uat.ewinerysolutions.com/1.12/")]
        public string WS_UATURL {
            get {
                return ((string)(this["WS_UATURL"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.SpecialSettingAttribute(global::System.Configuration.SpecialSetting.ConnectionString)]
        [global::System.Configuration.DefaultSettingValueAttribute("Data Source=UATDB;Initial Catalog=ewinerysolutionswineries;Persist Security Info=" +
            "True;User ID=webservices;Password=@Ddmiss4ble")]
        public string ewinerysolutionswineriesConnectionStringUAT {
            get {
                return ((string)(this["ewinerysolutionswineriesConnectionStringUAT"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.SpecialSettingAttribute(global::System.Configuration.SpecialSetting.ConnectionString)]
        [global::System.Configuration.DefaultSettingValueAttribute("Data Source=diageodb;Initial Catalog=ewinerysolutionswineries;Persist Security In" +
            "fo=True;User ID=benningfield;Password=Contr4c!ual")]
        public string ewinerysolutionswineriesConnectionString1 {
            get {
                return ((string)(this["ewinerysolutionswineriesConnectionString1"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.SpecialSettingAttribute(global::System.Configuration.SpecialSetting.ConnectionString)]
        [global::System.Configuration.DefaultSettingValueAttribute("Data Source=MainDB_RPT;Initial Catalog=ewinerysolutionswineriesDB05;Persist Secur" +
            "ity Info=True;User ID=report;Password=ew!ne@y")]
        public string ewinerysolutionswineriesConnectionString_rpt {
            get {
                return ((string)(this["ewinerysolutionswineriesConnectionString_rpt"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("Data Source=MainDB_RPT;Initial Catalog=TUS_Reporting_DB;Persist Security Info=Tru" +
            "e;User ID=report;Password=ew!ne@y")]
        public string ewinerysolutionswineriesConnectionStringTreasury_rpt {
            get {
                return ((string)(this["ewinerysolutionswineriesConnectionStringTreasury_rpt"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("Data Source=MainDB_RPT;Initial Catalog=TAU_Reporting_DB;Persist Security Info=Tru" +
            "e;User ID=report;Password=ew!ne@y")]
        public string ewinerysolutionswineriesConnectionStringFostersAU_rpt {
            get {
                return ((string)(this["ewinerysolutionswineriesConnectionStringFostersAU_rpt"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("Data Source=MainDB_RPT;Initial Catalog=Diageo_Reporting_DB;Persist Security Info=" +
            "True;User ID=report;Password=ew!ne@y")]
        public string ewinerysolutionswineriesConnectionStringDiageo_rpt {
            get {
                return ((string)(this["ewinerysolutionswineriesConnectionStringDiageo_rpt"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("Data Source=MainDB_RPT;Initial Catalog=KJ_Reporting_DB;Persist Security Info=True" +
            ";User ID=report;Password=ew!ne@y")]
        public string ewinerysolutionswineriesConnectionStringKJ_rpt {
            get {
                return ((string)(this["ewinerysolutionswineriesConnectionStringKJ_rpt"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.SpecialSettingAttribute(global::System.Configuration.SpecialSetting.WebServiceUrl)]
        [global::System.Configuration.DefaultSettingValueAttribute("http://ws.ewinerysolutions.com/1.13_DataService/Data.asmx")]
        public string EWSDataTaskSupport_EWSData_DataService {
            get {
                return ((string)(this["EWSDataTaskSupport_EWSData_DataService"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.SpecialSettingAttribute(global::System.Configuration.SpecialSetting.WebServiceUrl)]
        [global::System.Configuration.DefaultSettingValueAttribute("http://ws.ewinerysolutions.com/1.13_DataService/Product.asmx")]
        public string EWSDataTaskSupport_EWSProduct_ProductService {
            get {
                return ((string)(this["EWSDataTaskSupport_EWSProduct_ProductService"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.SpecialSettingAttribute(global::System.Configuration.SpecialSetting.WebServiceUrl)]
        [global::System.Configuration.DefaultSettingValueAttribute("http://ws.ewinerysolutions.com/1.13_DataService/Website.asmx")]
        public string EWSDataTaskSupport_EWSWebsite_WebsiteService {
            get {
                return ((string)(this["EWSDataTaskSupport_EWSWebsite_WebsiteService"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.SpecialSettingAttribute(global::System.Configuration.SpecialSetting.WebServiceUrl)]
        [global::System.Configuration.DefaultSettingValueAttribute("http://ws.ewinerysolutions.com/1.13_DataService/List.asmx")]
        public string EWSDataTaskSupport_EWSList_ListService {
            get {
                return ((string)(this["EWSDataTaskSupport_EWSList_ListService"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.SpecialSettingAttribute(global::System.Configuration.SpecialSetting.WebServiceUrl)]
        [global::System.Configuration.DefaultSettingValueAttribute("http://ws.ewinerysolutions.com/1.13_DataService/Order.asmx")]
        public string EWSDataTaskSupport_EWSOrder_OrderService {
            get {
                return ((string)(this["EWSDataTaskSupport_EWSOrder_OrderService"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.SpecialSettingAttribute(global::System.Configuration.SpecialSetting.WebServiceUrl)]
        [global::System.Configuration.DefaultSettingValueAttribute("http://ws.ewinerysolutions.com/1.13_DataService/Member.asmx")]
        public string EWSDataTaskSupport_EWSMember_MemberService {
            get {
                return ((string)(this["EWSDataTaskSupport_EWSMember_MemberService"]));
            }
        }
    }
}

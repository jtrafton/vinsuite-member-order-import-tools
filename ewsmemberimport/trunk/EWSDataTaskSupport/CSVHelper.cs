﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Collections;
using System.IO;
using System.Data;
using System.Text.RegularExpressions;

namespace EWSDataTaskSupport
{
  public class CSVHelper
  {
    private Dictionary<Type, PropertyInfo[]> typeProperties;
    private Dictionary<Type, Func<Object, String>> typeTransforms;
    private Regex newlineRegex;

    /// <summary>
    /// Assists in serializing objects to CSV format
    /// </summary>
    public CSVHelper()
    {
      typeProperties = new Dictionary<Type, PropertyInfo[]>();
      typeTransforms = new Dictionary<Type, Func<Object, String>>();
      newlineRegex = new Regex(@"(\r|\n)", RegexOptions.Compiled);
    }


    public void AddTransform<T>(Func<Object, String> func)
    {
      Type type = typeof(T);

      if (typeTransforms.ContainsKey(type))
        typeTransforms[type] = func;
      else
        typeTransforms.Add(type, func);
    }


    private PropertyInfo[] GetCachedProperties(Type t)
    {
      PropertyInfo[] props = null;
      if (typeProperties.TryGetValue(t, out props))
        return props;
      else
      {
        props = t.GetProperties().Where(prop => prop.PropertyType.BaseType.Name != "Array").ToArray();
        typeProperties.Add(t, props);
        return props;
      }
    }


    /// <summary>
    /// Write a CSV header from the properties in an object
    /// </summary>
    /// <param name="t"></param>
    /// <returns></returns>
    public String GetHeader(Type t)
    {
      PropertyInfo[] props = this.GetCachedProperties(t);
      String header = "";

      for (int i = 0; i < props.Length; ++i)
        header += csv(props[i].Name, i != 0);

      return header;
    }


    /// <summary>
    /// Write a CSV entry for an instance of an object
    /// </summary>
    /// <param name="o"></param>
    /// <returns></returns>
    public String Serialize(Object o)
    {
      PropertyInfo[] props = this.GetCachedProperties(o.GetType());
      String contents = "";

      for (int i = 0; i < props.Length; ++i)
        contents += csv(props[i].GetValue(o, null), i != 0);

      return contents;
    }
    

    /// <summary>
    /// Returns the object's string representation, quoted and with a leading delimiter
    /// </summary>
    /// <param name="o"></param>
    /// <returns></returns>
    private String csv(Object o)
    {
      return csv(o, true);
    }


    /// <summary>
    /// Returns the object's string representation with optional leading delimiter
    /// </summary>
    /// <param name="o"></param>
    /// <param name="addDelimiter"></param>
    /// <returns></returns>
    private String csv(Object o, bool addDelimiter)
    {
      String delimiter = addDelimiter ? "," : "";

      if (o == null)
        return delimiter + "\"\"";
      else
      {
        String fieldValue;
        Func<Object, String> xform;

        if (typeTransforms.TryGetValue(o.GetType(), out xform))
          fieldValue = xform.Invoke(o);
        else
          fieldValue = o.ToString();

        return delimiter + "\"" + newlineRegex.Replace(fieldValue, " ").Replace("\"", "\"\"") + "\"";
      }
    }


    /// <summary>
    /// Iterate over a collection and output the results to a csv.
    /// </summary>
    /// <param name="outputFile"></param>
    /// <param name="collection"></param>
    public void WriteCSVFile(String outputFile, IEnumerable collection)
    {
      WriteCSVFile(outputFile, collection, false);
    }


    /// <summary>
    /// Iterate over a collection and output the results to a csv
    /// Optionally, append to an existing file.
    /// </summary>
    /// <param name="outputFile"></param>
    /// <param name="collection"></param>
    /// <param name="append"></param>
    public void WriteCSVFile(String outputFile, IEnumerable collection, bool append)
    {
      if (!File.Exists(outputFile))
        append = false;

      using (TextWriter tw = new StreamWriter(outputFile, append))
      {
        int i = 0;
        foreach (var record in collection)
        {
          if (i == 0 && !append)
            tw.WriteLine(this.GetHeader(record.GetType()));

          tw.WriteLine(this.Serialize(record));
          ++i;
        }

        tw.Flush();
      }
    }


    /// <summary>
    /// Iterate of the rows in a table and create a csv file.
    /// Transforms will not be used.
    /// Optionally append to an existing file.
    /// </summary>
    /// <param name="outputFile"></param>
    /// <param name="table"></param>
    public void WriteCSVFile(String outputFile, DataTable table)
    {
      WriteCSVFile(outputFile, table, false);
    }


    /// <summary>
    /// Iterate of the rows in a table and create a csv file.
    /// Transforms will not be used.
    /// </summary>
    /// <param name="outputFile"></param>
    /// <param name="table"></param>
    public void WriteCSVFile(String outputFile, DataTable table, bool append)
    {
      if (!File.Exists(outputFile))
        append = false;

      using (TextWriter tw = new StreamWriter(outputFile, append))
      {
        int rowNum = 0;
        foreach (DataRow row in table.Rows)
        {
          if (rowNum == 0 && !append)
          {
            // Write the header
            tw.Write(csv(table.Columns[0], false));
            for(int i = 1; i < table.Columns.Count; ++i)
              tw.Write(csv(table.Columns[i]));

            tw.WriteLine();
          }

          // Write the row
          tw.Write(csv(row[table.Columns[0]], false));
          for(int j = 1; j < table.Columns.Count; ++j)
            tw.Write(csv(row[table.Columns[j]]));

          tw.WriteLine();
          ++rowNum;
        }
      }
    }
  }
}

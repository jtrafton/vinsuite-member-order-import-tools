﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EWSDataTaskSupport.EWSData;

namespace EWSDataTaskSupport
{
  /// <summary>
  /// Member Import specific error object
  /// </summary>
  public class ErrorReportMember : ErrorReportBase
  {    
    private String lastName;
    private String firstName;
    private String email;

    public ErrorReportMember()
    { }

    public ErrorReportMember(String errorMessage, String resolution, String source, ERRORTYPE errorType)
      : base(errorMessage, resolution, source, errorType)
    { }

    public ErrorReportMember(Member member, String errorMessage, String resolution, String source, ERRORTYPE errorType)
      : this(errorMessage, resolution, source, errorType)
    {
      this.recordID     = member.AltMemberID;
      this.email        = member.Email;
      this.firstName    = member.FirstName;
      this.lastName     = member.LastName;
    }

    public String LastName
    {
      get { return this.lastName; }
    }

    public String FirstName
    {
      get { return this.firstName; }
    }

    public String Email
    {
      get { return this.email; }
    }
  }
}

﻿using System.Configuration;

namespace EWSDataTaskSupport
{
  public class ConnectionManager
  {
    public string username { get; set; }
    public string password { get; set; }

    public ConnectionManager()
    {
      this.username = ConfigurationManager.AppSettings["WSUsername"].ToString();
      this.password = ConfigurationManager.AppSettings["WSPassword"].ToString();
    }
  }
}

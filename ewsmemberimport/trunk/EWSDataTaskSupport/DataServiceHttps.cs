﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EWSDataTaskSupport.EWSData;

namespace EWSDataTaskSupport
{
  /// <summary>
  /// This class exposes a normal DataService object 
  /// but changes the URL to use HTTPS
  /// </summary>
  public class DataServiceHttps : DataService
  {
    public DataServiceHttps()
      : base()
    {
      if(!(this.Url.Contains("localhost") || this.Url.Contains("127.0.0.1")))
        this.Url = this.Url.Replace("http:", "https:");
    }
  }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EWSDataTaskSupport.EWSData;

namespace EWSDataTaskSupport
{
  public abstract class EWSCallerBase<RQ, RS, ER, T>
    where RQ: WebServiceRequest
    where RS: WebServiceResponse
    where ER: ErrorReportBase
  {
    private UpdateDataTaskRequest updTaskRequest;
    private bool debug;
    private int recordsPerRound;
    private Func<RQ, RS> webService;
    private DataService ds;
    protected RQ wsRequest { get; set; }
    protected RS wsResponse { get; set; }
    protected ExcelWriter excelWriter;


    protected EWSCallerBase(Func<RQ, RS> webService, RQ wsRequest, DataService ds, ExcelWriter excelWriter, 
      UpdateDataTaskRequest updTaskRequest, bool debug, int recordsPerRound)
    {
      this.webService       = webService;
      this.wsRequest        = wsRequest;
      this.ds               = ds;
      this.excelWriter      = excelWriter;
      this.updTaskRequest   = updTaskRequest;
      this.debug            = debug;
      this.recordsPerRound  = recordsPerRound;
    }

    public List<ER> Import(IEnumerable<T> importItems)
    {
      List<ER> errors = new List<ER>();
      UpdateDataTaskResponse updTaskResponse;
      String exceptionErr = null;
      bool stopRequested = false;
      int round = 0;
      int lastRecord;

      #region Setup requests
      #endregion

      foreach (IEnumerable<T> subList in DataTaskHelper.getSubListsPerRound(importItems, recordsPerRound))
      {
        if (stopRequested)
          break;
        else
          SetRequestProperty(subList);

        lastRecord = round * recordsPerRound;
        Console.WriteLine("Importing round\t{0}:\t{1} - {2}", round + 1, lastRecord + 1, lastRecord + subList.Count());
        ++round;

        #region Import round
        if (!debug)
          wsResponse = DataTaskHelper.TryNTimes(webService, wsRequest, 90, TimeSpan.FromSeconds(10));

        if (wsResponse != null)
        {
          if (wsResponse.IsSuccessful)
          {
            errors.AddRange(ProcessResponseProperty());
          }
          else
          {
            exceptionErr = "EXCEPTION: Web service returned failed response: " + wsResponse.ErrorMessage;
            Console.WriteLine(exceptionErr);
          }
        }
        else
        {
          exceptionErr = debug ? "Debug enabled. Did not import" : "EXCEPTION: Web services did not respond. Check your connection or xml data";
          Console.WriteLine(exceptionErr);
        }

        if (!String.IsNullOrWhiteSpace(exceptionErr))
        {
          foreach (T item in subList)
            errors.Add(AddExceptionError(item, exceptionErr));
        }

        #endregion

        #region Update data task tracking progress
        if (updTaskRequest != null && updTaskRequest.DataTask != null && updTaskRequest.DataTask.DataTaskTrackingID != 0)
        {
          updTaskRequest.DataTask.JobProgress = subList.Count();
          updTaskResponse = DataTaskHelper.TryNTimes(ds.UpdateDataTask, updTaskRequest, 100, TimeSpan.FromSeconds(10));

          // Store if the task was requested to stop
          stopRequested = updTaskResponse.Task.StopRequested ?? false;
        }
        #endregion
      }

      return errors;
    }

    protected abstract void SetRequestProperty(IEnumerable<T> subList);
    protected abstract List<ER> ProcessResponseProperty();
    protected abstract ER AddExceptionError(T item, String exceptionErr);
  }
}

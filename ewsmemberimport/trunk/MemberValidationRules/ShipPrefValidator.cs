﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EWSDataTaskSupport.EWSData;
using EWSDataTaskSupport;

namespace MemberValidationRules
{
  class ShipPrefValidator : Validator<Member>
  {
    public ShipPrefValidator(WineryData wd)
      : base("Shipping Preference Validator", true, wd)
    {  }

    public override List<string> Validate()
    {
      List<string> errors = new List<string>();

      if (this.target.ClubMemberships != null)
      {
        foreach (ClubMembership cm in this.target.ClubMemberships)
        {
          if (!String.IsNullOrEmpty(cm.WineShippingType))
          {
            if (wd.shipTypeGetter.HasShippingType(cm.WineShippingType))
              cm.WineShippingTypeID = wd.shipTypeGetter.GetShippingType(cm.WineShippingType).ShippingTypeID;
            else
              errors.Add(cm.WineShippingType + " is invalid shipping type");
          }
        }
      }

      return errors;
    }
  }
}

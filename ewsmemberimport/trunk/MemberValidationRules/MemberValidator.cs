﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EWSDataTaskSupport.EWSData;
using EWSDataTaskSupport;

namespace MemberValidationRules
{
  class MemberValidator : Validator<Member>
  {
    public MemberValidator(WineryData wd)
      : base("Member Validator", false, wd)
    { }

    public override List<String> Validate()
    {
      List<String> errors = new List<String>();

      #region Fix Data
      // Ignore invalid salutations (more than 10 chars)
      if (!String.IsNullOrEmpty(this.target.Salutation))
        this.target.Salutation = this.target.Salutation.Length < 10 ? this.target.Salutation : null;

      // Truncate SourceCode to 50 chars
      if (!String.IsNullOrEmpty(this.target.SourceCode))
        if (this.target.SourceCode.Length > 50)
          this.target.SourceCode = this.target.SourceCode.Substring(0, 50);


      if (this.target.ClubMemberships != null)
      {
        // Generate missing birthday if Club Membership is present
        if (!this.target.BirthDate.HasValue)
          this.target.BirthDate = new DateTime(1920, 01, 01);
      }
      #endregion

      return errors;
    }
  }
}

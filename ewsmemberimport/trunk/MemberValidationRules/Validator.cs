﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EWSDataTaskSupport;

namespace MemberValidationRules
{
  public abstract class Validator<T> : IValidator
  {
    private String validatorName;
    protected T target;
    protected Boolean isCritical;
    protected WineryData wd;

    protected Validator(String validatorName, Boolean isCritical, WineryData wd)
    {
      this.validatorName = validatorName;
      this.isCritical = isCritical;
      this.wd = wd;
    }

    public String GetName()
    {
      return this.validatorName;
    }

    public Boolean IsCritical()
    {
      return this.isCritical;
    }

    public void SetValidationTarget(T target)
    {
      this.target = target;
    }

    public abstract List<String> Validate();
  }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EWSDataTaskSupport.EWSData;
using System.Text.RegularExpressions;
using System.Reflection;
using EWSDataTaskSupport;

namespace MemberValidationRules
{
  class BadCharValidator : Validator<Member>
  {
    public BadCharValidator(WineryData wd)
      : base("Bad Character Validator", false, wd)
    { }
    
    public override List<String> Validate()
    {
      List<String> errors = new List<String>();

      DataTaskHelper.CorrectBadChars(this.target, true);

      return errors;
    }
  }
}

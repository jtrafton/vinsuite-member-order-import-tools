﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EWSDataTaskSupport.EWSData;
using EWSDataTaskSupport;

namespace MemberValidationRules
{
  class ClubDataValidator : Validator<Member>
  {
    private DateTime today;

    public ClubDataValidator(WineryData wd)
      : base("Club Data Validator", true, wd)
    {
      today = DateTime.Today;
    }

    public override List<string> Validate()
    {
      List<String> errors = new List<string>();
      EWSDataTaskSupport.EWSMember.WineClub club;
      int shipments;

      //check if club(s) assigned to a current member
      if (this.target.ClubMemberships != null)
      {
        //Validate for duplicates
        // select club levels where number of distinct shipping addresses 
        // is less than a total number of shipping addresses assigned to a specific club
        // 2014-03-11 AC: same validation happens on the webservice side now and it is more granular.
        /*
        var dupes = from cm in target.ClubMemberships
                    group cm by cm.ClubLevel into cmgrp
                    let cmems = cmgrp.Count()
                    where cmems > 1 &&
                          cmgrp.Select(addr => addr.ShippingAddress).Distinct().Count() != cmems
                    select cmgrp.Key;

        if (dupes.Count() > 0)
          errors.Add("Duplicate shipping addresses found in " + String.Join(",", dupes) + " club(s)");
        */

        //Validate each club membership
        foreach (ClubMembership cm in this.target.ClubMemberships)
        {
          #region Fix Data
          cm.ShippingBirthDate = cm.ShippingBirthDate ?? this.target.BirthDate ?? new DateTime(1920, 01, 01);

          if (String.IsNullOrWhiteSpace(cm.ShippingEmail))
          {
            if (String.IsNullOrWhiteSpace(this.target.Email))
              cm.ShippingEmail = this.target.AltMemberID + "@noemail.com";
            else
              cm.ShippingEmail = this.target.Email;
          }

          if (cm.StartDate.HasValue && cm.EndDate.HasValue)
          {
            if (cm.StartDate.Value > cm.EndDate.Value)
              cm.EndDate = cm.CancelDate = cm.StartDate.Value.AddSeconds(1);
          }
          #endregion

          // Validate shipping information. AddUpdateClubMembership expects all fields to be filled
          if (String.IsNullOrWhiteSpace(cm.ShippingAddress))
            errors.Add(cm.ClubLevel + ": shipping address is missing.");
          if (String.IsNullOrWhiteSpace(cm.ShippingCity))
            errors.Add(cm.ClubLevel + ": shipping city is missing.");
          if (String.IsNullOrWhiteSpace(cm.ShippingState))
            errors.Add(cm.ClubLevel + ": shipping state is missing.");
          if (String.IsNullOrWhiteSpace(cm.ShippingZipCode))
            errors.Add(cm.ClubLevel + ": shipping zip code is missing.");

          if (cm.IsActive.HasValue)
          {            
            if (cm.IsActive.Value)
            {
              /* Commented out in MemberGetter. Probably per Jon
              //Validate active club data
              if (cm.EndDate < today)
                errors.Add(cm.ClubLevel + " end date is in the past. Club should be inactive.");
              */
            }
            else
            {
              /* Commented out in MemberGetter. Probably per Jon
              //Validate inactive club data
              if (!cm.EndDate.HasValue)
                errors.Add(cm.ClubLevel + " is inactive and missing end date.");
              */
            }
          }
          else
          {
            //Validate for missing ClubIsActive flag
            errors.Add(cm.ClubLevel + " is missing ClubIsActive flag.");
          }


          /* removed in MemberGetter per Jon 3/5/13
          //Club membership must have a start date
          if (!cm.StartDate.HasValue)
            errors.Add(cm.ClubLevel + " is missing start date.");          

          //Validate start and end dates (expected by ws request validation)
          if (cm.StartDate > cm.EndDate)
            errors.Add(cm.ClubLevel + " start date cannot be after the end date.");
          */
          if (cm.IsOnHold.HasValue)
          {
            if (cm.IsOnHold.Value)
            {
              //Validate onHold start date
              if (!cm.HoldStartDate.HasValue)
                errors.Add(cm.ClubLevel + " is on hold but missing an onhold start date.");
            }
            else
            {
              //Validate start date for clubs that are not on hold
              if (cm.HoldStartDate.HasValue && cm.HoldStartDate.Value <= today)
                errors.Add(cm.ClubLevel + " is not on hold but has an onhold start date.");
            }
          }

          // Calculate total number of shipments based on club frequency              
          if (cm.EndDate.HasValue)
          {
            if (cm.EndDate.Value > today && this.wd.clubGetter.HasClubLevel(cm.ClubLevel))
            {
              club = this.wd.clubGetter.GetClubLevel(cm.ClubLevel);
              shipments = 0;
              if (club.ClubFrequencyMonths > 0)
                shipments += (cm.EndDate.Value.Month - today.Month + (cm.EndDate.Value.Year - today.Year) * 12) / club.ClubFrequencyMonths.Value;
              if (club.ClubFrequencyWeeks > 0)
                shipments += (cm.EndDate.Value.DayOfYear / 7 - today.DayOfYear / 7) / club.ClubFrequencyWeeks.Value;
              cm.TotalNumberOfShipments = shipments;
            }
          }
        }
      }

      return errors;
    }
  }
}

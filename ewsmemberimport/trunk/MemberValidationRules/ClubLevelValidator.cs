﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EWSDataTaskSupport;
using EWSDataTaskSupport.EWSData;

namespace MemberValidationRules
{
  class ClubLevelValidator : Validator<Member>
  {
    private DateTime today;

    public ClubLevelValidator(WineryData wd)
      : base("Club Level Validator", true, wd)
    {
      today = DateTime.Today;
    }

    public override List<string> Validate()
    {
      List<String> errors = new List<String>();
     
      if (this.target.ClubMemberships != null)
      {
        foreach (ClubMembership cm in this.target.ClubMemberships)
        {
          // Validate club level
          if (String.IsNullOrEmpty(cm.ClubLevel))
          {
            errors.Add("Club Level is missing.");
          }
          else
          {
            if (wd.clubGetter.HasClubLevel(cm.ClubLevel))
              cm.ClubLevelID = wd.clubGetter.GetClubLevel(cm.ClubLevel).ClubLevelID;
            else
              errors.Add(cm.ClubLevel + " is an invalid Club Level.");
          }
        }
      }

      return errors;
    }
  }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EWSDataTaskSupport.EWSData;
using System.Reflection;
using EWSDataTaskSupport;

namespace MemberValidationRules
{
  public class ValidationDriver
  {
    private IEnumerable<Member> members;
    private List<ErrorReportMember> errors;
    private List<Type> allValidators;
    private List<Type> currentValidators;
    private String wsUsername;
    private String wsPassword;

    /// <summary>
    /// Validation driver constructor
    /// </summary>
    /// <param name="members">Collection of members to validate</param>
    /// <param name="wsUsername">EWS WS Username</param>
    /// <param name="wsPassword">EWS WS Password</param>
    public ValidationDriver(IEnumerable<Member> members, String wsUsername, String wsPassword)
    {
      this.members = members;
      errors = new List<ErrorReportMember>();

      // Array of types used by validator constructors
      Type[] constructorTypes = new Type[]{typeof(WineryData)};

      // Dynamically load all the IValidators in this assembly
      allValidators =
        (from type in Assembly.GetExecutingAssembly().GetTypes()
         where type.GetInterfaces().Contains(typeof(IValidator)) && type.GetConstructor(constructorTypes) != null
         select type).ToList();
      currentValidators = allValidators;

      this.wsUsername = wsUsername;
      this.wsPassword = wsPassword;
    }


    /// <summary>
    /// Get Validation Errors (stop import)
    /// </summary>
    /// <returns>List of ErrorReport</returns>
    public List<ErrorReportMember> GetValidationErrors()
    {
      return errors;
    }


    /// <summary>
    /// Get a list of loaded validators
    /// </summary>
    /// <returns></returns>
    public List<String> GetCurrentValidators()
    {
      return currentValidators.Select(cl => cl.ToString()).ToList();
    }


    /// <summary>
    /// Get a list of all available validators
    /// </summary>
    /// <returns></returns>
    public List<String> GetAllValidators()
    {
      return allValidators.Select(al => al.ToString()).ToList();
    }


    /// <summary>
    /// Set validation rules
    /// </summary>
    /// <param name="rules"></param>
    public void SetValidators(List<String> rules)
    {
      currentValidators = (from av in allValidators
                          where rules.Contains(av.ToString())
                          select av).ToList();
    }

    /// <summary>
    /// Run validation rules against the collection of members
    /// </summary>
    /// <returns>True if passed all rules, False otherwise</returns>
    public bool RunValidation()
    {
      List<Validator<Member>> memberValidators;
      List<Validator<IEnumerable<Member>>> memberCollectionValidators;
      ErrorReportMember.ERRORTYPE errorType = new ErrorReportMember.ERRORTYPE();
      using (OrderServiceHttps os = new OrderServiceHttps())
      {
        using (MemberServiceHttps ms = new MemberServiceHttps())
        {
          // Initialize Winery Data object
          WineryData wd = new WineryData(ms, os, wsUsername, wsPassword);
          // List of objects representing constructor parameters
          Object[] constrParams = new Object[] { wd };

          // Instantiate all member validators
          memberValidators =
            (from mv in currentValidators
             where mv.BaseType == typeof(Validator<Member>)
             select (Validator<Member>)Activator.CreateInstance(mv, constrParams)).ToList();

          // Instantiate all member collection validators
          memberCollectionValidators =
            (from cv in currentValidators
             where cv.BaseType == typeof(Validator<IEnumerable<Member>>)
             select (Validator<IEnumerable<Member>>)Activator.CreateInstance(cv, constrParams)).ToList();
        }
      }
      
      /*
      var b = from mv in memberValidators
              select mv.GetType().InvokeMember("GetName", BindingFlags.Instance | BindingFlags.Public | BindingFlags.InvokeMethod, null, mv, null);
      */

      /*
       * Entire collection must be validated first. BadCharValidator nullies empty strings.
       * This fixes coalescing issues in individual member validators
      */
      #region Validate the entire collection of Members against itself
      foreach (Validator<IEnumerable<Member>> memberCollectionValidator in memberCollectionValidators)
      {
        memberCollectionValidator.SetValidationTarget(members);
        Boolean isCritical = memberCollectionValidator.IsCritical();
        foreach (String error in memberCollectionValidator.Validate())
        {
          errorType = memberCollectionValidator.IsCritical() ? ErrorReportMember.ERRORTYPE.CRITICAL : ErrorReportMember.ERRORTYPE.WARNING;
          errors.Add(new ErrorReportMember(error, ErrorReportMember.FixTheFile, memberCollectionValidator.GetName(), errorType));
        }
      }
      #endregion

      #region Validate all members against the member validators
      foreach (Member currentMember in this.members)
      {
        foreach (Validator<Member> memberValidator in memberValidators)
        {
          memberValidator.SetValidationTarget(currentMember);
          errorType = memberValidator.IsCritical() ? ErrorReportMember.ERRORTYPE.CRITICAL : ErrorReportMember.ERRORTYPE.WARNING;
          foreach (String currentError in memberValidator.Validate())
          {         
            errors.Add(new ErrorReportMember(currentMember,
              currentError, 
              ErrorReportMember.FixTheFile,
              memberValidator.GetName(),
              errorType));
          }     
        }
      }
      #endregion

      // Spreadsheet is valid if no errors were encountered
      return errors.Count == 0;
    }
  }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MemberValidationRules
{
  interface IValidator
  {
    List<String> Validate();
  }
}

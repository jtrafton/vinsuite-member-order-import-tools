﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EWSDataTaskSupport.EWSData;
using EWSDataTaskSupport;

namespace MemberValidationRules
{
  class ClubCCValidator : Validator<Member>
  {
    public ClubCCValidator(WineryData wd)
      : base("Club Credit Card Validator", false, wd)
    { }

    public override List<string> Validate()
    {
      List<string> errors = new List<string>();

      if (this.target.ClubMemberships != null)
      {
        //Club members must have CC info. CCs are validated by a different validator

        if (this.target.CreditCards == null || this.target.CreditCards.Count() == 0)
          errors.Add("Club membership is missing credit card info.");
        /*
        if (String.IsNullOrWhiteSpace(this.target.CreditCardNumber) &&
            String.IsNullOrWhiteSpace(this.target.NameOnCreditCard) &&
            String.IsNullOrWhiteSpace(this.target.CreditCardType))
          errors.Add("Club membership is missing credit card info.");
         */
      }

      return errors;
    }
  }
}

﻿using System;
using System.Collections.Generic;
using EWSDataTaskSupport.EWSData;
using EWSDataTaskSupport;
using EWSDataTaskSupport.EWSOrder;
using System.Text.RegularExpressions;

namespace MemberValidationRules
{
  class CCValidator : Validator<Member>
  {
    private Regex spaces;

    public CCValidator(WineryData wd)
      : base("Credit Card Validator", true, wd)
    {
      spaces = new Regex(@"\W+", RegexOptions.IgnoreCase | RegexOptions.Compiled);
    }

    public override List<string> Validate()
    {
      List<String> errors = new List<string>();

      #region Validate Primary(Member) CC
      
      CreditCardRecord primaryCC = new CreditCardRecord();
      if (!String.IsNullOrWhiteSpace(this.target.CreditCardNumber) ||
          !String.IsNullOrWhiteSpace(this.target.CreditCardType) ||
          !String.IsNullOrWhiteSpace(this.target.NameOnCreditCard))
      {
        primaryCC.CreditCardExpirationMonth = this.target.CreditCardExpirationMonth;
        primaryCC.CreditCardExpirationYear  = this.target.CreditCardExpirationYear;
        primaryCC.CreditCardNumber          = this.target.CreditCardNumber;
        primaryCC.CreditCardType            = this.target.CreditCardType;
        primaryCC.NameOnCreditCard          = this.target.NameOnCreditCard;
        errors.AddRange(ValidateAndCorrect(primaryCC));
      }
      #endregion

      #region Validate CC Wallet
      if (this.target.CreditCards != null)
      {
        foreach (CreditCardRecord cc in this.target.CreditCards)
          errors.AddRange(ValidateAndCorrect(cc));
      }
      #endregion

      return errors;
    }

    private List<String> ValidateAndCorrect(CreditCardRecord cc)
    {
      List<String> errors = new List<string>();

      // Validate Credit Card Type
      if (wd.ccGetter.HasCreditCardType(cc.CreditCardType))
      {
        CreditCardTypeInfo cct = wd.ccGetter.GetCreditCardType(cc.CreditCardType);
        if (cct != null)
          cc.CreditCardTypeID = cct.CreditCardTypeID;
      }
      else
        errors.Add("Credit Card Type is invalid");

      //Validate CC number
      if (String.IsNullOrEmpty(cc.CreditCardNumber))
      {
        errors.Add("Credit Card number is missing.");
      }
      else
      {
        long ccnum;
        double numberOfDigits;
        // Remove non alpha-numeric chars from the ccnumber string
        cc.CreditCardNumber = spaces.Replace(cc.CreditCardNumber, "");
        if (!long.TryParse(cc.CreditCardNumber, out ccnum))
          errors.Add("Credit Card number is not a valid number.");
        else
        {
          //Calculate number of digits
          numberOfDigits = Math.Floor(Math.Log10(ccnum) + 1);
          // Validate CC num against CC type
          switch (cc.CreditCardType)
          {
            case "VISA":
              if (numberOfDigits != 13 && numberOfDigits != 16)
                errors.Add("Credit Card number is invalid for type VISA. Expected 13 or 16 digits. Received " + numberOfDigits + ".");
              break;
            case "MC":
            case "Discover":
              if (numberOfDigits != 16)
                errors.Add("Credit Card number is invalid for type MC or Discover. Expected 16 digits. Received " + numberOfDigits + ".");
              break;
            case "AmEx":
              if (numberOfDigits != 15)
                errors.Add("Credit Card number is invalid for type AmEx. Expected 15 digits. Received " + numberOfDigits + ".");
              break;
            default:
              //errors.Add("Valid Credit Card type is missing.");
              break;
          }
        }
      }

      // Validate exp mo
      if (String.IsNullOrEmpty(cc.CreditCardExpirationMonth))
        errors.Add("Credit Card expiration month is missing.");
      else
      {
        if (cc.CreditCardExpirationMonth.Length < 2)
          cc.CreditCardExpirationMonth = cc.CreditCardExpirationMonth.PadLeft(2, '0');
        else
        {
          int month = 0;
          bool isValid = int.TryParse(cc.CreditCardExpirationMonth, out month);
          if (!isValid || month < 1 || month > 12)
            errors.Add("Credit Card expiration month is invalid.");
        }
      }

      // Validate exp yr
      if (String.IsNullOrEmpty(cc.CreditCardExpirationYear))
        errors.Add("Credit Card expiration year is missing.");
      else
      {
        if (cc.CreditCardExpirationYear.Length == 2)
          cc.CreditCardExpirationYear = "20" + cc.CreditCardExpirationYear;
        else if (cc.CreditCardExpirationYear.Length != 4)
          errors.Add("Credit Card expiration year is invalid.");
      }

      // Validate Name On Card
      if (String.IsNullOrEmpty(cc.NameOnCreditCard))
        errors.Add("Name On Card is missing.");
      else
      {
        cc.NameOnCreditCard = cc.NameOnCreditCard.Replace("&", " And ");
      }

      return errors;
    }
  }
}

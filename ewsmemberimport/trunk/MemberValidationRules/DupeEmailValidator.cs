﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EWSDataTaskSupport.EWSData;
using EWSDataTaskSupport;

namespace MemberValidationRules
{
  class DupeEmailValidator : Validator<IEnumerable<Member>>
  {
    public DupeEmailValidator(WineryData wd)
      : base("Duplicate Email Validator", false, wd)
    { }

    public override List<string> Validate()
    {
      List<String> errors = new List<String>();
      //get all duplicates
      /*
       * 2013/06/27 DJH - Commented this rule out per Jon
       * 
      var dupes = from d in this.target
                  group d by d.Email into emailGrp
                  where emailGrp.Count() > 1
                  select emailGrp;

      var emails = from d in dupes
                   select d.Key + " (AltMemID: " + String.Join(",", d.Select(dd => dd.AltMemberID)) + ").";
       */

      //filter duplicates by first and last names
      /* 
       * 2013/06/27 DJH - Commented this rule out per Jon
       * 
      var mems = from d in dupes.SelectMany(dd => dd)
                 let x = new { d.Email, d.FirstName, d.LastName }
                 group x by x.Email into dMemGrp
                 where dMemGrp.Distinct().Count() > 1
                 select dMemGrp.Key + " is assigned to more than one unique member (" + String.Join(", ", dMemGrp.Select(dm => dm.LastName + " " + dm.FirstName)) + ")";
       */

      //errors.AddRange(emails);
      //errors.AddRange(mems);

      return errors;
    }
  }
}

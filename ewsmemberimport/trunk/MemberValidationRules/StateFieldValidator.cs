﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EWSDataTaskSupport.EWSData;
using EWSDataTaskSupport;

namespace MemberValidationRules
{
  class StateFieldValidator : Validator<Member>
  {
    public StateFieldValidator(WineryData wd)
      : base("Sate Field Validator", true, wd)
    { }

    public override List<String> Validate()
    {
      List<String> errors = new List<String>();

      if (!ValidateFieldLength(this.target.State))
        errors.Add("Member Billing State is invalid");

      if (this.target.ShippingAddresses != null)
      {
        foreach (ShippingAddress sa in this.target.ShippingAddresses)
        {
          if (!ValidateFieldLength(sa.State))
            errors.Add("Shipping Address State is invalid");
        }
      }

      if (this.target.ClubMemberships != null)
      {
        foreach (ClubMembership cm in this.target.ClubMemberships)
        {
          if (!ValidateFieldLength(cm.ShippingState))
            errors.Add("Club Shipping State is invalid");
        }
      }

      return errors;
    }

    private Boolean ValidateFieldLength(String field)
    {
        return String.IsNullOrWhiteSpace(field) || field.Length <= 3;
    }
  }
}

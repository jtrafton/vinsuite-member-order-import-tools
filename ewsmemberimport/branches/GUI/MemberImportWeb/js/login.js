﻿$(document).ready(function()
{
  /********************************************
   * Log in via web service
   ********************************************/
  $("#btnLogin").click(function()
  {
    $.ajax(
    {
      type: "POST",
      url: "DataTaskLoginServices.asmx/Login",
      data: JSON.stringify(
      {
        "username": $("#username").val(),
        "password": $("#password").val()
      }),
      contentType: "application/json; charset=utf-8",
      dataType: "json",
      success: function(response)
      {
        if (response.d == true)
          window.location = "DataTaskDashboard.aspx";
        else
          $("#error").html("Invalid credentials");
      },
      error: function(error)
      {
        $("#error").html("ERROR!");
      }
    });
  });
});
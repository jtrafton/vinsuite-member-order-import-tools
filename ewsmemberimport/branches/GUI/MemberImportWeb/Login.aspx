﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="DataTaskWeb.Login" MasterPageFile="~/DataTaskWeb.Master" %>

<asp:Content ContentPlaceHolderID="head" runat="server">
  <script type="text/javascript" src="js/login.js"></script>
</asp:Content>

<asp:Content ContentPlaceHolderID="content" runat="server">
<table>
  <tr>
    <td>Username:</td>
    <td><input id="username" name="username" type="text" /></td>
  </tr>
  <tr>
    <td>Password:</td>
    <td><input id="password" name="password" type="password" /></td>
  </tr>
  <tr>
    <td id="error" colspan="2"></td>
  </tr>
</table>
<button id="btnLogin">Login</button>
</asp:Content>
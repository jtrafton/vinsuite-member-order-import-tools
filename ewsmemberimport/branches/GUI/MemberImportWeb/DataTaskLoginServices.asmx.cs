﻿using System;
using System.Web.Script.Services;
using System.Web.Security;
using System.Web.Services;
using EWSDataTaskSupport;
using EWSDataTaskSupport.EWSData;
using System.Net;

namespace DataTaskWeb
{
  [WebService(Namespace = "http://tempuri.org/")]
  [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
  [System.ComponentModel.ToolboxItem(false)]
  [ScriptService]
  public class ImportWebServices : WebService
  {
    public ImportWebServices()
    {
      ServicePointManager.ServerCertificateValidationCallback += (se, cert, chain, sslerror) => true;
    }


    /// <summary>
    /// Ends the current session
    /// </summary>
    [WebMethod(EnableSession = true)]
    public void Logout()
    {
      Session.Clear();
      Session.Abandon();
      FormsAuthentication.SignOut();
    }


    /// <summary>
    /// Test data credentials against the web EWS services 
    /// and log the user in
    /// </summary>
    /// <param name="username"></param>
    /// <param name="password"></param>
    /// <returns></returns>
    [WebMethod(EnableSession = true)]
    public bool Login(String username, String password)
    {
      using (DataService ds = new DataServiceHttps())
      {
        if (ds.TestDataConnection(username, password))
        {
          // The credentials worked.  Store them and 
          // the wineries in the current session.
          Session["wsUsername"] = username;
          Session["wsPassword"] = password;
          FormsAuthentication.SetAuthCookie(Guid.NewGuid().ToString(), false);
          Session.Timeout = Convert.ToInt32(FormsAuthentication.Timeout.TotalMinutes);
          return true;
        }
        else
        {
          Session.Clear();
          return false;
        }
      }
    }
  }
}

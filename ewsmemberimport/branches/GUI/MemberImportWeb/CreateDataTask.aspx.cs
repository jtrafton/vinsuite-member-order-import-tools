﻿using System;
using System.IO;
using System.Web;
using System.Web.UI.WebControls;
using EWSDataTaskSupport;
using EWSDataTaskSupport.EWSData;
using System.Collections.Generic;

namespace DataTaskWeb
{
  public partial class MemberImport : System.Web.UI.Page
  {
    private Dictionary<String, String> importTypes;

    protected void Page_Load(object sender, EventArgs e)
    {
      using (DataServiceHttps ds = new DataServiceHttps())
      {
        CreateImportTypes();
        PopulateWineries(ds);
        PopulateImportTypes();
        bool validateTask = true;

        if (Session["newTask"] == null)
        {
          Session["newTask"] = new DataTask();
          validateTask = false;
        }

        DataTask newTask = Session["newTask"] as DataTask;
        newTask.TaskName = txtImportName.Text;
        newTask.TaskType = importTypes[selImportType.SelectedValue];

        newTask.WineryID = Guid.Parse(selWinery.SelectedValue);

        if (Request.Files.Count == 1 && Request.Files[0].ContentLength > 0)
        {
          #region Save the posted file
          HttpPostedFile file = Request.Files[0];
          String path = MapPath("DATA") + @"\" + newTask.WineryID + @"\member import\";

          if (!Directory.Exists(path))
            Directory.CreateDirectory(path);

          String filename = String.Format("{0:yyyyMMdd-hhmmss}", DateTime.Now) + "_" + file.FileName;

          file.SaveAs(path + filename);
          newTask.File = path + filename;
          #endregion
        }

        if (validateTask)
        {
          try
          {
            // Validate the file
            ImportSupportServices supportSvc = new ImportSupportServices();
            newTask.JobSize = supportSvc.StartDataTask(newTask, true);

            #region Save the data task
            CreateDataTaskRequest createTaskRequest = new CreateDataTaskRequest()
            {
              Username = Session["wsUsername"] as String,
              Password = Session["wsPassword"] as String,
              DataTask = newTask,
              WineryID = newTask.WineryID
            };

            CreateDataTaskResponse createTaskResponse = ds.CreateDataTask(createTaskRequest);
            if (createTaskResponse.IsSuccessful)
            {
              // remove the task from the session, we're done with it
              Session["newTask"] = null;
              Response.Redirect("DataTaskDashboard.aspx");
            }
            else
            {
              lblMessage.Text = "Error creating task: " + createTaskResponse.ErrorMessage;
            }
            #endregion
          }
          catch (Exception ex)
          {
            lblMessage.Text = "Exception: " + ex.Message;
            txtException.Text = ex.StackTrace;
          }
        }
      }
    }



    /// <summary>
    /// Make a dictionary of the available import types
    /// </summary>
    protected void CreateImportTypes()
    {
      importTypes = new Dictionary<String, String>();
      importTypes.Add("1", "Member import");
    }


    /// <summary>
    /// Get the list of wineries
    /// </summary>
    /// <param name="ds"></param>
    protected void PopulateWineries(DataService ds)
    {
      String username = Session["wsUsername"] as String;
      String password = Session["wsPassword"] as String;

      if (String.IsNullOrEmpty(username) || String.IsNullOrEmpty(password))
        return;

      // Populate the dropdown with the wineries
      SimpleWineryHolder[] wineries = ds.GetWineryInfo(username, password);

      foreach (SimpleWineryHolder win in wineries)
        selWinery.Items.Add(new ListItem(win.WineryName, win.WineryID.ToString()));
    }


    /// <summary>
    /// Create the list of available import types
    /// </summary>
    protected void PopulateImportTypes()
    {
      if (importTypes == null)
        CreateImportTypes();

      selImportType.Items.Clear();

      foreach (KeyValuePair<String, String> kvp in importTypes)
        selImportType.Items.Add(new ListItem(kvp.Value, kvp.Key));

      selImportType.SelectedIndex = 0;
    }
  }
}
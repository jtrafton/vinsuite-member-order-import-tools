﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CreateDataTask.aspx.cs" Inherits="DataTaskWeb.MemberImport" ClientIDMode="Static" MasterPageFile="~/DataTaskWeb.Master" %>

<asp:Content ContentPlaceHolderID="head" runat="server">
<link rel="Stylesheet" type="text/css" href="css/createTask.css" />
</asp:Content>

<asp:Content ContentPlaceHolderID="content" runat="server">
  <button id="btnBackToDash">Back</button>
  <form id="frmMemberImport" enctype="multipart/form-data" runat="server">
    <asp:HiddenField ID="hdnTaskID" runat="server" />
    <table>
      <tr>
        <td>Import name:</td>
        <td><asp:TextBox ID="txtImportName" runat="server" /></td>
      </tr>
      <tr>
        <td>Import type:</td>
        <td><asp:DropDownList ID="selImportType" runat="server" /></td>
      </tr>
      <tr>
        <td>Target winery:</td>
        <td><asp:DropDownList ID="selWinery" runat="server" /></td>
      </tr>
      <tr>
        <td>Import file:</td>
        <td><input type="file" name="memberFile" /></td>
      </tr>
    </table>
    <input type="submit" value="Submit" /><br />
    <asp:Label ID="lblMessage" runat="server" /><br />
    <asp:TextBox ID="txtException" TextMode="MultiLine" ReadOnly="true" runat="server" />
  </form>
</asp:Content>
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Script.Services;
using EWSDataTaskSupport.EWSData;
using EWSDataTaskSupport;
using System.Threading;
using EWSMemberImport;
using System.Net;

namespace DataTaskWeb
{
  [WebService(Namespace = "http://tempuri.org/")]
  [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
  [System.ComponentModel.ToolboxItem(false)]
  [ScriptService]
  public class ImportSupportServices : WebService
  {
    public ImportSupportServices()
      : base()
    {
      ServicePointManager.ServerCertificateValidationCallback += (se, cert, chain, sslerror) => true;
    }


    [WebMethod(EnableSession = true)]
    public GetDataTasksResponse GetDataTasks()
    {
      using (DataServiceHttps ds = new DataServiceHttps())
      {
        return GetDataTasks(ds, null);
      }
    }


    [WebMethod(EnableSession = true)]
    public bool StopDataTask(int dataTaskTrackingID)
    {
      using (DataServiceHttps ds = new DataServiceHttps())
      {
        GetDataTasksResponse getTaskResponse = GetDataTasks(ds, dataTaskTrackingID);

        if (getTaskResponse.IsSuccessful && getTaskResponse.Tasks.Length == 1)
        {
          DataTask task = getTaskResponse.Tasks[0];
          task.StopRequested = true;

          UpdateDataTaskRequest updTaskRequest = new UpdateDataTaskRequest()
          {
            Username = Session["wsUsername"] as String,
            Password = Session["wsPassword"] as String,
            DataTask = task
          };

          return ds.UpdateDataTask(updTaskRequest).IsSuccessful;
        }
        else
          return false;
      }
    }


    private GetDataTasksResponse GetDataTasks(DataService ds, int? dataTaskTrackingID)
    {
      GetDataTasksRequest request = new GetDataTasksRequest()
      {
        Username = Session["wsUsername"] as String,
        Password = Session["wsPassword"] as String,
        DataTaskTrackingID = dataTaskTrackingID
      };

      return ds.GetDataTasks(request);
    }



    [WebMethod(EnableSession = true)]
    public String StartDataTask(int dataTaskTrackingID)
    {
      using (DataServiceHttps ds = new DataServiceHttps())
      {
        GetDataTasksResponse response = GetDataTasks(ds, dataTaskTrackingID);

        if (response.IsSuccessful && response.Tasks.Length == 1)
        {
          DataTask task = response.Tasks[0];
          
          // TODO: run the following line in a background thread!
          StartDataTask(task, false);

          return "";
        }
        else
          return "Error retrieving data task: " + response.ErrorMessage;
      }
    }


    /// <summary>
    /// Validates and optionally starts a data task. 
    /// This method will throw exceptions to report validation errors.
    /// </summary>
    /// <param name="task"></param>
    /// <param name="validateOnly">
    /// If true, this call will block until validation is complete. 
    /// If false, this call will spawn a background thread and perfrm the data task</param>
    /// <returns>The size of the job</returns>
    public int StartDataTask(DataTask task, bool validateOnly)
    {
      String adminWSUsername = Session["wsUsername"] as String;
      String adminWSPassword = Session["wsPassword"] as String;

      if (String.IsNullOrEmpty(adminWSUsername) || String.IsNullOrEmpty(adminWSPassword))
        throw new Exception("Master admin web service credentials are required");

      if (String.IsNullOrEmpty(task.TaskName))
        throw new Exception("A task name is required");

      switch (task.TaskType.ToUpper())
      {
        case "MEMBER IMPORT":
          if (String.IsNullOrEmpty(task.File))
            throw new Exception("Member import requires an import file");

          if (validateOnly)
          {
            return EWSMemberImport.Program.RunMemberImport(
              validateOnly,
              task.File,
              0,
              true,
              task.NotifyEmail,
              adminWSUsername,
              adminWSPassword,
              task.WineryID,
              task.DataTaskTrackingID);
          }
          else
          {
            // Start the job in a background thread
            MemberImportPackage pkg = new MemberImportPackage(task, adminWSUsername, adminWSPassword);
            ThreadPool.QueueUserWorkItem(new WaitCallback(EWSMemberImport.Program.RunMemberImport), pkg);
            break;
          }
          

        default:
          throw new Exception("Unknown task type");
      }

      using (DataServiceHttps ds = new DataServiceHttps())
      {
        task.DateStarted = DateTime.Now;
        UpdateDataTaskRequest req = new UpdateDataTaskRequest()
        {
          Username = adminWSUsername,
          Password = adminWSPassword,
          DataTask = task
        };

        ds.UpdateDataTask(req);

        return task.JobSize ?? -1;
      }
    }
  }
}

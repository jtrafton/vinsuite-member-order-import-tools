﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EWSOrderImport
{
  public class ImportProduct
  {
    public String  ProductSKU   { get; set; }
    public String  ProductName  { get; set; }
    public String  ShortName    { get; set; }
    public bool    IsWine       { get; set; } // 'false' means this is a non-wine product
    public bool    IsTaxable    { get; set; }
    public String  Teaser       { get; set; }
    public String  Description  { get; set; }
    public int     BottleCount  { get; set; }    
    public bool    IsFirstParty { get; set; } // 'false' means this is a third party product
    public String  ReportGroup  { get; set; }
    public Decimal Price        { get; set; }

    public ImportProduct()
    { }
  }
}

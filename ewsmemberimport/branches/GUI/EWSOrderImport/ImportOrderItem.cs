﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EWSOrderImport
{
  class ImportOrderItem
  {
    public int      OrderNumber   { get; set; }
    public int      Quantity      { get; set; }
    public String   SKU           { get; set; }
    public Decimal? Price         { get; set; }
    public Decimal? OriginalPrice { get; set; }

    public ImportOrderItem()
    { }
  }
}

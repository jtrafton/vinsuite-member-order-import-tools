﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EWSOrderImport
{
  public class ImportOrder
  {
    public int       OrderNumber               { get; set; }
    /// <summary>
    /// MemberID should hold the member identifier from the spreadsheet.
    /// This could be MemberNumber, AltMemberID, maybe email address, etc
    /// </summary>
    public String    MemberID                  { get; set; }
    public DateTime? BillingBirthdate          { get; set; }
    public String    BillingFirstName          { get; set; }
    public String    BillingLastName           { get; set; }
    public String    BillingCompany            { get; set; }
    public String    BillingAddress            { get; set; }
    public String    BillingAddress2           { get; set; }
    public String    BillingCity               { get; set; }
    public String    BillingState              { get; set; }
    public String    BillingZipcode            { get; set; }
    public String    BillingPhone              { get; set; }
    public String    BillingEmail              { get; set; }
    public DateTime? ShippingBirthdate         { get; set; }
    public String    ShippingFirstName         { get; set; }
    public String    ShippingLastName          { get; set; }
    public String    ShippingAddress           { get; set; }
    public String    ShippingAddress2          { get; set; }
    public String    ShippingCity              { get; set; }
    public String    ShippingState             { get; set; }
    public String    ShippingZipcode           { get; set; }
    public String    ShippingPhone             { get; set; }
    public String    ShippingEmail             { get; set; }
    public String    GiftMessage               { get; set; }
    public String    OrderNote                 { get; set; }
    public Decimal?  Taxes                     { get; set; }
    public Decimal?  Handling                  { get; set; }
    public Decimal?  Shipping                  { get; set; }
    public String    CreditCardType            { get; set; }
    public String    CreditCardExpirationMonth { get; set; }
    public String    CreditCardExpirationYear  { get; set; }
    public String    NameonCreditCard          { get; set; }
    public DateTime? OrderDate                 { get; set; }
    public String    OrderType                 { get; set; }
    public Decimal?  SubTotal                  { get; set; }
    public Decimal?  Total                     { get; set; }
    public String    ShippingCompany           { get; set;}

    public ImportOrder()
    { }
  }
}

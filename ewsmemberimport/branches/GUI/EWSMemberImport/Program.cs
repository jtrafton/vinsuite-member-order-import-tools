﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using EWSDataTaskSupport;
using EWSDataTaskSupport.EWSData;

namespace EWSMemberImport
{
  /// <summary>
  /// Holds data about a member import.
  /// Should be created by calling code then sent into the background
  /// thread entry point method.
  /// </summary>
  public class MemberImportPackage
  {
    public bool     Debug           { get; set; }
    public DataTask Task            { get; set; }
    public int      MembersPerRound { get; set; }
    public String   AdminWSUsername { get; set; }
    public String   AdminWSPassword { get; set; }
    public bool     StrictMapping   { get; set; }

    public MemberImportPackage(DataTask task, String adminWSUsername, String adminWSPassword)
    {
      this.Task            = task;
      this.AdminWSUsername = adminWSUsername;
      this.AdminWSPassword = adminWSPassword;
      this.Debug           = false;
      this.StrictMapping   = true;
      this.MembersPerRound = 75;
    }
  }


  /// <summary>
  /// Entry point for a background thread
  /// </summary>
  public class Program
  {
    public static void RunMemberImport(object memberImportPackage)
    {
      MemberImportPackage pkg = memberImportPackage as MemberImportPackage;
      if(pkg == null)
        return;

      RunMemberImport(
        pkg.Debug,
        pkg.Task.File,
        pkg.MembersPerRound,
        pkg.StrictMapping,
        pkg.Task.NotifyEmail,
        pkg.AdminWSUsername,
        pkg.AdminWSPassword,
        pkg.Task.WineryID,
        pkg.Task.DataTaskTrackingID);
    }


    /// <summary>
    /// what does this do?
    /// </summary>
    /// <param name="args"></param>
    public static int RunMemberImport(bool debug, String importFile, int membersPerRound, bool strictMapping, 
      String adminEmail, String adminWSUsername, String adminWSPassword, Guid wineryID, int? dataTaskID)
    {
      // Allow self-signed certs.
      System.Net.ServicePointManager.ServerCertificateValidationCallback += (se, cert, chain, sslerror) => true;

      GetDataCredentialsResponse dataCredentials = null;

      using (DataServiceHttps ds = new DataServiceHttps())
      {
        ds.Timeout = int.MaxValue;

        DataRequest credentialsRequest = new DataRequest()
        {
          Username = adminWSUsername,
          Password = adminWSPassword,
          WineryID = wineryID
        };

        dataCredentials = ds.GetDataCredentials(credentialsRequest);


        String importDirectory                  = Path.GetDirectoryName(importFile);
        String importTimestamp                  = DateTime.Now.ToString().Replace(@"/", "").Replace(":", "");
        MemberGetter mg                         = new MemberGetter(importFile, strictMapping, dataCredentials.Username, dataCredentials.Password, debug);
        Dictionary<String, Member> members      = mg.getMembers();
        ClubGetter cg                           = new ClubGetter(dataCredentials.Username, dataCredentials.Password);
        ImportMemberRequest importMemberRequest = new ImportMemberRequest();
        List<ImportResponseLog> memResponses    = new List<ImportResponseLog>();
        List<ImportResponseLog> ccResponses     = new List<ImportResponseLog>();
        List<ImportResponseLog> clubResponses   = new List<ImportResponseLog>();
        List<ImportResponseLog> shipResponses   = new List<ImportResponseLog>();
        List<ImportResponseLog> noteResponses   = new List<ImportResponseLog>();
        CSVHelper csv                           = new EWSDataTaskSupport.CSVHelper();
        bool isMembersInQueue                   = true;
        int membersToImport                     = members.Count;
        int memberIndex                         = 0;
        int lastMemberIndex                     = 0;
        int responseIndex;
        bool isFullRound;
        ImportMemberResponse importMemberResponse;
        List<Member> importMembers;

        Console.WriteLine("Importing {0} members and {1} club memberships into {2}",
          membersToImport,
          members.Sum(m => m.Value.ClubMemberships == null ? 0 : m.Value.ClubMemberships.Count()),
          wineryID);

        if (debug)
        {
          Console.WriteLine("Not sending because Debug mode is enabled.");
          Console.ReadLine();
          return membersToImport;
        }

        #region Setup requests
        importMemberRequest.Username = adminWSUsername;
        importMemberRequest.Password = adminWSPassword;
        importMemberRequest.WineryID = wineryID;
        importMemberRequest.EmailTo  = adminEmail;

        UpdateDataTaskRequest updTaskRequest = new UpdateDataTaskRequest()
        {
          Username = adminWSUsername,
          Password = adminWSPassword,
          UpdateType = ProgressUpdateType.INCREMENT_PROGRESS,
          DataTask = new DataTask_Update()
          {
            DataTaskTrackingID = dataTaskID ?? 0
          }
        };

        GetDataTasksRequest getTaskRequest = new GetDataTasksRequest()
        {
          Username = adminWSUsername,
          Password = adminWSPassword,
          DataTaskTrackingID = dataTaskID
        };
        #endregion

        bool stopRequested = false;

        while (isMembersInQueue && membersToImport > 0 && !stopRequested)
        {
          //add members for each round of importing
          importMembers = new List<Member>();
          lastMemberIndex = memberIndex;
          isFullRound = false;
          while (!isFullRound)
          {
            if (memberIndex < membersToImport)
            {
              memberIndex++;
              importMembers.Add(members.ElementAt(memberIndex - 1).Value);
              isFullRound = memberIndex % membersPerRound == 0;
            }
            else
            {
              isMembersInQueue = false;
              break;
            }
          }

          Console.WriteLine("Importing members {0} through {1}", lastMemberIndex + 1, memberIndex);

          importMemberRequest.Members = importMembers.ToArray();
          //import members
          try
          {
            importMemberResponse = DataTaskHelper.TryNTimes(ds.ImportMembers, importMemberRequest, 90, TimeSpan.FromSeconds(10));

            if (importMemberResponse != null)
            {
              if (importMemberResponse.IsSuccessful)
              {
                #region write responses to log
                responseIndex = 0;
                foreach (ImportMemberResponses responses in importMemberResponse.Responses)
                {
                  ImportResponseLog memResponse = new ImportResponseLog
                  {
                    Key = importMembers[responseIndex].AltMemberID,
                    MemberID = responses.memberResponse.MemberID,
                    LastName = importMembers[responseIndex].LastName,
                    FirstName = importMembers[responseIndex].FirstName,
                    Email = importMembers[responseIndex].Email,
                    Error = responses.memberResponse.ErrorMessage
                  };
                  memResponses.Add(memResponse);

                  var ccResponse = from r in responses.ccResponses
                                   select new ImportResponseLog
                                   {
                                     Key = memResponse.Key,
                                     MemberID = memResponse.MemberID,
                                     LastName = importMembers[responseIndex].LastName,
                                     FirstName = importMembers[responseIndex].FirstName,
                                     Email = importMembers[responseIndex].Email,
                                     ObjectID = r.CreditCardID,
                                     Error = r.ErrorMessage
                                   };
                  ccResponses.AddRange(ccResponse);

                  var clubResponse = from r in responses.clubResponses
                                     select new ImportResponseLog
                                     {
                                       Key = memResponse.Key,
                                       MemberID = memResponse.MemberID,
                                       LastName = importMembers[responseIndex].LastName,
                                       FirstName = importMembers[responseIndex].FirstName,
                                       Email = importMembers[responseIndex].Email,
                                       ObjectID = r.ClubMemberID,
                                       Error = r.ErrorMessage
                                     };
                  clubResponses.AddRange(clubResponse);

                  var shipResponse = from r in responses.shipResponses
                                     select new ImportResponseLog
                                     {
                                       Key = memResponse.Key,
                                       MemberID = memResponse.MemberID,
                                       LastName = importMembers[responseIndex].LastName,
                                       FirstName = importMembers[responseIndex].FirstName,
                                       Email = importMembers[responseIndex].Email,
                                       ObjectID = r.ShippingAddressID,
                                       Error = r.ErrorMessage
                                     };
                  shipResponses.AddRange(shipResponse);

                  var noteResponse = from r in responses.notesResponses
                                     select new ImportResponseLog
                                     {
                                       Key = memResponse.Key,
                                       MemberID = memResponse.MemberID,
                                       LastName = importMembers[responseIndex].LastName,
                                       FirstName = importMembers[responseIndex].FirstName,
                                       Email = importMembers[responseIndex].Email,
                                       ObjectID = r.MemberNoteID,
                                       Error = r.ErrorMessage
                                     };
                  noteResponses.AddRange(noteResponse);

                  ++responseIndex;
                }
                #endregion

                #region Update data task tracking progress
                if (dataTaskID.HasValue)
                {
                  updTaskRequest.DataTask.JobProgress = importMemberRequest.Members.Length;
                  UpdateDataTaskResponse updTaskResponse = DataTaskHelper.TryNTimes(ds.UpdateDataTask, updTaskRequest, 100, TimeSpan.FromSeconds(10));

                  // Store if the task was requested to stop
                  stopRequested = updTaskResponse.Task.StopRequested ?? false;
                }
                #endregion
              }
            }
            else
            {
              throw new Exception("Error importing members: " + importMemberResponse.ErrorMessage);
            }
          }
          catch (Exception ex)
          {
            Console.WriteLine("Exception: " + ex.Message);
            String exStr = "Members " + (lastMemberIndex + 1) + " - " + memberIndex + " Exception: " + ex.Message;
            using (System.IO.StreamWriter file = new System.IO.StreamWriter(importDirectory + @"\Exceptions.txt", true))
            {
              file.WriteLine(exStr);
            }
          }
        }

        Console.WriteLine("Writing log");
        csv.WriteCSVFile(importDirectory + @"\memResponse " + importTimestamp + ".csv", memResponses);
        csv.WriteCSVFile(importDirectory + @"\shipResponse " + importTimestamp + ".csv", shipResponses);
        csv.WriteCSVFile(importDirectory + @"\clubResponse " + importTimestamp + ".csv", clubResponses);
        csv.WriteCSVFile(importDirectory + @"\ccResponse " + importTimestamp + ".csv", ccResponses);
        csv.WriteCSVFile(importDirectory + @"\noteResponse " + importTimestamp + ".csv", noteResponses);

        // TODO: Should probably email the logs to the data task owner

        return membersToImport;
      }
    }
  }
}
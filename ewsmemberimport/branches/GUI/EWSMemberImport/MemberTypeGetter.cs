﻿using System;
using System.Collections.Generic;
using EWSDataTaskSupport;
using EWSDataTaskSupport.EWSMember;

namespace EWSMemberImport
{
  class MemberTypeGetter
  {
    private Dictionary<String, EWSDataTaskSupport.EWSData.MemberType> memberTypes;
    private EWSDataTaskSupport.EWSData.MemberType memberType;
    private String wsUsername;
    private String wsPassword;
    private bool debug;

    /**
     * Pull all the member types for the winery and add them to a
     * sorted collection.
     */
    public MemberTypeGetter(String username, String password, bool debug)
    {
      GetWineryMemberTypeRequest req = new GetWineryMemberTypeRequest();
      GetWineryMemberTypeResponse resp;

      this.wsUsername = username;
      this.wsPassword = password;

      using (MemberServiceHttps serv = new MemberServiceHttps())
      {
        // Set up the credentials.
        req.Username = wsUsername;
        req.Password = wsPassword;

        // Get the member types.
        resp = serv.GetWineryMemberType(req);
        this.memberTypes = new Dictionary<String, EWSDataTaskSupport.EWSData.MemberType>();

        if (resp.IsSuccessful)
        {
          foreach (MemberType mt in resp.MemberTypes)
          {
            memberType = new EWSDataTaskSupport.EWSData.MemberType();
            memberType.MemberTypeDescription = mt.MemberTypeDescription;
            memberType.MemberTypeID = mt.MemberTypeID;
            memberType.WineryID = mt.WineryID;
            if (!this.memberTypes.ContainsKey(mt.MemberTypeDescription.ToUpper().Trim()))
              this.memberTypes.Add(mt.MemberTypeDescription.ToUpper().Trim(), memberType);
            else
              Console.WriteLine("Duplicate member type: {0}", mt.MemberTypeDescription);
          }
        }
        else
          throw new Exception("GetWineryMemberType error: " + resp.ErrorMessage);
      }

      this.debug = debug;
    }

    /**
     * Get the collection of member types.
     */
    public Dictionary<String, EWSDataTaskSupport.EWSData.MemberType> getMemberTypes()
    {
      return this.memberTypes;
    }

    /**
     * Add member type to the winery member types, update list, return MemberTypeID
     */
    public Guid? addToMemberTypes(string memberType)
    {
      Guid? memberTypeID = Guid.NewGuid();

      if (!debug)
      {
        AddMemberTypeRequest amtRequest = new AddMemberTypeRequest();
        AddMemberTypeResponse amtResponse;
        amtRequest.Username = wsUsername;
        amtRequest.Password = wsPassword;
        amtRequest.MemberType = new EWSDataTaskSupport.EWSMember.MemberType { MemberTypeDescription = memberType};
        using (MemberServiceHttps serv = new MemberServiceHttps())
        {
          amtResponse = serv.AddMemberType(amtRequest);
          if (amtResponse.IsSuccessful)
            memberTypeID = amtResponse.MemberTypeID;
          else
            throw new Exception("Error adding member type: " + amtResponse.ErrorMessage);
        }
      }
      return memberTypeID;
    }
  }
}

﻿using System;
using System.Collections.Generic;
using EWSDataTaskSupport;
using EWSDataTaskSupport.EWSMember;
using EWSDataTaskSupport.EWSOrder;

namespace EWSMemberImport
{
  class ClubGetter
  {
    private Dictionary<String, WineClub> clubs;
    private String wsUsername;
    private String wsPassword;

    /**
     * Pull a list of clubs and add them to a dictionary.
     */
    public ClubGetter(String username, String password)
    {
      this.wsUsername = username;
      this.wsPassword = password;

      using (MemberServiceHttps serv = new MemberServiceHttps())
      {
        GetClubRequest req = new GetClubRequest();
        GetClubResponse resp;

        // Set up the credentials.
        req.Username = wsUsername;
        req.Password = wsPassword;

        // Get all clubs, not just active ones.
        req.IncludeInactiveClubs = true;
        
        // Get the clubs.
        resp = serv.GetClub(req);
        this.clubs = new Dictionary<String, WineClub>();

        foreach (WineClub club in resp.WineClubs)
        {
          // added for use in testDB
          if (!this.clubs.ContainsKey(club.Title.Trim().ToUpper()))
            this.clubs.Add(club.Title.Trim().ToUpper(), club);
          else
            Console.WriteLine("Duplicate club level: {0}", club.Title);
        }
      }
    }

    /**
     * Get the list of clubs.
     */
    public Dictionary<String, WineClub> getClubs()
    {
      return this.clubs;
    }

    /**
     * TODO: move to ShippingTypeGetter.
     */
    public Dictionary<String, WineryShippingOptions> GetWineryShippingTypes()
    {
      Dictionary<String, WineryShippingOptions> shipOptions = new Dictionary<String, WineryShippingOptions>();
      GetShippingOptionsRequest gsReq;
      using (OrderServiceHttps os = new OrderServiceHttps())
      {
        gsReq = new GetShippingOptionsRequest();
        gsReq.Username = this.wsUsername;
        gsReq.Password = this.wsPassword;

        GetShippingOptionsResponse gsResp = os.GetShippingOptions(gsReq);
        if (gsResp.IsSuccessful)
        {
          foreach (WineryShippingOptions wso in gsResp.WineriesShippingOptions)
          {
            if (wso.ShippingOptionType == "WINE")
              shipOptions.Add(wso.ShippingType.ToUpper().Trim(), wso);
          }
        }
        else
          throw new Exception("Error getting winery shipping options: " + gsResp.ErrorMessage);
      }

      return shipOptions;
    }
  }
}

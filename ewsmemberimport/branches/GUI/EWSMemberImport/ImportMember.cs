﻿using System;

namespace EWSMemberImport
{
  class ImportMember
  {
    public String    AltMemberID            { get; set; }
    public Int32?    MemberNumber           { get; set; }
    public DateTime? BillingDateOfBirth     { get; set; }
    public String    Salutation             { get; set; }
    public String    FirstName              { get; set; }
    public String    LastName               { get; set; }
    public String    Company                { get; set; }
    public String    Address                { get; set; }
    public String    Address2               { get; set; }
    public String    City                   { get; set; }
    public String    State                  { get; set; }
    public String    Province               { get; set; }
    public String    ZipCode                { get; set; }
    public String    Phone                  { get; set; }
    public String    Phone2                 { get; set; }
    public String    Email                  { get; set; }
    public String    UserName               { get; set; }
    public String    Password               { get; set; }
    public String    CreditCardType         { get; set; }
    public String    CreditCardNumber       { get; set; }
    public String    CreditCardExpMo        { get; set; }
    public String    CreditCardExpYr        { get; set; }
    public String    NameOnCard             { get; set; }
    public String    MemberType             { get; set; }
    public String    SourceCode             { get; set; }
    public DateTime? CustomerCreationDate   { get; set; }
    public DateTime? CustomerLastUpdated    { get; set; }
    public String    CustomerNotes          { get; set; }
    public DateTime? ShippingBirthDate      { get; set; }
    public String    ShipSalutation         { get; set; }
    public String    ShipFirstName          { get; set; }
    public String    ShipLastName           { get; set; }
    public String    ShipCompany            { get; set; }
    public String    ShipAddress            { get; set; }
    public String    ShipAddress2           { get; set; }
    public String    ShipCity               { get; set; }
    public String    ShipState              { get; set; }
    public String    ShipProvince           { get; set; }
    public String    ShipZipCode            { get; set; }
    public Boolean   IsPrimaryShipAddress   { get; set; }
    public String    ShipPhone              { get; set; }
    public String    ShipPhone2             { get; set; }
    public String    ShipEmailAddress       { get; set; }
    public DateTime? ClubBirthDate          { get; set; }
    public String    ClubSalutation         { get; set; }
    public String    ClubFirstName          { get; set; }
    public String    ClubLastName           { get; set; }
    public String    ClubCompany            { get; set; }
    public String    ClubAddress            { get; set; }
    public String    ClubAddress2           { get; set; }
    public String    ClubCity               { get; set; }
    public String    ClubState              { get; set; }
    public String    ClubProvince           { get; set; }
    public String    ClubZipCode            { get; set; }
    public String    ClubPhone              { get; set; }
    public String    ClubPhone2             { get; set; }
    public String    ClubEmailAddress       { get; set; }
    public String    ClubShippingPreference { get; set; }
    public Boolean   IsPurchaser            { get; set; }
    public Boolean   IsSubscriber           { get; set; }
    public Boolean   IsUnsubscriber         { get; set; }
    public String    ClubLevel              { get; set; }
    public Boolean   ClubIsActive           { get; set; }
    public Boolean   ClubOnHold             { get; set; }
    public DateTime? ClubStartDate          { get; set; }
    public DateTime? ClubEndDate            { get; set; }
    public DateTime? ClubHoldStartDate      { get; set; }
    public DateTime? ClubHoldEndDate        { get; set; }
    public DateTime? ClubCancelDate         { get; set; }
    public Boolean   IsGiftMembership       { get; set; }
    public Int32?    TotalNoShipments       { get; set; }
    public Int32?    CurrentNoShipments     { get; set; }
    public String    GiftMessage            { get; set; }
  }
}

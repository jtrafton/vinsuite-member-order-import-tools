﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EWSDataTaskSupport.EWSWebsite;

namespace EWSDataTaskSupport
{
  public class WebsiteServiceHttps : WebsiteService
  {
    /// <summary>
    /// This class exposes a normal WebsiteService object 
    /// but changes the URL to use HTTPS
    /// </summary>
    public WebsiteServiceHttps()
      : base()
    {
      if (!(this.Url.Contains("localhost") || this.Url.Contains("127.0.0.1")))
        this.Url = this.Url.Replace("http:", "https:");
    }
  }
}

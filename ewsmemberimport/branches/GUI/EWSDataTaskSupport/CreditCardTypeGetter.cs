﻿using System;
using System.Collections.Generic;
using EWSDataTaskSupport.EWSOrder;
using System.Collections;

namespace EWSDataTaskSupport
{
  public class CreditCardTypeGetter
  {
    private Dictionary<String, CreditCardTypeInfo> cardTypes;
    

    public CreditCardTypeGetter(String username, String password)
    {
      this.cardTypes = new Dictionary<String, CreditCardTypeInfo>();

      using (OrderService serv = new OrderServiceHttps())
      {
        GetCreditCardTypeRequest req = new GetCreditCardTypeRequest();
        GetCreditCardTypeResponse resp;

        // Set up the credentials.
        req.Username = username;
        req.Password = password;

        // Get the member types.
        resp = serv.GetCreditCardType(req);
        this.cardTypes = new Dictionary<String, CreditCardTypeInfo>();

        foreach (CreditCardTypeInfo ct in resp.CreditCardTypes)
          this.cardTypes.Add(ct.CreditCardTypeShort.ToLower(), ct);
      }
    }

    /**
     * Get the card types dic.
     */
    public Dictionary<String, CreditCardTypeInfo> getCreditCardTypes()
    {
      return this.cardTypes;
    }
  }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace EWSDataTaskSupport
{
  public class DataTaskHelper
  {
    private static Random random = new Random();

    #region Common helpful transforms
    private static Func<String, Object> defaultBool = null;
    private static Func<String, Object> defaultDate = null;
    #endregion

    /// <summary>
    /// Generate a string of random characters of a specified length.
    /// Useful for generating passwords or password salt.
    /// </summary>
    /// <param name="length"></param>
    /// <returns></returns>
    public static String RandomString(int length)
    {
      length = Math.Abs(length);
      StringBuilder sb = new StringBuilder(length, length);

      for (int i = 0; i < length; ++i)
        sb.Append((char)random.Next(33, 127));

      return sb.ToString();
    }


    /// <summary>
    /// Transforms 1/0, yes/no, true/false to a boolean
    /// </summary>
    public static Func<String, Object> DefaultBooleanTransform
    {
      get 
      {
        if (defaultBool == null)
        {
          defaultBool = (cellValue) =>
          {
            if (string.IsNullOrEmpty(cellValue))
              return false;

            if (cellValue == "1" ||
              String.Equals("true", cellValue, StringComparison.OrdinalIgnoreCase) ||
              String.Equals("yes", cellValue, StringComparison.OrdinalIgnoreCase))
            {
              return true;
            }

            if (cellValue == "0" ||
              String.Equals("false", cellValue, StringComparison.OrdinalIgnoreCase) ||
              String.Equals("no", cellValue, StringComparison.OrdinalIgnoreCase))
            {
              return false;
            }

            throw new Exception("DataTaskHelper: Invalid boolean representation: \"" + cellValue + "\"");
          };
        }

        return defaultBool; 
      }
    }


    /// <summary>
    /// Transforms a date string into a date or null if it doesn't parse
    /// </summary>
    public static Func<String, Object> DefaultDateTransform
    {
      get
      {
        if (defaultDate == null)
        {
          defaultDate = (cellValue) =>
          {
            DateTime t;

            if (String.IsNullOrWhiteSpace(cellValue))
              return null;

            if (DateTime.TryParse(cellValue, out t))
              return t;
            else
              throw new Exception("Invalid date format: \"" + cellValue + "\"");
          };
        }

        return defaultDate;
      }
    }


    /// <summary>
    /// Call a web service method, wrapped in a try/catch block
    /// </summary>
    /// <typeparam name="RQ">Request type</typeparam>
    /// <typeparam name="RS">Response type</typeparam>
    /// <param name="wsMethod">Web service method</param>
    /// <param name="request">Request object</param>
    /// <param name="maxTries">Number of retries before failure</param>
    /// <param name="retryWait">How long to wait between retries</param>
    /// <returns>A valid instance of RS if the call succeeded, otherwise null</returns>
    public static RS TryNTimes<RQ, RS>(Func<RQ, RS> wsMethod, RQ request, int maxTries, TimeSpan retryWait)
      where RS : class
    {
      int failCount = 0;

      do
      {
        try
        {
          return wsMethod.Invoke(request);
        }
        catch (Exception ex)
        {
          ++failCount;
          Thread.Sleep(retryWait);
        }
      } while (failCount < maxTries);

      return null;
    }
  }
}

﻿using System;

namespace DirectDataExtraction
{
  class ComboBoxItem<K, V>
  {
    private K key;
    private V value;

    public ComboBoxItem(K key, V value)
    {
      this.key = key;
      this.value = value;
    }


    public K Key
    {
      get { return this.key; }
    }


    public V Value
    {
      get { return this.value; }
    }


    /// <summary>
    /// Returns the key as a string. This is the only reason this object exists...
    /// </summary>
    /// <returns></returns>
    public override string ToString()
    {
      return this.Key.ToString();
    }
  }
}

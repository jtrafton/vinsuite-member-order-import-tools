﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EWSCreditCardImport
{
  class ImportCreditCard
  {
    public String AltMemberID { get; set; }
    public String Type { get; set; }
    public String CardNumber { get; set; }
    public String ExpMo { get; set; }
    public String ExpYr { get; set; }
    public String NameOnCard { get; set; }
    public bool   IsPrimary { get; set; }
  }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using EWSDataTaskSupport.EWSMember;
using EWSDataTaskSupport.EWSProduct;
using EWSDataTaskSupport.EWSOrder;

namespace OrderExtraction
{
  public class Program
  {
    public static void Main(String[] args)
    {
      String username = "achernyak";
      String password = "99alex";
      String name = "andis";
      String path = @"C:\dotnet\data_exports\andis wines\";

      ExportMembers(username, password, path, name);
      ExportOrders(username, password, path, name);
      ExportProducts(username, password, path, name);
    }

    static void ExportMembers(String username, String password, String path, String name)
    {
      using (MemberService msvc = new MemberService())
      {
        //TODO: needs to be 20% cooler
        bool isIncludeCC = true;
        msvc.Timeout = int.MaxValue;
        msvc.Url = msvc.Url.Replace("http:", "https:");

        String memberFile =   path + name + " members.csv";
        String memTypesFile = path + name + " member types.csv";
        String clubFile =     path + name + " club memberships.csv";
        String addressFile =  path + name + " addresses.csv";
        String ccFile =       path + name + " ccs.csv";
        String notesFile =    path + name + " notes.csv";
        int connRetries = 0;
        int maxRetries = 3;

        GetMemberRequest gmRequest = new GetMemberRequest()
        {
          Username = username,
          Password = password,
          IncludeClubMemberships = true,
          IncludeShippingAddresses = true,
          IncludeCreditCardLastFour = false,
          StartRow = 1,
          MaxRows = 200
        };

        GetCreditCardRequest getCCRequest = new GetCreditCardRequest()
        {
          Username = username,
          Password = password
        };

        GetMemberNotesRequest gmNotesRequest = new GetMemberNotesRequest()
        {
          Username = username,
          Password = password
        };

        GetMemberResponse gmResponse;
        GetCreditCardResponse getCCResponse;
        GetMemberNotesResponse gmNotesResponse;
        TextWriter memberWriter = new StreamWriter(memberFile);
        TextWriter memTypesWriter = new StreamWriter(memTypesFile);
        TextWriter clubWriter = new StreamWriter(clubFile);
        TextWriter addressWriter = new StreamWriter(addressFile);
        TextWriter ccWriter = new StreamWriter(ccFile);
        TextWriter notesWriter = new StreamWriter(notesFile);
        int iterations = 0;

        #region Member Header
        memberWriter.WriteLine(
          "MemberID" +
          ",MemberNumber" +
          ",AltMemberID" +
          ",Salutation" +
          ",FirstName" +
          ",LastName" +
          ",DateOfBirth" +
          ",Address1" +
          ",Address2" +
          ",City" +
          ",State" +
          ",Province" +
          ",ZipPostalCode" +
          ",CountryCode" +
          ",Company" +
          ",Email" +
          ",Phone1" +
          ",Phone2" +
          ",Username" +
          ",Password"
          );
        #endregion

        #region MemberTypes header
        memTypesWriter.WriteLine(
          "MemberID" +
          ",MemberTypeID" +
          ",MemberType"
          );
        #endregion

        #region Club Header
        clubWriter.WriteLine(
          "MemberID" +
          ",AltClubMembershipID" +
          ",ClubLevelID" +
          ",ClubLevelName" +
          ",ClubFrequency" +
          ",EndDate" +
          ",GiftMessage" +
          ",IsActive" +
          ",IsOnHold" +
          ",OnHoldDate" +
          ",CancelDate" +
          ",LastBillDate" +
          ",LastPaymentStatus" +
          ",NextBillDate" +
          ",ShippingSalutation" +
          ",ShippingFirstName" +
          ",ShippingLastName" +
          ",ShippingAddress1" +
          ",ShippingAddress2" +
          ",ShippingCity" +
          ",ShippingState" +
          ",ShippingProvince" +
          ",ShippingZipPostalCode" +
          ",ShippingCountryCode" +
          ",ShippingBirthDate" +
          ",ShippingPhone1" + 
          ",ShippingPhone2" +
          ",ShippingEmail" +
          ",HoldReason" +
          ",CancelReason" +
          ",StartDate" +
          ",SourceCode");
        #endregion

        #region Address Header
        addressWriter.WriteLine(
          "MemberID" +
          ",IsPrimary" +
          ",Salutation" +
          ",FirstName" +
          ",Lastname" +
          ",Address1" +
          ",Address2" +
          ",City" +
          ",State" +
          ",Province" +
          ",ZipPostalCode" +
          ",CountryCode" +
          ",Email" +
          ",Phone1" +
          ",Phone2");
        #endregion

        #region CC Header
        ccWriter.WriteLine(
          "MemberID" +
          ",IsPrimary" +
          ",CardAlias" +
          ",NameOnCard" +
          ",ExpMonth" +
          ",ExpYear" +
          ",CardNumber" +
          ",CardType");
        #endregion

        #region Notes header
        notesWriter.WriteLine(
          "MemberID" +
          ",MemberNote");
        #endregion

        do
        {
          gmResponse = msvc.GetMember(gmRequest);
          
          foreach (Member m in gmResponse.Members)
          {
            #region Member Fields
            memberWriter.WriteLine(
              csv(m.MemberID, false) +
              csv(m.MemberNumber) +
              csv(m.AltMemberID) +
              csv(m.Salutation) +
              csv(m.FirstName) +
              csv(m.LastName) +
              csv(m.BirthDate) +
              csv(m.Address1) +
              csv(m.Address2) +
              csv(m.City) +
              csv(m.State) +
              csv(m.Province) +
              csv(m.ZipCode) +
              csv(m.CountryCode) +
              csv(m.Company) +
              csv(m.Email) +
              csv(m.Phone1) +
              csv(m.Phone2) +
              csv(m.Username) +
              csv(m.Password));
            #endregion

            if (m.MemberTypes != null)
            {
              foreach (MemberType mt in m.MemberTypes)
              {
                #region Member Types Fields
                memTypesWriter.WriteLine(
                  csv(mt.MemberID, false) +
                  csv(mt.MemberTypeID) +
                  csv(mt.MemberTypeDescription));
                #endregion
              }
            }
            
            if (m.ClubMemberships != null)
            {
              foreach (ClubMembership cm in m.ClubMemberships)
              {
                #region Club Membership Fields
                clubWriter.WriteLine(
                  csv(cm.MemberID, false) +
                  csv(cm.AltClubMembershipID) +
                  csv(cm.ClubLevelID) +
                  csv(cm.ClubLevelName) +
                  csv(cm.ClubFrequency) +
                  csv(cm.EndDate) +
                  csv(cm.GiftMessage) +
                  csv(cm.IsActive) +
                  csv(cm.IsOnHold) +
                  csv(cm.OnHoldDate) +
                  csv(cm.CancelDate) +
                  csv(cm.LastBillDate) +
                  csv(cm.LastPaymentStatus) +
                  csv(cm.NextBillDate) +
                  csv(cm.ShippingSalutation) +
                  csv(cm.ShippingFirstName) +
                  csv(cm.ShippingLastName) +
                  csv(cm.ShippingAddress1) +
                  csv(cm.ShippingAddress2) +
                  csv(cm.ShippingCity) +
                  csv(cm.ShippingState) +
                  csv(cm.ShippingProvince) +
                  csv(cm.ShippingZipCode) +
                  csv(cm.ShippingCountryCode) +
                  csv(cm.ShippingBirthDate) +
                  csv(cm.ShippingPhone1) +
                  csv(cm.ShippingPhone2) +
                  csv(cm.ShippingEmail) +
                  csv(cm.HoldReason) +
                  csv(cm.CancelReason) +
                  csv(cm.ActiveMembershipOn) +
                  csv(cm.SourceCode));
                #endregion
              }
            }

            if (m.ShippingAddresses != null)
            {
              foreach (EWSDataTaskSupport.EWSMember.ShippingAddress ad in m.ShippingAddresses)
              {
                #region Address Fields
                addressWriter.WriteLine(
                  csv(ad.MemberID, false) +
                  csv(ad.IsPrimary) +
                  csv(ad.Salutation) +
                  csv(ad.FirstName) +
                  csv(ad.LastName) +
                  csv(ad.Address1) +
                  csv(ad.Address2) +
                  csv(ad.City) +
                  csv(ad.State) +
                  csv(ad.Province) +
                  csv(ad.ZipCode) +
                  csv(ad.CountryCode) +
                  csv(ad.Email) +
                  csv(ad.Phone1) +
                  csv(ad.Phone2));
                #endregion
              }
            }

            //get cc's
            if (isIncludeCC)
            {
              while (connRetries < maxRetries)
              {
                try
                {
                  getCCRequest.MemberID = m.MemberID;
                  getCCResponse = msvc.GetCreditCard(getCCRequest);
                  if (!getCCResponse.IsSuccessful)
                    throw new Exception("Error retrieving cc's: " + getCCResponse.ErrorMessage);
                  foreach (CreditCardRecord cc in getCCResponse.CreditCards)
                  {
                    #region CC Fields
                    if (!String.IsNullOrEmpty(cc.CreditCardNumber))
                    {
                      ccWriter.WriteLine(
                        csv(cc.MemberID, false) +
                        csv(cc.IsPrimary) +
                        csv(cc.CreditCardAlias) +
                        csv(cc.NameOnCreditCard) +
                        csv(cc.CreditCardExpirationMonth) +
                        csv(cc.CreditCardExpirationYear) +
                        csv(cc.CreditCardNumber) +
                        csv(cc.CreditCardTypeShort).ToUpper());
                    }
                    #endregion
                  }
                  connRetries = 0;
                  break;
                }
                catch (Exception ex)
                {
                  ++connRetries;
                  Console.WriteLine("Failed to retrieve member cc info for member number:" + m.MemberNumber + " (" + ex.Message + ")" + " attempt: " + connRetries);
                  if (connRetries > maxRetries)
                    throw new Exception("Get CC call failed more than " + maxRetries + " times in a row.");
                  System.Threading.Thread.Sleep(5000);
                }
              }
            }

            //get member notes
            while (connRetries < maxRetries)
            {
              try
              {
                gmNotesRequest.MemberID = m.MemberID;
                gmNotesResponse = msvc.GetMemberNotes(gmNotesRequest);
                if (!gmNotesResponse.IsSuccessful)
                  throw new Exception("Error retrieving member notes: " + gmNotesResponse.ErrorMessage);
                foreach (MemberNote note in gmNotesResponse.MemberNotes)
                {
                  notesWriter.WriteLine(
                    csv(note.MemberID, false) +
                    csv(note.Notes));
                }
                connRetries = 0;
                break;
              }
              catch (Exception ex)
              {
                ++connRetries;
                Console.WriteLine("Failed to retrieve member notes for member number:" + m.MemberNumber + " (" + ex.Message + ")" + " attempt: " + connRetries);
                if (connRetries > maxRetries)
                  throw new Exception("Get Member Notes call failed more than " + maxRetries + " times in a row.");
                System.Threading.Thread.Sleep(5000);
              }
            }
          }

          ++iterations;
          Console.WriteLine(gmRequest.MaxRows * iterations);
          gmRequest.StartRow += gmRequest.MaxRows;

          //if (iterations == 1)
          //  break;

          memberWriter.Flush();
          memTypesWriter.Flush();
          clubWriter.Flush();
          addressWriter.Flush();
          ccWriter.Flush();
          notesWriter.Flush();

        } while (gmResponse.Members.Length == gmRequest.MaxRows);

        
        memberWriter.Close();
        memberWriter.Dispose();

        memTypesWriter.Close();
        memTypesWriter.Dispose();
        
        clubWriter.Close();
        clubWriter.Dispose();
        
        addressWriter.Close();
        addressWriter.Dispose();
        
        ccWriter.Close();
        ccWriter.Dispose();

        notesWriter.Close();
        notesWriter.Dispose();
      }
    }

    static void ExportProducts(String username, String password, String path, String name)
    {
      using (ProductService psvc = new ProductService())
      {
        psvc.Timeout = int.MaxValue;
        String outputFile = path + name + " products.csv";

        GetProductRequest gpRequest = new GetProductRequest()
        {
          Username = username,
          Password = password,
          IncludeProductPrices = true,
        };

        GetProductResponse gpResponse = psvc.GetProduct(gpRequest);

        using (FileStream fs = new FileStream(outputFile, FileMode.Create, FileAccess.Write))
        {
          using (TextWriter tw = new StreamWriter(fs))
          {
            tw.WriteLine(
              "ProductID" +
              ",SKU" +
              ",Name" +
              ",Wine" +
              ",Taxable" +
              ",MemberTeaser" +
              ",NonMemberTeaser" +
              ",Teaser" +
              ",Description" +
              ",ProductType" +
              ",ReportGroup" +
              ",Active" +
              ",Price1" +
              ",Price2" +
              ",PricePerBottle" +
              ",PricePerCase");

            foreach (Product p in gpResponse.Products)
            {
              tw.WriteLine(
                csv(p.ProductID, false) +
                csv(p.ProductPrices.Length > 0 ? p.ProductPrices[0].SKU : p.ProductSKU) +
                csv(p.ProductName) +
                csv(0) +
                csv((p.isTaxable ?? false) ? "1" : "0") +
                csv(p.MemberTeaser) +
                csv(p.NonMemberTeaser) +
                csv(p.Teaser) +
                csv(p.Description) +
                csv("First Party Product") +
                csv(p.ReportGroup) +
                csv(p.isActive) +
                csv(p.Price1) +
                csv(p.Price2) +
                csv(null) +
                csv(null));
            }

            GetWineRequest gwRequest = new GetWineRequest()
            {
              Username = username,
              Password = password,
              IsThirdParty = false
            };
            
            GetWineResponse gwResponse = psvc.GetWine(gwRequest);

            foreach (Wine w in gwResponse.Wines)
            {
              tw.WriteLine(
                csv(w.ProductID, false) +
                csv(String.IsNullOrEmpty(w.WineSKU) ? w.ProductSKU : w.WineSKU) + 
                csv((String.IsNullOrEmpty(w.ProductName) ? w.WineName : w.ProductName)) +
                csv(1) +
                csv((w.isTaxable ?? false) ? "1" : "0") +
                csv(w.MemberTeaser) +
                csv(w.NonMemberTeaser) +
                csv(w.Teaser) +
                csv((String.IsNullOrEmpty(w.Description) ? w.Description : w.Description)) +
                csv("First Party Wine (Bottles/Cases)") +
                csv(w.ReportGroup) +
                csv(w.isActive) +
                csv(w.Price1) +
                csv(w.Price2) +
                csv(w.WinePricePerBottle) +
                csv(w.WinePricePerCase));
            }

            tw.Flush();
          }
        }
      }
    }

    static void ExportOrders(String username, String password, String path, String name)
    {
      using (OrderService osvc = new OrderService())
      {
        osvc.Timeout = int.MaxValue;
        osvc.Url = osvc.Url.Replace("http://", "https://");

        GetOrderRequest goRequest = new GetOrderRequest()
        {
          Username = username,
          Password = password,
          //Status = "completed",
          MaxRows = 200,
          StartRow = 1
        };

        String outputFile = path + name + " orders.csv";

        using (FileStream fs = new FileStream(outputFile, FileMode.Create, FileAccess.Write))
        {
          using (TextWriter tw = new StreamWriter(fs))
          {
            tw.WriteLine(
              "Order Number" +
              ",Member ID" +
              ",Billing Birthdate" +
              ",Billing First Name" +
              ",Billing Last Name" +
              ",Billing Company" +
              ",Billing Address" +
              ",Billing Address 2" +
              ",Billing City" +
              ",Billing State" +
              ",Billing Zipcode" +
              ",Billing Phone" +
              ",Billing E-mail" +
              ",Shipping Birthdate" +
              ",Shipping First Name" +
              ",Shipping Last Name" +
              ",Shipping Company" +
              ",Shipping Address" +
              ",Shipping Address 2" +
              ",Shipping City" +
              ",Shipping State" +
              ",Shipping Zipcode" +
              ",Shipping Phone" +
              ",Shipping E-mail" +
              ",Gift Message" +
              ",Order Notes" +
              ",Taxes ($)" +
              ",Handling ($)" +
              ",Shipping ($)" +
              ",OrderSubtotal" +
              ",OrderTotal" +
              ",Credit Card Type" +
              ",Credit Card Expiration Month" +
              ",Credit Card Expiration Year" +
              ",Name on Credit Card" +
              ",Order Date" +
              ",OrderType" +
              ",Quantity" +
              ",SKU" +
              ",Price");

            GetOrderResponse goResponse;
            int iterations = 1;

            do
            {
              Console.WriteLine("Getting orders " + goRequest.StartRow + " - " + (goRequest.StartRow + (goRequest.MaxRows - 1)) + "...");
              goResponse = osvc.GetOrder(goRequest);

              foreach (Order order in goResponse.Orders)
              {
                foreach (OrderItem item in order.OrderItem)
                {
                  tw.WriteLine(
                    csv(order.OrderNumber, false) +
                    csv(order.MemberID) +
                    csv(order.BillingBirthDate) +
                    csv(order.BillingFirstName) +
                    csv(order.BillingLastName) +
                    csv(order.BillingCompany) +
                    csv(order.BillingAddress1) +
                    csv(order.BillingAddress2) +
                    csv(order.BillingCity) +
                    csv(order.BillingState) +
                    csv(order.BillingZipCode) +
                    csv(order.BillingPhone) +
                    csv(order.BillingEmail) +
                    csv(order.ShippingBirthDate) +
                    csv(order.ShippingFirstName) +
                    csv(order.ShippingLastName) +
                    csv(order.ShippingCompany) +
                    csv(order.ShippingAddress1) +
                    csv(order.ShippingAddress2) +
                    csv(order.ShippingCity) +
                    csv(order.ShippingState) +
                    csv(order.ShippingZipCode) +
                    csv(order.ShippingPhone) +
                    csv(order.ShippingEmail) +
                    csv(order.GiftMessage) +
                    csv(order.OrderNotes) +
                    csv(order.Taxes) +
                    csv(order.TaxableHandling) +
                    csv(order.Shipping) +
                    csv(order.SubTotal) +
                    csv(order.Total) +
                    csv(order.CreditCardType) +
                    csv(order.CreditCardExpirationMonth) +
                    csv(order.CreditCardExpirationYear) +
                    csv(order.NameOnCreditCard) +
                    csv(order.DateAdded) +
                    csv(order.OrderType) +
                    csv(item.Quantity) +
                    csv(item.SKU) +
                    csv(item.PriceAfterDiscount));
                }
              }
              
              tw.Flush();

              goRequest.StartRow += goRequest.MaxRows;
              ++iterations;

            } while (goResponse.Orders.Length > 0);

            Console.WriteLine("Done!");
            //Console.ReadLine();
          }
        }
      }
    }


    public static string csv(object o)
    {
      return csv(o, true);
    }

    public static string csv(object o, bool addDelimiter)
    {
      String delimiter = addDelimiter ? "," : "";

      if (o == null)
        return delimiter + "\"\"";
      else
      {
        return delimiter + "\"" + o.ToString().Replace("\"", "\"\"") + "\"";
      }
    }
  }
}
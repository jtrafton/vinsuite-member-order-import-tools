﻿using System;
using System.Linq;
using LinqToExcel;
using System.Collections.Generic;
using System.Configuration;
//using EWSDataTaskSupport.EWSOrder;
using EWSDataTaskSupport.EWSData;
using EWSDataTaskSupport;
using System.Reflection;

namespace EWSMemberImport
{
  class MemberGetter
  {
    //TODO: FIX NAMESPACE JUNK IN 1.13_DATA!!
    private string sheetName;
    Dictionary<String, Member> members;
    
    /**
     * Init.
     */
    public MemberGetter(string sheetName)
    {
      this.sheetName = sheetName;
    }

    /**
     * Get a collection of members.
     */
    public Dictionary<String, Member> getMembers()
    {
      #region init
      var                  spreadSheet  = new ExcelQueryFactory(this.sheetName);
      String               invalMapping = "";
      String               strict       = ConfigurationManager.AppSettings["Strict"].ToString();
      MemberTypeGetter     mtg          = new MemberTypeGetter();
      ClubGetter           cg           = new ClubGetter();
      CreditCardTypeGetter cctg         = new CreditCardTypeGetter();
      Dictionary<String, EWSDataTaskSupport.EWSOrder.WineryShippingOptions> 
        wineryShipOpts = cg.GetWineryShippingTypes();
      
      //MembersXMemberTypes
      Dictionary<String, Dictionary<Guid, MemberType>> memTypes =
        new Dictionary<String, Dictionary<Guid, MemberType>>();
      //ShipMembers
      Dictionary<String, SortedSet<ShippingAddress>> shippingAddresses = 
        new Dictionary<String, SortedSet<ShippingAddress>>();
      //ClubMembers
      Dictionary<String, SortedSet<ClubMembership>> clubMemberships =
        new Dictionary<String, SortedSet<ClubMembership>>();
      //MemberNotes
      Dictionary<String, SortedSet<MemberNote>> memberNotes =
        new Dictionary<String, SortedSet<MemberNote>>();
      //MemberTypes
      Dictionary<String, MemberType> wineryMemTypes = mtg.getMemberTypes();
      //ClubLevels
      Dictionary<String, EWSDataTaskSupport.EWSMember.WineClub> wineClubs      = cg.getClubs();
      //CreditCards
      Dictionary<String, SortedSet<CreditCardRecord>> creditCards =
        new Dictionary<String, SortedSet<CreditCardRecord>>();
      //CreditCardTypes
      Dictionary<String, EWSDataTaskSupport.EWSOrder.CreditCardTypeInfo> cardTypes   = cctg.getCreditCardTypes();
      
      MemberType clubType         = wineryMemTypes["CLUB MEMBERS"];
      MemberType purchaserType    = wineryMemTypes["PURCHASERS"];
      MemberType subscriberType   = wineryMemTypes["SUBSCRIBERS"];
      MemberType unsubscriberType = wineryMemTypes["UNSUBSCRIBER"];

      // Strict mapping is useful to make sure all the columns in the 
      // spreadsheet map to properties in the ImportMember class.
      if (strict == "1")
        spreadSheet.StrictMapping = true;

      // Already populated.
      if (this.members != null)
        return this.members;

      this.members = new Dictionary<String, Member>();
      #endregion

      #region Mappings and transforms
      spreadSheet.AddMapping<ImportMember>(m => m.AltMemberID, "AltMemberID");
      spreadSheet.AddTransformation<ImportMember>(m => m.Password, cellValue => cellValue ?? DataTaskHelper.RandomString(8));
      spreadSheet.AddTransformation<ImportMember>(m => m.CreditCardType,
        cellValue =>
        {
          if (cellValue.ToLower() == "mastercard" || cellValue.ToLower() == "master card" || cellValue.ToLower() == "mc")
            return "MC";
          else if (cellValue.ToLower() == "american express" || cellValue.ToLower() == "amex" || cellValue.ToLower() == "amx")
            return "AmEx";
          else if (cellValue.ToLower() == "visa")
            return "VISA";
          else if (cellValue.ToLower() == "discover card" || cellValue.ToLower() == "discover")
            return "Discover";
          else if (cellValue.Trim() == "")
            return null;
          return cellValue;
        });

      spreadSheet.AddTransformation<ImportMember>(m => m.BillingDateOfBirth,   DataTaskHelper.DefaultDateTransform);
      spreadSheet.AddTransformation<ImportMember>(m => m.CustomerCreationDate, DataTaskHelper.DefaultDateTransform);
      spreadSheet.AddTransformation<ImportMember>(m => m.CustomerLastUpdated,  DataTaskHelper.DefaultDateTransform);
      spreadSheet.AddTransformation<ImportMember>(m => m.ShippingBirthDate,    DataTaskHelper.DefaultDateTransform);
      spreadSheet.AddTransformation<ImportMember>(m => m.ClubBirthDate,        DataTaskHelper.DefaultDateTransform);
      spreadSheet.AddTransformation<ImportMember>(m => m.ClubStartDate,        DataTaskHelper.DefaultDateTransform);
      spreadSheet.AddTransformation<ImportMember>(m => m.ClubEndDate,          DataTaskHelper.DefaultDateTransform);
      spreadSheet.AddTransformation<ImportMember>(m => m.ClubHoldStartDate,    DataTaskHelper.DefaultDateTransform);
      spreadSheet.AddTransformation<ImportMember>(m => m.ClubHoldEndDate,      DataTaskHelper.DefaultDateTransform);
      spreadSheet.AddTransformation<ImportMember>(m => m.ClubCancelDate,       DataTaskHelper.DefaultDateTransform);
      spreadSheet.AddTransformation<ImportMember>(m => m.IsPurchaser,          DataTaskHelper.DefaultBooleanTransform);
      spreadSheet.AddTransformation<ImportMember>(m => m.IsSubscriber,         DataTaskHelper.DefaultBooleanTransform);
      spreadSheet.AddTransformation<ImportMember>(m => m.ClubIsActive,         DataTaskHelper.DefaultBooleanTransform);
      spreadSheet.AddTransformation<ImportMember>(m => m.ClubOnHold,           DataTaskHelper.DefaultBooleanTransform);
      spreadSheet.AddTransformation<ImportMember>(m => m.IsPrimaryShipAddress, DataTaskHelper.DefaultBooleanTransform);
      spreadSheet.AddTransformation<ImportMember>(m => m.IsGiftMembership,     DataTaskHelper.DefaultBooleanTransform);
      spreadSheet.AddTransformation<ImportMember>(m => m.IsUnsubscriber,       DataTaskHelper.DefaultBooleanTransform);

      #endregion

      #region validate mapping
      var notMapped = spreadSheet.GetColumnNames(spreadSheet.GetWorksheetNames().ElementAt(0)).Except(typeof(ImportMember).GetProperties().Select(props => props.Name));
      var notInSpreadsheet = from p in typeof(ImportMember).GetProperties()
                             join s in spreadSheet.GetColumnNames(spreadSheet.GetWorksheetNames().ElementAt(0)) on p.Name equals s into sGroup
                             from missing in sGroup.DefaultIfEmpty()
                             where missing == null
                             select p.Name;
      if (notMapped.Count() != 0)
      {
        invalMapping += "Columns not mapped to the properties: ";
        invalMapping += String.Join(", ", notMapped) + "\n";
      }
      if (notInSpreadsheet.Count() != 0)
      {
        invalMapping += "Properties missing columns: ";
        invalMapping += String.Join(", ", notInSpreadsheet);
      }
      if (!String.IsNullOrEmpty(invalMapping))
        throw new Exception("Spreadsheet mapping errors:\n" + invalMapping);
      #endregion

      Console.WriteLine("Spreadsheet matches template. Populating import objects");

      #region Query the spreadsheet
      var csvMembers = from m in spreadSheet.Worksheet<ImportMember>(0)
                       select m;
      #endregion

      #region De-dupe
      // Create actual Members from the ImportMember.  Members are
      // de-duped by their memberID.
      foreach (ImportMember member in csvMembers)
      {
        #region Create member
        if (!this.members.ContainsKey(member.AltMemberID))
        {
          Member curMem = new Member();
          curMem.Address1                  =  member.Address;
          curMem.Address2                  =  member.Address2;
          curMem.BirthDate                 =  member.BillingDateOfBirth;
          curMem.City                      =  member.City;
          curMem.Company                   =  member.Company;
          curMem.Email                     =  member.Email;
          curMem.FirstName                 =  member.FirstName;
          curMem.LastName                  =  member.LastName;
          curMem.AltMemberID               =  member.AltMemberID;
          curMem.Password                  =  member.Password;
          curMem.Phone1                    =  member.Phone;
          curMem.Phone2                    =  member.Phone2;
          curMem.Salutation                =  member.Salutation;
          curMem.SourceCode                =  member.SourceCode;
          curMem.State                     =  member.State;
          curMem.Province                  =  member.Province;
          curMem.Username                  =  member.UserName;
          curMem.ZipCode                   =  member.ZipCode;
          curMem.DateAdded                 =  member.CustomerCreationDate;
          curMem.DateModified              =  member.CustomerLastUpdated;

          //Console.WriteLine("Number: " + curMem.AltMemberID + " Zip: " + curMem.ZipCode);

          if (member.CreditCardType != null)
          {
            // Ensure that the credit card type is valid.
            if (!cardTypes.ContainsKey(member.CreditCardType.ToLower()))
              throw new Exception("Invalid card type: " + member.CreditCardType);

            curMem.CreditCardExpirationMonth =  member.CreditCardExpMo;
            curMem.CreditCardExpirationYear  =  member.CreditCardExpYr;
            curMem.CreditCardNumber          =  member.CreditCardNumber;
            curMem.NameOnCreditCard          =  member.NameOnCard;
            curMem.CreditCardTypeID          =  cardTypes[member.CreditCardType.ToLower()].CreditCardTypeID;
          }

          this.members.Add(member.AltMemberID, curMem);
        }

        #region Create member notes
        MemberNote curNote = new MemberNote();
        if (!String.IsNullOrEmpty(member.CustomerNotes))
        {
          curNote.Notes = member.CustomerNotes;
          curNote.IsSystemNote = true;
          curNote.DateAdded = DateTime.Now;

          //If this member already exists in nemberNotes dictionary
          if (memberNotes.ContainsKey(member.AltMemberID))
          {
            //add current note
            memberNotes[member.AltMemberID].Add(curNote);
          }
          //else add member to memberNotes dictionary
          else
          {
            SortedSet<MemberNote> notes =
              new SortedSet<MemberNote>(new ReflectiveComparer<MemberNote>());
            //add current note
            notes.Add(curNote);
            memberNotes.Add(member.AltMemberID, notes);
          }
        }
        #endregion

        #region Not in CSV
        //curMem.AltAccountNumber
        //curMem.Country
        //curMem.CountryCode
        //curMem.CreditCardAlias
        //curMem.MemberID
        //curMem.MemberNumber
        //curMem.Province
        //curMem.VirtualMemberID
        //curMem.WineryID
        //curMem.WineryName
        #endregion
        #endregion

        #region Create member types
        MemberType curType = new MemberType();

        // Add MemberType to a member if it was passed in
        if (!String.IsNullOrEmpty(member.MemberType))
        {
          curType.MemberTypeDescription = member.MemberType;
          // Find the MemberTypeID by type description (see MemberTypeGetter.cs).
          if (wineryMemTypes.ContainsKey(member.MemberType.ToUpper().Trim()))
          {
            // Assign MemberTypeID
            curType.MemberTypeID = wineryMemTypes[member.MemberType.ToUpper().Trim()].MemberTypeID;
          }
          else
          {
            // The member type does not exist - add it to the db.
            curType.MemberTypeID = mtg.addToMemberTypes(member.MemberType);
            wineryMemTypes.Add(member.MemberType.ToUpper().Trim(), curType);
          }
        }

        // If this member already exists in the member types dictionary.
        if (memTypes.ContainsKey(member.AltMemberID))
        {
          // A member type may not have been provided in the CSV.
          if (curType.MemberTypeID.HasValue)
          {
            // If the current member type is not already associated with the member.
            if (!memTypes[member.AltMemberID].ContainsKey(curType.MemberTypeID.Value))
              memTypes[member.AltMemberID].Add(curType.MemberTypeID.Value, curType);
          }
        }
        // Else this member doesn't exist in the dictionary yet.
        else
        {
          Dictionary<Guid, MemberType> types = new Dictionary<Guid, MemberType>();
          memTypes.Add(member.AltMemberID, types);

          // A member type was not provided in the CSV.
          if (curType.MemberTypeID.HasValue)
            types.Add(curType.MemberTypeID.Value, curType);
        }
        

        // If this member is a purchaser they should be assigned to the 
        // Purchasers member type if they are not already.
        if (member.IsPurchaser)
        {
          if (!memTypes[member.AltMemberID].ContainsKey(purchaserType.MemberTypeID.Value))
          {
            memTypes[member.AltMemberID].Add(purchaserType.MemberTypeID.Value, purchaserType);
            //Console.WriteLine("Member " + member.AltMemberID + " added to Purchasers.");
          }
        }

        // If this member is a purchaser they should be assigned to the
        // Subscriber member type if they are not already
        if (member.IsSubscriber)
        {
          if (!memTypes[member.AltMemberID].ContainsKey(subscriberType.MemberTypeID.Value))
          {
            memTypes[member.AltMemberID].Add(subscriberType.MemberTypeID.Value, subscriberType);
            //Console.WriteLine("Member " + member.AltMemberID + " added to Subscribers.");
          }
        }

        // If this member is an opt out add them to the Unsubscriber member type.
        if (member.IsUnsubscriber)
        {
          if (!memTypes[member.AltMemberID].ContainsKey(unsubscriberType.MemberTypeID.Value))
          {
            memTypes[member.AltMemberID].Add(unsubscriberType.MemberTypeID.Value, unsubscriberType);
            //Console.WriteLine("Member " + member.AltMemberID + " added to Unsubscribers.");
          }
        }
        #endregion

        #region Create shipping address
        ShippingAddress curSAddr = new ShippingAddress();

        curSAddr.Address1    = member.ShipAddress;
        curSAddr.Address2    = member.ShipAddress2;
        curSAddr.BirthDate   = member.ShippingBirthDate;
        curSAddr.City        = member.ShipCity;
        curSAddr.Company     = member.ShipCompany;
        curSAddr.Email       = member.ShipEmailAddress;
        curSAddr.FirstName   = member.ShipFirstName;
        curSAddr.IsPrimary   = member.IsPrimaryShipAddress;
        curSAddr.LastName    = member.ShipLastName;
        curSAddr.Phone1      = member.ShipPhone;
        curSAddr.Phone2      = member.ShipPhone2;
        curSAddr.Salutation  = member.ShipSalutation;
        curSAddr.State       = member.ShipState;
        curSAddr.Province    = member.ShipProvince;
        curSAddr.ZipCode     = member.ShipZipCode;
        curSAddr.AddressName = member.ShipAddress;

        #region Not in csv
        //curSAddr.AltShippingAddressID
        //curSAddr.Country 
        //curSAddr.CountryCode
        //curSAddr.DateAdded
        //curSAddr.DateModified
        //curSAddr.MemberID
        //curSAddr.Province
        //curSAddr.ShippingAddressID
        //curSAddr.WineryID
        #endregion

        // If this member already exists in the ShippingAddress dictionary.
        if (shippingAddresses.ContainsKey(member.AltMemberID))
        {
          // If the current shipping address is not already associated with the member.
          if (!shippingAddresses[member.AltMemberID].Contains(curSAddr))
            shippingAddresses[member.AltMemberID].Add(curSAddr);
        }
        // Else this member doesn't exist in the dictionary yet.
        else
        {
          SortedSet<ShippingAddress> addresses =
            new SortedSet<ShippingAddress>(new ReflectiveComparer<ShippingAddress>());
          addresses.Add(curSAddr);
          shippingAddresses.Add(member.AltMemberID, addresses);
        }
        #endregion

        #region Create club memberships
        if (member.ClubLevel != null)
        {
          ClubMembership curCMem = new ClubMembership();

          // Make sure that the club level is valid.
          if (!wineClubs.ContainsKey(member.ClubLevel.Trim().ToUpper()))
          {
            //todo: add club???
            throw new Exception("Invalid club level: " + member.ClubLevel + " for AltMemID: " + member.AltMemberID);
          }

          curCMem.CancelDate               = member.ClubEndDate;
          curCMem.ClubLevelID              = wineClubs[member.ClubLevel.Trim().ToUpper()].ClubLevelID;
          curCMem.ClubLevel                = member.ClubLevel;
          curCMem.CurrentNumberOfShipments = 0; // member.CurrentNoShipments; // hardcoded per Jon 3/29/13
          curCMem.DateAdded                = member.ClubStartDate;
          //curCMem.EndDate                  = member.ClubEndDate;
          curCMem.GiftMessage              = member.GiftMessage;
          curCMem.HoldStartDate            = member.ClubHoldStartDate;
          curCMem.IsActive                 = member.ClubIsActive;
          curCMem.IsOnHold                 = member.ClubOnHold;
          curCMem.OnHoldDate               = member.ClubHoldEndDate;
          curCMem.ShippingAddress          = member.ClubAddress      ?? member.Address ?? "INVALID ADDRESS";
          curCMem.ShippingAddress2         = member.ClubAddress2;
          curCMem.ShippingBirthDate        = member.ClubBirthDate;
          curCMem.ShippingCity             = member.ClubCity         ?? member.City    ?? "INVALID CITY";
          curCMem.ShippingCompany          = member.ClubCompany;
          curCMem.ShippingEmail            = member.ClubEmailAddress ?? member.Email   ?? member.AltMemberID + "@noemail.com";
          curCMem.ShippingFirstName        = member.ClubFirstName;
          curCMem.ShippingLastName         = member.ClubLastName;
          curCMem.ShippingBirthDate        = member.ShippingBirthDate ?? new DateTime(1920, 01, 01);
          curCMem.ShippingPhone1           = member.ClubPhone;
          curCMem.ShippingPhone2           = member.ClubPhone2;
          curCMem.ShippingSalutation       = member.ClubSalutation;
          curCMem.ShippingState            = member.ClubState         ?? member.State   ?? "INVALID STATE";
          curCMem.ShippingProvince         = member.ClubProvince;
          curCMem.WineShippingType         = member.ClubShippingPreference;
          curCMem.WineShippingTypeID       = curCMem.WineShippingType == null ? null : wineryShipOpts[curCMem.WineShippingType.ToUpper().Trim()].ShippingTypeID;
          curCMem.ShippingZipCode          = member.ClubZipCode       ?? member.ZipCode ?? "INVAL";
          //curCMem.StartDate                = member.ClubStartDate;  // stop updating this per Jon 3/5/13.
          curCMem.TotalNumberOfShipments   = 999; // member.TotalNoShipments; // hardcoded per Jon 3/29/13

          // Start date must be before the end date.
          if (curCMem.StartDate.HasValue && curCMem.EndDate.HasValue)
          {
            if (curCMem.StartDate.Value > curCMem.EndDate.Value)
            {
              Console.WriteLine("MemberNumber: " + member.AltMemberID + 
                " has a club start date after a club end date.  Correcting.");

              curCMem.EndDate = curCMem.StartDate.Value.AddSeconds(1);
            }
          }

          #region Not in ClubMembership
          //member.IsGiftMembership
          #endregion

          #region Not in csv
          //curCMem.ActiveMembershipOn
          //curCMem.Affiliate
          //curCMem.AffiliateCommission
          //curCMem.AltClubMembershipID
          //curCMem.AltShippingAddressID
          //curCMem.ClubFrequency
          //curCMem.ClubLevelID
          //curCMem.ClubMembershipID
          //curCMem.DateModified
          //curCMem.IsGiftMessage
          //curCMem.LastBatchNumber
          //curCMem.LastBillDate
          //curCMem.LastPaymentStatus
          //curCMem.MemberID
          //curCMem.NextBillDate
          //curCMem.NumberOfBottles
          //curCMem.OnHoldDate
          //curCMem.Rate
          //curCMem.ShippingAddressID
          //curCMem.ShippingCode
          //curCMem.ShippingCountry
          //curCMem.ShippingCountryCode
          //curCMem.ShippingProvince
          //curCMem.ShippingType
          //curCMem.SourceCode
          //curCMem.WineryID
          #endregion

          #region Not in ClubMembership
          #endregion

          // If this member already exists in the ClubMemberships dictionary.
          if (clubMemberships.ContainsKey(member.AltMemberID))
          {
            // If the current club membership is not already associated with the member.
            if (!clubMemberships[member.AltMemberID].Contains(curCMem))
              clubMemberships[member.AltMemberID].Add(curCMem);
          }
          // Else this member doesn't exist in the dictionary yet.
          else
          {
            SortedSet<ClubMembership> memberships =
              new SortedSet<ClubMembership>(new ReflectiveComparer<ClubMembership>());
            memberships.Add(curCMem);
            clubMemberships.Add(member.AltMemberID, memberships);
          }

          // This member is a club, so the member should be assigned to the 
          // Club Members member type if they are not already.
          if (!memTypes[member.AltMemberID].ContainsKey(clubType.MemberTypeID.Value))
          {
            memTypes[member.AltMemberID].Add(clubType.MemberTypeID.Value, clubType);
            //Console.WriteLine("Member " + member.AltMemberID + " added to Club Memberships.");
          }
        }
        #endregion

        #region Create credit card
        // TODO: Currently only one credit card is supported, and it is
        // added directly to the member.  This may need to be completed in the
        // future.
        /*
        if (member.CreditCardType != null)
        {
          if (!cardTypes.ContainsKey(member.CreditCardType.ToLower()))
            throw new Exception("Invalid card type: " + member.CreditCardType);

          CreditCardRecord curCard          = new CreditCardRecord();
          curCard.CreditCardExpirationMonth = member.CreditCardExpMo;
          curCard.CreditCardExpirationYear  = member.CreditCardExpYr;
          curCard.CreditCardNumber          = member.CreditCardNumber;
          curCard.NameOnCreditCard          = member.NameOnCard;
          curCard.CreditCardTypeID          = cardTypes[member.CreditCardType.ToLower()].CreditCardTypeID;
          
          // if current member exists in creditcards dictionary
          if (creditCards.ContainsKey(member.AltMemberID))
          {
            // If the current credictcard is not already associated with the member.
            if (!creditCards[member.AltMemberID].Contains(curCard))
              creditCards[member.AltMemberID].Add(curCard);
          }
          // add new member to creditcard dictionary
          else
          {
            SortedSet<CreditCardRecord> memberCCs =
              new SortedSet<CreditCardRecord>(new ReflectiveComparer<CreditCardRecord>());
            memberCCs.Add(curCard);
            creditCards.Add(member.AltMemberID, memberCCs);
          }
        }
        */
        #endregion

        #region Create member notes
        if (!String.IsNullOrEmpty(member.CustomerNotes))
        {
          MemberNote memberNote = new MemberNote();
          memberNote.IsSystemNote = true;
          memberNote.DateAdded = DateTime.Now;
          memberNote.Notes = member.CustomerNotes;

          #region Not in csv
          //memberNote.ModifiedBy = member.ModifiedBy;
          //memberNote.DateAdded  = member.DateAdded;
          #endregion

          // If this member already exists in the MemberNotes dictionary
          if (memberNotes.ContainsKey(member.AltMemberID))
          {
            // Add member note to a member if note does not exist
            if (!memberNotes[member.AltMemberID].Contains(memberNote))
              memberNotes[member.AltMemberID].Add(memberNote);
          }
          // Else this member doesn't exist in the dictionary yet
          else
          {
            SortedSet<MemberNote> notes =
              new SortedSet<MemberNote>(new ReflectiveComparer<MemberNote>());
            notes.Add(memberNote);
            memberNotes.Add(member.AltMemberID, notes);
          }
        }
        #endregion
      }
      #endregion

      #region Make them "know" each other
      foreach (KeyValuePair<String, Member> member in this.members)
      {
        #region Add types to member
        if (memTypes.ContainsKey(member.Key))
        {
          int numTypes = memTypes[member.Key].Count();
          int i        = 0;

          member.Value.MemberTypes = new MemberType[numTypes];

          foreach (KeyValuePair<Guid, MemberType> memType in memTypes[member.Key])
            member.Value.MemberTypes[i++] = memType.Value;
        }
        #endregion

        #region Add shipping addresses to member
        if (shippingAddresses.ContainsKey(member.Key))
        {
          int numAddr = shippingAddresses[member.Key].Count();
          int i       = 0;

          member.Value.ShippingAddresses = new ShippingAddress[numAddr];

          foreach (ShippingAddress addr in shippingAddresses[member.Key])
          {
            addr.AddressName = "Address" + i;
            member.Value.ShippingAddresses[i++] = addr;
          }
        }
        #endregion

        #region Add club memberships to member
        if (clubMemberships.ContainsKey(member.Key))
        {
          int numClub = clubMemberships[member.Key].Count();
          int i       = 0;

          member.Value.ClubMemberships = new ClubMembership[numClub];

          foreach (ClubMembership clubMem in clubMemberships[member.Key])
            member.Value.ClubMemberships[i++] = clubMem;
        }
        #endregion

        #region Add credit cards to member (not currently supported)
        /*if (creditCards.ContainsKey(member.Key))
        {
          int numCards = clubMemberships[member.Key].Count();
          int i       = 0;

          member.Value.ClubMemberships = new ClubMembership[numClub];

          foreach (ClubMembership clubMem in clubMemberships[member.Key])
            member.Value.ClubMemberships[i++] = clubMem;
        }*/
        #endregion

        #region Add member notes
        if (memberNotes.ContainsKey(member.Key))
        {
          int numNotes = memberNotes[member.Key].Count();
          int i = 0;

          member.Value.MemberNotes = new MemberNote[numNotes];

          foreach (MemberNote note in memberNotes[member.Key])
          {
            member.Value.MemberNotes[i++] = note;
          }
        }
        #endregion
      }
      #endregion

      return this.members;
    }

    /**
     * Dump a member using reflection.
     */
    public void pukeMember(Object member)
    {
      foreach (var prop in member.GetType().GetProperties())
      {
        var val = prop.GetValue(member, null);

        /*if (val is IEnumerable<>)
        {
          pukeMember(val);
        }
        else*/
        {
          Console.WriteLine(prop.Name + " => " + prop.GetValue(member, null));
        }
      }
    }
  }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EWSDataTaskSupport.EWSMember;
using EWSDataTaskSupport.EWSOrder;
using EWSDataTaskSupport;

namespace EWSMemberImport
{
  class ClubGetter
  {
    private ConnectionManager conMan;
    private Dictionary<String, WineClub> clubs;
    

    /**
     * Pull a list of clubs and add them to a dictionary.
     */
    public ClubGetter()
    {
      MemberService   serv = new MemberService();
      GetClubRequest  req  = new GetClubRequest();
      GetClubResponse resp = new GetClubResponse();

      // Set up the credentials.
      this.conMan  = EWSWebsiteService.getConnectionManager();
      req.Username = conMan.username;
      req.Password = conMan.password;

      // Get all clubs, not just active ones.
      req.IncludeInactiveClubs = true;
      serv.Url = serv.Url.Replace("http:", "https:");
      // Get the clubs.
      resp = serv.GetClub(req);
      this.clubs = new Dictionary<String, WineClub>();

      foreach (WineClub club in resp.WineClubs)
      {
        // added for use in testDB
        if (!this.clubs.ContainsKey(club.Title.Trim().ToUpper()))
          this.clubs.Add(club.Title.Trim().ToUpper(), club);
        else 
          Console.WriteLine("Duplicate club level: {0}", club.Title);
      }
    }

    /**
     * Get the list of clubs.
     */
    public Dictionary<String, WineClub> getClubs()
    {
      return this.clubs;
    }

    /**
     * TODO: move to ShippingTypeGetter.
     */
    public Dictionary<String, WineryShippingOptions> GetWineryShippingTypes()
    {
      Dictionary<String, WineryShippingOptions> shipOptions = new Dictionary<String, WineryShippingOptions>();
      GetShippingOptionsRequest gsReq;
      using (OrderService os = new OrderService())
      {
        //os.Url = os.Url.Replace("http:", "https:");
        gsReq = new GetShippingOptionsRequest();
        gsReq.Username = EWSWebsiteService.getConnectionManager().username;
        gsReq.Password = EWSWebsiteService.getConnectionManager().password;
        gsReq.WineryID = EWSWebsiteService.GetConnectionInfo().PrimaryAuthInfo.WineryID;

        GetShippingOptionsResponse gsResp = os.GetShippingOptions(gsReq);
        if (gsResp.IsSuccessful)
        {
          foreach (WineryShippingOptions wso in gsResp.WineriesShippingOptions)
          {
            if (wso.ShippingOptionType == "WINE")
              shipOptions.Add(wso.ShippingType.ToUpper().Trim(), wso);
          }
        }
        else
          throw new Exception("Error getting winery shipping options: " + gsResp.ErrorMessage);
      }

      return shipOptions;
    }
  }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EWSMemberImport
{
  class ImportResponseLog
  {
    public string Key        { get; set; }
    public Guid? MemberID    { get; set; }
    public string Error      { get; set; }
    public string FirstName  { get; set; }
    public string LastName   { get; set; }
    public string Email      { get; set; }
    public Guid? ObjectID    { get; set; }
  }
}

﻿using System;
using System.Collections.Generic;
using System.Configuration;
//using EWSDataTaskSupport.EWSOrder;
using EWSDataTaskSupport.EWSData;
using System.Linq;
using System.IO;
using EWSDataTaskSupport;

namespace EWSMemberImport
{
  class Program
  { 
    /// <summary>
    /// what does this do?
    /// </summary>
    /// <param name="args"></param>
    static void Main(string[] args)
    {
      // Allow self-signed certs.
      System.Net.ServicePointManager.ServerCertificateValidationCallback += (se, cert, chain, sslerror) =>
        {
            return true;
        };

      try
      {
        String debug                              = ConfigurationManager.AppSettings["Debug"].ToString();  
        String importFile                         = ConfigurationManager.AppSettings["ImportFile"].ToString();
        String importDirectory                    = Path.GetDirectoryName(importFile);
        String importTimestamp                    = DateTime.Now.ToString().Replace(@"/", "").Replace(":","");
        MemberGetter mg                           = new MemberGetter(importFile);
        Dictionary<String, Member> members        = mg.getMembers();            
        ClubGetter cg                             = new ClubGetter();
        DataService ds                            = new DataService();
        ImportMemberRequest importMemberRequest   = new ImportMemberRequest();
        List<Member> importMembers                = new List<Member>();
        List<ImportResponseLog> memResponses;
        List<ImportResponseLog> ccResponses;
        List<ImportResponseLog> clubResponses;
        List<ImportResponseLog> shipResponses;
        List<ImportResponseLog> noteResponses;
        CSVHelper csv                             = new EWSDataTaskSupport.CSVHelper();
        bool isMembersInQueue                     = true;
        bool isSuccessfulRound                    = true;
        int membersPerRound                       = int.Parse(ConfigurationManager.AppSettings["MemPerRound"]);        
        int membersToImport                       = members.Count;
        int memberIndex                           = 0;
        int lastMemberIndex                       = 0; 
        int responseIndex;
        bool isFullRound;
        ImportMemberResponse importMemberResponse;

        Console.WriteLine("Importing {0} members and {1} club memberships into {2}",
          membersToImport,
          members.Sum(m => m.Value.ClubMemberships == null ? 0 : m.Value.ClubMemberships.Count()),
          EWSWebsiteService.GetConnectionInfo().PrimaryAuthInfo.WineryID);

        if (debug == "1")
        {
          Console.WriteLine("Not sending because Debug mode is enabled.");
          Console.ReadLine();
          return;
        }

        #region Setup requests
        importMemberRequest.Username = ConfigurationManager.AppSettings["WSUsername"].ToString();
        importMemberRequest.Password = ConfigurationManager.AppSettings["WSPassword"].ToString();
        importMemberRequest.WineryID = EWSWebsiteService.GetConnectionInfo().PrimaryAuthInfo.WineryID;
        importMemberRequest.EmailTo = " ";
        ds.Timeout = int.MaxValue;
        ds.Url = ds.Url.Replace("http:", "https:");
        #endregion

        while (isMembersInQueue && membersToImport > 0)
        {
          //add members for each round of importing
          if (isSuccessfulRound)
          {
            importMembers = new List<Member>();
            lastMemberIndex = memberIndex;
            isFullRound = false;
            while (!isFullRound)
            {
              if (memberIndex < membersToImport)
              {
                memberIndex++;
                importMembers.Add(members.ElementAt(memberIndex - 1).Value);
                isFullRound = memberIndex % membersPerRound == 0;
              }
              else
              {
                isMembersInQueue = false;
                break;
              }
            }
            importMemberRequest.Members = importMembers.ToArray();
          }
          else
            isSuccessfulRound = true;

          Console.WriteLine("Importing members {0} through {1}", lastMemberIndex + 1, memberIndex);

          //import members
          try
          {
            importMemberResponse = ds.ImportMembers(importMemberRequest);
            if (importMemberResponse.IsSuccessful)
            {
              //write responses to log
              responseIndex = 0;
              memResponses  = new List<ImportResponseLog>();
              ccResponses   = new List<ImportResponseLog>();
              clubResponses = new List<ImportResponseLog>();
              shipResponses = new List<ImportResponseLog>();
              noteResponses = new List<ImportResponseLog>();
              foreach (ImportMemberResponses responses in importMemberResponse.Responses)
              {
                ImportResponseLog memResponse = new ImportResponseLog
                {
                  Key          = importMembers[responseIndex].AltMemberID,
                  MemberID     = responses.memberResponse.MemberID,
                  LastName     = importMembers[responseIndex].LastName,
                  FirstName    = importMembers[responseIndex].FirstName,
                  Email        = importMembers[responseIndex].Email,
                  Error        = responses.memberResponse.ErrorMessage
                };
                memResponses.Add(memResponse);

                var ccResponse = from r in responses.ccResponses
                                  select new ImportResponseLog
                                  {
                                    Key          = memResponse.Key,
                                    MemberID     = memResponse.MemberID,
                                    LastName     = importMembers[responseIndex].LastName,
                                    FirstName    = importMembers[responseIndex].FirstName,
                                    Email        = importMembers[responseIndex].Email,
                                    ObjectID     = r.CreditCardID,
                                    Error        = r.ErrorMessage
                                  };
                ccResponses.AddRange(ccResponse);
                  
                var clubResponse = from r in responses.clubResponses
                                    select new ImportResponseLog
                                    {
                                      Key          = memResponse.Key,
                                      MemberID     = memResponse.MemberID,
                                      LastName     = importMembers[responseIndex].LastName,
                                      FirstName    = importMembers[responseIndex].FirstName,
                                      Email        = importMembers[responseIndex].Email,
                                      ObjectID     = r.ClubMemberID,
                                      Error        = r.ErrorMessage
                                    };
                clubResponses.AddRange(clubResponse);
                  
                var shipResponse = from r in responses.shipResponses
                                    select new ImportResponseLog
                                    {
                                      Key               = memResponse.Key,
                                      MemberID          = memResponse.MemberID,
                                      LastName          = importMembers[responseIndex].LastName,
                                      FirstName         = importMembers[responseIndex].FirstName,
                                      Email             = importMembers[responseIndex].Email,
                                      ObjectID          = r.ShippingAddressID,
                                      Error             = r.ErrorMessage
                                    };
                shipResponses.AddRange(shipResponse);
                  
                var noteResponse = from r in responses.notesResponses
                                    select new ImportResponseLog
                                    {
                                      Key          = memResponse.Key,
                                      MemberID     = memResponse.MemberID,
                                      LastName     = importMembers[responseIndex].LastName,
                                      FirstName    = importMembers[responseIndex].FirstName,
                                      Email        = importMembers[responseIndex].Email,
                                      ObjectID     = r.MemberNoteID,
                                      Error        = r.ErrorMessage
                                    };
                noteResponses.AddRange(noteResponse);

                ++responseIndex;
              }
              csv.WriteCSVFile(importDirectory + @"\memResponse " + importTimestamp + ".csv", memResponses, true);
              csv.WriteCSVFile(importDirectory + @"\shipResponse " + importTimestamp + ".csv", shipResponses, true);
              csv.WriteCSVFile(importDirectory + @"\clubResponse " + importTimestamp + ".csv", clubResponses, true);
              csv.WriteCSVFile(importDirectory + @"\ccResponse " + importTimestamp + ".csv", ccResponses, true);
              csv.WriteCSVFile(importDirectory + @"\noteResponse " + importTimestamp + ".csv", noteResponses, true);
            }
            else
            {
              throw new Exception("Error importing members: " + importMemberResponse.ErrorMessage);
            }            
          }
          catch (Exception ex)
          {
            Console.WriteLine("Exception: " + ex.Message);
            String exStr = "Members " + (lastMemberIndex + 1) + " - " + memberIndex + " Exception: " + ex.Message;
            using (System.IO.StreamWriter file = new System.IO.StreamWriter(importDirectory + @"\Exceptions.txt", true))
            {
              file.WriteLine(exStr);
            }
            //sleep for 5 sec, fail current round, force to re-import the same data
            isSuccessfulRound = false;
            System.Threading.Thread.Sleep(5000);
          }
        }
      }
      catch (Exception ex)
      {
        Console.WriteLine("Exception: " + ex.Message);
        Console.WriteLine(ex.StackTrace);
        Console.ReadLine();
      }
      Console.WriteLine("Done");
      Console.ReadLine();
    }
  }
}

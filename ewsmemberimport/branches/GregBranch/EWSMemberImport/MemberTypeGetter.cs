﻿using System;
using System.Collections.Generic;
using EWSDataTaskSupport.EWSWebsite;
using System.Configuration;
using EWSDataTaskSupport.EWSMember;
using EWSDataTaskSupport;

namespace EWSMemberImport
{
  class MemberTypeGetter
  {
    private ConnectionManager conMan;
    private Dictionary<String, EWSDataTaskSupport.EWSData.MemberType> memberTypes;
    private EWSDataTaskSupport.EWSData.MemberType memberType;
    private TestConnectionResponse connectionInfo;
    private String debug;

    /**
     * Pull all the member types for the winery and add them to a
     * sorted collection.
     */
    public MemberTypeGetter()
    {
      GetWineryMemberTypeRequest req = new GetWineryMemberTypeRequest();
      GetWineryMemberTypeResponse resp;

      this.conMan = new ConnectionManager();
      // Set up connection info.
      connectionInfo = EWSWebsiteService.GetConnectionInfo();

      if (!connectionInfo.IsSuccessful)
        throw new Exception("Oops! Failed retrieving wineryID: " + connectionInfo.ErrorMessage);

      using (MemberService serv = new MemberService())
      {
        serv.Url = serv.Url.Replace("http:", "https:");
        // Set up the credentials.
        req.Username = conMan.username;
        req.Password = conMan.password;

        // Get the member types.
        resp = serv.GetWineryMemberType(req);
        this.memberTypes = new Dictionary<String, EWSDataTaskSupport.EWSData.MemberType>();

        if (resp.IsSuccessful)
        {
          foreach (MemberType mt in resp.MemberTypes)
          {
            memberType = new EWSDataTaskSupport.EWSData.MemberType();
            memberType.MemberTypeDescription = mt.MemberTypeDescription;
            memberType.MemberTypeID = mt.MemberTypeID;
            memberType.WineryID = mt.WineryID;
            if (!this.memberTypes.ContainsKey(mt.MemberTypeDescription.ToUpper().Trim()))
              this.memberTypes.Add(mt.MemberTypeDescription.ToUpper().Trim(), memberType);
            else
              Console.WriteLine("Duplicate member type: {0}", mt.MemberTypeDescription);
          }
        }
        else
          throw new Exception("GetWineryMemberType error: " + resp.ErrorMessage);
      }
      debug = ConfigurationManager.AppSettings["Debug"].ToString();
    }

    /**
     * Get the collection of member types.
     */
    public Dictionary<String, EWSDataTaskSupport.EWSData.MemberType> getMemberTypes()
    {
      return this.memberTypes;
    }

    /**
     * Add member type to the winery member types, update list, return MemberTypeID
     */
    public Guid? addToMemberTypes(string memberType)
    {
      Guid? memberTypeID = Guid.NewGuid();

      if (debug == "0")
      {
        AddMemberTypeRequest amtRequest = new AddMemberTypeRequest();
        AddMemberTypeResponse amtResponse;
        amtRequest.Username = this.conMan.username;
        amtRequest.Password = this.conMan.password;
        amtRequest.MemberType = new EWSDataTaskSupport.EWSMember.MemberType { MemberTypeDescription = memberType};
        using (MemberService serv = new MemberService())
        {
          serv.Url = serv.Url.Replace("http:", "https:");
          amtResponse = serv.AddMemberType(amtRequest);
          if (amtResponse.IsSuccessful)
            memberTypeID = amtResponse.MemberTypeID;
          else
            throw new Exception("Error adding member type: " + amtResponse.ErrorMessage);
        }
      }
      return memberTypeID;
    }
  }
}

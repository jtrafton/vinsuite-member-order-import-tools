﻿using System;
using System.Collections.Generic;
using EWSDataTaskSupport.EWSOrder;
using System.Collections;

namespace EWSDataTaskSupport
{
  public class CreditCardTypeGetter
  {
    private ConnectionManager conMan;
    private Dictionary<String, CreditCardTypeInfo> cardTypes;

    /**
     * Currently hard-coded because there is no web service for retreiving
     * card types.
     */
    public CreditCardTypeGetter()
    {
      this.cardTypes = new Dictionary<String, CreditCardTypeInfo>();

      OrderService               serv = new OrderService();
      GetCreditCardTypeRequest   req  = new GetCreditCardTypeRequest();
      GetCreditCardTypeResponse resp;

      // Set up the credentials.
      this.conMan  = new ConnectionManager();
      req.Username = conMan.username;
      req.Password = conMan.password;

      // Get the member types.
      resp = serv.GetCreditCardType(req);
      this.cardTypes = new Dictionary<String, CreditCardTypeInfo>();

      foreach (CreditCardTypeInfo ct in resp.CreditCardTypes)
        this.cardTypes.Add(ct.CreditCardTypeShort.ToLower(), ct);
    }

    /**
     * Get the card types dic.
     */
    public Dictionary<String, CreditCardTypeInfo> getCreditCardTypes()
    {
      return this.cardTypes;
    }
  }
}

﻿using System;
using System.Configuration;
using EWSDataTaskSupport.Properties;

namespace EWSDataTaskSupport
{
  /// <summary>
  /// Other projects can use this class to access connection strings
  /// and web service urls as needed
  /// </summary>
  public class AppConfigProxy
  {
    #region Database Connection Strings
    public static String MainConnectionString
    {
      get { return Settings.Default.ewinerysolutionswineriesConnectionString; }
    }

    public static String DiageoConnectionString
    {
      get { return Settings.Default.ewinerysolutionswineriesConnectionStringDiageo; }
    }

    public static String KJConnectionString
    {
      get { return Settings.Default.ewinerysolutionswineriesConnectionStringKJ; }
    }

    public static String TreasuryConnectionString
    {
      get { return Settings.Default.ewinerysolutionswineriesConnectionStringTreasury; }
    }

    public static String FostersAUConnectionString
    {
      get { return Settings.Default.ewinerysolutionswineriesConnectionStringFostersAU; }
    }

    //public static String UATDBConnectionString
    //{
    //  get { return Settings.Default.ewinerysolutionswineriesConnectionStringUAT; }
    //}

    //public static String TestDBConnectionString
    //{
    //  get { return Settings.Default.ewinerysolutionswineriesConnectionStringTest; }
    //}
    #endregion

    #region Reporting Database Connection Strings
    public static String MainConnectionString_Reporting { get { return Settings.Default.ewinerysolutionswineriesConnectionString_rpt; } }
    public static String DiageoConnectionString_Reporting { get { return Settings.Default.ewinerysolutionswineriesConnectionStringDiageo_rpt; } }
    public static String KJConnectionString_Reporting { get { return Settings.Default.ewinerysolutionswineriesConnectionStringKJ_rpt; } }
    public static String TreasuryConnectionString_Reporting { get { return Settings.Default.ewinerysolutionswineriesConnectionStringTreasury_rpt; } }
    public static String FostersAUConnectionString_Reporting { get { return Settings.Default.ewinerysolutionswineriesConnectionStringFostersAU_rpt; } }
    #endregion

    #region WS URLs
    /*
     * These aren't being used since all web references
     * currently point to ews.benningfieldgroup.com
     * 
    public static String ProductionWebServiceURL
    {
      get { return Settings.Default.WS_ProductionURL; }
    }

    public static String UATWebServiceURL
    {
      get { return Settings.Default.WS_UATURL; }
    }

    public static String TestWebServiceURL
    {
      get { return Settings.Default.WS_TestURL; }
    }
     * */
    #endregion
  }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EWSDataTaskSupport
{
  public class DataTaskHelper
  {
    private static Random random = new Random();

    #region Common helpful transforms
    private static Func<String, Object> defaultBool = null;
    private static Func<String, Object> defaultDate = null;
    #endregion

    /// <summary>
    /// Generate a string of random characters of a specified length.
    /// Useful for generating passwords or password salt.
    /// </summary>
    /// <param name="length"></param>
    /// <returns></returns>
    public static String RandomString(int length)
    {
      length = Math.Abs(length);
      StringBuilder sb = new StringBuilder(length, length);

      for (int i = 0; i < length; ++i)
        sb.Append((char)random.Next(33, 127));

      return sb.ToString();
    }


    /// <summary>
    /// Transforms 1/0, yes/no, true/false to a boolean
    /// </summary>
    public static Func<String, Object> DefaultBooleanTransform
    {
      get 
      {
        if (defaultBool == null)
        {
          defaultBool = (cellValue) =>
          {
            if (cellValue == null || cellValue == "")
              return false;

            if (cellValue == "1" ||
              String.Equals("true", cellValue, StringComparison.OrdinalIgnoreCase) ||
              String.Equals("yes", cellValue, StringComparison.OrdinalIgnoreCase))
            {
              return true;
            }

            if (cellValue == "0" ||
              String.Equals("false", cellValue, StringComparison.OrdinalIgnoreCase) ||
              String.Equals("no", cellValue, StringComparison.OrdinalIgnoreCase))
            {
              return false;
            }

            if (string.IsNullOrEmpty(cellValue))
              return false;

            throw new Exception("DataTaskHelper: Invalid boolean representation: \"" + cellValue + "\"");          };
        }

        return defaultBool; 
      }
    }


    /// <summary>
    /// Transforms a date string into a date or null if it doesn't parse
    /// </summary>
    public static Func<String, Object> DefaultDateTransform
    {
      get
      {
        if (defaultDate == null)
        {
          defaultDate = (cellValue) =>
          {
            DateTime t;

            if (DateTime.TryParse(cellValue, out t))
              return t;
            else
              return null;
          };
        }

        return defaultDate;
      }
    }
  }
}

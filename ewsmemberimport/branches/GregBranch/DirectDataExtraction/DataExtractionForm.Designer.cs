﻿namespace DirectDataExtraction
{
  partial class DataExtractionForm
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.label1 = new System.Windows.Forms.Label();
      this.cmbEnvironment = new System.Windows.Forms.ComboBox();
      this.cmbWinery = new System.Windows.Forms.ComboBox();
      this.label2 = new System.Windows.Forms.Label();
      this.cbWineryActive = new System.Windows.Forms.CheckBox();
      this.grpExportPreferences = new System.Windows.Forms.GroupBox();
      this.btnCheckNone = new System.Windows.Forms.Button();
      this.btnCheckAll = new System.Windows.Forms.Button();
      this.cbMemberNotes = new System.Windows.Forms.CheckBox();
      this.cbMemberTypes = new System.Windows.Forms.CheckBox();
      this.cbCreditCards = new System.Windows.Forms.CheckBox();
      this.cbClubMembers = new System.Windows.Forms.CheckBox();
      this.cbShipMembers = new System.Windows.Forms.CheckBox();
      this.cbFPProducts = new System.Windows.Forms.CheckBox();
      this.cbFPWines = new System.Windows.Forms.CheckBox();
      this.cbTPWines = new System.Windows.Forms.CheckBox();
      this.cbTPProducts = new System.Windows.Forms.CheckBox();
      this.cbOrderNotes = new System.Windows.Forms.CheckBox();
      this.cbCartItems = new System.Windows.Forms.CheckBox();
      this.cbCarts = new System.Windows.Forms.CheckBox();
      this.cbMembers = new System.Windows.Forms.CheckBox();
      this.btnRunExport = new System.Windows.Forms.Button();
      this.label3 = new System.Windows.Forms.Label();
      this.tbWSUsername = new System.Windows.Forms.TextBox();
      this.label4 = new System.Windows.Forms.Label();
      this.tbWSPassword = new System.Windows.Forms.TextBox();
      this.cbVirtualMembers = new System.Windows.Forms.CheckBox();
      this.grpExportPreferences.SuspendLayout();
      this.SuspendLayout();
      // 
      // label1
      // 
      this.label1.AutoSize = true;
      this.label1.Location = new System.Drawing.Point(12, 33);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(66, 13);
      this.label1.TabIndex = 0;
      this.label1.Text = "Environment";
      // 
      // cmbEnvironment
      // 
      this.cmbEnvironment.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.cmbEnvironment.FormattingEnabled = true;
      this.cmbEnvironment.Location = new System.Drawing.Point(94, 30);
      this.cmbEnvironment.Name = "cmbEnvironment";
      this.cmbEnvironment.Size = new System.Drawing.Size(251, 21);
      this.cmbEnvironment.TabIndex = 1;
      // 
      // cmbWinery
      // 
      this.cmbWinery.DropDownHeight = 303;
      this.cmbWinery.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.cmbWinery.FormattingEnabled = true;
      this.cmbWinery.IntegralHeight = false;
      this.cmbWinery.Location = new System.Drawing.Point(94, 57);
      this.cmbWinery.Name = "cmbWinery";
      this.cmbWinery.Size = new System.Drawing.Size(251, 21);
      this.cmbWinery.TabIndex = 2;
      // 
      // label2
      // 
      this.label2.AutoSize = true;
      this.label2.Location = new System.Drawing.Point(12, 60);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(40, 13);
      this.label2.TabIndex = 0;
      this.label2.Text = "Winery";
      // 
      // cbWineryActive
      // 
      this.cbWineryActive.AutoSize = true;
      this.cbWineryActive.Checked = true;
      this.cbWineryActive.CheckState = System.Windows.Forms.CheckState.Checked;
      this.cbWineryActive.Location = new System.Drawing.Point(351, 59);
      this.cbWineryActive.Name = "cbWineryActive";
      this.cbWineryActive.Size = new System.Drawing.Size(56, 17);
      this.cbWineryActive.TabIndex = 3;
      this.cbWineryActive.Text = "Active";
      this.cbWineryActive.UseVisualStyleBackColor = true;
      // 
      // grpExportPreferences
      // 
      this.grpExportPreferences.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.grpExportPreferences.Controls.Add(this.cbVirtualMembers);
      this.grpExportPreferences.Controls.Add(this.btnCheckNone);
      this.grpExportPreferences.Controls.Add(this.btnCheckAll);
      this.grpExportPreferences.Controls.Add(this.cbMemberNotes);
      this.grpExportPreferences.Controls.Add(this.cbMemberTypes);
      this.grpExportPreferences.Controls.Add(this.cbCreditCards);
      this.grpExportPreferences.Controls.Add(this.cbClubMembers);
      this.grpExportPreferences.Controls.Add(this.cbShipMembers);
      this.grpExportPreferences.Controls.Add(this.cbFPProducts);
      this.grpExportPreferences.Controls.Add(this.cbFPWines);
      this.grpExportPreferences.Controls.Add(this.cbTPWines);
      this.grpExportPreferences.Controls.Add(this.cbTPProducts);
      this.grpExportPreferences.Controls.Add(this.cbOrderNotes);
      this.grpExportPreferences.Controls.Add(this.cbCartItems);
      this.grpExportPreferences.Controls.Add(this.cbCarts);
      this.grpExportPreferences.Controls.Add(this.cbMembers);
      this.grpExportPreferences.Location = new System.Drawing.Point(15, 149);
      this.grpExportPreferences.Name = "grpExportPreferences";
      this.grpExportPreferences.Size = new System.Drawing.Size(519, 225);
      this.grpExportPreferences.TabIndex = 6;
      this.grpExportPreferences.TabStop = false;
      this.grpExportPreferences.Text = "What would you like to export?";
      // 
      // btnCheckNone
      // 
      this.btnCheckNone.Location = new System.Drawing.Point(69, 19);
      this.btnCheckNone.Name = "btnCheckNone";
      this.btnCheckNone.Size = new System.Drawing.Size(57, 23);
      this.btnCheckNone.TabIndex = 0;
      this.btnCheckNone.Text = "None";
      this.btnCheckNone.UseVisualStyleBackColor = true;
      // 
      // btnCheckAll
      // 
      this.btnCheckAll.Location = new System.Drawing.Point(6, 19);
      this.btnCheckAll.Name = "btnCheckAll";
      this.btnCheckAll.Size = new System.Drawing.Size(57, 23);
      this.btnCheckAll.TabIndex = 0;
      this.btnCheckAll.Text = "All";
      this.btnCheckAll.UseVisualStyleBackColor = true;
      // 
      // cbMemberNotes
      // 
      this.cbMemberNotes.AutoSize = true;
      this.cbMemberNotes.Checked = true;
      this.cbMemberNotes.CheckState = System.Windows.Forms.CheckState.Checked;
      this.cbMemberNotes.Location = new System.Drawing.Point(6, 173);
      this.cbMemberNotes.Name = "cbMemberNotes";
      this.cbMemberNotes.Size = new System.Drawing.Size(95, 17);
      this.cbMemberNotes.TabIndex = 5;
      this.cbMemberNotes.Text = "Member Notes";
      this.cbMemberNotes.UseVisualStyleBackColor = true;
      // 
      // cbMemberTypes
      // 
      this.cbMemberTypes.AutoSize = true;
      this.cbMemberTypes.Checked = true;
      this.cbMemberTypes.CheckState = System.Windows.Forms.CheckState.Checked;
      this.cbMemberTypes.Location = new System.Drawing.Point(6, 150);
      this.cbMemberTypes.Name = "cbMemberTypes";
      this.cbMemberTypes.Size = new System.Drawing.Size(96, 17);
      this.cbMemberTypes.TabIndex = 4;
      this.cbMemberTypes.Text = "Member Types";
      this.cbMemberTypes.UseVisualStyleBackColor = true;
      // 
      // cbCreditCards
      // 
      this.cbCreditCards.AutoSize = true;
      this.cbCreditCards.Checked = true;
      this.cbCreditCards.CheckState = System.Windows.Forms.CheckState.Checked;
      this.cbCreditCards.Location = new System.Drawing.Point(6, 127);
      this.cbCreditCards.Name = "cbCreditCards";
      this.cbCreditCards.Size = new System.Drawing.Size(80, 17);
      this.cbCreditCards.TabIndex = 3;
      this.cbCreditCards.Text = "CreditCards";
      this.cbCreditCards.UseVisualStyleBackColor = true;
      // 
      // cbClubMembers
      // 
      this.cbClubMembers.AutoSize = true;
      this.cbClubMembers.Checked = true;
      this.cbClubMembers.CheckState = System.Windows.Forms.CheckState.Checked;
      this.cbClubMembers.Location = new System.Drawing.Point(6, 104);
      this.cbClubMembers.Name = "cbClubMembers";
      this.cbClubMembers.Size = new System.Drawing.Size(112, 17);
      this.cbClubMembers.TabIndex = 2;
      this.cbClubMembers.Text = "Club Memberships";
      this.cbClubMembers.UseVisualStyleBackColor = true;
      // 
      // cbShipMembers
      // 
      this.cbShipMembers.AutoSize = true;
      this.cbShipMembers.Checked = true;
      this.cbShipMembers.CheckState = System.Windows.Forms.CheckState.Checked;
      this.cbShipMembers.Location = new System.Drawing.Point(6, 81);
      this.cbShipMembers.Name = "cbShipMembers";
      this.cbShipMembers.Size = new System.Drawing.Size(119, 17);
      this.cbShipMembers.TabIndex = 1;
      this.cbShipMembers.Text = "Shipping Addresses";
      this.cbShipMembers.UseVisualStyleBackColor = true;
      // 
      // cbFPProducts
      // 
      this.cbFPProducts.AutoSize = true;
      this.cbFPProducts.Checked = true;
      this.cbFPProducts.CheckState = System.Windows.Forms.CheckState.Checked;
      this.cbFPProducts.Location = new System.Drawing.Point(199, 58);
      this.cbFPProducts.Name = "cbFPProducts";
      this.cbFPProducts.Size = new System.Drawing.Size(117, 17);
      this.cbFPProducts.TabIndex = 6;
      this.cbFPProducts.Text = "First Party Products";
      this.cbFPProducts.UseVisualStyleBackColor = true;
      // 
      // cbFPWines
      // 
      this.cbFPWines.AutoSize = true;
      this.cbFPWines.Checked = true;
      this.cbFPWines.CheckState = System.Windows.Forms.CheckState.Checked;
      this.cbFPWines.Location = new System.Drawing.Point(199, 81);
      this.cbFPWines.Name = "cbFPWines";
      this.cbFPWines.Size = new System.Drawing.Size(105, 17);
      this.cbFPWines.TabIndex = 7;
      this.cbFPWines.Text = "First Party Wines";
      this.cbFPWines.UseVisualStyleBackColor = true;
      // 
      // cbTPWines
      // 
      this.cbTPWines.AutoSize = true;
      this.cbTPWines.Checked = true;
      this.cbTPWines.CheckState = System.Windows.Forms.CheckState.Checked;
      this.cbTPWines.Location = new System.Drawing.Point(199, 127);
      this.cbTPWines.Name = "cbTPWines";
      this.cbTPWines.Size = new System.Drawing.Size(110, 17);
      this.cbTPWines.TabIndex = 9;
      this.cbTPWines.Text = "Third Party Wines";
      this.cbTPWines.UseVisualStyleBackColor = true;
      // 
      // cbTPProducts
      // 
      this.cbTPProducts.AutoSize = true;
      this.cbTPProducts.Checked = true;
      this.cbTPProducts.CheckState = System.Windows.Forms.CheckState.Checked;
      this.cbTPProducts.Location = new System.Drawing.Point(199, 104);
      this.cbTPProducts.Name = "cbTPProducts";
      this.cbTPProducts.Size = new System.Drawing.Size(122, 17);
      this.cbTPProducts.TabIndex = 8;
      this.cbTPProducts.Text = "Third Party Products";
      this.cbTPProducts.UseVisualStyleBackColor = true;
      // 
      // cbOrderNotes
      // 
      this.cbOrderNotes.AutoSize = true;
      this.cbOrderNotes.Checked = true;
      this.cbOrderNotes.CheckState = System.Windows.Forms.CheckState.Checked;
      this.cbOrderNotes.Location = new System.Drawing.Point(401, 104);
      this.cbOrderNotes.Name = "cbOrderNotes";
      this.cbOrderNotes.Size = new System.Drawing.Size(83, 17);
      this.cbOrderNotes.TabIndex = 12;
      this.cbOrderNotes.Text = "Order Notes";
      this.cbOrderNotes.UseVisualStyleBackColor = true;
      // 
      // cbCartItems
      // 
      this.cbCartItems.AutoSize = true;
      this.cbCartItems.Checked = true;
      this.cbCartItems.CheckState = System.Windows.Forms.CheckState.Checked;
      this.cbCartItems.Location = new System.Drawing.Point(401, 81);
      this.cbCartItems.Name = "cbCartItems";
      this.cbCartItems.Size = new System.Drawing.Size(80, 17);
      this.cbCartItems.TabIndex = 11;
      this.cbCartItems.Text = "Order Items";
      this.cbCartItems.UseVisualStyleBackColor = true;
      // 
      // cbCarts
      // 
      this.cbCarts.AutoSize = true;
      this.cbCarts.Checked = true;
      this.cbCarts.CheckState = System.Windows.Forms.CheckState.Checked;
      this.cbCarts.Location = new System.Drawing.Point(401, 58);
      this.cbCarts.Name = "cbCarts";
      this.cbCarts.Size = new System.Drawing.Size(57, 17);
      this.cbCarts.TabIndex = 10;
      this.cbCarts.Text = "Orders";
      this.cbCarts.UseVisualStyleBackColor = true;
      // 
      // cbMembers
      // 
      this.cbMembers.AutoSize = true;
      this.cbMembers.Checked = true;
      this.cbMembers.CheckState = System.Windows.Forms.CheckState.Checked;
      this.cbMembers.Location = new System.Drawing.Point(6, 58);
      this.cbMembers.Name = "cbMembers";
      this.cbMembers.Size = new System.Drawing.Size(69, 17);
      this.cbMembers.TabIndex = 0;
      this.cbMembers.Text = "Members";
      this.cbMembers.UseVisualStyleBackColor = true;
      // 
      // btnRunExport
      // 
      this.btnRunExport.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
      this.btnRunExport.Location = new System.Drawing.Point(459, 380);
      this.btnRunExport.Name = "btnRunExport";
      this.btnRunExport.Size = new System.Drawing.Size(75, 23);
      this.btnRunExport.TabIndex = 0;
      this.btnRunExport.Text = "Run Export";
      this.btnRunExport.UseVisualStyleBackColor = true;
      // 
      // label3
      // 
      this.label3.AutoSize = true;
      this.label3.Location = new System.Drawing.Point(12, 87);
      this.label3.Name = "label3";
      this.label3.Size = new System.Drawing.Size(76, 13);
      this.label3.TabIndex = 5;
      this.label3.Text = "WS Username";
      // 
      // tbWSUsername
      // 
      this.tbWSUsername.Location = new System.Drawing.Point(94, 84);
      this.tbWSUsername.Name = "tbWSUsername";
      this.tbWSUsername.Size = new System.Drawing.Size(251, 20);
      this.tbWSUsername.TabIndex = 4;
      // 
      // label4
      // 
      this.label4.AutoSize = true;
      this.label4.Location = new System.Drawing.Point(12, 113);
      this.label4.Name = "label4";
      this.label4.Size = new System.Drawing.Size(74, 13);
      this.label4.TabIndex = 5;
      this.label4.Text = "WS Password";
      // 
      // tbWSPassword
      // 
      this.tbWSPassword.Location = new System.Drawing.Point(94, 110);
      this.tbWSPassword.Name = "tbWSPassword";
      this.tbWSPassword.Size = new System.Drawing.Size(251, 20);
      this.tbWSPassword.TabIndex = 5;
      // 
      // cbVirtualMembers
      // 
      this.cbVirtualMembers.AutoSize = true;
      this.cbVirtualMembers.Checked = true;
      this.cbVirtualMembers.CheckState = System.Windows.Forms.CheckState.Checked;
      this.cbVirtualMembers.Location = new System.Drawing.Point(6, 196);
      this.cbVirtualMembers.Name = "cbVirtualMembers";
      this.cbVirtualMembers.Size = new System.Drawing.Size(101, 17);
      this.cbVirtualMembers.TabIndex = 13;
      this.cbVirtualMembers.Text = "Virtual Members";
      this.cbVirtualMembers.UseVisualStyleBackColor = true;
      // 
      // DataExtractionForm
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(546, 415);
      this.Controls.Add(this.tbWSPassword);
      this.Controls.Add(this.tbWSUsername);
      this.Controls.Add(this.label4);
      this.Controls.Add(this.label3);
      this.Controls.Add(this.btnRunExport);
      this.Controls.Add(this.grpExportPreferences);
      this.Controls.Add(this.cbWineryActive);
      this.Controls.Add(this.cmbWinery);
      this.Controls.Add(this.cmbEnvironment);
      this.Controls.Add(this.label2);
      this.Controls.Add(this.label1);
      this.Name = "DataExtractionForm";
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
      this.Text = "EWS Data Extraction";
      this.grpExportPreferences.ResumeLayout(false);
      this.grpExportPreferences.PerformLayout();
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.ComboBox cmbEnvironment;
    private System.Windows.Forms.ComboBox cmbWinery;
    private System.Windows.Forms.Label label2;
    private System.Windows.Forms.CheckBox cbWineryActive;
    private System.Windows.Forms.GroupBox grpExportPreferences;
    private System.Windows.Forms.Button btnCheckNone;
    private System.Windows.Forms.Button btnCheckAll;
    private System.Windows.Forms.CheckBox cbMemberNotes;
    private System.Windows.Forms.CheckBox cbMemberTypes;
    private System.Windows.Forms.CheckBox cbCreditCards;
    private System.Windows.Forms.CheckBox cbClubMembers;
    private System.Windows.Forms.CheckBox cbShipMembers;
    private System.Windows.Forms.CheckBox cbFPProducts;
    private System.Windows.Forms.CheckBox cbFPWines;
    private System.Windows.Forms.CheckBox cbTPWines;
    private System.Windows.Forms.CheckBox cbTPProducts;
    private System.Windows.Forms.CheckBox cbOrderNotes;
    private System.Windows.Forms.CheckBox cbCartItems;
    private System.Windows.Forms.CheckBox cbCarts;
    private System.Windows.Forms.CheckBox cbMembers;
    private System.Windows.Forms.Button btnRunExport;
    private System.Windows.Forms.Label label3;
    private System.Windows.Forms.TextBox tbWSUsername;
    private System.Windows.Forms.Label label4;
    private System.Windows.Forms.TextBox tbWSPassword;
    private System.Windows.Forms.CheckBox cbVirtualMembers;
  }
}
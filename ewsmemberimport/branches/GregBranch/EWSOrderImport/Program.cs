﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using EWSDataTaskSupport;
using EWSDataTaskSupport.EWSMember;
using EWSDataTaskSupport.EWSOrder;
using EWSDataTaskSupport.EWSProduct;
using LinqToExcel;
using System.Net;

namespace EWSOrderImport
{
  class Program
  {
    /// <summary>
    /// Product and order import process.
    /// Don't forget to set up app.config!
    /// </summary>
    /// <param name="args"></param>
    static void Main(string[] args)
    {
      bool debugEnabled = ConfigurationManager.AppSettings["Debug"] == "1";
      bool importProducts = ConfigurationManager.AppSettings["ImportProducts"] == "1";
      bool importOrders = ConfigurationManager.AppSettings["ImportOrders"] == "1";

      // Allow self-signed ssl certificates
      ServicePointManager.ServerCertificateValidationCallback += (se, cert, chain, sslerror) => true;

      if (debugEnabled)
        Console.WriteLine("DEBUG mode is enabled, nothing will be sent");

      if (importProducts)
        ImportProducts();

      if (importOrders)
        ImportOrders();

      Console.WriteLine("Process complete!");
      Console.ReadLine();
    }


    /// <summary>
    /// Import products using data from app.config
    /// </summary>
    static void ImportProducts()
    {
      ImportProducts(
        ConfigurationManager.AppSettings["WSUsername"], 
        ConfigurationManager.AppSettings["WSPassword"],
        ConfigurationManager.AppSettings["ImportFile"],
        ConfigurationManager.AppSettings["Strict"] == "1",
        ConfigurationManager.AppSettings["Debug"] == "1");
    }


    /// <summary>
    /// Import orders using data from app.config
    /// </summary>
    static void ImportOrders()
    {
      ImportOrders(
        ConfigurationManager.AppSettings["WSUsername"], 
        ConfigurationManager.AppSettings["WSPassword"],
        ConfigurationManager.AppSettings["ImportFile"],
        ConfigurationManager.AppSettings["Strict"] == "1",
        ConfigurationManager.AppSettings["Debug"] == "1");
    }


    /// <summary>
    /// Import products using custom parameters
    /// </summary>
    /// <param name="wsUsername"></param>
    /// <param name="wsPassword"></param>
    /// <param name="spreadSheet"></param>
    static void ImportProducts(String wsUsername, String wsPassword, String spreadSheet, bool strictMapping, bool debug)
    {
      Console.WriteLine("Importing products...");
      ExcelQueryFactory sheet = new ExcelQueryFactory(spreadSheet);
      sheet.StrictMapping = strictMapping;

      #region ImportProduct mappings
      sheet.AddMapping<ImportProduct>(p => p.ProductSKU,   "SKU");
      sheet.AddMapping<ImportProduct>(p => p.ProductName,  "Name");
      sheet.AddMapping<ImportProduct>(p => p.ShortName,    "Short Name");
      sheet.AddMapping<ImportProduct>(p => p.IsWine,       "Wine? (yes/no)",    DataTaskHelper.DefaultBooleanTransform);
      sheet.AddMapping<ImportProduct>(p => p.IsTaxable,    "Taxable? (yes/no)", DataTaskHelper.DefaultBooleanTransform);
      sheet.AddMapping<ImportProduct>(p => p.Teaser,       "Teaser");
      sheet.AddMapping<ImportProduct>(p => p.Description,  "Description");
      sheet.AddMapping<ImportProduct>(p => p.BottleCount,  "BottleCount", x => String.IsNullOrWhiteSpace(x) ? 1 : int.Parse(x));
      sheet.AddMapping<ImportProduct>(p => p.IsFirstParty, "ProductType", x => x.StartsWith("first", StringComparison.OrdinalIgnoreCase));
      sheet.AddMapping<ImportProduct>(p => p.ReportGroup,  "Product Report Group");
      sheet.AddMapping<ImportProduct>(p => p.Price,         "Product Price");
      #endregion

      var allProducts =
        from p in sheet.Worksheet<ImportProduct>("Products")
        select p;

      List<AddUpdateProductRequest>           fpProductRequests = new List<AddUpdateProductRequest>();
      List<AddUpdateWineRequest>              fpWineRequests    = new List<AddUpdateWineRequest>();
      //List<AddUpdateThirdPartyProductRequest> tpProductRequests = new List<AddUpdateThirdPartyProductRequest>();
      //List<AddUpdateThirdPartyWineRequest>    tpWineRequests    = new List<AddUpdateThirdPartyWineRequest>();

      foreach (ImportProduct impProduct in allProducts)
      {
        if (impProduct.IsWine)
        {
          if (impProduct.IsFirstParty)
          {
            #region First party wine
            AddUpdateWineRequest request = new AddUpdateWineRequest();
            fpWineRequests.Add(request);

            request.Username = wsUsername;
            request.Password = wsPassword;
            request.BottlesInACase = impProduct.BottleCount;
            request.Description = impProduct.Description;
            request.IsActive = false;
            request.IsTaxable = impProduct.IsTaxable;
            request.Teaser = impProduct.Teaser;
            request.WinePricePerBottle1 = impProduct.Price;
            request.WinePricePerCase1 = impProduct.Price;
            request.ProductName = impProduct.ProductName;
            request.ProductSKU = impProduct.ProductSKU;
            request.ReportGroup = impProduct.ReportGroup;
            #endregion
          }
          else
          {
            #region Third party wine
            // Third party credentials required
            // Not yet implemented
            #endregion
          }
        }
        else
        {
          if (impProduct.IsFirstParty)
          {
            #region First party product
            AddUpdateProductRequest request = new AddUpdateProductRequest();
            fpProductRequests.Add(request);

            request.Username = wsUsername;
            request.Password = wsPassword;
            request.Description = impProduct.Description;
            request.IsActive = false;
            request.IsTaxable = impProduct.IsTaxable;
            request.Price1 = impProduct.Price;
            request.ProductName = impProduct.ProductName;
            request.ProductSKU = impProduct.ProductSKU;
            request.ShortName = impProduct.ShortName;
            request.Teaser = impProduct.Teaser;
            request.ReportGroup = impProduct.ReportGroup;
            #endregion
          }
          else
          {
            #region Third party product
            // Third party credentials required
            // Not yet implemented
            #endregion
          }
        }
      }

      Console.WriteLine(allProducts.Count() + " Products and Wines found");

      if (!debug)
      {
        #region Insert the products
        int numProcessed = 0;

        List<Tuple<String, Guid>> insertedProducts = new List<Tuple<String, Guid>>();
        List<Tuple<String, String>> failedProducts = new List<Tuple<String, String>>();

        using (ProductService ps = new ProductService())
        {
          ps.Timeout = 900000;

          foreach (AddUpdateProductRequest req in fpProductRequests)
          {
            if (!debug)
            {
              AddUpdateProductWineResponse response = ps.AddUpdateProduct(req);
              if (response.IsSuccessful)
                insertedProducts.Add(new Tuple<String, Guid>(req.ProductSKU, response.ProductID));
              else
                failedProducts.Add(new Tuple<String, String>(req.ProductSKU, response.ErrorMessage));
            }
            ++numProcessed;
            Console.WriteLine(numProcessed);
          }

          foreach (AddUpdateWineRequest req in fpWineRequests)
          {
            if (!debug)
            {
              if(numProcessed == 50)
              {
                Console.WriteLine("STOP HERE!");
              }

              AddUpdateProductWineResponse response = ps.AddUpdateWine(req);
              if (response.IsSuccessful)
                insertedProducts.Add(new Tuple<String, Guid>(req.ProductSKU, response.ProductID));
              else
                failedProducts.Add(new Tuple<String, String>(req.ProductSKU, response.ErrorMessage));
            }
            ++numProcessed;
            Console.WriteLine(numProcessed);
          }
        }
        #endregion

        #region Write results to CSV
        CSVHelper helper = new CSVHelper();
        String path = Path.GetDirectoryName(spreadSheet) + Path.PathSeparator;
        helper.WriteCSVFile(path + "Successful Products.csv", insertedProducts.Select(rec => new { SKU = rec.Item1, ProductID = rec.Item2 }));
        helper.WriteCSVFile(path + "Failed Products.csv", failedProducts.Select(rec => new { SKU = rec.Item1, ErrorMessage = rec.Item2 }));
        #endregion
      }
    }


    /// <summary>
    /// Import orders using custom parameters
    /// </summary>
    /// <param name="wsUsername"></param>
    /// <param name="wsPassword"></param>
    /// <param name="orderSpreadSheet"></param>
    /// <param name="orderItemSpreadsheet"></param>
    static void ImportOrders(String wsUsername, String wsPassword, String spreadsheet, bool strictMapping, bool debug)
    {
      ExcelQueryFactory sheet = new ExcelQueryFactory(spreadsheet);
      sheet.StrictMapping = strictMapping;

      #region Order Mappings
      sheet.AddMapping<ImportOrder>(o => o.OrderNumber,               "Order Number");
      sheet.AddMapping<ImportOrder>(o => o.MemberID,                  "MemberID");
      sheet.AddMapping<ImportOrder>(o => o.BillingBirthdate,          "Billing Birthdate");
      sheet.AddMapping<ImportOrder>(o => o.BillingFirstName,          "Billing First Name");
      sheet.AddMapping<ImportOrder>(o => o.BillingLastName,           "Billing Last Name");
      sheet.AddMapping<ImportOrder>(o => o.BillingCompany,            "Billing Company");
      sheet.AddMapping<ImportOrder>(o => o.BillingAddress,            "Billing Address");
      sheet.AddMapping<ImportOrder>(o => o.BillingAddress2,           "Billing Address 2");
      sheet.AddMapping<ImportOrder>(o => o.BillingCity,               "Billing City");
      sheet.AddMapping<ImportOrder>(o => o.BillingState,              "Billing State");
      sheet.AddMapping<ImportOrder>(o => o.BillingZipcode,            "Billing Zipcode");
      sheet.AddMapping<ImportOrder>(o => o.BillingPhone,              "Billing Phone");
      sheet.AddMapping<ImportOrder>(o => o.BillingEmail,              "Billing E-mail");
      sheet.AddMapping<ImportOrder>(o => o.ShippingBirthdate,         "Shipping Birthdate");
      sheet.AddMapping<ImportOrder>(o => o.ShippingFirstName,         "Shipping First Name");
      sheet.AddMapping<ImportOrder>(o => o.ShippingLastName,          "Shipping Last Name");
      sheet.AddMapping<ImportOrder>(o => o.ShippingAddress,           "Shipping Address");
      sheet.AddMapping<ImportOrder>(o => o.ShippingAddress2,          "Shipping Address 2");
      sheet.AddMapping<ImportOrder>(o => o.ShippingCity,              "Shipping City");
      sheet.AddMapping<ImportOrder>(o => o.ShippingState,             "Shipping State");
      sheet.AddMapping<ImportOrder>(o => o.ShippingZipcode,           "Shipping Zipcode");
      sheet.AddMapping<ImportOrder>(o => o.ShippingPhone,             "Shipping Phone");
      sheet.AddMapping<ImportOrder>(o => o.ShippingEmail,             "Shipping E-mail");
      sheet.AddMapping<ImportOrder>(o => o.GiftMessage,               "Gift Message");
      sheet.AddMapping<ImportOrder>(o => o.OrderNote,                 "Order Note");
      sheet.AddMapping<ImportOrder>(o => o.Taxes,                     "*Taxes($)");
      sheet.AddMapping<ImportOrder>(o => o.Handling,                  "*Handling($)");
      sheet.AddMapping<ImportOrder>(o => o.Shipping,                  "*Shipping($)");
      sheet.AddMapping<ImportOrder>(o => o.CreditCardType,            "Credit Card Type");
      sheet.AddMapping<ImportOrder>(o => o.CreditCardExpirationMonth, "Credit Card Expiration Month");
      sheet.AddMapping<ImportOrder>(o => o.CreditCardExpirationYear,  "Credit Card Expiration Year");
      sheet.AddMapping<ImportOrder>(o => o.NameonCreditCard,          "Name on Credit Card");
      sheet.AddMapping<ImportOrder>(o => o.OrderDate,                 "*Order Date");
      sheet.AddMapping<ImportOrder>(o => o.OrderType,                 "Order Type");
      sheet.AddMapping<ImportOrder>(o => o.OrderSubtotal,             "OrderSubtotal");
      sheet.AddMapping<ImportOrder>(o => o.OrderTotal,                "OrderTotal");
      #endregion

      #region Create the Import Orders
      Console.WriteLine("Loading orders...");
      // Put all the orders in a list
      var allOrders =
        (from o in sheet.Worksheet<ImportOrder>("Orders")
         select o).ToList();
      #endregion

      #region Order Item Mappings
      sheet.AddMapping<ImportOrderItem>(oi => oi.OrderNumber, "*Order Number");
      sheet.AddMapping<ImportOrderItem>(oi => oi.Quantity,    "*Quantity");
      sheet.AddMapping<ImportOrderItem>(oi => oi.SKU,         "*SKU");
      sheet.AddMapping<ImportOrderItem>(oi => oi.Price,       "*Price ($)");
      #endregion

      #region Create the Import Order Items lookup
      Console.WriteLine("Loading order items...");
      // Put all the order items in a lookup
      var allOrderItems =
        (from oi in sheet.Worksheet<ImportOrderItem>("Order Items")
         select oi).ToLookup(oi => oi.OrderNumber, oi => oi);
      #endregion

      Console.WriteLine(allOrders.Count() + " Orders found");

      if (!debug)
      {
        #region Insert the Orders
        using (MemberService memberService = new MemberService())
        {
          #region Set up GetMember objects
          memberService.Timeout = 900000;

          GetMemberRequest gmRequest = new GetMemberRequest();
          gmRequest.Username = wsUsername;
          gmRequest.Password = wsPassword;

          GetMemberResponse gmResponse;
          #endregion

          using (OrderService orderService = new OrderService())
          {
            #region Set up AddOrder objects
            orderService.Timeout = 900000;

            AddOrderRequest addOrderRequest = new AddOrderRequest();
            addOrderRequest.Username = wsUsername;
            addOrderRequest.Password = wsPassword;

            AddOrderResponse addOrderResponse;
            #endregion

            Dictionary<String, Guid?> cachedMemberIDs = new Dictionary<String, Guid?>();
            Dictionary<int, Guid> insertedOrders = new Dictionary<int, Guid>();
            Dictionary<int, String> failedOrders = new Dictionary<int, String>();

            int numProcessed = 0;
            foreach (ImportOrder impOrder in allOrders)
            {
              #region Create a web service order
              Order wsOrder = new Order();

              // Get the MemberID for the order
              if (cachedMemberIDs.ContainsKey(impOrder.MemberID))
              {
                // This MemberID was cached
                wsOrder.MemberID = cachedMemberIDs[impOrder.MemberID];
              }
              else
              {
                // Look this member up at the winery
                gmRequest.AltMemberID = impOrder.MemberID;
                gmResponse = memberService.GetMember(gmRequest);

                if (gmResponse.Members.Length == 1)
                {
                  // We found the MemberID, cache it and use it
                  cachedMemberIDs.Add(impOrder.MemberID, gmResponse.Members[0].MemberID);
                  wsOrder.MemberID = gmResponse.Members[0].MemberID;
                }
              }

              wsOrder.OrderNumber               = impOrder.OrderNumber.ToString();
              //wsOrder.OrderNumberPrefix         = "IMP";
              wsOrder.BillingBirthDate          = impOrder.BillingBirthdate;
              wsOrder.BillingFirstName          = impOrder.BillingFirstName;
              wsOrder.BillingLastName           = impOrder.BillingLastName;
              wsOrder.BillingCompany            = impOrder.BillingCompany;
              wsOrder.BillingAddress1           = impOrder.BillingAddress;
              wsOrder.BillingAddress2           = impOrder.BillingAddress2;
              wsOrder.BillingCity               = impOrder.BillingCity;
              wsOrder.BillingState              = impOrder.BillingState;
              wsOrder.BillingZipCode            = impOrder.BillingZipcode;
              wsOrder.BillingPhone              = impOrder.BillingPhone;
              wsOrder.BillingEmail              = impOrder.BillingEmail;
              wsOrder.ShippingBirthDate         = impOrder.ShippingBirthdate;
              wsOrder.ShippingFirstName         = impOrder.ShippingFirstName;
              wsOrder.ShippingLastName          = impOrder.ShippingLastName;
              wsOrder.ShippingAddress1          = impOrder.ShippingAddress;
              wsOrder.ShippingAddress2          = impOrder.ShippingAddress2;
              wsOrder.ShippingCity              = impOrder.ShippingCity;
              wsOrder.ShippingState             = impOrder.ShippingState;
              wsOrder.ShippingZipCode           = impOrder.ShippingZipcode;
              wsOrder.ShippingPhone             = impOrder.ShippingPhone;
              wsOrder.ShippingEmail             = impOrder.ShippingEmail;
              wsOrder.GiftMessage               = impOrder.GiftMessage;
              wsOrder.OrderNotes                = impOrder.OrderNote;
              wsOrder.Taxes                     = impOrder.Taxes;
              wsOrder.TaxableHandling           = impOrder.Handling;
              wsOrder.Shipping                  = impOrder.Shipping;
              wsOrder.CreditCardType            = impOrder.CreditCardType;
              wsOrder.CreditCardExpirationMonth = impOrder.CreditCardExpirationMonth;
              wsOrder.CreditCardExpirationYear  = impOrder.CreditCardExpirationYear;
              wsOrder.NameOnCreditCard          = impOrder.NameonCreditCard;
              wsOrder.DateAdded                 = impOrder.OrderDate;
              wsOrder.OrderType                 = impOrder.OrderType;
              wsOrder.SubTotal                  = impOrder.OrderSubtotal;
              wsOrder.Total                     = impOrder.OrderTotal;
              #endregion

              #region Link the order items to the order
              wsOrder.OrderItem = allOrderItems[impOrder.OrderNumber].Select(impItem =>
                new OrderItem()
                {
                  Quantity = impItem.Quantity,
                  SKU = impItem.SKU,
                  Price = impItem.Price,
                  BottleCount = 1
                }).ToArray();
              #endregion

              #region Do some post-processing to the order
              // Calculate the order subtotal and total
              //wsOrder.SubTotal =
              //  (from item in wsOrder.OrderItem
              //   select item.Price * item.Quantity).Sum();

              //wsOrder.Total = (wsOrder.TaxableHandling ?? 0) + (wsOrder.Shipping ?? 0) + (wsOrder.Taxes ?? 0) + wsOrder.SubTotal;
              #endregion

              addOrderRequest.Order = wsOrder;

              addOrderResponse = orderService.AddOrder(addOrderRequest);
              if (addOrderResponse.IsSuccessful)
                insertedOrders.Add(impOrder.OrderNumber, addOrderResponse.OrderID);
              else
                failedOrders.Add(impOrder.OrderNumber, addOrderResponse.ErrorMessage);

              ++numProcessed;
              Console.WriteLine(numProcessed);
            }

            #region Write results to CSV
            CSVHelper helper = new CSVHelper();
            String path = Environment.CurrentDirectory + Path.DirectorySeparatorChar;
            helper.WriteCSVFile(path + "Successful Orders.csv", insertedOrders.Select(rec => new { OrderNumber = rec.Key, CartID = rec.Value }));
            helper.WriteCSVFile(path + "Failed Orders.csv", failedOrders.Select(rec => new { OrderNumber = rec.Key, CartID = rec.Value }));
            #endregion
          }
        }
        #endregion
      }
    }
  }
}

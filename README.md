# vinSUITE Order Import Utility

This repo contains the Visual Studio 2017 project for EWSOrderImport, the application used to perform order history imports into vinSUITE.

At the time of this writing, only EWSOrderImport is used in this suite of projects. TODO: Remove all non-order import components from project.

Instructions for order imports
1. Open EWSOrderImport.sln project in VisualStudio 2017
2. Open App.config and make sure settings are correct for each import.

Here is a sample of project settings. 

  <appSettings>
    <add key="ImportFile" value="J:\Projects\St. Michael's\Order Import.xlsx" /> --This is the path to the orders file. Be sure to use vinSUITE Order Import Template.xlsx.
    <add key="ImportProducts" value="0" /> --products import by default. Leave this at 0.
    <add key="ImportOrders" value="1" /> --leave this at 1.
    <add key="Debug" value="1" /> -- this is good to run as a test to make sure the file template is correct.
    <add key="WSUsername" value="mposws@stmichaelswinery.com" /> -- use getwsuserinfo stored procedure to get winery ws creds.
    <add key="WSPassword" value="mpos_9A67DB91" /> -- use getwsuserinfo stored procedure to get winery ws creds.
    <add key="OrderNumberPrefix" value="" /> -- it is preferable (not required) to set this in the orders file. You MUST set a prefix, though.
    <!-- 
      Settings for member matching criteria.
      Key represents what to search by, value is the ORDER.
      If value <= "0", the criteria will not be used.
    -->
    <add key="SearchByAltMemberID" value="1" /> -- optional, but not a bad idea to set this to 1 to match on altmemberID.
    <add key="SearchByMemberNumber" value="0" /> -- leave 0 always.
    <add key="SearchByEmail" value="1" /> -- required. leave this at 1.
    <add key="SearchByName" value="0" /> -- leave this at 0.
    <!-- End settings for member matching criteria -->
    <add key="ClientSettingsProvider.ServiceUri" value="" />
  </appSettings>


  3. In Visual Studio click Debug to kick off import. A console window will pop up and show progress until import is completed.
  4. Close console window to stop import process after import completes.
  5. Review \ewsmemberimport\trunk\EWSOrderImport\bin\Debug to find success and fail order logs.
   